// ==UserScript============================================
// @name               Translator API test
// @name:zh-CN         百度翻译API测试
// @name:en            Translator API test
// @namespace          Translator-API-Test
// @version            0.0.1
// @description        Translator API test script
// @description:zh-CN  百度翻译API测试脚本
// @description:en     Translator API test script
// @author             ZhouQi
// @license            WTFPL - See https://www.wtfpl.net/
// @match              *://*
// @require            file:///eng_bdt.js
// @grant              GM_xmlhttpRequest
// @connect            fanyi.baidu.com
// @run-at             document-end 
// ==/UserScript===========================================
// @require            https://greasyfork.org/scripts/452362-baidu-translate/code/Baidu%20Translate.js

(function __MAIN__() {
	bdTransReady(function() {
		baidu_translate({
			text: 'Welcome to Greasy Fork, a website that provides user scripts.',
			// dst: 'en',
			callback: function(result_text) {
				console.log(result_text);
			},
			onerror: function(reason) {
				console.log('something unexpected happened');
				debugger;
			}
		});
	});
})();