
  // page_popup.tsx
  var isInit = !1;
  async function main(ctx) {
    if (!ctx) {
      let config = await getConfig2(), options2 = {
        url: globalThis.location.href,
        config
      };
      ctx = await getContext(options2);
    }
    ctx.config.debug && log_default.setLevel("debug"), ctx.isTranslateExcludeUrl ? log_default.debug("detect exclude url, do not inject anything.") : (isMobile().any || isMonkey()) && ctx.rule.isShowUserscriptPagePopup && (isInit || (isInit = !0, initPopup().catch((e) => {
      log_default.error("init popup error", e);
    })));
  }
  async function ensurePopupInit() {
    isInit || (isInit = !0, initPopup().catch((e) => {
      log_default.error("init popup error", e);
    }));
  }