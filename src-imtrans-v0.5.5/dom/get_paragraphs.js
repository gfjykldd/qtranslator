
  // dom/get_paragraphs.ts
  var paragraphAutoIncreaceId = 1, paragraphEntities = /* @__PURE__ */ new Map();
  function getParagraphEntities() {
    return paragraphEntities;
  }
  function getParagraph(id) {
    return paragraphEntities.get(id);
  }
  function setParagraph(id, paragraph) {
    paragraphEntities.set(id, paragraph);
  }
  function cleanParagraphs() {
    paragraphEntities.clear();
  }
  function isMarkedByParagraph(targetElement) {
    if (!isMarked(targetElement, sourceElementMarkAttributeName))
      return !1;
    if (isMarked(targetElement, sourceElementTranslatedMarkAttributeName))
      return !0;
    let markId = getAttribute(
      targetElement,
      sourceElementParagraphAttributeName
    );
    if (!markId)
      return !1;
    let paragraphId = parseInt(markId), isExist = paragraphEntities.has(paragraphId);
    if (!isExist) {
      let maybeTargetElement = document.getElementById(
        `${translationTargetElementWrapperClass}-${paragraphId}`
      );
      maybeTargetElement && maybeTargetElement.remove();
    }
    return isExist;
  }
  function addToParagraphs(paragraph, allParagraphs) {
    let newParagraph = {
      ...paragraph,
      id: paragraphAutoIncreaceId++,
      languageByClient: "auto",
      languageByLocal: "auto"
    };
    newParagraph.elements.forEach((element) => {
      element instanceof HTMLElement && (setAttribute(element, sourceElementMarkAttributeName, "1"), setAttribute(
        element,
        sourceElementParagraphAttributeName,
        `${newParagraph.id}`
      ));
    }), allParagraphs.push(newParagraph), paragraphEntities.set(newParagraph.id, {
      ...newParagraph,
      state: "Original",
      observers: []
    });
  }
  async function getParagraphs(rootFrame, containers, ctx, forceTranslate) {
    let allParagraphs = [], { targetLanguage, rule } = ctx;
    for (let container of containers) {
      if (isExcludeElement(container, rule, !1))
        continue;
      let isPreWhitespaceContainer = isMarked(
        container,
        sourcePreWhitespaceMarkAttributeName
      ), inlineElementGroups = [], elementsToParagraphIfForce = (elements, isPreWhitespace, rootFrame2, ctx2) => (forceTranslate && (elements = elements.map(
        (element) => element.element ? (element.forceTranslate = !0, element) : {
          element,
          forceTranslate: !0
        }
      )), elementsToParagraph(elements, isPreWhitespace, rootFrame2, ctx2));
      if (isMarked(container, sourceAtomicBlockElementMarkAttributeName)) {
        if (!isMarkedByParagraph(container)) {
          let paragraph = elementsToParagraphIfForce(
            [container],
            !0,
            rootFrame,
            ctx
          );
          paragraph && addToParagraphs(paragraph, allParagraphs);
        }
        continue;
      }
      let currentVariableIndex = 0, filter = (node2) => {
        if (!(node2.nodeType === Node.TEXT_NODE || node2.nodeType === Node.ELEMENT_NODE))
          return NodeFilter.FILTER_REJECT;
        if (node2.nodeType === Node.ELEMENT_NODE) {
          let element = node2;
          if (element.isContentEditable || isMarkedByParagraph(
            node2
          ))
            return NodeFilter.FILTER_REJECT;
          if (setAttribute(element, sourceElementMarkAttributeName, "1"), isMarked(element, sourceAtomicBlockElementMarkAttributeName)) {
            if (inlineElementGroups.length > 0) {
              let paragraph2 = elementsToParagraphIfForce(
                [...inlineElementGroups],
                isPreWhitespaceContainer,
                rootFrame,
                ctx
              );
              currentVariableIndex = 0, paragraph2 && addToParagraphs(paragraph2, allParagraphs), inlineElementGroups.length = 0;
            }
            inlineElementGroups.push(element);
            let paragraph = elementsToParagraphIfForce(
              [...inlineElementGroups],
              isPreWhitespaceContainer,
              rootFrame,
              ctx
            );
            return currentVariableIndex = 0, paragraph && addToParagraphs(paragraph, allParagraphs), inlineElementGroups.length = 0, NodeFilter.FILTER_REJECT;
          }
        }
        if (isExcludeElement(node2, rule, !0)) {
          if (isMatchTags(node2.nodeName, ["CODE", "TT"]) && isMatchTags(node2.parentNode?.nodeName, ["PRE"]))
            return NodeFilter.FILTER_REJECT;
          if (isInlineElement(
            node2,
            rule
          ))
            return currentVariableIndex = handleInlineElement(
              node2,
              inlineElementGroups,
              allParagraphs,
              isPreWhitespaceContainer,
              rootFrame,
              ctx,
              currentVariableIndex
            ).currentVariableIndex, NodeFilter.FILTER_REJECT;
          if (inlineElementGroups.length > 0) {
            let paragraph = elementsToParagraphIfForce(
              [...inlineElementGroups],
              isPreWhitespaceContainer,
              rootFrame,
              ctx
            );
            currentVariableIndex = 0, paragraph && addToParagraphs(paragraph, allParagraphs), inlineElementGroups.length = 0;
          }
          return NodeFilter.FILTER_REJECT;
        }
        return isMatchTags(node2.nodeName, ["PRE"]) && node2.classList.contains("code") ? NodeFilter.FILTER_REJECT : isInlineElement(
          node2,
          rule
        ) ? (currentVariableIndex = handleInlineElement(
          node2,
          inlineElementGroups,
          allParagraphs,
          isPreWhitespaceContainer,
          rootFrame,
          ctx,
          currentVariableIndex
        ).currentVariableIndex, NodeFilter.FILTER_REJECT) : NodeFilter.FILTER_ACCEPT;
      }, elementIter = document.createTreeWalker(
        container,
        NodeFilter.SHOW_ELEMENT,
        filter
      ), node = elementIter.nextNode();
      for (node || container === rootFrame && (node = container); node; ) {
        if (inlineElementGroups.length > 0) {
          let paragraph = elementsToParagraphIfForce(
            [...inlineElementGroups],
            isPreWhitespaceContainer,
            rootFrame,
            ctx
          );
          currentVariableIndex = 0, paragraph && addToParagraphs(paragraph, allParagraphs), inlineElementGroups.length = 0;
        }
        node = elementIter.nextNode();
      }
      if (inlineElementGroups.length > 0) {
        let paragraph = elementsToParagraphIfForce(
          [...inlineElementGroups],
          isPreWhitespaceContainer,
          rootFrame,
          ctx
        );
        currentVariableIndex = 0, paragraph && addToParagraphs(paragraph, allParagraphs), inlineElementGroups.length = 0;
      }
    }
    let promises = allParagraphs.map((paragraph) => {
      let { text } = paragraph;
      return detectLanguage({
        text,
        minLength: 10
      });
    }), results = await Promise.all(promises), filterdParagraphs = [], excludeLanguages = ctx?.config?.translationLanguagePattern?.excludeMatches || [], currentPageLanguageByClient2 = "auto";
    ctx.state.isDetectParagraphLanguage || (currentPageLanguageByClient2 = getCurrentPageLanguageByClient());
    let currentPageLanguageByLocal = getCurrentPageLanguage();
    return results.forEach((result, index) => {
      let currentLanguageByLocal = result;
      currentLanguageByLocal === "auto" && (currentLanguageByLocal = currentPageLanguageByLocal);
      let newParagraph = {
        ...allParagraphs[index],
        languageByLocal: currentLanguageByLocal,
        languageByClient: currentPageLanguageByClient2 || "auto"
      };
      if (newParagraph.text.length < ctx.rule.languageDetectMinTextCount && (newParagraph.languageByLocal = "auto"), paragraphEntities.set(newParagraph.id, {
        ...newParagraph,
        state: "Original",
        observers: []
      }), isSameTargetLanguage(result, targetLanguage))
        paragraphEntities.delete(newParagraph.id);
      else {
        if (excludeLanguages.length > 0 && excludeLanguages.some((language) => isSameTargetLanguage(result, language))) {
          paragraphEntities.delete(newParagraph.id);
          return;
        }
        filterdParagraphs.push(newParagraph);
      }
    }), filterdParagraphs;
  }
  function getInlineElementsOfInlineElement(root2, isPreWhitespaceContainer, rootFrame, ctx, currentVariableIndex) {
    let elementState = {
      element: root2
    }, node = null, isWhiteSpaceNodeOfLastElement = !1, fullText = "", variables = [], elements = [], filter = (node2) => {
      if (node2.nodeType === Node.TEXT_NODE)
        return NodeFilter.FILTER_ACCEPT;
      if (node2.nodeType === Node.ELEMENT_NODE && isStayOriginalElement(node2, ctx.rule)) {
        let parentElementParagraph = elementsToParagraph(
          [{
            element: node2,
            forceTranslate: !0,
            currentVariableIndex
          }],
          isPreWhitespaceContainer,
          rootFrame,
          ctx
        );
        return currentVariableIndex += parentElementParagraph?.variables?.length || 0, parentElementParagraph && (fullText += parentElementParagraph.text, parentElementParagraph && parentElementParagraph.variables && (variables = variables.concat(parentElementParagraph.variables))), NodeFilter.FILTER_REJECT;
      }
      return NodeFilter.FILTER_ACCEPT;
    }, treeWalker = document.createTreeWalker(
      root2,
      NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT,
      filter
    );
    for (; node = treeWalker.nextNode(); ) {
      if (node.nodeType !== Node.TEXT_NODE)
        continue;
      let rawText = node.textContent || "", textContent = rawText.trim();
      if (!isWhiteSpaceNodeOfLastElement && rawText.length > 0 && textContent.length === 0) {
        isWhiteSpaceNodeOfLastElement = !0, fullText += " ", elements.push(" ");
        continue;
      }
      if (textContent.length > 0) {
        let parent = node.parentElement;
        if (parent === root2) {
          let text = textContent.replace(/\s+/g, " ");
          fullText += text, elements.push(text);
        } else {
          elements.push(parent);
          let parentElementParagraph = elementsToParagraph(
            [{
              element: parent,
              forceTranslate: !0,
              currentVariableIndex
            }],
            isPreWhitespaceContainer,
            rootFrame,
            ctx
          );
          currentVariableIndex += parentElementParagraph?.variables?.length || 0, parentElementParagraph && (fullText += parentElementParagraph.text, parentElementParagraph && parentElementParagraph.variables && (variables = variables.concat(parentElementParagraph.variables)));
        }
        isWhiteSpaceNodeOfLastElement = !1;
      }
    }
    let lastElement = elements[elements.length - 1];
    if (lastElement && typeof lastElement != "string") {
      let whitespace = getWhitespace(
        lastElement.nextSibling,
        isPreWhitespaceContainer
      );
      whitespace && (elements.push(whitespace), fullText += " ");
    }
    if (typeof elements[elements.length - 1] != "string") {
      let whitespace = getWhitespace(
        root2.nextSibling,
        isPreWhitespaceContainer
      );
      whitespace && (elements.push(whitespace), fullText += " ");
    }
    return elementState.text = fullText, elementState.variables = variables, elementState;
  }
  function handleInlineElement(node, inlineElementGroups, allParagraphs, isPreWhitespaceContainer, rootFrame, ctx, currentVariableIndex) {
    let previouseElement = node.previousElementSibling;
    if (previouseElement && !isInlineElement(
      previouseElement,
      ctx.rule
    ) && inlineElementGroups.length > 0) {
      let paragraph = elementsToParagraph(
        [...inlineElementGroups],
        isPreWhitespaceContainer,
        rootFrame,
        ctx
      );
      paragraph && addToParagraphs(paragraph, allParagraphs), inlineElementGroups.length = 0;
    }
    if (isExcludeElement(
      node,
      ctx.rule,
      !1
    ))
      isMetaElement(node, ctx.rule) || (isStayOriginalElement(node, ctx.rule) && (currentVariableIndex += 1), inlineElementGroups.push(node));
    else if (isStayOriginalElement(node, ctx.rule))
      inlineElementGroups.push(node), currentVariableIndex += 1;
    else if (isContainsStayOriginalElement(node, ctx.rule)) {
      let inlineGroupElementState = getInlineElementsOfInlineElement(
        node,
        isPreWhitespaceContainer,
        rootFrame,
        ctx,
        currentVariableIndex
      );
      inlineGroupElementState && inlineGroupElementState.text && (inlineGroupElementState.variables && (currentVariableIndex += inlineGroupElementState.variables.length), inlineElementGroups.push(
        inlineGroupElementState
      ));
    } else
      inlineElementGroups.push(node);
    return {
      currentVariableIndex
    };
  }