
  // dom/paragraph_to_html.ts
  function getTranslationWrapperClassNames(translationTheme, isInline, additonalClassNames = [], isPreWhitespace, isPdf) {
    let classList = ["notranslate"];
    return isPreWhitespace && classList.push(
      translationTargetTranslationElementPreWhitespaceWrapperClass
    ), translationTheme && (classList.push(
      `${brandId}-target-translation-theme-${translationTheme}`
    ), isInline ? classList.push(
      `${translationTargetTranslationElementInlineWrapperClass}-theme-${translationTheme}`
    ) : classList.push(
      `${translationTargetTranslationElementBlockWrapperClass}-theme-${translationTheme}`
    )), additonalClassNames.length > 0 && classList.push(...additonalClassNames), isInline ? classList.push(translationTargetTranslationElementInlineWrapperClass) : (classList.push(translationTargetTranslationElementBlockWrapperClass), isPdf && classList.push(translationTargetTranslationPdfElementBlockWrapperClass)), classList;
  }
  function getTranslationInnerClassNames(translationTheme) {
    let innerClassList = [
      "notranslate",
      translationTargetInnerElementWrapperClass
    ];
    return translationTheme && innerClassList.push(
      `${brandId}-target-translation-theme-${translationTheme}-inner`
    ), innerClassList;
  }
  function paragraphToHtml(sourceItem, sentence, ctx) {
    let { rule, state } = ctx, { translationTheme } = state, { variables, isVertical } = sourceItem;
    variables = variables || [];
    let { text: targetText } = sentence, { wrapperPrefix, wrapperSuffix } = rule, delimiters = getPlaceholderDelimiters(ctx), position = "afterend", regex = new RegExp(`${delimiters[0]}(\\d+)${delimiters[1]}`, "g"), html = escapeHTML(targetText);
    variables.length > 0 && (html = html.replace(regex, (match) => {
      let matchPositonAtHtml = html.indexOf(match), isStartWhiteSpace = html[matchPositonAtHtml - 1] === " ", isEndWhiteSpace = html[matchPositonAtHtml + match.length] === " ", matchNumberStr = match.slice(
        delimiters[0].length,
        -delimiters[1].length
      ), matchNumber = Number(matchNumberStr);
      if (isNaN(matchNumber))
        return match;
      let variable = variables[Number(matchNumber)];
      if (variable && variable.type === "element") {
        let variableHtml = variable.value.outerHTML;
        return isStartWhiteSpace || (variableHtml = " " + variableHtml), isEndWhiteSpace || (variableHtml = variableHtml + " "), variableHtml;
      } else
        log_default.error("variable type not supported", variable, match);
      return match;
    }));
    let classList = getTranslationWrapperClassNames(
      translationTheme,
      sourceItem.inline,
      rule.translationClasses || [],
      sourceItem.preWhitespace,
      ctx.rule.isPdf
    );
    isVertical && classList.push(translationTargetTranslationElementVerticalBlockClass);
    let innerClassList = getTranslationInnerClassNames(
      translationTheme
    ), blockStyleStr = "";
    return rule.translationBlockStyle && (blockStyleStr = `style="${rule.translationBlockStyle}"`), html = `<${ctx.rule.targetWrapperTag} ${blockStyleStr} class="${classList.join(" ")}"><${rule.targetWrapperTag} class="${innerClassList.join(" ")}">${html}</${rule.targetWrapperTag}></${rule.targetWrapperTag}>`, sourceItem.inline || (wrapperPrefix === "smart" ? html = `<br />${html}` : html = `${wrapperPrefix}${html}`, wrapperSuffix === "smart" ? html = `${html}` : html = `${html}${wrapperSuffix}`), sourceItem.inline && (html = `<${rule.targetWrapperTag} class="notranslate">&#160;</${rule.targetWrapperTag}>${html}`), {
      html,
      position
    };
  }