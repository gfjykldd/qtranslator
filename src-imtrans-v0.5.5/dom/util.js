
  // dom/util.ts
  var isProd = !1, isInUserscript = isMonkey();
  function duplicatedElements(root2, array, rule) {
    let allHeaders = root2.querySelectorAll("header"), main3 = root2.querySelectorAll("main"), headers3 = [];
    for (let header of allHeaders)
      main3.length > 0 && main3[0].contains(header) || headers3.push(header);
    for (let i3 = 0; i3 < array.length; i3++) {
      let a6 = array[i3].element;
      for (let j6 = i3 + 1; j6 < array.length; j6++) {
        let b5 = array[j6].element;
        if (a6.contains(b5))
          array.splice(j6, 1), j6--;
        else if (b5.contains(a6)) {
          array.splice(i3, 1), i3--;
          break;
        } else
          a6 === b5 && (array.splice(j6, 1), j6--);
      }
    }
    return array.filter((container) => {
      let element = container.element;
      if (container.reserve)
        return !0;
      let isHeader = !1;
      for (let header of headers3) {
        if (isMatchTags(element.nodeName, ["H1"]))
          continue;
        let h1Container = header.querySelector("h1");
        if (!(h1Container && isValidTextByCount(
          h1Container.textContent || "",
          rule.paragraphMinTextCount,
          rule.paragraphMinWordCount
        )) && (element === header || header.contains(element))) {
          isHeader = !0;
          break;
        }
      }
      return !isHeader;
    }).map((container) => container.element);
  }
  function getLastHTMLElement(elements) {
    for (let i3 = elements.length - 1; i3 >= 0; i3--) {
      let element = elements[i3].element || elements[i3];
      if (typeof element != "string")
        return element;
    }
    return null;
  }
  function getHTMLElements(elements) {
    let result = [];
    for (let i3 = elements.length - 1; i3 >= 0; i3--) {
      let element = elements[i3].element || elements[i3];
      (typeof element != "string" || element !== " ") && result.push(element);
    }
    return result;
  }
  function getFirstHTMLElement(elements) {
    for (let i3 = 0; i3 < elements.length; i3++) {
      let element = elements[i3];
      if (typeof element != "string")
        return element;
    }
    return null;
  }
  function getWhitespace(nextNode, isPreWhitespace) {
    return nextNode && nextNode.nodeType === Node.TEXT_NODE && nextNode.textContent && nextNode.textContent?.length > 0 ? isPreWhitespace ? nextNode.textContent : " " : null;
  }
  function customTrim(str) {
    if (!str)
      return "";
    let nbspChar = String.fromCharCode(160);
    if (str.includes(nbspChar)) {
      let trimedStr = str.trim(), leftPosition = str.indexOf(trimedStr), rightPosition = leftPosition + trimedStr.length, leftStr = str.substring(0, leftPosition), rightStr = str.substring(rightPosition), leftNbspCount = leftStr.split(nbspChar).length - 1, rightNbspCount = rightStr.split(nbspChar).length - 1, finalLeftStr = leftNbspCount > 0 ? " ".repeat(leftNbspCount) : "", finalRightStr = rightNbspCount > 0 ? " ".repeat(rightNbspCount) : "";
      return finalLeftStr + trimedStr + finalRightStr;
    } else
      str = str.trim();
    return str;
  }
  function getElementsBySelectors(root2, selectors) {
    let elements = [];
    for (let selector of selectors) {
      let nodes = root2.querySelectorAll(selector);
      for (let node of nodes)
        elements.push(node);
    }
    return elements;
  }
  function isInlineElementByTreeWalker(element, rule) {
    let filterInlineElement = function(node) {
      return node.nodeType === Node.ELEMENT_NODE || node.nodeType === Node.TEXT_NODE ? node.nodeType === Node.ELEMENT_NODE && isExcludeElement(node, rule, !0) ? NodeFilter.FILTER_REJECT : NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_REJECT;
    }, treeWalker = document.createTreeWalker(
      element,
      NodeFilter.SHOW_ELEMENT,
      filterInlineElement
    ), isInline = !0;
    for (; treeWalker.nextNode(); ) {
      let node = treeWalker.currentNode;
      if (node.nodeType === Node.ELEMENT_NODE) {
        if (isMarked(
          node,
          sourceInlineElementMarkAttributeName
        )) {
          if (isInline === !0)
            return !0;
          continue;
        }
        if (isMatchTags(node.nodeName, rule.inlineTags))
          return !0;
        if (!isUnknowTag(node, rule))
          return !1;
      }
    }
    return !0;
  }
  function isInlineElement(element, rule) {
    let inlineTags = rule.inlineTags;
    if (element.nodeType === Node.ELEMENT_NODE)
      if (isMatchTags(element.nodeName, inlineTags) || isUnknowTag(element, rule)) {
        if (isMarked(
          element,
          sourceBlockElementMarkAttributeName
        ) || isMatchTags(element.nodeName, ["BR"]))
          return !1;
        if (isMarked(element, sourceInlineElementMarkAttributeName))
          return !0;
        if (isUnknowTag(element, rule)) {
          let style = globalThis.getComputedStyle(element);
          if (style.display === "block" || style.display === "flex")
            return !1;
        }
        return isInlineElementByTreeWalker(element, rule);
      } else
        return isMarked(
          element,
          sourceInlineElementMarkAttributeName
        );
    return !1;
  }
  function isDuplicateElement(element, elements) {
    for (let e of elements)
      if (e === element)
        return !0;
    return !1;
  }
  function isMetaElement(element, rule) {
    return !!isMatchTags(element.nodeName, rule.metaTags);
  }
  function isExcludeElement(element, rule, includeStayElements) {
    if (!(element.nodeType === Node.ELEMENT_NODE || element.nodeType === Node.TEXT_NODE) || element.nodeType === Node.ELEMENT_NODE && isMarked(element, sourceElementExcludeAttributeName, !0))
      return !0;
    if (element.nodeType === Node.ELEMENT_NODE && isMarked(
      element,
      specifiedTargetContainerElementAttributeName
    ))
      return !1;
    let { stayOriginalTags, excludeTags } = rule, finalExcludeTags = [];
    return includeStayElements && excludeTags && excludeTags.length > 0 ? finalExcludeTags = excludeTags || [] : finalExcludeTags = excludeTags.filter((tag) => !stayOriginalTags.includes(tag)), !!(element.nodeType === Node.ELEMENT_NODE && element.isContentEditable || element.nodeType === Node.ELEMENT_NODE && (element.getAttribute("translate") === "no" || element.classList.contains("notranslate")) || isMatchTags(element.nodeName, finalExcludeTags));
  }
  function isNeedToTranslate(item, minTextCount, minWordCount, ctx) {
    let delimiters = getPlaceholderDelimiters(ctx), stayInOriginalRegex = new RegExp(
      `${delimiters[0]}(\\d+)${delimiters[1]}`,
      "gi"
    ), text = item.text, trimedText = text.trim();
    return trimedText = trimedText.replace(stayInOriginalRegex, ""), trimedText = trimedText.trim(), trimedText === "" || trimedText.length === 1 && trimedText.charCodeAt(0) === 8203 || /^\d+(,\d+)*(\.\d+)?$/.test(text) || trimedText.includes("</style>") || trimedText.includes("< styles>") || isAtTag(trimedText) || isUrl(trimedText) || isHashTag(trimedText) || stayInOriginalRegex.test(trimedText) ? !1 : isValidTextByCount(text, minTextCount, minWordCount);
  }
  function isValidTextByCount(rawText, minTextCount, minWordCount) {
    let text = rawText.trim();
    return text.length >= minTextCount || text.split(" ").length >= minWordCount || !isAllAscii(text) && text.length >= minWordCount;
  }
  function isAllAscii(text) {
    for (let i3 = 0; i3 < text.length; i3++)
      if (text.charCodeAt(i3) > 127)
        return !1;
    return !0;
  }
  function isUrl(text) {
    if (text && text.includes("://"))
      try {
        return new URL(text), !0;
      } catch {
        return !1;
      }
    else
      return !1;
  }
  function isHashTag(text) {
    return !!(text && text.startsWith("#") && text.indexOf(" ") === -1);
  }
  function isAtTag(text) {
    return !!(text && text.startsWith("@") && text.indexOf(" ") === -1);
  }
  function isStockTag(text) {
    return !!(text && text.startsWith("$") && text.indexOf(" ") === -1);
  }
  function isMarked(element, markedAttribute, explicit = !1) {
    return isMarkedWith(element, markedAttribute, "1", explicit);
  }
  function isMarkedWith(element, markedAttribute, value, explicit = !1) {
    return isProd && !explicit ? element[elementMarkRootKey] ? (
      // @ts-ignore: it's ok
      !!(element[elementMarkRootKey] && // @ts-ignore: it's ok
      element[elementMarkRootKey][markedAttribute] === value)
    ) : !1 : element.dataset[markedAttribute] === value;
  }
  function hasMark(element, markedAttribute, explicit = !1) {
    return isProd && !explicit ? element[elementMarkRootKey] ? (
      // @ts-ignore: it's ok
      !!(element[elementMarkRootKey] && // @ts-ignore: it's ok
      element[elementMarkRootKey][markedAttribute])
    ) : !1 : element.dataset[markedAttribute] !== void 0;
  }
  function getMainText(root2) {
    return (root2.innerText || root2.textContent || "").trim();
  }
  function getAllIframeMainText(root2) {
    let iframes = root2.querySelectorAll("iframe"), text = "";
    for (let i3 = 0; i3 < iframes.length; i3++) {
      let doc = iframes[i3].contentDocument;
      if (doc && (text += getMainText(doc.body), text.length > 2e3))
        break;
    }
    return text;
  }
  function getPairsSourceText(root2, pairs) {
    let keys = Object.keys(pairs), text = "";
    for (let i3 = 0; i3 < keys.length; i3++) {
      let sourceSelector = keys[i3], elements = root2.querySelectorAll(sourceSelector);
      for (let j6 = 0; j6 < elements.length; j6++) {
        let element = elements[j6];
        if (text += (element.innerText || element.textContent || "") + `
`, text.length > 2e3)
          break;
      }
    }
    return text;
  }
  function isMatchSelectors(selectors) {
    return selectors ? typeof selectors == "string" ? document.querySelector(selectors) !== null : selectors.some((selector) => document.querySelector(selector)) : !1;
  }
  function setAttribute(element, name, value, explicit = !1) {
    element.isContentEditable || (isProd && !explicit ? (element.dataset[sourceElementEffectAttributeNameForJs] || (element.dataset[sourceElementEffectAttributeNameForJs] = "1"), element[elementMarkRootKey] || (element[elementMarkRootKey] = {}), element[elementMarkRootKey][name] || (element[elementMarkRootKey][name] = value)) : (element.dataset[sourceElementEffectAttributeNameForJs] || (element.dataset[sourceElementEffectAttributeNameForJs] = "1"), element.dataset[name] !== value && (element.dataset[name] = value)));
  }
  function removeAttribute(element, name, explicit = !1) {
    if (isProd && !explicit) {
      if (!element[elementMarkRootKey] || !element[elementMarkRootKey][name])
        return;
      delete element[elementMarkRootKey][name];
    } else
      delete element.dataset[name];
  }
  function getAttribute(element, name, explicit = !1) {
    return isProd && !explicit ? !element[elementMarkRootKey] || !element[elementMarkRootKey][name] ? void 0 : element[elementMarkRootKey][name] : element.dataset[name];
  }
  function isStayOriginalElement(element, rule) {
    let isStayOriginal = !1, allTags = [
      ...rule.stayOriginalTags,
      ...rule.additionalStayOriginalTags
    ];
    return (isMatchTags(element.nodeName, allTags) || isMarked(element, sourceElementStayOriginalAttributeName)) && (isStayOriginal = !0), isStayOriginal;
  }
  function isContainsStayOriginalElement(element, rule) {
    let isStayOriginal = !1, allTags = [
      ...rule.stayOriginalTags,
      ...rule.additionalStayOriginalTags
    ];
    if ((isMatchTags(element.nodeName, allTags) || isMarked(element, sourceElementStayOriginalAttributeName)) && (isStayOriginal = !0), isStayOriginal)
      return !0;
    let allSelectors = allTags.map((tag) => tag.toLowerCase());
    return rule.stayOriginalSelectors && allSelectors.push(...rule.stayOriginalSelectors), rule.additionalStayOriginalSelectors && allSelectors.push(...rule.additionalStayOriginalSelectors), isContainsSelectors(element, allSelectors);
  }
  function isUnknowTag(element, rule) {
    let allKnowTags = rule.allBlockTags.concat(rule.inlineTags).concat(
      rule.excludeTags
    );
    return !isMatchTags(element.nodeName, allKnowTags);
  }
  function getPlaceholderDelimiters(ctx) {
    let { config } = ctx, delimiters = defaultPlaceholderDelimiters;
    return config.translationServices[ctx.translationService] && config.translationServices[ctx.translationService].placeholderDelimiters && (delimiters = config.translationServices[ctx.translationService].placeholderDelimiters), delimiters;
  }
  function isContainsSelectors(element, selectors) {
    if (!selectors)
      return !1;
    Array.isArray(selectors) || (selectors = [selectors]);
    for (let selector of selectors)
      if (element.querySelector(selector))
        return !0;
    return !1;
  }
  function getTheLastTextNodeParentElement(element) {
    let treeWalker = document.createTreeWalker(
      element,
      NodeFilter.SHOW_TEXT,
      (node) => node.textContent && node.textContent.trim() ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_REJECT
    ), lastTextNode = null;
    for (; treeWalker.nextNode(); )
      lastTextNode = treeWalker.currentNode;
    return lastTextNode ? lastTextNode.parentElement : null;
  }
  function getRealUrl() {
    if (!getIsInIframe())
      return globalThis.location.href;
    try {
      let currentUrl = globalThis.location.href, currentUrlObj = new URL(currentUrl);
      if (currentUrlObj.protocol === "about:" || currentUrlObj.protocol === "blob:") {
        if (globalThis.location.ancestorOrigins && globalThis.location.ancestorOrigins.length > 0)
          return globalThis.location.ancestorOrigins[0];
        let href = "";
        try {
          href = globalThis.parent.location.href;
        } catch {
        }
        return href || (globalThis.location != globalThis.parent.location ? document.referrer : document.location.href);
      } else
        return currentUrl;
    } catch {
    }
    return globalThis.location.href;
  }
  function injectCSS(rootDocument, css, id) {
    if (rootDocument && rootDocument.head && rootDocument.head.appendChild) {
      let styleElement = rootDocument.createElement("style");
      if (id) {
        let existingStyleElement = rootDocument.querySelector(
          `style[data-id="${id}"]`
        );
        existingStyleElement && existingStyleElement.remove(), styleElement.dataset.id = id;
      }
      styleElement.textContent = css, rootDocument.head.appendChild(styleElement);
    } else
      log_default.warn(
        "injectCSS failed, rootDocument does not have head node",
        rootDocument
      );
  }
  function isInlineIframe(frame) {
    let src = frame.getAttribute("src");
    if (src) {
      if (isInUserscript) {
        if (src.startsWith("blob:"))
          return !0;
      } else if (src.startsWith("blob:") && !src.startsWith("blob:http"))
        return !0;
      return !1;
    }
    return !!(frame.getAttribute("srcdoc") && frame.contentDocument && frame.contentDocument.body);
  }
  function isMatchTags(nodeName, tags) {
    if (!nodeName || !tags)
      return !1;
    Array.isArray(tags) || (tags = [tags]), nodeName = nodeName.toUpperCase();
    for (let tag of tags)
      if (nodeName === tag)
        return !0;
    return !1;
  }
  function hexToRgb(hexValue) {
    let hex2 = hexValue.replace("#", ""), r2 = parseInt(hex2.substring(0, 2), 16), g7 = parseInt(hex2.substring(2, 4), 16), b5 = parseInt(hex2.substring(4, 6), 16);
    return { r: r2, g: g7, b: b5 };
  }
  function getRangeFromPoint(x4, y4, rule) {
    if (document.caretPositionFromPoint) {
      let position = document.caretPositionFromPoint(x4, y4);
      if (position) {
        let range = document.createRange(), offsetNode = position.offsetNode;
        if (!offsetNode || offsetNode.nodeType !== Node.TEXT_NODE || isExcludeElement(offsetNode, rule, !0))
          return null;
        try {
          range.setStart(offsetNode, position.offset), range.setEnd(offsetNode, position.offset);
        } catch (e) {
          return log_default.warn("getRangeFromPoint error", e), null;
        }
        return range;
      }
      return null;
    } else
      return document.caretRangeFromPoint ? document.caretRangeFromPoint(x4, y4) : null;
  }
  function findElementInShadow(ele, x4, y4) {
    let findCount = 0, finder = (ele2, x5, y5, preShadow) => {
      if (++findCount > 100 || preShadow === ele2)
        return ele2;
      let innerShadowDom = ele2.shadowRoot;
      if (!innerShadowDom || typeof innerShadowDom.elementFromPoint != "function")
        return ele2;
      let innerDom = innerShadowDom.elementFromPoint(x5, y5);
      return innerDom ? finder(innerDom, x5, y5, ele2) : ele2;
    };
    return finder(ele, x4, y4);
  }