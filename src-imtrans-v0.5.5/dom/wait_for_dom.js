
  // dom/wait_for_dom.ts
  async function waitForDomElementReady(ctx) {
    try {
      return await waitFor(() => {
        let mainText = ctx.mainFrame.innerText || "";
        if (isValidTextByCount(
          mainText,
          ctx.rule.mainFrameMinTextCount,
          ctx.rule.mainFrameMinWordCount
        ))
          return !0;
        throw new Error("there is no main text");
      }, { timeout: 5e3 }), !0;
    } catch (e) {
      if (getIsInIframe())
        throw e;
      return log_default.debug("check dom element ready failed:", e, ctx), !0;
    }
  }