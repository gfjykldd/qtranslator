
  // dom/normalize_pdf_container.ts
  function normalizeContainer2(containers, _rule) {
    let targetContainers = [];
    for (let container of containers) {
      let maxRight = 0, minLeft = 1e5, rightContainer = document.createElement("div"), treeFilter = (node) => {
        let element = node;
        if (isMatchTags(element.nodeName, ["DIV", "BR"]))
          return NodeFilter.FILTER_REJECT;
        if (element.classList.contains("markedContent"))
          return NodeFilter.FILTER_ACCEPT;
        if (isMatchTags(element.nodeName, ["SPAN"])) {
          let rect = element.getBoundingClientRect(), style = globalThis.getComputedStyle(element), right = rect.right, left = rect.left, top = style.top.slice(0, -2), fontsize = style.fontSize.slice(0, -2);
          return right > maxRight && (maxRight = right), left < minLeft && (minLeft = left), setAttribute(element, sourceElementLeft, `${left}`), setAttribute(element, sourceElementRight, `${right}`), setAttribute(element, sourceElementTop, top), setAttribute(element, sourceElementFontSize, fontsize), NodeFilter.FILTER_ACCEPT;
        } else
          return NodeFilter.FILTER_ACCEPT;
      }, walk = document.createTreeWalker(
        container,
        NodeFilter.SHOW_ELEMENT,
        treeFilter
      );
      for (; walk.nextNode(); )
        ;
      let realWidth = maxRight - minLeft;
      realWidth < 600 && (realWidth = 600), targetContainers.push(rightContainer), rightContainer.style.left = maxRight + "px", rightContainer.style.width = maxRight + "px", rightContainer.classList.add(translationPdfTargetContainerClass), container.childNodes.length > 0 && container.insertBefore(rightContainer, container.childNodes[0]);
    }
    return { targetContainers };
  }