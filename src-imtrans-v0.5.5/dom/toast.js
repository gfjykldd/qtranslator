
  // dom/toast.ts
  function success(text) {
    toast({
      type: "success",
      text
    });
  }
  function error(text) {
    toast({
      type: "error",
      text
    });
  }