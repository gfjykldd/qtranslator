
  // dom/current_language.ts
  var currentPageLanguage = "auto", currentPageLanguageByClient = "auto", currentPageLanguageByRemote = "auto";
  function setCurrentPageLanguage(lang) {
    currentPageLanguage = lang;
  }
  function setCurrentPageLanguageByClient(lang) {
    currentPageLanguageByClient = lang;
  }
  function getCurrentPageLanguage() {
    return currentPageLanguageByClient !== "auto" ? currentPageLanguageByClient : currentPageLanguageByRemote !== "auto" ? currentPageLanguageByRemote : currentPageLanguage;
  }
  function getCurrentPageLanguageByClient() {
    return currentPageLanguageByClient;
  }