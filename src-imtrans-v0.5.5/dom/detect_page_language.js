
  // dom/detect_page_language.ts
  async function detectPageLanguage() {
    let lang = "auto";
    return document.body && document.body.textContent && document.body.textContent.trim() && (lang = await detectLanguage({
      text: getMainText(document.body)
    })), lang === "auto" && document.documentElement && document.documentElement.lang && (lang = formatLanguage(document.documentElement.lang)), lang;
  }