
  // dom/translate_page.ts
  var allResizebleObserver = [], waitToTranslateParagraphIds = /* @__PURE__ */ new Set(), controller = new AbortController(), { signal } = controller, pageStatus = "Original", currentParagraphIds = [], allNewDynamicElements = [], allIntersectionObserver = [], currentNewDynamicElements = [], oldUrl = getRealUrl().split("#")[0], currentTranslatedTextLength = 0, globalContext, initialTranslationTheme, isSetupForOnce = !1, clientX = 0, clientY = 0, mouseMoved = !1, mouseMovedCount = 0, isHoldMouseHoverKey = !1, delayChecker, stopChangePageStatus = !1, lastTimeOfModifierKeyAndNormalKeyPress = 0, throttleMap = {
    300: se3(
      translateCurrentQueue,
      300
    ),
    1e3: se3(
      translateCurrentQueue,
      1e3
    ),
    3e3: se3(
      translateCurrentQueue,
      3e3
    )
  }, debounceMap = {
    300: debounce(
      translateNewDynamicNodes,
      300
    ),
    1e3: debounce(
      translateNewDynamicNodes,
      1e3
    ),
    3e3: debounce(
      translateNewDynamicNodes,
      3e3
    )
  }, env2 = getEnv(), isProd2 = env2.PROD === "1", translationServiceInitmap = {}, titleMutationObserver, mutationObserverMap = /* @__PURE__ */ new Map(), mainMutaionObserver, originalPageTitle = "";
  async function toggleTranslatePage() {
    if (getPageStatus() === "Original") {
      let ctx = await getGlobalContext(getRealUrl(), {
        isNeedClean: !0
      });
      initialTranslationTheme ? ctx = await getGlobalContext(getRealUrl(), {
        translationTheme: initialTranslationTheme
      }) : initialTranslationTheme = ctx.state.translationTheme, await translatePage(globalContext);
    } else
      (getPageStatus() === "Translated" || getPageStatus() === "Error") && restorePage();
  }
  async function toggleTranslationMask() {
    if (getPageStatus() === "Original")
      globalContext = await getGlobalContext(getRealUrl(), {}), initialTranslationTheme || (initialTranslationTheme = globalContext.state.translationTheme), globalContext = await getGlobalContext(getRealUrl(), {
        translationTheme: "mask"
      }), await translatePage(globalContext);
    else if (getPageStatus() === "Translated") {
      let allFrames = [
        globalContext.mainFrame,
        ...mutationObserverMap.keys()
      ], currentTranslationTheme = globalContext?.state.translationTheme;
      for (let frame of allFrames) {
        let currentRootTheme = getAttribute(
          frame,
          translationFrameRootThemeAttributeNameForJs,
          !0
        );
        currentTranslationTheme === "mask" ? currentRootTheme !== "none" ? setAttribute(
          frame,
          translationFrameRootThemeAttributeNameForJs,
          "none",
          !0
        ) : setAttribute(
          frame,
          translationFrameRootThemeAttributeNameForJs,
          "mask",
          !0
        ) : currentRootTheme !== "mask" ? setAttribute(
          frame,
          translationFrameRootThemeAttributeNameForJs,
          "mask",
          !0
        ) : setAttribute(
          frame,
          translationFrameRootThemeAttributeNameForJs,
          "none",
          !0
        );
      }
    }
  }
  function restorePage() {
    if (document.dispatchEvent(
      new CustomEvent(documentMessageTypeIdentifierForTellThirdParty, {
        detail: JSON.stringify({
          type: "restorePage",
          payload: {}
        })
      })
    ), clean(), !globalContext || !globalContext.mainFrame)
      return;
    let allFrames = [
      globalContext.mainFrame,
      ...mutationObserverMap.keys()
    ];
    for (let rootFrame of allFrames)
      revertFrame(rootFrame);
    disableTitleMutationObserver(), setPageTranslatedStatus("Translating"), originalPageTitle && (document.title = originalPageTitle), setPageTranslatedStatus("Original");
  }
  function revertFrame(rootFrame) {
    disableMutatinObserver(rootFrame);
    let elements = rootFrame.querySelectorAll(
      "." + translationTargetElementWrapperClass
    );
    elements.forEach((element) => {
      element.remove();
    });
    let pdfElements = rootFrame.querySelectorAll(
      "." + translationPdfTargetContainerClass
    );
    pdfElements.forEach((element) => {
      element.remove();
    });
    let elementsEffected = rootFrame.querySelectorAll(
      "[" + sourceElementEffectAttributeName + "]"
    );
    return elementsEffected.forEach((element) => {
      if (isProd2) {
        delete element[elementMarkRootKey];
        let keys = Object.keys(element.dataset).filter((key) => key.startsWith(brandIdForJs));
        for (let key of keys)
          delete element.dataset[key];
      } else {
        let keys = Object.keys(element.dataset).filter((key) => key.startsWith(brandIdForJs));
        for (let key of keys)
          delete element.dataset[key];
      }
      element.removeAttribute(sourceElementEffectAttributeName);
    }), elements.length + pdfElements.length + elementsEffected.length;
  }
  function onElementVisible(paragraph, callback) {
    let isCalled = !1, firstElement = getFirstHTMLElement(paragraph.elements), lastElement = getLastHTMLElement(paragraph.elements);
    waitToTranslateParagraphIds.add(paragraph.id);
    let observers = [];
    if (firstElement) {
      let observe = new IntersectionObserver((entries, observer) => {
        entries.forEach((entry) => {
          entry.intersectionRatio > 0 && (observer.disconnect(), isCalled || (isCalled = !0, callback(paragraph)));
        });
      });
      allIntersectionObserver.push(observe), observers.push(observe), observe.observe(firstElement);
    }
    if (lastElement && lastElement !== firstElement) {
      let observe = new IntersectionObserver((entries, observer) => {
        entries.forEach((entry) => {
          entry.intersectionRatio > 0 && (observer.disconnect(), isCalled || (isCalled = !0, callback(paragraph)));
        });
      });
      allIntersectionObserver.push(observe), observers.push(observe), observe.observe(lastElement);
    }
    let paragraphEntiry = getParagraph(paragraph.id);
    paragraphEntiry && (paragraphEntiry.observers = observers, setParagraph(paragraph.id, paragraphEntiry));
  }
  function onHiddenElementVisible(element, callback) {
    if (element) {
      let resizewatcher = new ResizeObserver((entries, observer) => {
        for (let entry of entries)
          entry.contentRect.width > 10 && (observer.disconnect(), callback(entry.target));
      });
      resizewatcher.observe(element), allResizebleObserver.push(resizewatcher);
    }
  }
  async function translateNewDynamicNodes(ctx) {
    let currentNewDynamicNodes = [...currentNewDynamicElements];
    currentNewDynamicElements = [];
    let frameMap = /* @__PURE__ */ new Map();
    currentNewDynamicNodes.forEach((node) => {
      frameMap.has(node.rootFrame) || frameMap.set(node.rootFrame, []), frameMap.get(node.rootFrame)?.push(node.element);
    });
    for (let [rootFrame, elements] of frameMap)
      try {
        let allContainers = [];
        for (let element of elements) {
          let rawContainers = getContainers(
            element,
            ctx
          );
          if (rawContainers.length === 0)
            continue;
          let containers = rawContainers;
          allContainers.push(...containers);
        }
        await translateContainers(allContainers, rootFrame, ctx, !0);
      } catch (e3) {
        log_default.error(`translateNewDynamicNodes error: ${e3.message}`);
      }
  }
  function translationParagraph(visibleParagraph, ctx) {
    waitToTranslateParagraphIds.delete(visibleParagraph.id);
    let pairKeys = Object.keys(ctx.rule.pairs), paragraphWithState = getParagraph(visibleParagraph.id);
    paragraphWithState && (paragraphWithState.observers && paragraphWithState.observers.length > 0 && paragraphWithState.observers.forEach((observer) => {
      observer.disconnect();
    }), paragraphWithState.observers = [], paragraphWithState.state = "Translating", setParagraph(visibleParagraph.id, paragraphWithState));
    let id = visibleParagraph.id;
    currentTranslatedTextLength += visibleParagraph.text.length;
    let realElements = getHTMLElements(visibleParagraph.elements);
    if (pairKeys.length > 0) {
      let targetTranslationContainer = visibleParagraph.targetContainer, targetTranslationWrapper = document.createElement(
        ctx.rule.targetWrapperTag
      );
      targetTranslationContainer.appendChild(targetTranslationWrapper), targetTranslationWrapper.classList.add(
        "notranslate",
        translationTargetElementWrapperClass
      ), targetTranslationWrapper.id = `${translationTargetElementWrapperClass}-${id}`, targetTranslationWrapper.setAttribute("lang", ctx.targetLanguage);
      let loadingHtml = getLoadingHTML(
        ctx.config.loadingTheme,
        ctx.rule
      );
      targetTranslationWrapper.innerHTML = loadingHtml;
    } else if (visibleParagraph.isPdf) {
      let firstElement = getFirstHTMLElement(visibleParagraph.elements), elementStyle = globalThis.getComputedStyle(firstElement), top = elementStyle.top, fontSize = elementStyle.fontSize, fontSizeNumber = parseFloat(fontSize.slice(0, -2));
      isNaN(fontSizeNumber) || fontSizeNumber > 20 && (fontSize = "20px");
      let targetContainer = visibleParagraph.targetContainer, paragraphTarget = document.createElement(ctx.rule.targetWrapperTag);
      realElements.length === 1 && (paragraphTarget.style.fontSize = fontSize), paragraphTarget.id = `${translationTargetElementWrapperClass}-${id}`, paragraphTarget.style.top = top;
      let firstElementLeft = getAttribute(firstElement, sourceElementLeft), minLeft = realElements.reduce((prev, current) => {
        let left = getAttribute(current, sourceElementLeft);
        return left && left < prev ? left : prev;
      }, 1e3), width = realElements.reduce((prev, current) => {
        let right = getAttribute(current, sourceElementRight);
        return right && right > prev ? right : prev;
      }, 0) - minLeft;
      width < 30, width > 600 && (width = 600), firstElementLeft < 200 && (firstElementLeft = 10), firstElementLeft && firstElementLeft < 0 && (firstElementLeft = 0), paragraphTarget.style.left = `${minLeft || 10}px`, minLeft < 400 ? paragraphTarget.style.width = width + "px" : paragraphTarget.style.width = `calc(100% - ${minLeft}px)`, paragraphTarget.classList.add(
        "notranslate",
        `${translationTargetElementWrapperClass}`
      ), targetContainer.appendChild(paragraphTarget);
    } else {
      let lastElement = getLastHTMLElement(visibleParagraph.elements), position = "afterend";
      visibleParagraph.elements.length > 0 && lastElement && (realElements.length === 1 ? position = "beforeend" : isInlineElement(
        visibleParagraph.elements[0],
        ctx.rule
      ) || (position = "beforeend")), ctx.rule.insertPosition && (position = ctx.rule.insertPosition);
      let { rule } = ctx, targetTranslationWrapper = document.createElement(
        rule.targetWrapperTag
      );
      targetTranslationWrapper.classList.add(
        "notranslate",
        translationTargetElementWrapperClass
      ), targetTranslationWrapper.id = `${translationTargetElementWrapperClass}-${id}`, targetTranslationWrapper.setAttribute("lang", ctx.targetLanguage);
      let loadingHtml = getLoadingHTML(
        ctx.config.loadingTheme,
        ctx.rule
      );
      if (targetTranslationWrapper.innerHTML = loadingHtml, position === "beforeend") {
        let innerElement = getTheLastTextNodeParentElement(lastElement);
        innerElement ? innerElement.appendChild(targetTranslationWrapper) : lastElement.appendChild(targetTranslationWrapper);
      } else if (position === "afterend")
        lastElement.insertAdjacentElement(position, targetTranslationWrapper);
      else
        throw new Error("not support position");
    }
    currentParagraphIds.push(id), throttleMap[ctx.state.translationDebounce](ctx);
  }
  function addParagraphToQueue(paragraph, ctx) {
    ctx.state.translationStartMode === "dynamic" && !ctx.rule.isEbookBuilder && !ctx.rule.isSubtitleBuilder && currentTranslatedTextLength > ctx.state.immediateTranslationTextCount ? onElementVisible(paragraph, (visibleParagraph) => {
      ctx.rule.visibleDelay > 0 ? setTimeout(() => {
        translationParagraph(visibleParagraph, ctx);
      }, ctx.rule.visibleDelay) : translationParagraph(visibleParagraph, ctx);
    }) : translationParagraph(paragraph, ctx);
  }
  async function translatePage(ctx) {
    if (pageStatus === "Translating")
      return;
    let isInIframe = getIsInIframe();
    if (setPageTranslatedStatus("Translating"), ctx || (ctx = await getGlobalContext(getRealUrl(), {})), report("translage_page_daily", [
      {
        name: "translage_page_daily"
      }
    ], ctx), report("translate_page", [
      {
        name: "translate_page"
      }
    ], {
      ...ctx,
      sourceLanguage: getCurrentPageLanguage()
    }), ctx.state.isNeedClean && restorePage(), document.dispatchEvent(
      new CustomEvent(documentMessageTypeIdentifierForTellThirdParty, {
        detail: JSON.stringify({
          type: "translateStart",
          payload: {}
        })
      })
    ), document.dispatchEvent(
      new CustomEvent(documentMessageTypeIdentifierForTellThirdParty, {
        detail: JSON.stringify({
          type: "targetLanguage",
          payload: {
            targetLanguage: ctx.targetLanguage
          }
        })
      })
    ), ctx.rule.isPdf)
      return;
    if (ctx.rule.normalizeBody && document.querySelector(ctx.rule.normalizeBody)) {
      let boydHtml = document.body.innerHTML;
      document.body.innerHTML = "", document.body.innerHTML = boydHtml;
    }
    if (await setupOnceForInitPage(globalContext), !ctx.state.isAutoTranslate && ctx.config.tempTranslateDomainMinutes > 0) {
      let now = Date.now(), currentDomain = new URL(ctx.url).hostname, currentTempTranslationDomains = ctx.localConfig.tempTranslationUrlMatches || [], index = currentTempTranslationDomains.findIndex(
        (item) => item.match === currentDomain && item.expiredAt > now
      ), isChanged = !1;
      index > -1 || (currentTempTranslationDomains.push({
        match: currentDomain,
        expiredAt: now + ctx.config.tempTranslateDomainMinutes * 60 * 1e3
      }), isChanged = !0), isChanged && await setLocalConfig({
        ...ctx.localConfig,
        tempTranslationUrlMatches: [
          ...currentTempTranslationDomains
        ]
      });
    }
    ctx.state.isAutoTranslate = !0;
    let currentScrollOffset = globalThis.scrollY, currentWindowHeight = globalThis.innerHeight;
    if (currentScrollOffset >= currentWindowHeight && (ctx.state.immediateTranslationTextCount = 0), translationServiceInitmap[ctx.translationService] || (translationServiceInitmap[ctx.translationService] = !0, isInIframe || initTranslationEngine(ctx).catch((e3) => {
      log_default.warn("init translation engine error", e3);
    })), log_default.debug("ctx", ctx), Object.keys(ctx.rule.pairs).length > 0) {
      let paragraphs = getParagraphs2(ctx.mainFrame, ctx);
      document.dispatchEvent(
        new CustomEvent(documentMessageTypeIdentifierForTellThirdParty, {
          detail: JSON.stringify({
            type: "totalParagraphsCount",
            payload: {
              totalParagraphsCount: paragraphs.length
            }
          })
        })
      );
      for (let paragraph of paragraphs)
        addParagraphToQueue(paragraph, ctx);
      return;
    }
    addToUnmountQueue(() => {
      currentTranslatedTextLength = 0, cleanParagraphs(), allIntersectionObserver.forEach((observer) => {
        observer.disconnect();
      }), allResizebleObserver.forEach((observer) => {
        observer.disconnect();
      }), allIntersectionObserver = [], waitToTranslateParagraphIds.clear();
    }), setPageTranslatedStatus("Translating");
    try {
      let allFrames = getAllPageFrames(ctx), containersCount = 0;
      setPageTranslatedStatus("Translating"), log_default.debug("allFrames", allFrames);
      for (let rootFrame of allFrames) {
        let containerCount = await translateFrame(rootFrame, ctx);
        containersCount += containerCount;
      }
      containersCount === 0 && setPageTranslatedStatus("Translated"), ctx.rule.isTranslateTitle && !isInIframe && (translateTitle(ctx).catch((e3) => {
        log_default.error(
          "translateTitle error:",
          e3.name,
          e3.message,
          e3.details || ""
        );
      }), enableTitleMutationObserver(ctx));
      let paragraphEntities2 = getParagraphEntities();
      document.dispatchEvent(
        new CustomEvent(documentMessageTypeIdentifierForTellThirdParty, {
          detail: JSON.stringify({
            type: "totalParagraphsCount",
            payload: {
              totalParagraphsCount: paragraphEntities2.size
            }
          })
        })
      );
    } catch (e3) {
      setPageTranslatedStatus("Error"), log_default.error(e3);
    }
  }
  function getAllPageFrames(ctx) {
    let allFrames = [ctx.mainFrame];
    return document.querySelectorAll("iframe").forEach((frame) => {
      isInlineIframe(frame) && allFrames.push(frame.contentDocument.body);
    }), ctx.rule.shadowRootSelectors && ctx.rule.shadowRootSelectors.length > 0 && getElementsBySelectors(
      ctx.mainFrame,
      ctx.rule.shadowRootSelectors
    ).forEach((host) => {
      host.shadowRoot && host.shadowRoot.mode === "open" && allFrames.push(host.shadowRoot);
    }), allFrames;
  }
  async function translateFrame(rootFrame, ctx, nonGlobalTranslate) {
    markContainers(rootFrame, ctx.rule, rootFrame, !1);
    let containers = getContainers(rootFrame, ctx);
    log_default.debug("detect containers", containers), stopChangePageStatus = !!nonGlobalTranslate;
    let { rule } = ctx;
    containers.length > 0 && await translateContainers(containers, rootFrame, ctx, !1);
    let observer = enableMutatinObserver(rootFrame, rule, ctx);
    return rootFrame === ctx.mainFrame ? mainMutaionObserver = observer : mutationObserverMap.set(rootFrame, observer), containers.length;
  }
  async function getGlobalContext(url, state) {
    let config = await getConfig2(), stateKeys = Object.keys(state);
    if (globalContext) {
      let options = {
        url,
        config,
        state: { ...globalContext.state, ...state }
      };
      globalContext = await getContext(options);
    } else {
      let realState = state;
      stateKeys.length === 0 && (realState = void 0), globalContext = await getContext({
        url,
        config,
        state: realState
      });
    }
    return globalContext;
  }
  async function toggleTranslateTheMainPage() {
    getPageStatus() === "Original" ? await translateTheMainPage() : (getPageStatus() === "Translated" || getPageStatus() === "Error") && (globalContext = await getGlobalContext(getRealUrl(), {}), globalContext.state.translationArea !== "main" ? await translateTheMainPage() : restorePage());
  }
  async function translateTheMainPage() {
    globalContext = await getGlobalContext(getRealUrl(), {
      translationArea: "main"
    }), await translatePage(globalContext);
  }
  async function translateTheWholePage() {
    globalContext = await getGlobalContext(getRealUrl(), {
      translationArea: "body"
    }), await translatePage(globalContext);
  }
  async function toggleTranslateTheWholePage() {
    getPageStatus() === "Original" ? await translateTheWholePage() : (getPageStatus() === "Translated" || getPageStatus() === "Error") && (globalContext = await getGlobalContext(getRealUrl(), {}), globalContext.state.translationArea !== "body" ? (globalContext.state.translationArea = "body", globalContext = await getGlobalContext(getRealUrl(), {}), await translatePage(globalContext)) : restorePage());
  }
  async function translateToThePageEndImmediately() {
    globalContext = await getGlobalContext(getRealUrl(), {
      translationStartMode: "immediate"
    }), await translatePage(globalContext), await translateNewDynamicNodes(globalContext);
  }
  async function translateTitle(ctx) {
    let pageTitle = document.title;
    if (!pageTitle || pageTitle.includes(titleDelimiters))
      return;
    originalPageTitle !== pageTitle && (originalPageTitle = pageTitle);
    let currentLang = "auto";
    if (ctx.state.isDetectParagraphLanguage || (currentLang = getCurrentPageLanguageByClient()), currentLang === "auto") {
      let detectedLang = await detectLanguage({
        text: pageTitle,
        minLength: 10
      });
      if (isSameTargetLanguage(detectedLang, ctx.targetLanguage))
        return;
    }
    try {
      let result = await translateSingleSentence({
        id: 0,
        url: ctx.url,
        text: pageTitle,
        from: currentLang,
        to: ctx.targetLanguage,
        fromByClient: currentLang
      }, ctx);
      result && result.text && (document.title = originalPageTitle + titleDelimiters + result.text);
    } catch (e3) {
      throw e3;
    }
  }
  function setLoadingToParagraph(rootFrame, id, ctx) {
    let element = rootFrame.querySelector(
      "#" + translationTargetElementWrapperClass + "-" + id
    );
    element && (element.innerHTML = getLoadingHTML(
      ctx.config.loadingTheme,
      ctx.rule
    ));
  }
  function getLoadingHTML(theme, rule) {
    return `&#160;<${rule.targetWrapperTag} class="${brandId}-loading-${theme} notranslate"></${rule.targetWrapperTag}>`;
  }
  async function translateContainers(containers, rootFrame, ctx, isDynamic) {
    let { rule } = ctx;
    for (let container of containers)
      markContainers(container, rule, rootFrame, isDynamic);
    let targetContainers = [];
    if (ctx.rule.isPdf)
      containers.length > 0 && (setPageTranslatedStatus("Translating"), targetContainers = normalizeContainer2(
        containers,
        rule
      ).targetContainers);
    else {
      setPageTranslatedStatus("Translating");
      let normalizeResult = normalizeContainer(
        containers,
        rule
      ), { hiddenElements } = normalizeResult;
      for (let element of hiddenElements)
        onHiddenElementVisible(element, () => {
          removeAttribute(element, sourceElementExcludeAttributeName, !0), translateContainers([element], rootFrame, ctx, !0);
        });
      setPageTranslatedStatus("Translating");
    }
    let paragraphs = [];
    if (ctx.rule.isPdf ? paragraphs = await getParagraphs3(
      rootFrame,
      containers,
      ctx,
      targetContainers
    ) : (containers = containers.filter((container) => !isExcludeElement(container, rule, !1)), paragraphs = await getParagraphs(
      rootFrame,
      containers,
      ctx,
      !1
    )), setPageTranslatedStatus("Translating"), paragraphs.length === 0) {
      setPageTranslatedStatus("Translated");
      return;
    }
    log_default.debug("detect paragraphs", paragraphs);
    for (let paragraph of paragraphs)
      addParagraphToQueue(paragraph, ctx);
    setPageTranslatedStatus("Translated");
  }
  async function retryFailedParagraphs() {
    document.dispatchEvent(
      new CustomEvent(documentMessageTypeIdentifierForTellThirdParty, {
        detail: JSON.stringify({
          type: "retryFailedParagraphsStart",
          payload: {}
        })
      })
    );
    let allParagraphEntities = getParagraphEntities(), ids = [];
    for (let [id, paragraph] of allParagraphEntities)
      paragraph.state === "Error" && ids.push(id);
    let currentParagraphLang = "auto", ctx = await getGlobalContext(getRealUrl(), {});
    ctx.state.isDetectParagraphLanguage || (currentParagraphLang = getCurrentPageLanguage());
    let payload = {
      sentences: ids.filter((id) => getParagraph(id)).map((id) => {
        let paragraph = getParagraph(id), from = paragraph.languageByLocal;
        return from === "auto" && (from = currentParagraphLang), setLoadingToParagraph(paragraph.rootFrame, id, ctx), {
          id: paragraph.id,
          url: ctx.encryptedUrl,
          text: paragraph.text,
          from,
          fromByClient: paragraph.languageByClient,
          to: ctx.targetLanguage
        };
      })
    };
    if (payload.sentences.length > 0) {
      setPageTranslatedStatus("Translating");
      try {
        await translateMultipleSentences(
          payload,
          ctx,
          (err, translatedSentence, sentenceRequest) => {
            onParagraphTranslated(err, translatedSentence, sentenceRequest, ctx);
          }
        );
      } catch (e3) {
        setPageTranslatedStatus("Error"), log_default.error(
          "translateCurrentQueue error",
          e3.name,
          e3.message,
          e3.details || " "
        );
        return;
      }
    }
  }
  function onParagraphTranslated(err, translatedSentence, sentenceRequest, ctx) {
    let translatedOk = !1, paragraphWithState = getParagraph(sentenceRequest.id);
    if (paragraphWithState && (err || !translatedSentence)) {
      err || (log_default.error("translate error", sentenceRequest, err, translatedSentence), err = new Error("no response from server"));
      let { rule } = ctx, wrapperId = sentenceRequest.id, wrapper = paragraphWithState.rootFrame.querySelector(
        `#${translationTargetElementWrapperClass}-${wrapperId}`
      ), errorMessage = err.message.replaceAll(`
`, "");
      errorMessage = errorMessage.replaceAll('"', "&quot;"), paragraphWithState && (paragraphWithState.state = "Error", setParagraph(paragraphWithState.id, paragraphWithState));
      let errorHtml = `<${rule.targetWrapperTag} class="${brandId}-error notranslate"> <${rule.targetWrapperTag} class="immersive-translate-tooltip" data-immersive-translate-tooltip-text="${errorMessage}"><button class="${brandId}-clickable-button notranslate" title="${errorMessage}">\u2757</button></${rule.targetWrapperTag}> <button class="${brandId}-clickable-button notranslate" data-${brandId}-paragraph-id="${wrapperId}" data-${brandId}-action="retry">\u{1F504}</button></${rule.targetWrapperTag}>`;
      wrapper && (wrapper.innerHTML = errorHtml);
    } else {
      let paragraph = getParagraph(sentenceRequest.id);
      if (paragraph) {
        paragraph.state = "Translated", setParagraph(paragraph.id, paragraph);
        let wrapperId = translatedSentence.id, wrapper = paragraph.rootFrame.querySelector(
          `#${translationTargetElementWrapperClass}-${wrapperId}`
        );
        if (wrapper) {
          if (paragraph.text === translatedSentence.text)
            wrapper.innerHTML = "";
          else {
            let targetItem = paragraphToHtml(
              paragraph,
              translatedSentence,
              ctx
            );
            wrapper.innerHTML = "", wrapper.insertAdjacentHTML("afterbegin", targetItem.html);
          }
          paragraph.rootFrame.querySelectorAll(
            `[${sourceElementParagraphAttributeName}="${wrapperId}"]`
          ).forEach((element) => {
            setAttribute(
              element,
              sourceElementTranslatedMarkAttributeName,
              "1"
            );
          }), translatedOk = !0;
        }
      } else
        log_default.error("paragraph not found", sentenceRequest.id);
    }
    document.dispatchEvent(
      new CustomEvent(documentMessageTypeIdentifierForTellThirdParty, {
        detail: JSON.stringify({
          type: "paragraphTranslated",
          payload: {
            ok: translatedOk
          }
        })
      })
    );
  }
  async function translateCurrentQueue(ctx) {
    if (currentParagraphIds.length === 0)
      return Promise.resolve();
    let ids = [...currentParagraphIds];
    currentParagraphIds = [];
    let currentParagraphLang = "auto";
    ctx.state.isDetectParagraphLanguage || (currentParagraphLang = getCurrentPageLanguage());
    let payload = {
      sentences: ids.filter((id) => getParagraph(id)).map((id) => {
        let paragraph = getParagraph(id), from = paragraph.languageByLocal;
        return from === "auto" && (from = currentParagraphLang), {
          id: paragraph.id,
          url: ctx.encryptedUrl,
          text: paragraph.text,
          from,
          fromByClient: paragraph.languageByClient,
          to: ctx.targetLanguage
        };
      })
    };
    if (payload.sentences.length > 0) {
      setPageTranslatedStatus("Translating");
      try {
        await translateMultipleSentences(
          payload,
          ctx,
          (err, translatedSentence, sentenceRequest) => {
            onParagraphTranslated(err, translatedSentence, sentenceRequest, ctx);
          }
        );
      } catch (e3) {
        setPageTranslatedStatus("Error"), log_default.error(
          "translateCurrentQueue error",
          e3.name,
          e3.message,
          e3.details || " "
        );
        return;
      }
    }
    setPageTranslatedStatus("Translated");
  }
  function setPageTranslatedStatus(_pageStatus) {
    stopChangePageStatus || (pageStatus = _pageStatus, sendPageTranslatedStatus(pageStatus));
  }
  function enableMutatinObserver(rootFrame, rule, ctx) {
    log_default.debug("enableMutatinObserver for ", rootFrame), disableMutatinObserver(rootFrame), allNewDynamicElements = [], currentNewDynamicElements = [];
    let inlineAndIgnoreAndTextTags = rule.inlineTags.concat(rule.excludeTags).concat("#text", "BR"), mutationObserver = new MutationObserver(function(mutations) {
      mutations.forEach((mutation) => {
        if (rootFrame === ctx.mainFrame) {
          let currentUrl = getRealUrl();
          if (currentUrl.split("#")[0] !== oldUrl && rule.observeUrlChange) {
            oldUrl = currentUrl.split("#")[0], clean(), disableMutatinObserver(rootFrame), disableTitleMutationObserver(), initPage();
            let event = new Event(pageUrlChangedEventName);
            document.dispatchEvent(event);
            return;
          }
        }
        mutation.addedNodes.forEach((addedNode) => {
          if (addedNode.nodeType === Node.ELEMENT_NODE) {
            let element = addedNode;
            if (element.nodeName === "IFRAME")
              isInlineIframe(element) && setTimeout(() => {
                injectCssToFrame(
                  element.contentDocument,
                  ctx
                ), translateFrame(
                  element.contentDocument.body,
                  ctx
                ).catch((e3) => {
                  log_default.error(
                    "translateFrame error",
                    e3.details || " ",
                    e3
                  );
                });
              }, ctx.rule.urlChangeDelay);
            else if (mutationElementIsBlock(rule, element) || !inlineAndIgnoreAndTextTags.includes(
              element.nodeName
            )) {
              if (element.classList.contains("notranslate") || element.getAttribute("translate") === "no")
                return;
              isDuplicateElement(element, allNewDynamicElements) || (allNewDynamicElements.push(element), setTimeout(() => {
                currentNewDynamicElements.push({
                  element,
                  rootFrame
                }), debounceMap[ctx.state.translationDebounce](ctx);
              }, rule.mutationChangeDelay || 0)), element && element.querySelectorAll && element.querySelectorAll("iframe").forEach((inlineIframe) => {
                isInlineIframe(inlineIframe) && setTimeout(() => {
                  injectCssToFrame(
                    inlineIframe.contentDocument,
                    ctx
                  ), translateFrame(
                    inlineIframe.contentDocument.body,
                    ctx
                  ).catch((e3) => {
                    log_default.error(
                      "translateFrame error",
                      e3.details || " ",
                      e3
                    );
                  });
                }, ctx.rule.urlChangeDelay);
              });
            }
          }
        });
      });
    });
    return mutationObserver.observe(rootFrame, {
      childList: !0,
      subtree: !0
    }), mutationObserver;
  }
  function enableTitleMutationObserver(ctx) {
    let titleElement = document.querySelector("title");
    titleElement && (titleMutationObserver = new MutationObserver(function(mutations) {
      mutations.length > 0 && (mutations[0].target.text.includes(titleDelimiters) || translateTitle(ctx).catch((e3) => {
        log_default.error(
          "translateTitle error:",
          e3.name,
          e3.message,
          e3.details || ""
        );
      }));
    }), titleMutationObserver.observe(titleElement, {
      subtree: !0,
      characterData: !0,
      childList: !0
    }));
  }
  function mutationElementIsBlock(rule, element) {
    if (rule.extraBlockSelectors) {
      for (let match of rule.extraBlockSelectors)
        if (element.matches(match))
          return !0;
    }
    return !1;
  }
  async function initPage() {
    let isInIframe = getIsInIframe(), ctx = await getGlobalContext(getRealUrl(), {});
    ctx.rule.urlChangeDelay && await delay(ctx.rule.urlChangeDelay), ctx.rule.waitForSelectors && ctx.rule.waitForSelectors.length > 0 && await waitForSelectors(
      ctx.rule.waitForSelectors,
      ctx.rule.waitForSelectorsTimeout
    ), ctx.rule.globalMeta && Object.keys(ctx.rule.globalMeta).forEach((key) => {
      let meta = document.createElement("meta");
      meta.name = key, meta.content = ctx.rule.globalMeta[key], document.head.appendChild(meta);
    });
    let lang = ctx.sourceLanguage;
    lang === "auto" ? lang = await detectCurrentPageLanguage(ctx) : setCurrentPageLanguageByClient(lang);
    let isAutoTranslate = ctx.state.isAutoTranslate || ctx.isTranslateUrl || ctx.rule.isPdf;
    if (!isAutoTranslate && !ctx.isTranslateExcludeUrl && (log_default.debug(`detect page language: ${ctx.url} ${lang}`), isSameTargetLanguage(lang, ctx.targetLanguage) || lang === "auto" || isMatchLanguage(lang, ctx.config.translationLanguagePattern) && (isAutoTranslate = !0, log_default.debug(`match language pattern ${lang}, auto translate`))), ctx.rule.isEbookBuilder && (isAutoTranslate = !1), isMouseSupport() && setupTranslateListener(ctx), isAutoTranslate)
      globalContext.state.isAutoTranslate = !0, await translatePage(globalContext);
    else if (log_default.debug("do not auto translate", ctx), ctx.rule.initTranslationServiceAsSoonAsPossible && ctx.translationService === "deepl") {
      if (isSameTargetLanguage(lang, ctx.targetLanguage) || lang === "auto")
        return;
      ctx.config && ctx.config.translationServices && ctx.config.translationServices.deepl && ctx.config.translationServices.deepl.authKey && typeof ctx.config.translationServices.deepl.authKey == "string" && ctx.config.translationServices.deepl.authKey.startsWith("immersive_") && (translationServiceInitmap[ctx.translationService] || (translationServiceInitmap[ctx.translationService] = !0, isInIframe || initTranslationEngine(ctx).catch((e3) => {
        log_default.warn("init translation engine error", e3);
      })));
    }
  }
  async function setupOnceForInitPage(ctx) {
    isSetupForOnce || (isSetupForOnce = !0, globalThis.top !== globalThis.self && ctx.rule.useIframePostMessage && await setupChildIframeChannel(), injectCssToFrame(document, ctx), document.querySelectorAll("iframe").forEach((frame) => {
      isInlineIframe(frame) ? injectCssToFrame(frame.contentDocument, ctx) : ctx.rule.isEbook && injectCssToFrame(frame.contentDocument, ctx);
    }));
  }
  function disableMutatinObserver(rootFrame) {
    if (mutationObserverMap.has(rootFrame)) {
      let mutationObserver = mutationObserverMap.get(rootFrame);
      mutationObserver.disconnect(), mutationObserver.takeRecords(), mutationObserverMap.delete(rootFrame);
    } else if (globalContext && rootFrame === globalContext.mainFrame || rootFrame === document.body) {
      let mutationObserver = mainMutaionObserver;
      mutationObserver && (mutationObserver.disconnect(), mutationObserver.takeRecords());
    }
  }
  function disableTitleMutationObserver() {
    titleMutationObserver && (titleMutationObserver.disconnect(), titleMutationObserver.takeRecords(), titleMutationObserver = void 0);
  }
  function getPageStatus() {
    return pageStatus;
  }
  async function detectCurrentPageLanguage(ctx) {
    ctx || (globalContext = await getGlobalContext(getRealUrl(), {}), ctx = globalContext);
    let isInIframe = getIsInIframe(), lang = "auto", pairKeys = Object.keys(ctx.rule.pairs);
    if (isMonkey()) {
      let mainText = "";
      ctx.rule.isEbook || ctx.rule.isEbookBuilder ? mainText = getAllIframeMainText(ctx.mainFrame) : pairKeys.length > 0 ? mainText = getPairsSourceText(ctx.mainFrame, ctx.rule.pairs) : mainText = getMainText(ctx.mainFrame).slice(0, 1e3), lang = await detectLanguage({
        text: mainText
      });
    } else if (isInIframe)
      lang = await detectLanguage({
        text: getMainText(ctx.mainFrame).slice(0, 1e3)
      });
    else if (ctx.rule.isEbook || ctx.rule.isEbookBuilder) {
      let mainText = "";
      mainText = getAllIframeMainText(ctx.mainFrame), lang = await detectLanguage({
        text: mainText
      });
    } else if (pairKeys.length > 0) {
      let mainText = getPairsSourceText(ctx.mainFrame, ctx.rule.pairs);
      lang = await detectLanguage({
        text: mainText
      });
    } else
      lang = await detectTabLanguage();
    return lang === "auto" && (lang = await detectPageLanguage()), setCurrentPageLanguage(lang), lang;
  }
  function waitForSelectors(selectors, timeout = 3e3) {
    return new Promise((resolve, _reject) => {
      let timer = timeout ? setTimeout(() => {
        resolve(new Error("timeout"));
      }, timeout) : void 0, interval = setInterval(() => {
        selectors.every((selector) => document.querySelector(selector) !== null) && (clearInterval(interval), timer && clearTimeout(timer), resolve(null));
      }, 50);
    });
  }
  function getMouseOverParagraph(clientX2, clientY2, rule) {
    let range = getRangeFromPoint(clientX2, clientY2, rule);
    if (range == null)
      return;
    let checkTheUnTextElement = () => {
      let pointElement = document.elementFromPoint(
        clientX2,
        clientY2
      );
      if (!pointElement)
        return;
      let realInnerElement = findElementInShadow(
        pointElement,
        clientX2,
        clientY2
      );
      return realInnerElement === pointElement ? pointElement.nodeName === "BUTTON" ? pointElement : void 0 : getBlockParentNode(realInnerElement, rule);
    }, checkTheTextNode = () => {
      try {
        range.setStartBefore(range.startContainer), range.setEndAfter(range.startContainer);
      } catch (error2) {
        log_default.debug("get mouse over word fail", error2);
      }
      let rect = range.getBoundingClientRect();
      if (!(rect.left > clientX2 || rect.right < clientX2 || rect.top > clientY2 || rect.bottom < clientY2))
        return getBlockParentNode(range.startContainer, rule);
    }, findedElement;
    return range.startContainer.nodeType !== Node.TEXT_NODE ? findedElement = checkTheUnTextElement() : findedElement = checkTheTextNode(), findedElement;
  }
  function getSelectionText(rule) {
    return getMouseOverParagraph(clientX, clientY, rule);
  }
  async function translateHoverPartial(ctx, selectionDom) {
    if (selectionDom = selectionDom || getSelectionText(ctx.rule), !selectionDom) {
      log_default.debug("can not find selection part!");
      return;
    }
    await setupOnceForInitPage(globalContext), (async () => {
      if (selectionDom) {
        if (isMarked(selectionDom, targetContainerElementAttributeName) || selectionDom.closest(
          `.${translationTargetElementWrapperClass}`
        ))
          return;
        normalizeContainer([selectionDom], ctx.rule);
        let paragraphs = await getParagraphs(
          selectionDom.getRootNode(),
          [selectionDom],
          ctx,
          !0
        );
        if (paragraphs.length > 0)
          for (let paragraph of paragraphs)
            translationParagraph(paragraph, ctx);
      }
    })();
  }
  function setupTranslateListener(context) {
    loadEventListener(context);
  }
  function addEventHandler(eventName, callbackFunc) {
    return addEventListener(eventName, callbackFunc, { signal });
  }
  async function loadEventListener(originalCtx) {
    let ctxOptions = {
      url: originalCtx.url,
      config: originalCtx.config,
      state: {
        ...originalCtx.state,
        translationArea: "body"
      }
    }, ctx = await getContext(ctxOptions), config = ctx.config, closeMouseTranslate = config.generalRule.mouseHoverHoldKey === "Off", isTranslateDirectlyOnHover = config.generalRule.mouseHoverHoldKey === "Auto", mousemoveThrottleHandle = se3((e3) => {
      if (mouseMoved == !1 && Math.abs(e3.clientX - clientX) + Math.abs(e3.clientY - clientY) > 3 && (mouseMovedCount < 2 ? mouseMovedCount += 1 : mouseMoved = !0), clientX = e3.clientX, clientY = e3.clientY, isTranslateDirectlyOnHover || isHoldMouseHoverKey && !delayChecker) {
        let selectioPparagraph = getSelectionText(ctx.rule);
        selectioPparagraph && translateHoverPartial(ctx, selectioPparagraph);
      }
    }, isTranslateDirectlyOnHover ? 700 : 300), mouseTriggerTranslateHandle = (event) => {
      let targetElement = event.target;
      isExcludeElement(targetElement, ctx.rule, !0) || translateHoverPartial(ctx);
    }, keydownListener = (event) => {
      let configKey = config?.generalRule?.mouseHoverHoldKey?.toLowerCase() || "alt", codes = v3.getPressedKeyCodes();
      if (codes.length > 1 && v3[configKey] && (lastTimeOfModifierKeyAndNormalKeyPress = Date.now()), codes.length === 1 && v3[configKey]) {
        let modifierPressTime = Date.now();
        isHoldMouseHoverKey = !0, delayChecker && clearTimeout(delayChecker), delayChecker = setTimeout(() => {
          let diff = lastTimeOfModifierKeyAndNormalKeyPress - modifierPressTime;
          diff > 0 && diff <= 150 ? isHoldMouseHoverKey = !1 : mouseTriggerTranslateHandle(event), delayChecker = void 0;
        }, 150);
      }
    };
    if (!closeMouseTranslate)
      if (addEventHandler("mousemove", mousemoveThrottleHandle), isTranslateDirectlyOnHover)
        addEventHandler("blur", () => {
          mouseMoved = !1, mouseMovedCount = 0, mousemoveThrottleHandle.cancel();
        });
      else {
        let lowercaseKey = config?.generalRule?.mouseHoverHoldKey?.toLowerCase() || "alt", modifierKeys = [
          "ctrl",
          "alt",
          "shift",
          "cmd",
          "command",
          "option",
          "control"
        ];
        addEventHandler("keyup", (event) => {
          isHoldMouseHoverKey = !1;
        }), modifierKeys.includes(lowercaseKey) ? v3("*", keydownListener) : v3(
          config.generalRule.mouseHoverHoldKey,
          mouseTriggerTranslateHandle
        );
      }
  }