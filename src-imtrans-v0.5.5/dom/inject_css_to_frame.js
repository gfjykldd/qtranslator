
  // dom/inject_css_to_frame.ts
  function injectCssToFrame(root2, ctx) {
    let defaultInjectedCss = getEnv().IMMERSIVE_TRANSLATE_INJECTED_CSS;
    injectCSS(
      root2,
      defaultInjectedCss,
      "immersive-translate-default-injected-css"
    );
    let translationThemePattern = ctx.config.translationThemePatterns || {}, translationTheme = ctx.state.translationTheme, translationThemePatternConfig = translationThemePattern[translationTheme] || {};
    applyUserConfigCss(
      root2,
      translationTheme,
      translationThemePatternConfig
    );
    let injectedCss = "";
    (ctx.rule.injectedCss || ctx.rule.additionalInjectedCss) && (ctx.rule.injectedCss && ctx.rule.injectedCss.length > 0 && (injectedCss += ctx.rule.injectedCss.join(`
`)), ctx.rule.additionalInjectedCss && ctx.rule.additionalInjectedCss.length > 0 && (injectedCss += `
` + ctx.rule.additionalInjectedCss.join(`
`))), injectedCss && injectCSS(root2, injectedCss, "immersive-translate-dynamic-injected-css");
  }