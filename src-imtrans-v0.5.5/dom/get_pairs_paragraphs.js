
  // dom/get_pairs_paragraphs.ts
  function getParagraphs2(rootFrame, ctx) {
    let pariKeys = Object.keys(ctx.rule.pairs), allParagraphs = [], id = 1, currentPageLanguageByClient2 = "auto", currentPageLanguage2 = getCurrentPageLanguage();
    ctx.state.isDetectParagraphLanguage || (currentPageLanguageByClient2 = getCurrentPageLanguageByClient());
    for (let pairKey of pariKeys) {
      let targetPair = ctx.rule.pairs[pairKey], sourceElements = rootFrame.querySelectorAll(
        pairKey
      ), targetElements = rootFrame.querySelectorAll(
        targetPair
      );
      for (let i3 = 0; i3 < sourceElements.length; i3++) {
        let sourceElement = sourceElements[i3], targetElement = targetElements[i3];
        if (sourceElement && targetElement) {
          let paragraph = {
            elements: [sourceElement],
            isVertical: !1,
            rootFrame,
            text: sourceElement.textContent || "",
            variables: [],
            inline: !1,
            preWhitespace: !0,
            isPdf: !1,
            targetContainer: targetElement,
            id: id++,
            languageByClient: currentPageLanguageByClient2,
            languageByLocal: currentPageLanguage2
          };
          addToParagraphs(paragraph, allParagraphs);
        }
      }
    }
    return allParagraphs;
  }