
  // dom/ready_state.js
  var options = {
    capture: !0,
    once: !0,
    passive: !0
  }, isReady = () => document.readyState === "interactive" || document.readyState === "complete", isCurrentState = (state) => document.readyState === state, resolveState = (state, fn) => isCurrentState(state) || isReady() ? (fn(state), !0) : !1, loading = () => new Promise((resolve) => {
    resolveState("loading", resolve) || document.addEventListener(
      "readystatechange",
      () => {
        document.readyState === "loading" && resolve("loading");
      },
      options
    );
  }), interactive = () => new Promise((resolve) => {
    resolveState("interactive", resolve) || document.addEventListener(
      "readystatechange",
      () => {
        document.readyState === "interactive" && resolve("interactive");
      },
      options
    );
  }), complete = () => new Promise((resolve) => {
    resolveState("complete", resolve) || document.addEventListener(
      "readystatechange",
      () => {
        document.readyState === "complete" && resolve("complete");
      },
      options
    );
  }), domready = () => new Promise((resolve) => {
    resolveState("domready", resolve) || document.addEventListener(
      "DOMContentLoaded",
      () => {
        resolve("domready");
      },
      options
    );
  }), load = () => new Promise((resolve) => {
    resolveState("load", resolve) || window.addEventListener(
      "load",
      () => {
        resolve("load");
      },
      options
    );
  }), readyState = {};
  Object.defineProperties(readyState, {
    state: {
      get: function() {
        return document.readyState;
      }
    },
    loading: {
      get: function() {
        return loading();
      }
    },
    interactive: {
      get: function() {
        return interactive();
      }
    },
    complete: {
      get: function() {
        return complete();
      }
    },
    window: {
      get: function() {
        return load();
      }
    },
    load: {
      get: function() {
        return load();
      }
    },
    domready: {
      get: function() {
        return domready();
      }
    },
    dom: {
      get: function() {
        return domready();
      }
    },
    ready: {
      get: function() {
        return isReady();
      }
    }
  });
  var ready_state_default = readyState;