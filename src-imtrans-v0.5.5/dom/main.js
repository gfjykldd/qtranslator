
  // dom/main.ts
  async function main2(ctx) {
    if (!ctx) {
      let config = await getConfig2();
      ctx = await getContext({
        config,
        url: getRealUrl()
      });
    }
    ctx.config.debug ? log_default.setLevel("debug") : log_default.setLevel("info"), globalThis.top != globalThis.self || await main().catch((e) => {
      log_default.error(`init popup page error: ${e}`);
    }), ctx.isTranslateExcludeUrl || (report("init_page_daily", [
      {
        name: "init_page_daily"
      }
    ], ctx), ctx.rule.isPdf ? await initPdf(ctx) : ctx.rule.isEbook || ctx.rule.isEbookBuilder || ctx.rule.isSubtitleBuilder || await initPage(), checkCronAndRunOnce());
  }