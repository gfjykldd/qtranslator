
  // dom/context.ts
  async function getContext(options) {
    let { url, config, state } = options, urlObj = new URL(url), sourceLanguage = "auto", {
      translationParagraphLanguagePattern,
      translationService,
      translationServices,
      translationTheme,
      translationThemePatterns,
      translationUrlPattern,
      targetLanguage,
      sourceLanguageUrlPattern,
      immediateTranslationPattern
    } = config, isDetectParagraphLanguage = isMatched(
      url,
      translationParagraphLanguagePattern
    ), isImmediateTranslate = isMatched(url, immediateTranslationPattern), defaultTranslationService = translationService, services = Object.keys(translationServices);
    for (let service of services) {
      let serviceConfig = translationServices[service];
      if (isMatched(url, serviceConfig)) {
        defaultTranslationService = service;
        break;
      }
    }
    let defaultTheme = translationTheme, themes = Object.keys(translationThemePatterns);
    for (let theme of themes) {
      let themeConfig = translationThemePatterns[theme];
      if (themeConfig && isMatched(url, themeConfig)) {
        defaultTheme = theme;
        break;
      }
    }
    let isTranslateUrl = isMatched(url, translationUrlPattern), isTranslateExcludeUrl = isMatchedExclude(
      url,
      translationUrlPattern
    );
    isTranslateExcludeUrl || (isTranslateExcludeUrl = isMatchUrl(url, buildinExcludeUrls));
    let sourceConfigLanguages = Object.keys(sourceLanguageUrlPattern), sourceLanguageReverseMap = {};
    for (let language of sourceConfigLanguages) {
      let matchPattern = sourceLanguageUrlPattern[language];
      if (matchPattern && matchPattern.matches)
        for (let match of matchPattern.matches)
          sourceLanguageReverseMap[match] = language;
    }
    let sourceUrlMatches = Object.keys(sourceLanguageReverseMap), sourceUrlMatched = getMatchedUrl(url, sourceUrlMatches);
    sourceUrlMatched && (sourceLanguage = sourceLanguageReverseMap[sourceUrlMatched] ?? "auto", sourceLanguageReverseMap[sourceUrlMatched] && sourceLanguageReverseMap[sourceUrlMatched] !== "auto" && setCurrentPageLanguageByClient(
      sourceLanguageReverseMap[sourceUrlMatched]
    ));
    let defaultTargetLanguage = targetLanguage || "zh-CN", hostname2 = urlObj.hostname, encryptedHostname = await sha256(hostname2), pathAndQueryAndHash = urlObj.pathname + urlObj.search + urlObj.hash, encryptedPath = await sha256(pathAndQueryAndHash), encryptedUrl = `https://${encryptedHostname}.com/${encryptedPath}`, localConfig = await getLocalConfig(), buildinConfig = await getLatestBuildinConfig(), translationStartMode = config.translationStartMode;
    translationStartMode === "dynamic" && isImmediateTranslate && (translationStartMode = "immediate");
    let ctx = {
      targetLanguage: defaultTargetLanguage,
      config,
      translationService: defaultTranslationService,
      isTranslateUrl,
      sourceLanguage,
      mainFrame: document.body,
      isTranslateExcludeUrl,
      rule: config.generalRule,
      url,
      encryptedUrl,
      state: state ? Object.assign({
        translationArea: config.translationArea,
        translationStartMode,
        immediateTranslationTextCount: config.immediateTranslationTextCount,
        isAutoTranslate: !1,
        translationDebounce: 300,
        isNeedClean: !1,
        isDetectParagraphLanguage,
        cache: config.cache,
        translationTheme: defaultTheme
      }, state) : {
        translationArea: config.translationArea,
        translationStartMode,
        immediateTranslationTextCount: config.immediateTranslationTextCount,
        isAutoTranslate: !1,
        translationDebounce: 300,
        isNeedClean: !1,
        isDetectParagraphLanguage,
        cache: config.cache,
        translationTheme: defaultTheme
      },
      localConfig
    };
    ctx.state.translationArea === "body" && (ctx.config.generalRule.excludeTags = ctx.config.generalRule.excludeTags.filter((tag) => !ctx.config.generalRule.bodyTranslateTags.includes(tag)), ctx.config.generalRule.additionalExcludeSelectors = ctx.config.generalRule.additionalExcludeSelectors.filter(
      (selector) => selector !== ".btn"
    ));
    let translationServiceConfig = config.translationServices[ctx.translationService] || {};
    translationServiceConfig.immediateTranslationTextCount !== void 0 && isNumber(translationServiceConfig.immediateTranslationTextCount) && translationServiceConfig.immediateTranslationTextCount >= 0 && (ctx.state.immediateTranslationTextCount = translationServiceConfig.immediateTranslationTextCount), ctx.translationService === "deepl" && translationServiceConfig && translationServiceConfig.authKey && translationServiceConfig.authKey.startsWith("immersive_") && translationServiceConfig.immediateTranslationTextCountForImmersiveDeepl !== void 0 && translationServiceConfig.immediateTranslationTextCountForImmersiveDeepl >= 0 && (ctx.state.immediateTranslationTextCount = translationServiceConfig.immediateTranslationTextCountForImmersiveDeepl), translationServiceConfig && translationServiceConfig.translationDebounce && typeof translationServiceConfig.translationDebounce == "number" && (ctx.state.translationDebounce = translationServiceConfig.translationDebounce);
    let buildinImmediateTranslationTextCount = buildinConfig.immediateTranslationTextCount;
    config.immediateTranslationTextCount !== buildinImmediateTranslationTextCount && (ctx.state.immediateTranslationTextCount = config.immediateTranslationTextCount);
    let rules = config.rules, rule;
    globalThis.PDFViewerApplication ? rule = rules.find((rule2) => rule2.isPdf) : globalThis.immersiveTranslateEbookViewer ? rule = rules.find((rule2) => rule2.isEbook) : globalThis.immersiveTranslateEbookBuilder ? rule = rules.find((rule2) => rule2.isEbookBuilder) : rule = rules.find((rule2) => isMatched(url, rule2)), ctx.rule.isPdf && (ctx.state.translationArea = "main"), ctx.state.translationArea === "body" && (ctx.rule.paragraphMinTextCount = 1, ctx.rule.paragraphMinWordCount = 1);
    let generalRule = config.generalRule;
    if (rule && (ctx.rule = mergeRule(generalRule, rule)), ctx.state.translationArea === "body" && ctx.rule.excludeTags && (ctx.rule.excludeTags = ctx.rule.excludeTags.filter((tag) => !ctx.rule.bodyTranslateTags.includes(tag) && !ctx.rule.forceTranslateTags.includes(tag))), ctx.rule.mainFrameSelector) {
      let mainFrame = document.querySelector(ctx.rule.mainFrameSelector);
      mainFrame && (ctx.mainFrame = mainFrame);
    }
    return ctx;
  }
  function isMatched(url, matchPattern) {
    if (!matchPattern)
      return !1;
    let { matches, excludeMatches, selectorMatches, excludeSelectorMatches } = matchPattern;
    return excludeMatches && excludeMatches.length > 0 && isMatchUrl(url, excludeMatches) ? !1 : matches && matches.length > 0 && isMatchUrl(url, matches) ? !0 : excludeSelectorMatches && excludeSelectorMatches.length > 0 && isMatchSelectors(excludeSelectorMatches) ? !1 : !!(selectorMatches && selectorMatches.length > 0 && isMatchSelectors(selectorMatches));
  }
  function isMatchedExclude(url, matchPattern) {
    if (!matchPattern)
      return !1;
    let { excludeMatches, excludeSelectorMatches } = matchPattern;
    return !!(excludeMatches && excludeMatches.length > 0 && isMatchUrl(url, excludeMatches) || excludeSelectorMatches && excludeSelectorMatches.length > 0 && isMatchSelectors(excludeSelectorMatches));
  }
  function isNumber(value) {
    return typeof value == "number";
  }