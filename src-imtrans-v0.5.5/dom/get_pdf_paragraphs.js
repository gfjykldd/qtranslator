
  // dom/get_pdf_paragraphs.ts
  async function getParagraphs3(rootFrame, containers, ctx, targetContainers) {
    let allParagraphs = [], { rule } = ctx;
    for (let i3 = 0; i3 < containers.length; i3++) {
      let container = containers[i3], targetContainer = targetContainers[i3];
      if (!targetContainer)
        throw new Error("targetContainer is null");
      let inlineElementGroups = [], isFirstElementOfParagraph = !0, lastLineFirstElementInfo = null, pdfContainerFilter = function(node2) {
        let element = node2;
        if (isMatchTags(element.nodeName, ["DIV", "BR"]))
          return isFirstElementOfParagraph = !0, NodeFilter.FILTER_REJECT;
        if (element.classList.contains("markedContent"))
          return NodeFilter.FILTER_ACCEPT;
        if (isMarkedByParagraph2(
          node2
        ))
          return NodeFilter.FILTER_REJECT;
        if (setAttribute(element, sourceElementMarkAttributeName, "1"), isInlineElement(element, rule)) {
          let lastElement = getLastHTMLElement(inlineElementGroups), currentElementStyle = globalThis.getComputedStyle(element);
          if (!lastElement)
            inlineElementGroups.push(formatElement(element, currentElementStyle));
          else {
            let lastElementStyle = globalThis.getComputedStyle(lastElement), lastElementInfo = getElementInfoByComputedStyle(
              lastElementStyle
            ), currentElementInfo = getElementInfoByComputedStyle(
              currentElementStyle
            ), distanceInfo = getDistance(currentElementInfo, lastElementInfo), isNewParagraph = !1;
            if (isFirstElementOfParagraph && lastLineFirstElementInfo && getDistance(currentElementInfo, lastLineFirstElementInfo).left >= 1.5 && lastLineFirstElementInfo.left > -3 && (isNewParagraph = !0), !isNewParagraph && isFirstElementOfParagraph) {
              let trimedText = (element.innerText || element.textContent || "").trim();
              (trimedText.startsWith("\u2022") || trimedText.charCodeAt(0) === 61623 || /^\d+\./.test(trimedText)) && (isNewParagraph = !0);
            }
            !isNewParagraph && isFirstElementOfParagraph && getLastHTMLElement(inlineElementGroups) && getHTMLElements(inlineElementGroups).reduce(
              (max, element2) => {
                let elementInfo = getElementInfoByComputedStyle(
                  globalThis.getComputedStyle(element2)
                );
                return Math.max(max, elementInfo.right);
              },
              0
            ) - lastElementInfo.right > rule.pdfNewParagraphIndentRightIndentPx && (isNewParagraph = !0), isFirstElementOfParagraph && (lastLineFirstElementInfo = currentElementInfo, isFirstElementOfParagraph = !1), isNewParagraph || (isNewParagraph = getIsNewParagraph(distanceInfo, rule)), isNewParagraph && tryToAddToParagraph(
              inlineElementGroups,
              allParagraphs,
              rootFrame,
              ctx,
              targetContainer
            ), inlineElementGroups.push(formatElement(element, currentElementStyle)), inlineElementGroups.push(" ");
          }
          return NodeFilter.FILTER_REJECT;
        }
        return NodeFilter.FILTER_ACCEPT;
      }, elementIter = document.createTreeWalker(
        container,
        NodeFilter.SHOW_ELEMENT,
        pdfContainerFilter
      ), node = elementIter.nextNode();
      for (; node; )
        node = elementIter.nextNode();
      tryToAddToParagraph(
        inlineElementGroups,
        allParagraphs,
        rootFrame,
        ctx,
        targetContainer
      );
    }
    return allParagraphs;
  }
  function tryToAddToParagraph(inlineElementGroups, allParagraphs, rootFrame, ctx, targetContainer) {
    if (inlineElementGroups.length > 0) {
      let paragraph = elementsToParagraph(
        [...inlineElementGroups],
        !1,
        rootFrame,
        ctx
      );
      paragraph && (paragraph.isPdf = !0, paragraph.targetContainer = targetContainer, paragraph.inline = !1, addToParagraphs(paragraph, allParagraphs)), inlineElementGroups.length = 0;
    }
  }
  function getElementInfoByComputedStyle(style) {
    return {
      top: parseFloat(style.top.slice(0, -2)),
      left: parseFloat(style.left.slice(0, -2)),
      right: parseFloat(style.left.slice(0, -2)) + parseFloat(style.width.slice(0, -2)),
      fontSize: parseFloat(style.fontSize.slice(0, -2))
    };
  }
  function getIsNewParagraph(distance, rule) {
    return distance.fontSize > 2 || distance.fontSize < -2 || distance.top >= rule.pdfNewParagraphLineHeight || distance.top <= rule.pdfNewParagraphLineHeight * -1;
  }
  function getDistance(elementInfo1, elementInfo2) {
    let elementBasedFontSize = elementInfo2.fontSize, currentElementFontSize = elementInfo1.fontSize;
    return {
      top: (elementInfo1.top - elementInfo2.top) / elementBasedFontSize,
      left: (elementInfo1.left - elementInfo2.left) / elementBasedFontSize,
      fontSize: currentElementFontSize - elementBasedFontSize
    };
  }
  function formatElement(element, style) {
    return style.fontFamily === "monospace" ? {
      element,
      isStayOriginal: !0,
      targetTagName: "code"
    } : element;
  }
  function isMarkedByParagraph2(targetElement) {
    if (!isMarked(targetElement, sourceElementMarkAttributeName))
      return !1;
    if (isMarked(targetElement, sourceElementTranslatedMarkAttributeName))
      return !0;
    let markId = getAttribute(
      targetElement,
      sourceElementParagraphAttributeName
    );
    if (!markId)
      return !1;
    let paragraphId = parseInt(markId), isExist = getParagraphEntities().has(paragraphId);
    if (!isExist) {
      let maybeTargetElement = document.getElementById(
        `${translationTargetElementWrapperClass}-${paragraphId}`
      );
      maybeTargetElement && maybeTargetElement.remove();
    }
    return isExist;
  }