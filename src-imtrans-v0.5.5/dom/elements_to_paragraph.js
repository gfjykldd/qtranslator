
  // dom/elements_to_paragraph.ts
  function elementsToParagraph(elements, isPreWhitespace, rootFrame, ctx) {
    let { rule } = ctx, delimiters = getPlaceholderDelimiters(ctx);
    if (elements.length === 0)
      return null;
    elements = elements.map(
      (element) => element.element ? element : {
        element
      }
    );
    let isForceTranslate = elements.some(
      (element) => element.forceTranslate
    ), text = "", variables = [], currentVariableIndex = 0;
    elements && elements.length > 0 && elements[0].currentVariableIndex && (currentVariableIndex = elements[0].currentVariableIndex);
    let isHasMeaningfulText = isForceTranslate;
    for (let i3 = 0; i3 < elements.length; i3++) {
      let elementState = elements[i3], element = elementState.element;
      if (elementState.text) {
        text += elementState.text, elementState.variables && elementState.variables.length > 0 && variables.push(...elementState.variables), isHasMeaningfulText = !0;
        continue;
      }
      if (typeof element == "string") {
        text += element;
        continue;
      }
      let rawText = "";
      isContainsSelectors(
        element,
        `[${sourceElementExcludeAttributeNameForSelector}]`
      ) ? rawText = getTextWithExcludeElement(element) || "" : rawText = element.innerText || "";
      let isStartWithSpace = rawText.startsWith(" "), isEndWithSpace = rawText.endsWith(" ");
      element.tagName === "A" && (isStartWithSpace = !0, isEndWithSpace = !0);
      let isStayOriginal = isStayOriginalElement(element, rule);
      if (rawText === "" || isStayOriginal) {
        if (element && isMatchTags(element.nodeName, ["IMG"])) {
          let style = globalThis.getComputedStyle(element), width = parseInt(style.width, 10), height = parseInt(style.height, 10);
          if (width > 36 || height > 36)
            continue;
        }
        isStayOriginal && (isMatchTags(element.nodeName, ["SUP", "SUB"]) || (isStartWithSpace = !0, isEndWithSpace = !0));
        let variableElement = element;
        if (isStayOriginal && element.tagName === "IMG") {
          let clonedElement = element.cloneNode(!0), originalStyle = clonedElement.getAttribute("style") || "", rect = element.getBoundingClientRect();
          clonedElement.setAttribute(
            "style",
            `${originalStyle} width: ${rect.width}px; height: ${rect.height}px;`
          ), variableElement = clonedElement;
        }
        let variable = {
          type: "element",
          value: variableElement
        };
        variables.push(variable);
        let index = variables.length - 1 + currentVariableIndex, delimiter = `${delimiters[0]}${index}${delimiters[1]}`;
        text += (isStartWithSpace ? " " : "") + delimiter + (isEndWithSpace ? " " : "");
        continue;
      }
      if (!isExcludeElement(element, rule, !0)) {
        {
          let finalText = isPreWhitespace ? rawText : customTrim(rawText).replace(/\n/g, " ");
          if (isUrl(finalText) || isHashTag(finalText) || isAtTag(finalText) || isStockTag(finalText)) {
            let variable = {
              type: "element",
              value: element
            };
            variables.push(variable);
            let index = variables.length - 1, delimiter = `${delimiters[0]}${index}${delimiters[1]}`;
            text += (isStartWithSpace ? " " : "") + delimiter + (isEndWithSpace ? " " : "");
          } else
            isHasMeaningfulText = !0, text += (isStartWithSpace ? " " : "") + finalText + (isEndWithSpace ? " " : "");
        }
        if (typeof element != "string") {
          let whitespace = getWhitespace(
            element.nextSibling,
            isPreWhitespace
          );
          whitespace && (text += whitespace);
        }
      }
    }
    if (!isHasMeaningfulText)
      return null;
    let inline = !1, wordCount = text.split(" ").length, lineCount = text.split(`
`).length;
    wordCount <= rule.blockMinWordCount && text.length <= rule.blockMinTextCount && lineCount < 2 && (inline = !0);
    let finalElements = elements.map(
      (element) => element.element
    ), lastElement = getLastHTMLElement(elements), isVertical = !1;
    if (lastElement) {
      let writtingMode = globalThis.getComputedStyle(lastElement).writingMode;
      isVertical = writtingMode ? writtingMode.includes("vertical") : !1;
    }
    let paragraph = {
      rootFrame,
      isVertical,
      elements: finalElements,
      text,
      variables,
      inline,
      preWhitespace: isPreWhitespace
    };
    return isForceTranslate || isNeedToTranslate(
      paragraph,
      ctx.state.translationArea === "body" ? 2 : rule.paragraphMinTextCount,
      ctx.state.translationArea === "body" ? 1 : rule.paragraphMinWordCount,
      ctx
    ) ? paragraph : null;
  }
  function getTextWithExcludeElement(element) {
    let finalText = "", filterExcludeElement = (node) => node.nodeType === Node.ELEMENT_NODE ? isMarked(node, sourceElementExcludeAttributeName, !0) ? NodeFilter.FILTER_REJECT : NodeFilter.FILTER_ACCEPT : node.nodeType === Node.TEXT_NODE ? (node.textContent && node.textContent.trim() !== "" && (finalText += node.textContent.replace(/\s+/g, " ")), NodeFilter.FILTER_REJECT) : NodeFilter.FILTER_ACCEPT, treeWalker = document.createTreeWalker(
      element,
      NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_TEXT,
      filterExcludeElement
    );
    for (; treeWalker.nextNode(); )
      ;
    return finalText;
  }