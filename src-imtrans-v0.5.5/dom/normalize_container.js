
  // dom/normalize_container.ts
  function normalizeContainer(containers, rule) {
    let hiddenElements = [];
    for (let container of containers) {
      if (isMarked(container, sourceAtomicBlockElementMarkAttributeName))
        continue;
      if (setAttribute(container, targetContainerElementAttributeName, "1"), container.normalize(), rule.lineBreakMaxTextCount > 0) {
        let treeFilter = (node) => node.nodeType === Node.ELEMENT_NODE && isExcludeElement(node, rule, !0) ? NodeFilter.FILTER_REJECT : (node.nodeType === Node.TEXT_NODE && (node.textContent ? node.textContent.trim() : "").length >= rule.lineBreakMaxTextCount && addLineBreakToText(node, rule.lineBreakMaxTextCount), NodeFilter.FILTER_ACCEPT), walk = document.createTreeWalker(
          container,
          NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT,
          treeFilter
        );
        for (; walk.nextNode(); )
          ;
      }
      if (!(rule.excludeTags.includes("PRE") || rule.additionalExcludeTags.includes("PRE")) && rule.isTransformPreTagNewLine) {
        let preTags = container.querySelectorAll("pre");
        for (let preTag of preTags)
          formatPreHtml(preTag);
      }
      rule.preWhitespaceDetectedTags.includes(container.tagName) && (hasMark(container, sourcePreWhitespaceMarkAttributeName) || (isPreElementByStyle(container) ? (setAttribute(container, sourcePreWhitespaceMarkAttributeName, "1"), rule.isTransformPreTagNewLine && formatPreHtml(container)) : setAttribute(container, sourcePreWhitespaceMarkAttributeName, "0"))), wrapTextNode(
        0,
        container,
        rule,
        hiddenElements
      );
    }
    return {
      hiddenElements
    };
  }
  function wrapTextNode(depth, root2, rule, displayNoneElements = []) {
    if (isMarked(root2, sourceAtomicBlockElementMarkAttributeName))
      return;
    let isSingleInlineElement = !1;
    if (depth === 0) {
      let isStayOriginal = isStayOriginalElement(root2, rule);
      root2.childNodes && root2.childNodes.length === 1 && root2.nodeType === Node.ELEMENT_NODE && isInlineElement(
        root2,
        rule
      ) && !isStayOriginal && (isSingleInlineElement = !0);
    }
    if (root2 && root2.childNodes && root2.childNodes.length > 0) {
      for (let node of root2.childNodes)
        if (node.nodeType === Node.ELEMENT_NODE) {
          if (isExcludeElement(node, rule, !1))
            continue;
          {
            let computedStyle = globalThis.getComputedStyle(
              node
            ), tempIsExcludeElement = !1;
            if (computedStyle.display === "none" && (displayNoneElements.push(node), tempIsExcludeElement = !0), !tempIsExcludeElement) {
              let clip = computedStyle.clip, zIndex = computedStyle.zIndex, height = computedStyle.height, width = computedStyle.width, formatedZIndex = 0;
              if (zIndex.startsWith("-")) {
                let zIndexNumber = parseInt(zIndex);
                isNaN(zIndexNumber) || (formatedZIndex = zIndexNumber);
              }
              if (clip === "rect(1px, 1px, 1px, 1px)" && (tempIsExcludeElement = !0), formatedZIndex < rule.minZIndex && (tempIsExcludeElement = !0), !tempIsExcludeElement && node.nodeName !== "BR") {
                let heightNumber = parseInt(height), widthNumber = parseInt(width);
                !isNaN(heightNumber) && !isNaN(widthNumber) && (heightNumber > 0 && heightNumber < 8 || widthNumber > 0 && widthNumber <= 8) && (tempIsExcludeElement = !0);
              }
            }
            if (tempIsExcludeElement) {
              setAttribute(
                node,
                sourceElementExcludeAttributeName,
                "1",
                !0
              );
              continue;
            } else {
              if (isMatchTags(node.nodeName, ["DIV"])) {
                let nodeElement = node;
                if (node && nodeElement.style && nodeElement.style.display && nodeElement.style.display === "initial")
                  continue;
                if (computedStyle.display === "inline" && !isMarked(
                  node,
                  sourceBlockElementMarkAttributeName
                )) {
                  setAttribute(
                    node,
                    sourceInlineElementMarkAttributeName,
                    "1"
                  );
                  continue;
                }
              } else if (isMatchTags(node.nodeName, ["SPAN", "A"])) {
                if (!computedStyle.display.startsWith("inline")) {
                  isMarked(
                    node,
                    sourceInlineElementMarkAttributeName
                  ) || (setAttribute(
                    node,
                    sourceBlockElementMarkAttributeName,
                    "1"
                  ), wrapTextNode(
                    depth + 1,
                    node,
                    rule,
                    displayNoneElements
                  ));
                  continue;
                }
              } else if (isUnknowTag(node, rule))
                if (isInlineElement(
                  node,
                  rule
                )) {
                  if (!isMarked(
                    node,
                    sourceBlockElementMarkAttributeName
                  ) && !isMarked(
                    node,
                    sourceInlineElementMarkAttributeName
                  )) {
                    setAttribute(
                      node,
                      sourceInlineElementMarkAttributeName,
                      "1"
                    );
                    continue;
                  }
                } else {
                  !isMarked(
                    node,
                    sourceBlockElementMarkAttributeName
                  ) && !isMarked(
                    node,
                    sourceInlineElementMarkAttributeName
                  ) && setAttribute(
                    node,
                    sourceBlockElementMarkAttributeName,
                    "1"
                  ), wrapTextNode(
                    depth + 1,
                    node,
                    rule,
                    displayNoneElements
                  );
                  continue;
                }
              if (isInlineElement(node, rule) && !isSingleInlineElement)
                continue;
              wrapTextNode(
                depth + 1,
                node,
                rule,
                displayNoneElements
              );
            }
          }
        } else if (node.nodeType === Node.TEXT_NODE) {
          let text = node.textContent;
          if (text && text.trim().length > 0) {
            let span = document.createElement(rule.targetWrapperTag);
            node.after(span), span.appendChild(node);
          }
        }
    }
  }
  function isPreElementByStyle(element) {
    let style = window.getComputedStyle(element);
    return style.whiteSpace.startsWith("pre") || style.whiteSpace === "break-spaces";
  }
  function formatPreHtml(preElement) {
    let newHtml = preElement.innerHTML.replace(/\n/g, "<br />");
    preElement.innerHTML = newHtml;
  }
  function addLineBreakToText(textNode, maxLength) {
    let text = textNode.textContent || "";
    if (text.trim().length <= maxLength)
      return;
    let boundaryIndex = [". ", "? ", "! ", "\u3002", "\uFF1F", "\uFF01"].reduce((acc, boundary) => {
      let index = text.lastIndexOf(boundary, maxLength);
      return index > acc ? index : acc;
    }, -1);
    if (boundaryIndex > 1) {
      let prevChar = text[boundaryIndex - 1] || "", prevPrevChar = text[boundaryIndex - 2] || "", nextChar = text[boundaryIndex + 1] || "";
      prevChar === "." || nextChar === "." || nextChar === ")" || prevPrevChar === "." && prevChar === "S" || prevPrevChar.toUpperCase() === "M" && (prevChar.toUpperCase() === "R" || prevChar.toUpperCase() === "S") ? boundaryIndex = -1 : isNaN(Number(prevChar)) || (boundaryIndex = -1);
    }
    if (boundaryIndex === -1)
      text.length > maxLength + 20 && addLineBreakToText(textNode, maxLength + 20);
    else {
      let theText = text.slice(boundaryIndex + 1);
      boundaryIndex++, theText.startsWith(" ") && boundaryIndex++;
      let theLastTextNode = textNode.splitText(boundaryIndex), br = document.createElement("br");
      theLastTextNode.parentNode?.insertBefore(br, theLastTextNode), boundaryIndex + 1 < text.length && addLineBreakToText(theLastTextNode, maxLength);
    }
  }