
  // dom/mark_containers.ts
  function markContainers(container, rule, rootFrame, isDynamic) {
    let {
      excludeSelectors,
      additionalExcludeSelectors,
      extraInlineSelectors,
      additionalInlineSelectors,
      extraBlockSelectors,
      atomicBlockSelectors,
      atomicBlockTags,
      globalStyles,
      stayOriginalTags,
      stayOriginalSelectors,
      additionalStayOriginalSelectors,
      globalAttributes,
      additionalSelectors
    } = rule, globalStyleSelectors = Object.keys(globalStyles);
    if (globalStyleSelectors.length > 0)
      for (let selector of globalStyleSelectors) {
        let elements = getElementsBySelectors(
          isDynamic ? rootFrame : container,
          [selector]
        );
        for (let element of elements)
          if (!isMarked(element, sourceElementWithGlobalStyleMarkAttributeName)) {
            setAttribute(
              element,
              sourceElementWithGlobalStyleMarkAttributeName,
              "1"
            );
            let cssText = globalStyles[selector];
            element.style.cssText += cssText;
          }
      }
    let globalAttributesSelectors = Object.keys(globalAttributes);
    if (globalAttributesSelectors.length > 0)
      for (let selector of globalAttributesSelectors) {
        let attributes = globalAttributes[selector], attributesKeys = Object.keys(attributes), elements = getElementsBySelectors(
          isDynamic ? rootFrame : container,
          [selector]
        );
        for (let element of elements)
          for (let key of attributesKeys) {
            let value = attributes[key];
            element.getAttribute(key) !== value && (value === null ? element.removeAttribute(key) : element.setAttribute(key, value));
          }
      }
    let allExcludeSelectors = [
      ...excludeSelectors,
      ...additionalExcludeSelectors
    ], allInlineSelectors = [
      ...extraInlineSelectors,
      ...additionalInlineSelectors
    ], allAtomicBlockSelectors = [...atomicBlockSelectors], allAtomicBlockTagsSelectors = atomicBlockTags.map(
      (item) => item.toLowerCase()
    ), allBlockSelectos = extraBlockSelectors;
    getElementsBySelectors(
      isDynamic ? rootFrame : container,
      additionalSelectors
    ).forEach((element) => {
      isMarked(element, specifiedTargetContainerElementAttributeName) || setAttribute(
        element,
        specifiedTargetContainerElementAttributeName,
        "1"
      );
    }), getElementsBySelectors(
      isDynamic ? rootFrame : container,
      allExcludeSelectors
    ).forEach((element) => {
      isMarked(element, sourceElementExcludeAttributeName, !0) || setAttribute(element, sourceElementExcludeAttributeName, "1", !0);
    });
    let atomicBlockElements = [];
    if (allAtomicBlockSelectors.length > 0 && (atomicBlockElements = getElementsBySelectors(
      isDynamic ? rootFrame : container,
      allAtomicBlockSelectors
    ).filter((element) => !isMarked(element, sourceAtomicBlockElementMarkAttributeName))), allAtomicBlockTagsSelectors.length > 0) {
      let stayOriginalTagsHTMLStringArr = stayOriginalTags.reduce(
        (arr, item) => {
          let tagLower = item.toLowerCase();
          return arr.push(`<${tagLower}>`, `</${tagLower}>`, `<${tagLower} />`), arr;
        },
        []
      ), httpLinkTags = [">http://", ">https://"];
      stayOriginalTagsHTMLStringArr.push(...httpLinkTags);
      let atomicBlockTagsElements = getElementsBySelectors(
        isDynamic ? rootFrame : container,
        allAtomicBlockTagsSelectors
      ).filter((element) => {
        if (isMarked(
          element,
          sourceAtomicBlockElementMarkAttributeName
        ))
          return !1;
        {
          let htmlString = element.innerHTML;
          return !stayOriginalTagsHTMLStringArr.some(
            (item) => htmlString.includes(item)
          );
        }
      });
      atomicBlockElements.push(...atomicBlockTagsElements);
    }
    atomicBlockElements.forEach((element) => {
      isMarked(element, sourceAtomicBlockElementMarkAttributeName) || setAttribute(element, sourceAtomicBlockElementMarkAttributeName, "1");
    });
    let extraInlineElements = [];
    allInlineSelectors.length > 0 && extraInlineElements.push(
      ...getElementsBySelectors(
        isDynamic ? rootFrame : container,
        allInlineSelectors
      )
    ), extraInlineElements.forEach((element) => {
      setAttribute(element, sourceInlineElementMarkAttributeName, "1");
    });
    let extraBlockElements = [];
    allBlockSelectos.length > 0 && extraBlockElements.push(
      ...getElementsBySelectors(
        isDynamic ? rootFrame : container,
        allBlockSelectos
      )
    ), extraBlockElements.forEach((element) => {
      setAttribute(element, sourceBlockElementMarkAttributeName, "1");
    });
    let stayOriginalElements = [], allStayOriginalSelectors = [
      ...stayOriginalSelectors,
      ...additionalStayOriginalSelectors
    ];
    allStayOriginalSelectors.length > 0 && stayOriginalElements.push(
      ...getElementsBySelectors(
        isDynamic ? rootFrame : container,
        allStayOriginalSelectors
      )
    ), stayOriginalElements.forEach((element) => {
      setAttribute(element, sourceElementStayOriginalAttributeName, "1");
    });
  }