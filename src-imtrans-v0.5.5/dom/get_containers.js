
  // dom/get_containers.ts
  function getContainers(root2, ctx) {
    if (!(root2 && root2.textContent && root2.textContent.trim()))
      return [];
    let { rule, state: { translationArea } } = ctx, contentContainers = [];
    if (translationArea === "body")
      return [root2];
    let pairsKeys = Object.keys(rule.pairs);
    if (rule && rule.selectors.length > 0) {
      let containers = rule.selectors.map((selector) => {
        let isMatch = root2.matches(selector), elements = [];
        isMatch ? elements = [root2] : elements = root2.querySelectorAll(
          selector
        );
        for (let element of elements)
          isMarked(element, specifiedTargetContainerElementAttributeName) || setAttribute(
            element,
            specifiedTargetContainerElementAttributeName,
            "1"
          );
        return Array.from(elements);
      }).flat();
      contentContainers.push(
        ...containers.map((container) => ({
          element: container,
          reserve: !0
        }))
      );
    } else {
      if (rule && rule.additionalSelectors.length > 0) {
        let additionalElements = getElementsBySelectors(
          root2,
          rule.additionalSelectors
        );
        for (let element of additionalElements)
          isMarked(element, specifiedTargetContainerElementAttributeName) || setAttribute(
            element,
            specifiedTargetContainerElementAttributeName,
            "1"
          );
        contentContainers.push(
          ...additionalElements.map((element) => ({
            element,
            reserve: !0
          }))
        );
      }
      let articleElements = getElementsBySelectors(root2, ["article"]);
      contentContainers.push(
        ...articleElements.map((element) => ({
          element,
          reserve: !0
        }))
      );
      let mains;
      if (contentContainers.length === 0 && (mains = root2.querySelectorAll("[role=main]"), mains.length === 0 && (mains = root2.querySelectorAll("main")), mains.length === 0 && (mains = root2.querySelectorAll(".main")), mains.length > 0)) {
        let mainsArray = Array.from(mains);
        contentContainers = contentContainers.concat(
          mainsArray.map((main3) => ({
            element: main3,
            reserve: !0
          }))
        );
      }
      let detectedContainers = [], treeFilter = (node) => {
        if (node.nodeType === Node.ELEMENT_NODE && isExcludeElement(node, ctx.rule, !0))
          return NodeFilter.FILTER_REJECT;
        if (node.nodeType === Node.TEXT_NODE) {
          let trimedText = node.textContent ? node.textContent.trim() : "";
          if (isValidTextByCount(
            trimedText,
            ctx.rule.containerMinTextCount,
            ctx.rule.containerMinWordCount
          )) {
            let parentNode = node.parentNode;
            parentNode && parentNode.parentNode && (parentNode = parentNode.parentNode), parentNode && parentNode.nodeType === Node.ELEMENT_NODE && (detectedContainers.includes(parentNode) || detectedContainers.push(parentNode));
          }
        }
        return NodeFilter.FILTER_ACCEPT;
      }, walk = document.createTreeWalker(
        root2,
        NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT,
        treeFilter
      );
      for (; walk.nextNode(); )
        ;
      contentContainers.push(
        ...detectedContainers.map((element) => ({
          element,
          reserve: !1
        }))
      );
    }
    let finalContainers = duplicatedElements(root2, contentContainers, rule);
    return finalContainers.sort(function(a6, b5) {
      return a6.compareDocumentPosition(b5) & Node.DOCUMENT_POSITION_FOLLOWING ? -1 : 1;
    }), finalContainers;
  }
  function getFirstParentBlockElement(element, rule) {
    let parent = element.parentNode;
    if (parent.nodeName === "BODY" || isShadowElement(parent))
      return element;
    for (; parent && parent.nodeName !== "BODY" && !isShadowElement(parent) && isInlineElement(parent, rule); ) {
      let tempParent = parent.parentNode;
      if (tempParent && isShadowElement(tempParent))
        break;
      parent = tempParent;
    }
    return parent;
  }
  function isShadowElement(element) {
    return !!(element.host && element.mode);
  }
  function getBlockParentNode(startNode, rule) {
    return startNode.nodeType === Node.TEXT_NODE || isInlineElement(startNode, rule) ? getFirstParentBlockElement(startNode, rule) : startNode;
  }