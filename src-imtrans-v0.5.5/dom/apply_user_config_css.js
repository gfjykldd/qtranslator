
  // dom/apply_user_config_css.ts
  function applyUserConfigCss(root2, translationTheme, translationThemePatternConfig, remove2) {
    let theme = translationTheme, props = themeOptions[theme] || [], injectedCss = "";
    root2 && props && props.length > 0 && props.forEach((prop) => {
      let { name } = prop;
      if (root2 && translationThemePatternConfig && translationThemePatternConfig[name]) {
        let value = translationThemePatternConfig[name];
        if (["borderRadius"].includes(name) && (value += "px"), injectedCss += `--immersive-translate-theme-${theme}-${name}: ${value};
`, translationTheme === "marker" && name === "backgroundColor" && typeof value == "string") {
          let { r: r2, g: g7, b: b5 } = hexToRgb(value);
          injectedCss += `--immersive-translate-theme-${theme}-${name}-rgb: ${r2}, ${g7}, ${b5};
`;
        }
      }
    });
    let otherCss = "";
    if (translationThemePatternConfig && translationThemePatternConfig.textColor) {
      let value = translationThemePatternConfig.textColor;
      otherCss += `
.immersive-translate-target-translation-theme-${theme}-inner{color: ${value};}
`;
    }
    if (translationThemePatternConfig && translationThemePatternConfig.zoom) {
      let value = translationThemePatternConfig.zoom;
      otherCss += `
.immersive-translate-target-translation-theme-${theme}-inner{font-size: max(13px, ${value}%);}
`;
    }
    let finalCss = "";
    injectedCss && (finalCss = `:root {
${injectedCss}}
`), otherCss && (finalCss += otherCss), finalCss ? injectCSS(
      root2,
      finalCss,
      "immersive-translate-user-custom-style"
    ) : remove2 && injectCSS(
      root2,
      "",
      "immersive-translate-user-custom-style"
    );
  }