
  // report.ts
  var measurement_id = "G-MKMD9LWFTR";
  async function report(key, events, ctx) {
    try {
      let env3 = getEnv(), isUserscript = isMonkey(), isInIframe = getIsInIframe(), isProd3 = env3.PROD === "1", reportKey = `report_${key}`, isDaily = key.endsWith("_daily");
      if (isDaily) {
        if (isInIframe)
          return;
        let lastReportTime = await get(reportKey, 0), lastReportDay = getBeijingDay(new Date(lastReportTime)), now = Date.now(), nowDay2 = getBeijingDay(new Date(now));
        if (lastReportDay === nowDay2)
          return;
        await set(reportKey, now);
      } else if (!ctx.config.telemetry)
        return;
      let api_secret = "sitc4WmvShWYwfU0dANM3Q", userId = await get("fakeUserId", ""), version = getVersion(), nowDate = /* @__PURE__ */ new Date(), installedAt = await get(
        "installedAt",
        ""
      );
      userId ? installedAt || (installedAt = (/* @__PURE__ */ new Date(0)).toISOString(), await set("installedAt", installedAt)) : (userId = makeid(32), await set("fakeUserId", userId)), installedAt || (installedAt = nowDate.toISOString(), await set("installedAt", installedAt));
      let installAtDate = new Date(installedAt), installDay = getBeijingDay(installAtDate), nowDay = getBeijingDay(nowDate), isNewUser = installDay === nowDay, isNewUserThisWeek = nowDate.getTime() - installAtDate.getTime() < 7 * 24 * 60 * 60 * 1e3, url = `https://www.google-analytics.com/mp/collect?measurement_id=${measurement_id}&api_secret=${api_secret}`;
      isProd3 || (url = `https://www.google-analytics.com/debug/mp/collect?measurement_id=${measurement_id}&api_secret=${api_secret}`);
      let theBrowserInfo = U7.parse(window.navigator.userAgent), formatedEvents = events.map((event) => {
        let currentParam = event.params || {};
        theBrowserInfo.os && (currentParam.os_name = theBrowserInfo.os.name || "unknown", currentParam.os_version = theBrowserInfo.os.version || "unknown", currentParam.os_version_name = theBrowserInfo.os.versionName || "unknown"), theBrowserInfo.browser && (currentParam.browser_name = theBrowserInfo.browser.name || "unknown", currentParam.browser_version = theBrowserInfo.browser.version || "unknown"), theBrowserInfo.platform && (currentParam.platform_type = theBrowserInfo.platform.type || "unknown"), theBrowserInfo.engine && (currentParam.engine_name = theBrowserInfo.engine.name || "unknown", currentParam.engine_version = theBrowserInfo.engine.version || "unknown"), ctx.translationService && (currentParam.translation_service = ctx.translationService), ctx.targetLanguage && (currentParam.target_language = ctx.targetLanguage), ctx.config.interfaceLanguage && (currentParam.interface_language = ctx.config.interfaceLanguage), version && (currentParam.version = version), ctx.config.translationTheme && (currentParam.translation_theme = ctx.config.translationTheme), ctx.config.alpha && (currentParam.alpha = ctx.config.alpha.toString()), ctx.config.translationArea && (currentParam.translation_area = ctx.config.translationArea), currentParam.userscript = isUserscript.toString(), isNewUser ? currentParam.is_new_user_today = "1" : currentParam.is_new_user_today = "0", isNewUserThisWeek ? currentParam.is_new_user_this_week = "1" : currentParam.is_new_user_this_week = "0";
        let pageType = "html";
        if (ctx.rule.isEbook ? pageType = "ebookReader" : ctx.rule.isPdf ? pageType = "pdfReader" : ctx.rule.isEbookBuilder ? pageType = "ebookBuilder" : ctx.rule.isSubtitleBuilder && (pageType = "subtitleBuilder"), currentParam.page_type = pageType, isInIframe ? currentParam.main_frame = 0 : currentParam.main_frame = 1, !isDaily) {
          let siteUrl = ctx.url;
          try {
            let urlObj = new URL(siteUrl);
            currentParam.site_host = urlObj.hostname;
          } catch {
            currentParam.site_host = "unknown";
          }
          ctx.sourceLanguage && (currentParam.source_language = ctx.sourceLanguage);
        }
        return {
          ...event,
          params: currentParam
        };
      }), _response = await request2(
        {
          responseType: "text",
          url,
          method: "POST",
          body: JSON.stringify({
            client_id: userId,
            user_id: userId,
            events: formatedEvents
          })
        }
      );
    } catch {
    }
  }
  function makeid(length) {
    let result = "", characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", charactersLength = characters.length, counter = 0;
    for (; counter < length; )
      result += characters.charAt(Math.floor(Math.random() * charactersLength)), counter += 1;
    return result;
  }
  function getBeijingDay(date) {
    let beijingDay = date.toLocaleString("en-US", {
      timeZone: "Asia/Shanghai"
    }).split(" ")[0];
    return beijingDay.endsWith(",") && (beijingDay = beijingDay.slice(0, -1)), beijingDay;
  }
