
  // content_main.ts
  ready_state_default.domready.then(async () => {
    let config = await getConfig(), realUrl = getRealUrl(), ctx = await getContext({
      config,
      url: realUrl
    });
    if (ctx.isTranslateExcludeUrl && isWebOptionsPage2())
      log_default.debug("detect web options page"), setupDomListenersForAll(ctx), setupWebOptionsPage();
    else {
      if (!ctx.config.enabled || isMatchUrl(ctx.url, ctx.config.blockUrls))
        return;
      setupDomListenersForAll(ctx), waitForDomElementReady(ctx).then(() => {
        main2(ctx).catch((e) => {
          e && log_default.error(
            "translate page error",
            e.name,
            e.message,
            e.details || "",
            e
          );
        });
      }).catch((e) => {
        log_default.debug("can not detect a valid body: ", e);
      });
    }
  }).catch((e) => {
    e && log_default.error(
      "translate dom ready detect error",
      e
    );
  });