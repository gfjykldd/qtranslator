
  // env.ts
  function getEnv() {
    return typeof process > "u" && typeof Deno < "u" ? Deno.env.toObject() : define_process_env_default;
  }
  var env = getEnv();
  function isMonkey() {
    return env.IMMERSIVE_TRANSLATE_USERSCRIPT === "1";
  }
  function isInCSP() {
    return env.HAS_CSP_ERROR == "1";
  }
  function isSafari() {
    if (env.IMMERSIVE_TRANSLATE_SAFARI === "1")
      return !0;
    if (
      // @ts-ignore: it's ok
      typeof globalThis.immersiveTranslateBrowserAPI < "u" && // @ts-ignore: it's ok
      globalThis.immersiveTranslateBrowserAPI.runtime && // @ts-ignore: it's ok
      globalThis.immersiveTranslateBrowserAPI.runtime.getManifest
    ) {
      let manifest = globalThis.immersiveTranslateBrowserAPI.runtime.getManifest();
      return !!(manifest && manifest._isSafari);
    } else
      return !1;
  }
  function isDeno() {
    return typeof Deno < "u";
  }
  function isWebOptionsPage() {
    return (
      // @ts-ignore: ok
      typeof globalThis.__IS_IMMERSIVE_TRANSLATE_WEB_OPTIONS_PAGE__ < "u"
    );
  }