
  // userscript/popup_app.tsx
  function PopupApp(props) {
    let { onClose } = props, [pageStatus2, setPageStatus] = P5("Original"), [settings, setSettings, _isPersistent, _error] = useUserConfig(), [config, setConfig] = P5(null), [currentUrl, setCurrentUrl] = P5(
      globalThis.location.href
    ), [currentLang, setCurrentLang] = P5("auto"), [ctx, setContext] = P5(null), onToggleTranslate = (event) => {
      setPageStatus(event.detail);
    }, onSetPageLanguage = (lang) => {
      setCurrentLang(lang);
      let newSourceLanguageUrlPattern = handleSourceLanguageUrlPattern(
        currentUrl,
        lang,
        config.sourceLanguageUrlPattern
      );
      setSettings((state) => ({
        ...state,
        sourceLanguageUrlPattern: newSourceLanguageUrlPattern
      })), setCurrentPageLanguageByClient(lang);
    }, onUrlChange = () => {
      setCurrentUrl(globalThis.location.href);
    };
    j5(() => (document.addEventListener(
      pageTranslatedStatusEventName,
      onToggleTranslate,
      !1
    ), getConfig2().then((result) => {
      setConfig(result);
      let currentLanguage = getCurrentPageLanguage();
      setCurrentLang(currentLanguage);
      let currentPageStatus = getPageStatus();
      setPageStatus(currentPageStatus), runCron(result.interval);
    }), document.addEventListener("urlChange", onUrlChange), () => {
      document.removeEventListener("pageTranslatedStatus", onToggleTranslate), document.removeEventListener("urlChange", onUrlChange);
    }), []), j5(() => {
      getConfig2().then((config2) => {
        setConfig(config2);
      });
    }, [settings]), j5(() => {
      currentUrl && config && getContext({
        url: currentUrl,
        config
      }).then((ctx2) => {
        setContext(ctx2);
      });
    }, [currentUrl, config]);
    let handleSendMessageToContent = (method, isClose) => () => {
      sendMessageToContent({
        method
      }), isClose && onClose();
    }, handleClose = () => {
      onClose();
    }, handleToggleEnabled = () => {
      setSettings((state) => ({
        ...state,
        enabled: !state.enabled
      })), setTimeout(() => {
        handleClose();
      }, 50);
    }, handleTranslatePdf = () => {
      openPdfViewerPage(), setTimeout(() => {
        handleClose();
      }, 50);
    }, handleTranslateLocalPdfFile = () => {
      openPdfViewerPage(), setTimeout(() => {
        handleClose();
      }, 50);
    }, handleOpenOptionsPage = (pageRoute = "") => {
      openOptionsPage2(!0, pageRoute), setTimeout(() => {
        onClose();
      }, 50);
    }, handleOpenAboutPage = () => {
      openAboutPage2(), setTimeout(() => {
        onClose();
      }, 50);
    }, handleOpenEbookViewerPage = () => {
      openEbookViewerPage2(), setTimeout(() => {
        onClose();
      }, 50);
    }, handleOpenEbookBuilderPage = () => {
      openEbookBuilderPage2(), setTimeout(() => {
        onClose();
      }, 50);
    }, handleOpenSubtitleBuilderPage = () => {
      openSubtitleBuilderPage2(), setTimeout(() => {
        onClose();
      }, 50);
    }, handleMouseTranslateTriggerConfig = (trigger) => {
      setSettings((state) => ({
        ...state,
        generalRule: {
          ...state.generalRule,
          mouseHoverHoldKey: trigger
        }
      }));
    };
    return !config || !ctx ? null : /* @__PURE__ */ p5(
      Popup,
      {
        openEbookViewerPage: handleOpenEbookViewerPage,
        openEbookBuilderPage: handleOpenEbookBuilderPage,
        onTranslateLocalHtmlFile: () => {
        },
        onTranslateLocalSubtitleFile: handleOpenSubtitleBuilderPage,
        request: request2,
        onClose: handleClose,
        onToggleEnabled: handleToggleEnabled,
        onTranslateTheWholePage: handleSendMessageToContent(
          "translateTheWholePage",
          !0
        ),
        openOptionsPage: handleOpenOptionsPage,
        onToggleTranslate: handleSendMessageToContent(
          "toggleTranslatePage",
          !0
        ),
        onTranslateTheMainPage: handleSendMessageToContent(
          "translateTheMainPage",
          !0
        ),
        ontranslateToThePageEndImmediately: handleSendMessageToContent(
          "translateToThePageEndImmediately",
          !0
        ),
        onTranslatePage: handleSendMessageToContent("translatePage", !0),
        onRestorePage: handleSendMessageToContent("restorePage", !1),
        onTranslatePdf: handleTranslatePdf,
        openAboutPage: handleOpenAboutPage,
        onTranslateLocalPdfFile: handleTranslateLocalPdfFile,
        onSetPageLanguage,
        onUserConfigChange: setSettings,
        config,
        pageStatus: pageStatus2,
        ctx,
        currentUrl,
        currentLang,
        onSetLocalConfig: setLocalConfig2,
        onSetBuildinConfig: setBuildinConfig2,
        onMouseTriggerChanged: handleMouseTranslateTriggerConfig
      }
    );
  }