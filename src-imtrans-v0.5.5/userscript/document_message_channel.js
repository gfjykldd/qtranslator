
  // userscript/document_message_channel.ts
  var messageHandlers = /* @__PURE__ */ new Map();
  function ask(request3) {
    let id = makeid(64), event = new CustomEvent(documentMessageTypeIdentifierForAsk, {
      detail: JSON.stringify({
        ...request3,
        type: "ask",
        id
      })
    });
    return document.dispatchEvent(event), new Promise((resolve, reject) => {
      messageHandlers.set(id, (e, data) => {
        e ? reject(e) : resolve(data);
      });
    });
  }
  function makeid(length) {
    let result = "", characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", charactersLength = characters.length, counter = 0;
    for (; counter < length; )
      result += characters.charAt(Math.floor(Math.random() * charactersLength)), counter += 1;
    return result;
  }