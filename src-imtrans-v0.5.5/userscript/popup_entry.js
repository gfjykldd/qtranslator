
  // userscript/popup_entry.tsx
  function addCSSLegacy(root2, csses) {
    for (let css of csses) {
      if (isMonkey() && !isFirefox() && typeof GM !== void 0 && GM.addElement) {
        GM.addElement(root2, "style", {
          textContent: css
        });
        continue;
      }
      root2.appendChild(document.createElement("style")).textContent = css;
    }
  }
  var originalPagePopupConfig = {
    position: "right",
    right: 0,
    top: 335
  }, currentPagePopupConfig = {
    ...originalPagePopupConfig
  }, positionChanged = !1, rootRef = null, btnRef = null, mountPointRef = null, shadowRef = null, timer = null, localConfig = null, delta = 6, startX, startY, lastBtnStyle = null, lastRootStyle = null;
  async function initPopup() {
    let env3 = getEnv();
    localConfig = await getLocalConfig2(), currentPagePopupConfig = localConfig.pagePopupConfig || currentPagePopupConfig;
    let popup = document.createElement("div");
    popup.id = "immersive-translate-popup", popup.setAttribute("style", "all: initial"), document.documentElement.appendChild(popup);
    let shadow = popup.attachShadow({ mode: "open" });
    shadowRef = shadow;
    let cssStr = [
      env3.IMMERSIVE_TRANSLATE_PICO_CSS,
      env3.IMMERSIVE_TRANSLATE_COMMON_CSS,
      env3.IMMERSIVE_TRANSLATE_POPUP_CSS,
      env3.IMMERSIVE_TRANSLATE_PAGE_POPUP_CSS
    ].join(`
`);
    addCSSLegacy(shadow, [cssStr]);
    let mountRoot = document.createElement("div");
    mountRoot.innerHTML = env3.IMMERSIVE_TRANSLATE_POPUP_HTML, shadow.appendChild(mountRoot), rootRef = shadow.querySelector(
      "#immersive-translate-popup-container"
    );
    let btn = shadow.querySelector(
      "#immersive-translate-popup-btn"
    );
    btnRef = btn, mountPointRef = shadow.querySelector("#mount"), rootRef.setAttribute(
      "style",
      objToStyle(currentPagePopupConfig)
    ), setBtnTransform(), btn.addEventListener("mousedown", onMouseDown), btn.addEventListener("touchstart", onTouchStart), globalThis.addEventListener("resize", (_e3) => {
      rootRef.setAttribute(
        "style",
        objToStyle(currentPagePopupConfig)
      );
    });
  }
  function showButton() {
    re(null, mountPointRef), mountPointRef.style.display = "none", btnRef.style.display = "block", timer = setTimeout(() => {
      setBtnTransform(!0);
    }, 2e3);
  }
  function renderPopup(shadow) {
    let mountPoint = shadow.querySelector("#mount"), handleOnClose = () => {
      showButton();
    }, handleClickOverLay = (e) => {
      e && e.target && e.target.id === "immersive-translate-popup-overlay" && handleOnClose();
    };
    (async () => {
      let config = await getConfig2();
      re(
        /* @__PURE__ */ p5(
          TranslateProvider,
          {
            lang: config.interfaceLanguage,
            fallbackLang: "zh-CN",
            translations: locales_default,
            children: /* @__PURE__ */ p5(
              "div",
              {
                onClick: handleClickOverLay,
                id: "immersive-translate-popup-overlay",
                class: "immersive-translate-popup-overlay",
                children: /* @__PURE__ */ p5(
                  "div",
                  {
                    class: "immersive-translate-popup-wrapper",
                    style: calculateMountPointPosition(),
                    children: /* @__PURE__ */ p5(PopupApp, { onClose: handleOnClose })
                  }
                )
              }
            )
          }
        ),
        mountPoint
      );
    })().then(() => {
      btnRef.style.display = "none", mountPointRef.style.display = "block";
    });
  }
  function calculateMountPointPosition() {
    let screenSize = getScreenSize(), windowHeight = screenSize.height, { position, top, left } = currentPagePopupConfig, style = {
      position: "fixed"
    }, popupHeight = 300, popupWidth = 300, offset = 100;
    return isInCSP() && (position = originalPagePopupConfig.position, top = originalPagePopupConfig.top), position === "right" || position === "left" ? (style.top = top - offset, style.top + popupHeight >= windowHeight ? (style.bottom = 30, delete style.top) : style.top <= 10 && (style.top = 10), position === "right" ? style.right = 0 : position === "left" && (style.left = 0)) : (position === "top" || position === "bottom") && (style.left = left - offset, style.left + popupWidth >= screenSize.width ? (style.right = 0, delete style.left) : style.left <= 10 && (style.left = 0), position === "top" ? style.top = 0 : position === "bottom" && (style.bottom = 0)), style;
  }
  function getScreenSize() {
    return {
      width: Math.max(
        document.documentElement.clientWidth,
        window.innerWidth || 0
      ),
      height: Math.max(
        document.documentElement.clientHeight,
        window.innerHeight || 0
      )
    };
  }
  function onMouseDown(e) {
    lastRootStyle = rootRef.getAttribute("style"), startX = e.pageX, startY = e.pageY, timer && clearTimeout(timer), positionChanged = !1, btnRef.style.opacity = "1", btnRef.style.transform = "none", lastBtnStyle = btnRef.getAttribute("style"), globalThis.addEventListener("mousemove", onMouseMove), globalThis.addEventListener("mouseup", onMouseUp), globalThis.addEventListener("touchmove", onTouchMove), globalThis.addEventListener("touchend", onTouchEnd), globalThis.addEventListener("touchcancel", onTouchEnd);
  }
  function onTouchStart(e) {
    e.preventDefault && e.preventDefault(), onMouseDown(e.changedTouches[0]);
  }
  function onTouchMove(e) {
    onMouseMove(e.changedTouches[0]);
  }
  function onTouchEnd(e) {
    e.preventDefault && e.preventDefault(), onMouseUp(e.changedTouches[0]);
  }
  function onMouseMove(e) {
    e.preventDefault && e.preventDefault(), positionChanged = !0, rootRef.setAttribute(
      "style",
      `left:${e.clientX}px;top:${e.clientY}px;transform:scale(1.6);`
    );
  }
  function onMouseUp(e) {
    e.preventDefault && e.preventDefault(), removeListeners(), timer && clearTimeout(timer), startX = startX || 0, startY = startY || 0;
    let diffX = Math.abs(e.pageX - startX), diffY = Math.abs(e.pageY - startY);
    diffX < delta && diffY < delta ? (rootRef.setAttribute("style", lastRootStyle), btnRef.setAttribute("style", lastBtnStyle), renderPopup(shadowRef)) : positionChanged ? snapToSide(e) : renderPopup(shadowRef), positionChanged = !1;
  }
  function snapToSide(e) {
    let screenSize = getScreenSize(), left = e.clientX, top = e.clientY, toTop = top, toBottom = screenSize.height - top, toLeft = left, toRight = screenSize.width - left;
    toTop < toBottom && toTop < toLeft && toTop < toRight ? currentPagePopupConfig = {
      position: "top",
      left,
      top: 0
    } : toBottom < toTop && toBottom < toLeft && toBottom < toRight ? currentPagePopupConfig = {
      position: "bottom",
      bottom: 0,
      left: e.clientX
    } : toLeft < toTop && toLeft < toBottom && toLeft < toRight ? currentPagePopupConfig = {
      position: "left",
      left: 0,
      top: e.clientY
    } : toRight < toTop && toRight < toBottom && toRight < toLeft && (currentPagePopupConfig = {
      position: "right",
      right: 0,
      top: e.clientY
    });
    let finalStyle = objToStyle(currentPagePopupConfig);
    rootRef.setAttribute(
      "style",
      finalStyle
    ), setLocalConfig2({
      ...localConfig,
      pagePopupConfig: currentPagePopupConfig
    }), timer = setTimeout(() => {
      setBtnTransform(!0);
    }, 2e3);
  }
  function setBtnTransform(transition = !1) {
    btnRef.style.opacity = "0.4";
    let transform = "";
    currentPagePopupConfig.position === "left" ? transform = "translateX(-40%)" : currentPagePopupConfig.position === "right" ? transform = "translateX(40%)" : currentPagePopupConfig.position === "top" ? transform = "translateY(-40%)" : currentPagePopupConfig.position === "bottom" && (transform = "translateY(40%)"), btnRef.style.transform = transform, transition && (btnRef.style.transition = "transform 0.2s ease-in-out, opacity 0.2s ease-in-out");
  }
  function removeListeners() {
    globalThis.removeEventListener("mousemove", onMouseMove), globalThis.removeEventListener("mouseup", onMouseUp), globalThis.removeEventListener("touchmove", onTouchMove), globalThis.removeEventListener("touchend", onTouchEnd), globalThis.removeEventListener("touchcancel", onTouchEnd);
  }
  function objToStyle(rawObj) {
    let obj = getValidStyleObj(rawObj);
    return Object.keys(obj).map((key) => typeof obj[key] == "number" ? `${key}:${obj[key]}px;` : "").join("");
  }
  function getValidStyleObj(positionConfig) {
    let { position, ...rest } = positionConfig, screenSize = getScreenSize(), styleObj = {};
    return position === "left" ? (styleObj.left = 0, rest.top > screenSize.height ? styleObj.top = screenSize.height - 100 : styleObj.top = rest.top) : position === "right" ? (styleObj.right = 0, rest.top > screenSize.height ? styleObj.top = screenSize.height - 100 : styleObj.top = rest.top) : position === "top" ? (styleObj.top = 0, rest.left > screenSize.width ? styleObj.left = screenSize.width - 100 : styleObj.left = rest.left) : position === "bottom" && (styleObj.bottom = 0, rest.left > screenSize.width ? styleObj.left = screenSize.width - 100 : styleObj.left = rest.left), styleObj;
  }