
  // hooks/use_user_config.ts
  var SETTINGS_KEY = "userConfig", INITIAL_VALUE = {}, rawUseUserConfig = createChromeStorageStateHookSync(
    SETTINGS_KEY,
    INITIAL_VALUE
  );
  function useUserConfig() {
    let [value, setValue, isPersistent, error2] = rawUseUserConfig(), formatSetValue = L2((newValue) => {
      let toStore = typeof newValue == "function" ? newValue(value) : newValue;
      toStore && (toStore.updatedAt = (/* @__PURE__ */ new Date()).toISOString()), setValue(toStore);
    }, [value]);
    return [value, formatSetValue, isPersistent, error2, setValue];
  }