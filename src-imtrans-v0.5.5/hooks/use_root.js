
  // hooks/use_route.ts
  function useRoute() {
    let [route, setRoute] = P2(location.hash);
    return j2(() => {
      let updateRoute = () => setRoute(location.hash);
      return globalThis.addEventListener("hashchange", updateRoute, !1), () => {
        globalThis.removeEventListener("hashchange", updateRoute);
      };
    }, []), route;
  }
