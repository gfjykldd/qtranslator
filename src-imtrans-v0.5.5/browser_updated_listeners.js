
  // browser_updated_listeners.ts
  function onUpdated() {
    tryToCreateContextMenu();
  }
  function tryToCreateContextMenu() {
    getConfig().then((config) => {
      createContextMenu(config);
    }).catch((e) => {
      log_default.error("create menu error", e);
    });
  }
  function setupOnInstalledListener() {
    browserAPI.runtime.onInstalled.addListener((details) => {
      log_default.debug(`onInstalled reason: ${details.reason}`), log_default.debug(details), details.reason == "install" ? (get("hasRun", !1).then(
        (hasRun) => {
          hasRun || (browserAPI.tabs.create({
            url: "https://immersive-translate.owenyoung.com/start/"
          }), set("hasRun", !0).catch((e) => {
            log_default.error("set hasRun error", e);
          }));
        }
      ), onUpdated()) : (details.reason == "update" && browserAPI.runtime.getManifest().version != details.previousVersion, onUpdated());
    });
  }