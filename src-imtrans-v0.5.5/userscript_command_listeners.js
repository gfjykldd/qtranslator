
  // userscript_command_listeners.ts
  function setupCommandListeners(config) {
    let shortcuts = config.shortcuts || {}, keyMap = Object.keys(shortcuts).reduce((acc, key) => (acc[shortcuts[key]] = key, acc), {}), shortcutsKeys = Object.keys(keyMap);
    for (let key of shortcutsKeys) {
      let realKey = key.replace(/MacCtrl/ig, "Ctrl");
      realKey && v3(realKey, (e) => {
        e.preventDefault(), sendMessageToContent({
          method: keyMap[key]
        });
      });
    }
  }