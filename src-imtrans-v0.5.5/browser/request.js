
  // browser/request.ts
  async function request(options2) {
    let response;
    if (options2 && options2.retry && options2.retry > 0)
      try {
        response = await retry(rawRequest.bind(null, options2), {
          multiplier: 2,
          maxAttempts: options2.retry
        });
      } catch (e) {
        throw e && e.name === "RetryError" && e.cause ? e.cause : e;
      }
    else
      response = await rawRequest(options2);
    return response;
  }
  async function rawRequest(options2) {
    options2.body;
    let { url, responseType, ...fetchOptions } = options2;
    responseType || (responseType = "json"), fetchOptions = {
      mode: "cors",
      ...fetchOptions
    };
    let isNativeFetch = !0;
    options2.fetchPolyfill && (isNativeFetch = !1);
    let fetchFn = options2.fetchPolyfill || fetch, timeout = 12e4;
    if (options2.timeout && (timeout = options2.timeout), isNativeFetch) {
      let controller2 = new AbortController(), signal2 = controller2.signal;
      setTimeout(() => {
        controller2.abort();
      }, timeout), fetchOptions.signal = signal2;
    }
    let response = await fetchFn(url, fetchOptions);
    if (response.ok && response.status >= 200 && response.status < 400) {
      if (responseType === "json")
        return await response.json();
      if (responseType === "text")
        return await response.text();
      if (responseType === "raw") {
        let data = await response.text(), responseHeaders = Object.fromEntries([
          ...response.headers.entries()
        ]), finalUrl = response.url;
        return finalUrl || (response.headers.get("X-Final-URL") ? finalUrl = response.headers.get("X-Final-URL") : finalUrl = url), {
          body: data,
          headers: responseHeaders,
          status: response.status,
          statusText: response.statusText,
          url: finalUrl
        };
      } else if (responseType === "stream") {
        let buffer = "", answer;
        if (response.body && response.body instanceof ReadableStream)
          for await (let chunk of streamAsyncIterable(response.body)) {
            let str = new TextDecoder().decode(chunk);
            buffer += str;
            let lineEndIndex;
            for (; (lineEndIndex = buffer.indexOf(`
`)) >= 0; ) {
              let line = buffer.slice(0, lineEndIndex).trim();
              if (buffer = buffer.slice(lineEndIndex + 1), line.startsWith("event:") || line === "")
                continue;
              let eventData = "";
              if (line.startsWith("data:") && (eventData = line.slice(5).trim()), eventData === "[DONE]")
                break;
              let data;
              try {
                data = JSON.parse(eventData ?? "");
              } catch (error) {
                log_default.debug("json error", error);
                continue;
              }
              answer = data;
            }
          }
        return answer;
      }
    } else {
      let details;
      try {
        details = await response.text();
      } catch (_e3) {
        log_default.error("parse response failed", _e3);
      }
      details && log_default.error("fail response", details);
      let shortDetail = "";
      throw details && (shortDetail = details.slice(0, 150)), new CommonError(
        "fetchError",
        response.status + ": " + (response.statusText || "") + shortDetail,
        details
      );
    }
  }
  async function* streamAsyncIterable(stream) {
    let reader = stream.getReader();
    try {
      for (; ; ) {
        let { done, value } = await reader.read();
        if (done)
          return;
        yield value;
      }
    } finally {
      reader.releaseLock();
    }
  }