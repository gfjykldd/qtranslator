
  // log.ts
  var theConsole = console, Timing = class {
    #t = performance.now();
    reset() {
      this.#t = performance.now();
    }
    stop(message) {
      let now = performance.now(), d7 = Math.round(now - this.#t), cf = colors_exports.green;
      d7 > 1e4 ? cf = colors_exports.red : d7 > 1e3 && (cf = colors_exports.yellow), theConsole.debug(
        colors_exports.dim(brandName + " TIMING:"),
        message,
        "in",
        cf(d7 + "ms")
      ), this.#t = now;
    }
  }, Logger = class {
    #level = 1 /* Info */;
    get level() {
      return this.#level;
    }
    setLevel(level) {
      switch (level) {
        case "debug":
          this.#level = 0 /* Debug */;
          break;
        case "info":
          this.#level = 1 /* Info */;
          break;
        case "warn":
          this.#level = 2 /* Warn */;
          break;
        case "error":
          this.#level = 3 /* Error */;
          break;
        case "fatal":
          this.#level = 4 /* Fatal */;
          break;
      }
    }
    debug(...args) {
      this.#level <= 0 /* Debug */ && theConsole.log(colors_exports.dim(brandName + " DEBUG:"), ...args);
    }
    v(...args) {
      this.#level <= 0 /* Debug */;
    }
    info(...args) {
      this.#level <= 1 /* Info */ && theConsole.log(colors_exports.green(brandName + " INFO:"), ...args);
    }
    l(...args) {
      this.#level <= 1 /* Info */;
    }
    warn(...args) {
      this.#level <= 2 /* Warn */ && theConsole.warn(colors_exports.yellow(brandName + " WARN:"), ...args);
    }
    error(...args) {
      this.#level <= 3 /* Error */ && theConsole.error(colors_exports.red(brandName + " ERROR:"), ...args);
    }
    fatal(...args) {
      this.#level <= 4 /* Fatal */ && theConsole.error(colors_exports.red(brandName + " FATAL:"), ...args);
    }
    timing() {
      return this.level === 0 /* Debug */ ? new Timing() : { reset: () => {
      }, stop: () => {
      } };
    }
  }, log_default = new Logger();