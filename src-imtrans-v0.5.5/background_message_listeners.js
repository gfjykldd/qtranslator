
  // background_message_listeners.ts
  var connection, messageHandler = async function(payload, author) {
    let { method, data } = payload;
    if (log_default.debug("background received message", method, data || " "), method === "mock")
      await delay(150);
    else {
      if (method === "queryParagraphCache")
        return queryParagraphCache(data);
      if (method === "setParagraphCache")
        return setParagraphCache(data);
      if (method === "calculateSize")
        return calculateSize();
      if (method === "fetch")
        return request(data);
      if (method === "getConfig")
        return getConfig();
      if (method === "getLocalConfig")
        return getLocalConfig();
      if (method === "openOptionsPage")
        if (isSafari()) {
          let optionsUrl = getEnv().OPTIONS_URL;
          browserAPI.tabs.create({
            url: optionsUrl
          });
        } else {
          let optionsUrl = browserAPI.runtime.getURL("options.html"), pageRoute = data && data.pageRoute ? data.pageRoute : "";
          pageRoute = pageRoute || "", browserAPI.tabs.create({
            url: optionsUrl + pageRoute
          });
        }
      else if (method === "openAboutPage")
        if (isSafari()) {
          let optionsUrl = getEnv().OPTIONS_URL;
          browserAPI.tabs.create({
            url: optionsUrl + "#about"
          });
        } else
          browserAPI.tabs.create({
            url: browserAPI.runtime.getURL("options.html#about")
          });
      else if (method === "openEbookViewerPage")
        browserAPI.tabs.create({
          url: browserAPI.runtime.getURL("ebook/index.html")
        });
      else if (method === "openSubtitleBuilderPage") {
        let url = browserAPI.runtime.getURL("ebook/subtitle/index.html");
        isSafari() && (url = getEnv().SUBTITLE_BUILDER_URL), browserAPI.tabs.create({
          url
        });
      } else if (method === "openEbookBuilderPage") {
        let url = browserAPI.runtime.getURL("ebook/make/index.html");
        isSafari() && (url = getEnv().EBOOK_BUILDER_URL), browserAPI.tabs.create({
          url
        });
      } else if (method === "openPdfViewerPage") {
        let url = browserAPI.runtime.getURL("pdf/index.html");
        browserAPI.tabs.create({
          url
        });
      } else {
        if (method === "setLocalConfig")
          return setLocalConfig(data);
        if (method === "detectLanguage") {
          let { text, minLength } = data;
          if (!minLength && minLength !== 0 && (minLength = 50), text.length <= minLength)
            return "auto";
          if (browserAPI.i18n && browserAPI.i18n.detectLanguage)
            try {
              let result = await browserAPI.i18n.detectLanguage(
                text
              );
              return result.languages.length > 0 ? formatLanguage(result.languages[0].language) : "auto";
            } catch (e) {
              return log_default.debug("detect language error", e), "auto";
            }
          else
            return "auto";
        } else if (method === "detectTabLanguage")
          try {
            let lang = await browserAPI.tabs.detectLanguage(
              author.id
            );
            return formatLanguage(lang);
          } catch (e) {
            return log_default.debug("detect tab language error, use auto ", e), "auto";
          }
        else if (method === "autoSyncLatestConfig") {
          try {
            await autoSyncLatestConfig();
          } catch (e) {
            log_default.debug("auto sync latest config error", e);
          }
          return "";
        }
      }
    }
  };
  function steupMessageListeners() {
    getConnection();
    let manifest = browserAPI.runtime.getManifest();
    if (manifest.manifest_version > 2, manifest.manifest_version === 2 && browserAPI.webRequest && browserAPI.webRequest.onBeforeSendHeaders) {
      let urlsFilter = request_modifier_rule_default.map(
        (item) => item.condition.urlFilter
      ), types = request_modifier_rule_default.reduce((acc, item) => (item.condition.resourceTypes.forEach((type) => {
        acc.includes(type) || acc.push(type);
      }), acc), []);
      browserAPI.webRequest.onBeforeSendHeaders.addListener(
        function(details) {
          if (!(details.originUrl && details.originUrl.startsWith("http")) && details.originUrl && details.requestHeaders)
            for (let i = 0; i < urlsFilter.length; i++) {
              let rule = request_modifier_rule_default[i];
              if (rule.condition.urlFilter && isMatchUrl(details.url, rule.condition.urlFilter))
                return { requestHeaders: formatHeadersByRule(
                  details.requestHeaders,
                  rule.action.requestHeaders
                ) };
            }
        },
        // @ts-ignore: it's ok
        { urls: urlsFilter, types },
        ["blocking", "requestHeaders"]
      );
    }
  }
  function getConnection() {
    return connection || (connection = new Messager("background", !1).getConnection("main", messageHandler), connection);
  }