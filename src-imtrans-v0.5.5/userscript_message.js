
  // userscript_message.ts
  function sendMessageToContent(request3) {
    asyncMessageHandler(request3, {
      // @ts-ignore: it's ok
      tab: {
        id: 1,
        url: "https://www.fake.com",
        active: !0
      }
    }).catch((e) => {
      log_default.error("send content message request failed", request3, e);
    }), document.querySelectorAll("iframe").forEach((iframe) => {
      iframe.contentWindow && iframe.contentWindow.postMessage(
        {
          author: iframeMessageIdentifier,
          payload: request3
        },
        "*"
      );
    });
    let event = new CustomEvent(userscriptCommandEventName, {
      detail: request3
    });
    globalThis.document.dispatchEvent(event);
  }