
  // config.ts
  function getEnvUserConfig() {
    if (env.PROD === "1")
      return {};
    let defaultUserConfig = {};
    if (env.IMMERSIVE_TRANSLATE_SECRET_TENCENT_SECRET_ID && env.IMMERSIVE_TRANSLATE_SECRET_TENCENT_SECRET_KEY) {
      let tencentAuthConfig = {
        secretId: env.IMMERSIVE_TRANSLATE_SECRET_TENCENT_SECRET_ID,
        secretKey: env.IMMERSIVE_TRANSLATE_SECRET_TENCENT_SECRET_KEY
      };
      defaultUserConfig.translationServices = {}, defaultUserConfig.translationServices.tencent = tencentAuthConfig;
    }
    if (env.IMMERSIVE_TRANSLATE_SECRET_BAIDU_APPID && env.IMMERSIVE_TRANSLATE_SECRET_BAIDU_KEY) {
      let baiduAuthConfig = {
        appid: env.IMMERSIVE_TRANSLATE_SECRET_BAIDU_APPID,
        key: env.IMMERSIVE_TRANSLATE_SECRET_BAIDU_KEY
      };
      defaultUserConfig.translationServices || (defaultUserConfig.translationServices = {}), defaultUserConfig.translationServices.baidu = baiduAuthConfig;
    }
    if (env.IMMERSIVE_TRANSLATE_SECRET_CAIYUN_TOKEN) {
      let caiyunAuthConfig = {
        token: env.IMMERSIVE_TRANSLATE_SECRET_CAIYUN_TOKEN
      };
      defaultUserConfig.translationServices || (defaultUserConfig.translationServices = {}), defaultUserConfig.translationServices.caiyun = caiyunAuthConfig;
    }
    if (env.IMMERSIVE_TRANSLATE_SECRET_OPENL_APIKEY) {
      let openlAuthConfig = {
        apikey: env.IMMERSIVE_TRANSLATE_SECRET_OPENL_APIKEY
      };
      defaultUserConfig.translationServices || (defaultUserConfig.translationServices = {}), defaultUserConfig.translationServices.openl = openlAuthConfig;
    }
    if (env.IMMERSIVE_TRANSLATE_SECRET_YOUDAO_APP_ID && env.IMMERSIVE_TRANSLATE_SECRET_YOUDAO_APP_SECRET) {
      let youdaoAuthConfig = {
        appId: env.IMMERSIVE_TRANSLATE_SECRET_YOUDAO_APP_ID,
        appSecret: env.IMMERSIVE_TRANSLATE_SECRET_YOUDAO_APP_SECRET
      };
      defaultUserConfig.translationServices || (defaultUserConfig.translationServices = {}), defaultUserConfig.translationServices.youdao = youdaoAuthConfig;
    }
    if (env.IMMERSIVE_TRANSLATE_SECRET_VOLC_ACCESS_KEY_ID && env.IMMERSIVE_TRANSLATE_SECRET_VOLC_SECRET_ACCESS_KEY) {
      let volcAuthConfig = {
        accessKeyId: env.IMMERSIVE_TRANSLATE_SECRET_VOLC_ACCESS_KEY_ID,
        secretAccessKey: env.IMMERSIVE_TRANSLATE_SECRET_VOLC_SECRET_ACCESS_KEY
      };
      defaultUserConfig.translationServices || (defaultUserConfig.translationServices = {}), defaultUserConfig.translationServices.volc = volcAuthConfig;
    }
    if (env.IMMERSIVE_TRANSLATE_SECRET_DEEPL_AUTH_KEY) {
      let deeplAuthConfig = {
        authKey: env.IMMERSIVE_TRANSLATE_SECRET_DEEPL_AUTH_KEY
      };
      defaultUserConfig.translationServices || (defaultUserConfig.translationServices = {}), defaultUserConfig.translationServices.deepl = deeplAuthConfig;
    }
    if (env.DEEPL_PROXY_ENDPOINT && (defaultUserConfig.translationServices || (defaultUserConfig.translationServices = {}), defaultUserConfig.translationServices.deepl || (defaultUserConfig.translationServices.deepl = {}), defaultUserConfig.translationServices.deepl.immersiveTranslateApiUrl = env.DEEPL_PROXY_ENDPOINT), env.IMMERSIVE_TRANSLATE_DEEPL_ENDPOINT && (defaultUserConfig.translationServices || (defaultUserConfig.translationServices = {}), defaultUserConfig.translationServices.deepl || (defaultUserConfig.translationServices.deepl = {}), defaultUserConfig.translationServices.deepl.immersiveTranslateDeeplTokenUrl = env.IMMERSIVE_TRANSLATE_DEEPL_ENDPOINT), env.IMMERSIVE_TRANSLATE_SECRET_OPENAI_API_KEY) {
      let openaiAuthConfig = {
        APIKEY: env.IMMERSIVE_TRANSLATE_SECRET_OPENAI_API_KEY
      };
      defaultUserConfig.translationServices || (defaultUserConfig.translationServices = {}), defaultUserConfig.translationServices.openai = openaiAuthConfig;
    }
    return env.DEBUG === "1" && (defaultUserConfig.debug = !0, defaultUserConfig.cache = !1, defaultUserConfig.alpha = !0), env.MOCK === "1" && (defaultUserConfig.translationService = "mock"), env.IMMERSIVE_TRANSLATE_SERVICE && (defaultUserConfig.translationService = env.IMMERSIVE_TRANSLATE_SERVICE), defaultUserConfig;
  }
  async function getLocalConfig() {
    let localConfig = await browserAPI.storage.local.get(localConfigStorageKey);
    if (localConfig[localConfigStorageKey]) {
      let currentConfig = localConfig[localConfigStorageKey], currentTempTranslationDomains = currentConfig.tempTranslationUrlMatches || [], newDomains = currentTempTranslationDomains.filter(
        (item) => item.expiredAt > Date.now()
      ), isChanged = !1;
      newDomains.length !== currentTempTranslationDomains.length && (currentTempTranslationDomains = newDomains, isChanged = !0);
      let newLocalConfig = {
        ...currentConfig,
        tempTranslationUrlMatches: [
          ...currentTempTranslationDomains
        ]
      };
      return isChanged && await setLocalConfig(newLocalConfig), newLocalConfig;
    } else
      return {};
  }
  async function setLocalConfig(localConfig) {
    await browserAPI.storage.local.set({ [localConfigStorageKey]: localConfig });
  }
  async function setBuildinConfig(buildinConfig) {
    await browserAPI.storage.local.set({ [buildinConfigStorageKey]: buildinConfig });
  }
  async function getLatestBuildinConfig() {
    let storageBuildInConfig = await browserAPI.storage.local.get(
      buildinConfigStorageKey
    ), finalBuildInConfig = {
      ...getBuildInConfig(),
      ...buildin_config_default,
      buildinConfigUpdatedAt: env.BUILD_TIME
    };
    if (storageBuildInConfig[buildinConfigStorageKey]) {
      let storageBuildInConfigValue = storageBuildInConfig[buildinConfigStorageKey];
      if (storageBuildInConfigValue && storageBuildInConfigValue.buildinConfigUpdatedAt) {
        let storageBuildinConfigUpdatedAt = new Date(
          storageBuildInConfigValue.buildinConfigUpdatedAt
        ), buildinConfigUpdatedAt = new Date(
          finalBuildInConfig.buildinConfigUpdatedAt
        );
        storageBuildinConfigUpdatedAt > buildinConfigUpdatedAt && (finalBuildInConfig = storageBuildInConfigValue);
      }
    }
    return finalBuildInConfig;
  }
  async function getConfig() {
    let storageBuildInConfig = await browserAPI.storage.local.get(
      buildinConfigStorageKey
    ), finalBuildInConfig = {
      ...buildin_config_default,
      buildinConfigUpdatedAt: env.BUILD_TIME
    };
    if (storageBuildInConfig[buildinConfigStorageKey]) {
      let storageBuildInConfigValue = storageBuildInConfig[buildinConfigStorageKey];
      if (storageBuildInConfigValue && storageBuildInConfigValue.buildinConfigUpdatedAt) {
        let storageBuildinConfigUpdatedAt = new Date(
          storageBuildInConfigValue.buildinConfigUpdatedAt
        ), buildinConfigUpdatedAt = new Date(
          finalBuildInConfig.buildinConfigUpdatedAt
        );
        storageBuildinConfigUpdatedAt > buildinConfigUpdatedAt && (finalBuildInConfig = storageBuildInConfigValue);
      }
    }
    let shortcutsFromBrowser = {};
    if (!isMonkey() && browserAPI.commands && browserAPI.commands.getAll) {
      let commandResult = await browserAPI.commands.getAll();
      for (let command of commandResult)
        command.name && command.shortcut && (shortcutsFromBrowser[command.name] = command.shortcut);
    }
    let defaultConfig = getBuildInConfig(), envUserConfig = getEnvUserConfig(), userConfig = await getUserConfig(), globalUserConfig = globalThis.IMMERSIVE_TRANSLATE_CONFIG || {}, localConfig = await getLocalConfig(), now = /* @__PURE__ */ new Date();
    if (localConfig && localConfig.tempTranslationUrlMatches && localConfig.tempTranslationUrlMatches.length > 0) {
      let validUrlMatches = localConfig.tempTranslationUrlMatches.filter(
        (urlMatch) => new Date(urlMatch.expiredAt) > now
      );
      if (validUrlMatches.length > 0) {
        let currentMatches = userConfig.translationUrlPattern ? userConfig.translationUrlPattern?.matches || [] : [], currentMatchesArray = Array.isArray(currentMatches) ? currentMatches : [currentMatches], finalMatches = Array.from(
          new Set(
            currentMatchesArray.concat(
              validUrlMatches.map((urlMatch) => urlMatch.match)
            )
          )
        );
        userConfig.translationUrlPattern = {
          ...userConfig.translationUrlPattern,
          matches: finalMatches
        };
      }
    }
    let mergedUserConfig = Object.assign(
      {},
      globalUserConfig,
      envUserConfig,
      userConfig
    );
    if (!mergedUserConfig.interfaceLanguage) {
      let defaultInterfaceLanguage = await getBrowserIntefaceLanguage();
      mergedUserConfig.interfaceLanguage = defaultInterfaceLanguage;
    }
    let finalConfig = Object.assign(defaultConfig, finalBuildInConfig), configKeys = Object.keys(finalConfig), assignKeys = [
      "translationUrlPattern",
      "translationLanguagePattern",
      "immediateTranslationPattern",
      "translationBodyAreaPattern",
      "translationParagraphLanguagePattern",
      "translationThemePatterns",
      "translationGeneralConfig",
      "shortcuts"
    ];
    for (let key of configKeys) {
      let configKey = key;
      if (configKey === "generalRule")
        typeof mergedUserConfig[configKey] == "object" && (finalConfig[configKey] = mergeRule(
          defaultConfig[configKey],
          mergedUserConfig[configKey]
        ));
      else if (configKey === "translationServices") {
        let userConfigValue = mergedUserConfig[configKey] || {}, buildInConfigValue = finalBuildInConfig[configKey] || {}, buildInConfigKeys = Object.keys(buildInConfigValue), userConfigKeys = Object.keys(userConfigValue), allUniqueKeys = [
          .../* @__PURE__ */ new Set([...buildInConfigKeys, ...userConfigKeys])
        ], finalConfigValue = {};
        for (let key2 of allUniqueKeys)
          finalConfigValue[key2] = {
            // @ts-ignore: it's ok
            ...buildInConfigValue[key2],
            ...userConfigValue[key2]
          };
        finalConfig[configKey] = finalConfigValue;
      } else if (typeof mergedUserConfig[configKey] != "string" && typeof mergedUserConfig[configKey] != "boolean" && typeof mergedUserConfig[configKey] != "number" && assignKeys.includes(configKey))
        mergedUserConfig[configKey] && (finalConfig[configKey] = Object.assign(
          // @ts-ignore: ignore type error
          finalConfig[configKey],
          mergedUserConfig[configKey]
        )), configKey === "shortcuts" && (isMonkey() || isSafari() ? finalConfig[configKey] = {
          ...finalConfig[configKey],
          ...shortcutsFromBrowser
        } : finalConfig[configKey] = {
          ...shortcutsFromBrowser
        });
      else if (configKey === "rules") {
        if (Array.isArray(mergedUserConfig[configKey]) && (finalConfig[configKey] = [
          ...mergedUserConfig[configKey],
          ...finalConfig[configKey]
        ]), env.PROD === "0" && env.DEV_RULES) {
          let devRules = JSON.parse(env.DEV_RULES);
          finalConfig[configKey] = [
            ...devRules,
            ...finalConfig[configKey]
          ];
        }
      } else
        mergedUserConfig[configKey] !== void 0 && (finalConfig[configKey] = mergedUserConfig[configKey]);
    }
    return finalConfig.donateUrl = finalBuildInConfig.donateUrl, finalConfig.minVersion = finalBuildInConfig.minVersion, finalConfig.feedbackUrl = finalBuildInConfig.feedbackUrl, finalConfig;
  }
  async function getUserConfig() {
    return (await browserAPI.storage.sync.get("userConfig") || {}).userConfig || {};
  }
  var getBrowserIntefaceLanguage = async () => {
    let languages2 = ["zh-CN"];
    try {
      languages2 = await browserAPI.i18n.getAcceptLanguages();
    } catch (e) {
      log_default.warn("get browser language error:", e);
    }
    let defaultInterfaceLanguage = languages2.map((lang) => formatLanguage(lang)).find((lang) => translations[lang]);
    return defaultInterfaceLanguage || "en";
  }, getBuildInConfig = () => {
    let finalBuildInConfig = {
      ...buildin_config_default,
      buildinConfigUpdatedAt: env.BUILD_TIME
    };
    return {
      ...finalBuildInConfig,
      targetLanguage: fallbackLanguage,
      interfaceLanguage: "en",
      debug: !1,
      alpha: !1,
      translationUrlPattern: {
        matches: [],
        excludeMatches: []
      },
      translationLanguagePattern: {
        matches: [],
        excludeMatches: []
      },
      translationThemePatterns: {},
      translationParagraphLanguagePattern: {
        matches: [],
        excludeMatches: [],
        selectorMatches: [],
        excludeSelectorMatches: []
      },
      translationBodyAreaPattern: {
        matches: [],
        excludeMatches: [],
        selectorMatches: [],
        excludeSelectorMatches: []
      },
      translationTheme: "none",
      translationService: "bing",
      translationArea: "main",
      translationStartMode: "dynamic",
      translationServices: {},
      generalRule: {
        ...finalBuildInConfig.generalRule
      },
      translationGeneralConfig: { engine: "bing" },
      rules: []
    };
  };