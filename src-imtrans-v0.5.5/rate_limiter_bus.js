
  // rate_limiter_bus.ts
  var defaultLimiter = new RateLimiter({ limit: 7, interval: 1250 }), limiterMap = {
    tencent: new RateLimiter({ limit: 3, interval: 1350 }),
    baidu: new RateLimiter({ limit: 1, interval: 1550 }),
    strict: new RateLimiter({ limit: 1, interval: 1050 }),
    d: new RateLimiter({ limit: 1, interval: 1050 }),
    youdao: new RateLimiter({ limit: 5, interval: 1050 }),
    you: new RateLimiter({ limit: 5, interval: 1050 }),
    cai: new RateLimiter({ limit: 5, interval: 1050 }),
    google: new RateLimiter({ limit: 10, interval: 1050 }),
    deepl: new RateLimiter({ limit: 10, interval: 1050 }),
    transmart: new RateLimiter({ limit: 30, interval: 1050 }),
    papago: new RateLimiter({ limit: 3, interval: 1150 }),
    openai: new RateLimiter({ limit: 5, interval: 1300 }),
    chatgpt: new RateLimiter({ limit: 1, interval: 1350 })
  };
  function getLimiter(key) {
    return limiterMap[key] || defaultLimiter;
  }
  async function getRateLimiterDelay(key) {
    if (getIsInIframe()) {
      let channel = getIframeMessageChannel();
      if (channel)
        try {
          return (await channel.ask("getRateLimitDelay", {
            key
          })).value;
        } catch (e) {
          return log_default.error("can not comunicate with root frame, use strict limiter", e), getLimiter("strict").getDelay();
        }
      else
        return getLimiter("strict").getDelay();
    } else
      return getLimiter(key).getDelay();
  }
  async function setRateLimiter(key, options2) {
    if (!getIsInIframe()) {
      let limiter = getLimiter(key);
      options2 && limiter.setOptions(options2);
    }
  }
  async function onRateLimiterDelayRequest(body) {
    let key = body.key;
    return { value: getLimiter(key).getDelay() };
  }