
  // popup.tsx
  var mountPoint = document.getElementById("mount");
  setupMessageListeners();
  mountPoint && (async () => {
    let config = await getConfig();
    config.debug && log_default.setLevel("debug"), re(
      /* @__PURE__ */ p3(
        TranslateProvider,
        {
          lang: config.interfaceLanguage,
          translations: locales_default,
          fallbackLang: "zh-CN",
          children: /* @__PURE__ */ p3(Popup2, {})
        }
      ),
      mountPoint
    );
  })();
