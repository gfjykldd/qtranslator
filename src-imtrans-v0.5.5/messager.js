
  // messager.ts
  var listeners2 = /* @__PURE__ */ new Map(), Messager = class {
    constructor(fromType, debug = !1) {
      this.logger = new Logger(), debug && this.logger.setLevel("debug"), this.fromType = fromType, listeners2.has(fromType) || (listeners2.set(fromType, /* @__PURE__ */ new Map()), browserAPI.runtime.onMessage.addListener(
        // @ts-ignore: it's ok
        (message, sender, sendResponse) => {
          let from = message.from, to = message.to, tabId, tabUrl, tabActive;
          sender.tab && sender.tab.id && (tabId = sender.tab.id, from = `${from}:${tabId}`, tabUrl = sender.tab.url, tabActive = sender.tab.active), this.logger.debug(
            `${message.to} received message [${message.payload.method}] from ${message.from}`,
            message.payload.data ? message.payload.data : " "
          );
          let parsedTo = parseType(to), { type: toType, name: toName } = parsedTo;
          if (toType !== fromType)
            return !1;
          let parsedMessageFrom = parseType(from), connectionInfo = listeners2.get(toType).get(toName);
          if (!connectionInfo)
            return this.logger.debug(
              `no message handler for ${toType}:${to}, but it's ok`
            ), !1;
          let { messageHandler, sync } = connectionInfo, messageAuthor = {
            type: fromType,
            name: parsedMessageFrom.name,
            id: tabId,
            url: tabUrl,
            active: tabActive
          };
          if (sync) {
            try {
              let handlerResult = messageHandler(
                message.payload,
                messageAuthor
              );
              sendResponse({
                ok: !0,
                data: handlerResult
              });
            } catch (e) {
              sendResponse({
                ok: !1,
                errorName: e.name,
                errorMessage: e.message,
                errorDetails: e.details
              });
            }
            return !1;
          } else
            return messageHandler(
              message.payload,
              messageAuthor
            ).then((data) => {
              sendResponse({
                ok: !0,
                data
              });
            }).catch(
              (e) => {
                sendResponse({
                  ok: !1,
                  errorName: e.name,
                  errorMessage: e.message,
                  errorDetails: e.details
                });
              }
            ), !0;
        }
      ));
    }
    getConnection(name, messageHandler, options2) {
      let sync = !1;
      options2 && options2.sync && (sync = !0);
      let fromType = this.fromType, currentListeners = listeners2.get(fromType);
      if (currentListeners.has(name))
        return currentListeners.get(name).connectionInstance;
      {
        let connection2 = new Connection(`${fromType}:${name}`, this.logger);
        return listeners2.get(fromType).set(name, {
          messageHandler,
          sync,
          connectionInstance: connection2
        }), connection2;
      }
    }
  }, Connection = class {
    constructor(from, logger) {
      this.from = from, this.logger = logger;
    }
    async sendMessage(to, messagePayload) {
      let parsedTo = parseType(to), { type, id } = parsedTo;
      if (type !== "content_script") {
        let message = {
          to,
          from: this.from,
          payload: messagePayload
        };
        this.logger.debug(
          `${message.from} send message [${message.payload.method}] to ${message.to}`,
          message.payload.data ? message.payload.data : " "
        );
        try {
          let response = await browserAPI.runtime.sendMessage(message);
          return handleResponse(message, response, this.logger);
        } catch (e) {
          if (type === "popup") {
            let errorMessage = `popup ${to} is not active, so the message does not send, ignore this error, ${JSON.stringify(messagePayload)}`;
            return this.logger.debug(
              errorMessage,
              messagePayload,
              to,
              e
            ), Promise.resolve({
              message: errorMessage
            });
          } else
            throw e;
        }
      } else {
        let message = {
          from: this.from,
          to,
          payload: messagePayload
        };
        this.logger.debug(
          `${message.from} send message [${message.payload.method}] to ${message.to}`,
          message.payload.data ? message.payload.data : " "
        );
        let response = await browserAPI.tabs.sendMessage(id, message);
        return handleResponse(message, response, this.logger);
      }
    }
  };
  function handleResponse(message, response, logger) {
    if (response) {
      if (response.ok)
        return logger.debug(
          `${message.from} received response from ${message.to}:`,
          response.data ? response.data : " "
        ), response.data;
      throw new CommonError(
        response.errorName || "UnknownError",
        response.errorMessage || "Unknown error",
        response.errorDetails
      );
    } else
      throw new CommonError(
        "noResponse",
        "Unknown error"
      );
  }
  function parseType(str) {
    let parts = str.split(":");
    if (parts.length < 2)
      throw new Error("not a valid to string");
    let messageTo = {
      type: parts[0],
      name: parts[1]
    };
    if (parts[0] === "content_script") {
      let tabId = parseInt(parts[2]);
      if (!isNaN(tabId))
        messageTo.id = tabId;
      else
        throw new Error("tab id not a valid number");
    }
    return messageTo;
  }
  
  // content_message_listeners.ts
  var asyncMessageHandler = async function(payload, _author) {
    let { method, data } = payload;
    method === "translateTheWholePage" ? await translateTheWholePage() : method === "translateTheMainPage" ? await translateTheMainPage() : method === "translateToThePageEndImmediately" ? await translateToThePageEndImmediately() : method === "toggleTranslatePage" ? await toggleTranslatePage() : method === "toggleTranslateTheWholePage" ? await toggleTranslateTheWholePage() : method === "toggleTranslateTheMainPage" ? await toggleTranslateTheMainPage() : method === "translatePage" ? await translatePage() : method === "toggleTranslationMask" ? await toggleTranslationMask() : method === "restorePage" ? restorePage() : method === "showTranslationOnly" ? void 0 : method === "setCurrentPageLanguageByClient" ? setCurrentPageLanguageByClient(data) : method === "retryFailedParagraphs" && retryFailedParagraphs();
  }, syncMessageHandler = function(payload, _author) {
    let { method, data } = payload;
    if (log_default.debug(
      `content script received sync message: ${method}`,
      data || " "
    ), method === "ping")
      return "pong";
    if (method === "getPageStatus")
      return getPageStatus();
    if (method === "getCurrentPageLanguage") {
      let language = getCurrentPageLanguage();
      return detectCurrentPageLanguage().catch((e) => {
        log_default.warn("detectCurrentPageLanguage failed", e);
      }), language;
    }
  }, connection, syncConnection;
  function setupMessageListeners() {
    let asyncConnection = getConnection();
    getIsInIframe() || getSyncConnection(), asyncConnection.sendMessage("popup:main_sync", { method: "ready" }).catch(
      (_e3) => {
      }
    );
  }
  function getConnection() {
    return connection || (connection = new Messager("content_script", !1).getConnection("main", asyncMessageHandler), connection);
  }
  function getSyncConnection() {
    return syncConnection || (syncConnection = new Messager("content_script", !1).getConnection("main_sync", syncMessageHandler, {
      sync: !0
    }), syncConnection);
  }