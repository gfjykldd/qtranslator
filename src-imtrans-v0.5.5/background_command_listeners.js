
  // background_command_listeners.ts
  function setupCommandListeners() {
    typeof browserAPI.commands < "u" && browserAPI.commands.onCommand.addListener(async (command) => {
      if (log_default.debug(`received command: ${command}`), ["toggleTranslatePage"].includes(command)) {
        let tab = await browserAPI.tabs.query({
          active: !0,
          currentWindow: !0
        });
        if (tab.length === 0 || typeof tab[0].id > "u")
          return;
        let tabUrl = tab[0].url;
        if (isPdfUrl(tabUrl)) {
          browserAPI.tabs.create({
            url: formatToPdfViewerUrl(tabUrl)
          });
          return;
        }
      }
      await sendMessageToContent({
        method: command
      });
    });
  }
  async function sendMessageToContent(request2) {
    let tabId = (await browserAPI.tabs.query({
      currentWindow: !0,
      active: !0
    }))[0].id;
    getConnection().sendMessage(`content_script:main:${tabId}`, request2).catch((e) => {
      log_default.error("send content message request failed", request2, e);
    });
  }