
  // menu.ts
  var actions = isChrome() ? ["action"] : ["browser_action", "page_action"], menus = [
    {
      id: "toggleTranslatePage",
      contexts: ["page", "frame", "selection", ...actions]
    },
    {
      id: contextOpenOptionsMenuId,
      contexts: actions
    },
    {
      id: contextOpenLocalEbookViewer,
      contexts: actions
    },
    {
      id: contextOpenLocalEbookBuilder,
      contexts: actions
    },
    {
      id: contextTranslateLocalPdfFileMenuId,
      contexts: actions
    },
    {
      id: contextDonateMenuId,
      contexts: actions
    }
  ];
  async function createContextMenu(config) {
    log_default.debug("createContextMenu", menus);
    for (let menu of menus) {
      let visible = !0;
      config.isShowContextMenu === !1 && menu.id === "toggleTranslatePage" && (visible = !1);
      try {
        browserAPI.contextMenus.create({
          id: menu.id,
          title: t2(`browser.${menu.id}`, config.interfaceLanguage),
          contexts: menu.contexts,
          visible
        }, () => browserAPI.runtime.lastError);
      } catch (e) {
        log_default.debug(
          "create context menu error, it's ok!!",
          e,
          `menu id: ${menu.id}`
        );
      }
    }
  }
  function setupContextMenuListeners() {
    browserAPI.contextMenus.onClicked.addListener(
      (info) => {
        if (info.menuItemId === contextOpenOptionsMenuId)
          if (isSafari()) {
            let optionsUrl = getEnv().OPTIONS_URL;
            browserAPI.tabs.create({
              url: optionsUrl
            });
          } else
            browserAPI.runtime.openOptionsPage();
        else if (info.menuItemId === contextTranslateLocalPdfFileMenuId) {
          let pdfViewerRuntimeUrl = browserAPI.runtime.getURL(pdfViewerUrl);
          browserAPI.tabs.create({
            url: pdfViewerRuntimeUrl
          });
        } else if (info.menuItemId === contextOpenLocalEbookBuilder) {
          let url = browserAPI.runtime.getURL(epubBuilderUrl);
          isSafari() && (url = getEnv().EBOOK_BUILDER_URL), browserAPI.tabs.create({
            url
          });
        } else if (info.menuItemId === contextOpenLocalEbookViewer)
          browserAPI.tabs.create({
            url: browserAPI.runtime.getURL(epubViewerUrl)
          });
        else if (info.menuItemId === contextOpenLocalSubtitleBuilder) {
          let url = browserAPI.runtime.getURL(subtitleBuilderUrl);
          isSafari() && (url = getEnv().SUBTITLE_BUILDER_URL), browserAPI.tabs.create({
            url
          });
        } else
          info.menuItemId === contextDonateMenuId ? browserAPI.tabs.create({
            url: "https://immersive-translate.owenyoung.com/donate"
          }) : sendMessageToContent({
            method: info.menuItemId
          });
      }
    );
  }