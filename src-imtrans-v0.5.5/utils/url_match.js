
  // utils/url_match.ts
  var matchAll = ["*://*/*", "*", "*://*"], placeholder = "immersive-translate-wildcard-placeholder.com";
  function getMatchedUrl(rawUrl, rawMatches) {
    let matches = [];
    if (!rawMatches || (rawMatches && !Array.isArray(rawMatches) ? matches = [rawMatches] : matches = rawMatches, matches.length === 0))
      return null;
    if (matches.some((m3) => matchAll.includes(m3)))
      return rawUrl;
    let urlObj = new URL(rawUrl);
    urlObj.hash = "", urlObj.search = "";
    let url = urlObj.href, hostname2 = urlObj.hostname;
    if (matches && matches.length > 0) {
      let matched = matches.find((match) => {
        let rawMatch = match;
        if (match === hostname2)
          return !0;
        if (matchAll.includes(match))
          return !0;
        if (!match.includes("*") && match.includes("://")) {
          try {
            let matchUrl = new URL(match);
            return matchUrl.pathname === "/" && !match.endsWith("/") ? matchUrl.hostname === hostname2 : isTwoUrlMatched(url, match);
          } catch {
          }
          return !1;
        } else {
          let scheme, rawMatch2 = match;
          if (match.includes("://")) {
            let parts = match.split("://");
            scheme = parts[0], scheme === "*" && parts.length > 1 && (scheme = "*", match = "https://" + parts[1]);
          } else
            scheme = "*", match = "https://" + match;
          let validUrlMatch = match.replace(
            /\*/g,
            placeholder
          ), validUrlObj;
          try {
            validUrlObj = new URL(validUrlMatch);
          } catch {
            return log_default.debug(
              "invalid match pattern",
              validUrlMatch,
              "raw match value:",
              rawMatch2
            ), !1;
          }
          let hostname3 = validUrlObj.hostname, pathname = validUrlObj.pathname;
          pathname === "/" && (rawMatch2.replace("://", "").includes("/") || (pathname = "/*"));
          let regex = makeRegExp(
            scheme + ":",
            restorePlaceholderToWildcard(hostname3),
            restorePlaceholderToWildcard(pathname)
          );
          if (regex) {
            let clonedUrl = new URL(url);
            return clonedUrl.port = "", regex.test(clonedUrl.href);
          } else
            return !1;
        }
      });
      if (matched)
        return matched;
    }
    return null;
  }
  function restorePlaceholderToWildcard(str) {
    return str.replaceAll(placeholder, "*");
  }
  function makeRegExp(scheme, host, path) {
    let regex = "^";
    return scheme === "*:" ? regex += "(http:|https:|file:)" : regex += scheme, regex += "//", host && (scheme === "file:" || (host === "*" ? regex += "[^/]+?" : (host.match(/^\*\./) && (regex += "[^/]*?", host = host.substring(2)), regex += host.replace(/\./g, "\\.").replace(/\*/g, "[^/]*")))), path ? path === "*" || path === "/*" ? regex += "(/.*)?" : path.includes("*") ? (regex += path.replace(/\*/g, ".*?"), regex += "/?") : regex += path.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&") : regex += "/?", regex += "$", new RegExp(regex);
  }
  function isMatchUrl(url, rawMatches) {
    return getMatchedUrl(url, rawMatches) !== null;
  }
  function isTwoUrlMatched(url1, url2) {
    let urlObj1 = new URL(url1), urlObj2 = new URL(url2);
    return urlObj1.hostname === urlObj2.hostname && urlObj1.pathname === urlObj2.pathname && urlObj1.protocol === urlObj2.protocol && urlObj1.port === urlObj2.port;
  }