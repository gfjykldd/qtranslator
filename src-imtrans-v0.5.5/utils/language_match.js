
  // utils/language_match.ts
  function isMatchLanguage(lang, matchPattern) {
    let matches = matchPattern.matches || [], excludeMatches = matchPattern.excludeMatches || [];
    if (excludeMatches && !Array.isArray(excludeMatches) && (excludeMatches = [excludeMatches]), matches && !Array.isArray(matches) && (matches = [matches]), excludeMatches.length > 0) {
      if (excludeMatches.includes(lang) || excludeMatches.includes("<all>"))
        return !1;
      for (let match of excludeMatches)
        if (match.includes("*") && new RegExp(match).test(lang))
          return !1;
    }
    if (matches.length === 0)
      return !1;
    if (matches.length > 0) {
      if (matches.includes(lang) || matches.includes("<all>"))
        return !0;
      for (let match of matches)
        if (match.includes("*") && new RegExp(match).test(lang))
          return !0;
    }
    return !1;
  }