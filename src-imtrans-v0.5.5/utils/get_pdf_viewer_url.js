
  // utils/get_pdf_viewer_url.ts
  function formatToPdfViewerUrl(url) {
    let pdfViewerRuntimeUrl = browserAPI.runtime.getURL(pdfViewerUrl), pdfViewUrlObj = new URL(pdfViewerRuntimeUrl);
    return (url.startsWith("http") || !isFirefox()) && pdfViewUrlObj.searchParams.set(
      "file",
      url
    ), pdfViewUrlObj.href;
  }