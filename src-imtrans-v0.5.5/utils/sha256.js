
  // utils/sha256.ts
  var sha256Fn = sha256_default.sha256;
  function sha256(message) {
    return Promise.resolve(sha256Fn(message));
  }
  function hex(hashBuffer) {
    return Array.from(new Uint8Array(hashBuffer)).map((b8) => b8.toString(16).padStart(2, "0")).join(
      ""
    );
  }
  function hmacSha256(str, keyString) {
    let hash = sha256Fn.hmac.create(keyString);
    return hash.update(str), Promise.resolve(hash.array());
  }
  async function hmacSha256ByString(str, keyString) {
    let sig = await hmacSha256(str, keyString);
    return hex(sig);
  }
  async function hmacSha256ByArrayBuffer(str, keyString) {
    let buffer = decodeHex(keyString), sig = await hmacSha256(str, buffer);
    return hex(sig);
  }
  function decodeHex(string) {
    let bytes = [];
    return string.replace(/../g, function(pair) {
      return bytes.push(parseInt(pair, 16)), "";
    }), new Uint8Array(bytes).buffer;
  }