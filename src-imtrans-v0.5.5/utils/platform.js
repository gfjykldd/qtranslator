
  // utils/platform.ts
  var DENO = "DENO", CHROME = "CHROME", FIREFOX = "FIREFOX";
  function isBrowser(toCheck) {
    let currentBrowser = CHROME;
    try {
      let userAgent = navigator?.userAgent || "";
      /firefox/i.test(userAgent) ? currentBrowser = FIREFOX : /deno/i.test(userAgent) && (currentBrowser = DENO);
    } catch {
    }
    return toCheck === CHROME && currentBrowser === CHROME || toCheck === FIREFOX && currentBrowser === FIREFOX || toCheck === DENO && currentBrowser === DENO;
  }
  function isChrome() {
    return isBrowser(CHROME);
  }
  function isDeno2() {
    return typeof Deno < "u";
  }
  function isFirefox() {
    return isBrowser(FIREFOX);
  }
  function isTouchDevice() {
    return !!navigator.maxTouchPoints || "ontouchstart" in document.documentElement;
  }
  function isMouseSupport() {
    return !!globalThis.matchMedia("(pointer:fine)").matches;
  }
