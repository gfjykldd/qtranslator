
  // utils/iframe.ts
  function getIsInIframe() {
    try {
      return globalThis.self !== globalThis.top;
    } catch {
      return !0;
    }
  }