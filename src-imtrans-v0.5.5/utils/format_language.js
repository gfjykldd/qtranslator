
  // utils/format_language.ts
  function formatLanguage(rawLangCode) {
    if (typeof rawLangCode != "string")
      return "auto";
    let lowerCaseLangCode = rawLangCode.toLowerCase();
    if (lowerCaseLangCode === "zh" || lowerCaseLangCode === "zh-hans")
      return "zh-CN";
    if (lowerCaseLangCode === "zh-hant" || lowerCaseLangCode === "zh-hk")
      return "zh-TW";
    if (lowerCaseLangCode === "iw")
      return "he";
    if (lowerCaseLangCode === "jv")
      return "jw";
    let lowerCaseLanguages = languages.map((lang) => lang.toLowerCase()), indexOfLanguages = lowerCaseLanguages.indexOf(
      lowerCaseLangCode
    );
    if (indexOfLanguages === -1)
      if (lowerCaseLangCode.indexOf("-") >= 0) {
        lowerCaseLangCode = lowerCaseLangCode.split("-")[0];
        let firstPartIndex = lowerCaseLanguages.indexOf(lowerCaseLangCode);
        return firstPartIndex === -1 ? "auto" : languages[firstPartIndex];
      } else
        return "auto";
    else
      return languages[indexOfLanguages];
  }