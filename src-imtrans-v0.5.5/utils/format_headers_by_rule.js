
  // utils/format_headers_by_rule.ts
  function formatHeadersByRule(requestHeaders, rules) {
    let ruleMap = /* @__PURE__ */ new Map();
    for (let rule of rules)
      ruleMap.set(rule.header.toLowerCase(), rule);
    let addedHeaders = [], newRequestHeaders = requestHeaders.filter(
      (header) => {
        let rule = ruleMap.get(header.name.toLowerCase());
        if (rule) {
          if (rule.operation === "remove")
            return !1;
          if (rule.operation === "set")
            return !1;
        }
        return !0;
      }
    );
    for (let rule of rules)
      rule.operation === "set" && addedHeaders.push({
        name: rule.header,
        value: rule.value || ""
      });
    return newRequestHeaders.concat(addedHeaders);
  }