
  // utils/rate_limiter.ts
  var RateLimiter = class {
    constructor(options2) {
      this.strictTicks = [];
      this.options = options2, this.setOptions(options2);
    }
    setOptions(options2) {
      options2 && (options2.interval !== void 0 && (this.options.interval = Number(options2.interval)), options2.limit !== void 0 && (this.options.limit = Number(options2.limit)));
    }
    wait() {
      return new Promise((resolve, _reject) => {
        setTimeout(resolve, this.getDelay());
      });
    }
    getDelay() {
      let strictTicks = this.strictTicks, limit = this.options.limit, interval = this.options.interval, now = Date.now();
      if (strictTicks.length < limit)
        return strictTicks.push(now), 0;
      let earliestTime = strictTicks.shift() + interval;
      return now >= earliestTime ? (strictTicks.push(now), 0) : (strictTicks.push(earliestTime), earliestTime - now);
    }
  };
