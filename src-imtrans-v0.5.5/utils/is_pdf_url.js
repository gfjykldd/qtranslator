
  // utils/is_pdf_url.ts
  function isPdfUrl(url) {
    return new URL(url)?.pathname.toLowerCase().endsWith(".pdf");
  }