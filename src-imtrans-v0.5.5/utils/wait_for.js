
  // utils/wait_for.ts
  var ERRORS = {
    NOT_FUNCTION: "Your executor is not a function. functions and promises are valid.",
    FAILED_TO_WAIT: "Failed to wait"
  };
  function promisify(fn) {
    return async () => await fn();
  }
  function validateExecution(executeFn) {
    if (typeof executeFn != "function")
      throw new Error(ERRORS.NOT_FUNCTION);
  }
  var PollUntil = class {
    constructor({
      interval = 100,
      timeout = 1e3,
      stopOnFailure = !1,
      verbose = !1,
      backoffFactor = 1,
      backoffMaxInterval,
      message = ""
    } = {}) {
      this._interval = interval, this._timeout = timeout, this._stopOnFailure = stopOnFailure, this._isWaiting = !1, this._isResolved = !1, this._verbose = verbose, this._userMessage = message, this.originalStacktraceError = new Error(), this._Console = console, this._backoffFactor = backoffFactor, this._backoffMaxInterval = backoffMaxInterval || timeout, this.start = +Date.now();
    }
    tryEvery(interval) {
      return this._interval = interval, this;
    }
    stopAfter(timeout) {
      return this._timeout = timeout, this;
    }
    execute(executeFn) {
      return this._applyPromiseHandlers(), validateExecution(executeFn), this._executeFn = promisify(executeFn), this.start = Date.now(), this._isWaiting = !0, this._log("starting to execute"), this._runFunction(), this.promise;
    }
    getPromise() {
      return this.promise;
    }
    isResolved() {
      return this._isResolved;
    }
    isWaiting() {
      return this._isWaiting;
    }
    stopOnFailure(stop) {
      return this._stopOnFailure = stop, this;
    }
    _applyPromiseHandlers() {
      this.promise = new Promise((resolve, reject) => {
        this.resolve = resolve, this.reject = reject;
      });
    }
    _timeFromStart() {
      return Date.now() - this.start;
    }
    _shouldStopTrying() {
      return this._timeFromStart() > this._timeout;
    }
    _executeAgain() {
      this._log("executing again");
      let currentInterval = this._interval, nextInterval = currentInterval * this._backoffFactor;
      this._interval = nextInterval > this._backoffMaxInterval ? this._backoffMaxInterval : nextInterval, setTimeout(this._runFunction.bind(this), currentInterval);
    }
    _failedToWait() {
      let waitErrorText = `${ERRORS.FAILED_TO_WAIT} after ${this._timeFromStart()}ms`;
      if (this._userMessage && (waitErrorText = `${waitErrorText}: ${this._userMessage}`), this._lastError) {
        this._lastError.message = `${waitErrorText}
${this._lastError.message}`;
        let originalStack = this.originalStacktraceError.stack;
        originalStack && (this._lastError.stack += originalStack.substring(
          originalStack.indexOf(`
`) + 1
        ));
      } else
        this._lastError = this.originalStacktraceError, this._lastError.message = waitErrorText;
      return this._log(this._lastError), this._lastError;
    }
    _runFunction() {
      if (this._shouldStopTrying()) {
        this._isWaiting = !1, this.reject?.(this._failedToWait());
        return;
      }
      this._executeFn().then((result) => {
        if (result === !1) {
          this._log(`then execute again with result: ${result}`), this._executeAgain();
          return;
        }
        this.resolve?.(result), this._isWaiting = !1, this._isResolved = !0, this._log(`then done waiting with result: ${result}`);
      }).catch((err) => this._stopOnFailure ? (this._log(`stopped on failure with err: ${err}`), this.reject?.(err)) : (this._lastError = err, this._log(`catch with err: ${err}`), this._executeAgain()));
    }
    _log(message) {
      this._verbose && this._Console && this._Console.log && this._Console.log(message);
    }
  }, waitFor = (waitForFunction, options2) => new PollUntil(options2).execute(waitForFunction);
