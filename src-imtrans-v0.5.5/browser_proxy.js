
  // browser_proxy.ts
  async function sendMessage(options2) {
    return await getConnection().sendMessage(
      "background:main",
      options2
    );
  }
  function request2(options2) {
    return isWebOptionsPage() ? ask({
      method: "request",
      data: options2
    }) : isMonkey() || isDeno() ? (options2.fetchPolyfill = globalThis.GM_fetch, request(options2)) : sendMessage({
      method: "fetch",
      data: options2
    });
  }
  function getConfig2() {
    return isMonkey() ? getConfig() : sendMessage(
      {
        method: "getConfig"
      }
    );
  }
  function getLocalConfig2() {
    return isMonkey() ? getLocalConfig() : sendMessage(
      {
        method: "getLocalConfig"
      }
    );
  }
  function setLocalConfig2(config) {
    return isMonkey() ? setLocalConfig(config) : sendMessage(
      {
        method: "setLocalConfig",
        data: config
      }
    );
  }
  function setBuildinConfig2(config) {
    return isMonkey() ? setBuildinConfig(config) : sendMessage(
      {
        method: "setBuildinConfig",
        data: config
      }
    );
  }
  function detectLanguage(options2) {
    if (options2.text) {
      let chineseLike = detectChinese(options2.text);
      if (chineseLike !== "auto")
        return Promise.resolve(chineseLike);
    }
    if (isMonkey() || isSafari()) {
      let result = languageDetect(
        options2.text,
        options2.minLength
      );
      return Promise.resolve(result);
    }
    return sendMessage(
      {
        method: "detectLanguage",
        data: options2
      }
    );
  }
  function detectTabLanguage() {
    return sendMessage(
      {
        method: "detectTabLanguage"
      }
    );
  }
  function sendPageTranslatedStatus(status) {
    if (isMonkey()) {
      let event = new CustomEvent(pageTranslatedStatusEventName, {
        detail: status
      });
      document.dispatchEvent(event);
      return;
    }
    getConnection().sendMessage("popup:main_sync", {
      method: "setPageStatus",
      data: status
    }).catch((_e3) => {
    });
  }
  function queryDb(query) {
    return isMonkey() ? queryParagraphCache(query) : sendMessage(
      {
        method: "queryParagraphCache",
        data: query
      }
    );
  }
  async function setDbStore(params) {
    if (isMonkey()) {
      await setParagraphCache(params);
      return;
    }
    return sendMessage(
      {
        method: "setParagraphCache",
        data: params
      }
    );
  }
  async function mockRequest() {
    if (isMonkey())
      return Promise.resolve();
    await sendMessage({
      method: "mockRequest"
    });
  }
  function openOptionsPage(newTab = !1, pageRoute = "") {
    return isMonkey() ? (browserAPI.runtime.openOptionsPage(newTab, pageRoute), Promise.resolve()) : sendMessage({
      method: "openOptionsPage"
    });
  }
  function openAboutPage(newTab = !1) {
    return isMonkey() ? (browserAPI.extra.openAboutPage(newTab), Promise.resolve()) : sendMessage({
      method: "openAboutPage"
    });
  }
  function openEbookViewerPage(newTab = !1) {
    return isMonkey() ? (browserAPI.extra.openEbookViewerPage(newTab), Promise.resolve()) : sendMessage({
      method: "openEbookViewerPage"
    });
  }
  function openEbookBuilderPage(newTab = !1) {
    return isMonkey() ? (browserAPI.extra.openEbookBuilderPage(newTab), Promise.resolve()) : sendMessage({
      method: "openEbookBuilderPage"
    });
  }
  function updateCommands(details) {
    isSafari();
  }
  function openPdfViewerPage(newTab = !1) {
    return isMonkey() ? (alert("it's not support in userscript"), Promise.resolve()) : sendMessage({
      method: "openPdfViewerPage"
    });
  }
  function openSubtitleBuilderPage(newTab = !1) {
    return isMonkey() ? (browserAPI.extra.openSubtitleBuilderPage(newTab), Promise.resolve()) : sendMessage({
      method: "openSubtitleBuilderPage"
    });
  }
  function autoSyncLatestConfig() {
    return isMonkey() ? (log_default.warn("autoSyncLatestConfig is not support in monkey"), Promise.resolve()) : sendMessage({
      method: "autoSyncLatestConfig"
    });
  }