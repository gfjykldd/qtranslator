
  // content_dom_listeners.ts
  var debounceOpenOptionsPage = se(async () => {
    await openOptionsPage2();
  }, 50), debounceOpenAboutPage = se(async () => {
    await openAboutPage2();
  }, 50), deboundOpenEbookBuilder = se(async () => {
    await openEbookBuilderPage2();
  }, 50), deboundOpenEbookViewer = se(async () => {
    await openEbookViewerPage2();
  }, 50), debounceToggleTranslatePage = se((id) => {
    sendMessageToContent({
      method: id
    });
  }, 50), throttleToggleTranslatePage = se(() => {
    toggleTranslatePage();
    let event = new CustomEvent(userscriptCommandEventName, {
      detail: {
        method: "toggleTranslatePage"
      }
    });
    globalThis.document.dispatchEvent(event);
  }, 200), throttleToggleTranslationMask = se(() => {
    toggleTranslationMask();
    let event = new CustomEvent(userscriptCommandEventName, {
      detail: {
        method: "toggleTranslationMask"
      }
    });
    globalThis.document.dispatchEvent(event);
  }, 200);
  function setupDomListenersForAll(ctx) {
    if (isMonkey() || setupMessageListeners(), document.addEventListener("securitypolicyviolation", (e) => {
      env.HAS_CSP_ERROR = "1";
    }), document.addEventListener("immersiveTranslateEbookLoaded", () => {
      setTimeout(() => {
        initPage();
      }, 10);
    }), document.addEventListener(
      documentMessageTypeIdentifierForThirdPartyTell,
      (e) => {
        let event = e;
        if (log_default.debug("receive third party message", event), event && event.detail)
          try {
            let detailObj = JSON.parse(event.detail);
            detailObj && detailObj.type && (detailObj.type === "retryFailedParagraphs" ? sendMessageToContent2({
              method: "retryFailedParagraphs"
            }) : detailObj.type === "updateCommands" ? updateCommands(detailObj.data) : debounceToggleTranslatePage(detailObj.type));
          } catch (e2) {
            log_default.warn("parse message error", e2);
          }
      }
    ), document.addEventListener("click", (e) => {
      let target = e.target;
      if (!target || !target.getAttribute)
        return;
      let action = target.getAttribute("data-immersive-translate-action");
      action && action === "retry" && (e.preventDefault(), typeof e.stopPropagation == "function" && e.stopPropagation(), retryFailedParagraphs());
    }), ctx.rule.fingerCountToToggleTranslagePageWhenTouching >= 2 && document.addEventListener("touchstart", (e) => {
      e.touches.length == ctx.rule.fingerCountToToggleTranslagePageWhenTouching ? throttleToggleTranslatePage() : e.touches.length === ctx.rule.fingerCountToToggleTranslationMaskWhenTouching && throttleToggleTranslationMask();
    }), isMonkey() && (globalThis.top != globalThis.self ? globalThis.addEventListener("message", (event) => {
      event && event.data && event.data.payload && event.data.author === iframeMessageIdentifier && asyncMessageHandler(event.data.payload, {
        // @ts-ignore: it's ok
        tab: {
          id: 1,
          url: "https://www.fake-iframe.com",
          active: !0
        }
      });
    }, !1) : (setupCommandListeners(ctx.config), registerCommands(ctx.config), globalThis.document.addEventListener(
      userscriptCommandEventName,
      // @ts-ignore: hard to type
      (_e3) => {
        ensurePopupInit();
      }
    ))), isSafari() && setupCommandListeners(ctx.config), globalThis.top === globalThis.self) {
      let channel = ProtoframePubsub.rootIframe(
        childFrameToRootFrameIdentifier
      );
      channel.handleAsk("getRateLimitDelay", onRateLimiterDelayRequest), channel.handleAsk("throttleRequest", onThrottleRequest);
    }
  }
  function registerCommands(config) {
    if (isMonkey() && typeof GM < "u" && GM && GM.registerMenuCommand) {
      let commandsMap = manifest_default.commands, menus = [
        ...Object.keys(commandsMap).filter((item) => item === "toggleTranslatePage").map((command) => {
          let titlePlaceholder = commandsMap[command].description, title = titlePlaceholder;
          return titlePlaceholder.startsWith("__MSG_") && titlePlaceholder.endsWith("__") && (title = t3(
            `browser.${titlePlaceholder.slice(6, -2)}`,
            config.interfaceLanguage
          )), {
            id: command,
            title
          };
        }),
        {
          id: contextOpenLocalEbookViewer,
          title: t3("browser.openEbookViewer", config.interfaceLanguage),
          key: "e"
        },
        {
          id: contextOpenLocalEbookBuilder,
          title: t3("browser.openEbookBuilder", config.interfaceLanguage),
          key: "m"
        },
        {
          id: contextOpenOptionsMenuId,
          title: t3("browser.openOptionsPage", config.interfaceLanguage),
          key: "o"
        },
        {
          id: contextOpenAboutMenuId,
          title: t3("browser.openAboutPage", config.interfaceLanguage),
          key: "a"
        }
      ];
      for (let menu of menus)
        GM.registerMenuCommand(
          menu.title,
          () => {
            menu.id === contextOpenOptionsMenuId ? debounceOpenOptionsPage() : menu.id === contextOpenAboutMenuId ? debounceOpenAboutPage() : menu.id === contextOpenLocalEbookBuilder ? deboundOpenEbookBuilder() : menu.id === contextOpenLocalEbookViewer ? deboundOpenEbookViewer() : debounceToggleTranslatePage(menu.id);
          },
          menu.key
        );
    }
  }
  function sendMessageToContent2(request3) {
    asyncMessageHandler(request3, {
      // @ts-ignore: it's ok
      tab: {
        id: 1,
        url: "https://www.fake.com",
        active: !0
      }
    }).catch((e) => {
      log_default.error("send content message request failed", request3, e);
    });
    let event = new CustomEvent(userscriptCommandEventName, {
      detail: request3
    });
    globalThis.document.dispatchEvent(event);
  }