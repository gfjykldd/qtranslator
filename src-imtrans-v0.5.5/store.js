
  // store.ts
  var CACHE_KEY_PREFIX = brandIdForJs + "StoreKey_";
  function get(rawKey, defaultValue) {
    let key = CACHE_KEY_PREFIX + rawKey;
    return browserAPI.storage.local.get(key).then((result) => result[key] === void 0 ? defaultValue : result[key]);
  }
  function set(rawKey, value) {
    let key = CACHE_KEY_PREFIX + rawKey;
    return browserAPI.storage.local.set({ [key]: value });
  }