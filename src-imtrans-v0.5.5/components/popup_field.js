
  // components/popup_field.tsx
  function PopupField(props) {
    let { field, onChange, value } = props;
    value = value || field.default || "";
    let { t: t2 } = useI18n(), finalLabel = field.name;
    return field.label && (finalLabel = field.label), field.labelKey && (finalLabel = t2(field.labelKey)), field.type === "select" ? /* @__PURE__ */ p3("div", { class: "flex justify-between mb-2", children: [
      /* @__PURE__ */ p3("label", { class: "inline-block", children: [
        finalLabel,
        "\uFF1A"
      ] }),
      /* @__PURE__ */ p3(
        SelectLink,
        {
          items: field.options.map(
            (fieldOption) => ({
              label: `${fieldOption.label ? t2(fieldOption.label) : fieldOption.value}`,
              value: fieldOption.value,
              selected: value === fieldOption.value,
              onSelected: () => {
                onChange(fieldOption.value);
              }
            })
          )
        }
      )
    ] }) : null;
  }