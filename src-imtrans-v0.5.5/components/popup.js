
  // components/popup.tsx
  function Popup(props) {
    let version = getVersion(), {
      onTranslateTheMainPage,
      onUserConfigChange,
      request: request2,
      onTranslateLocalHtmlFile,
      onTranslateLocalSubtitleFile,
      onSetBuildinConfig,
      pageStatus,
      openEbookBuilderPage,
      openEbookViewerPage,
      config,
      openAboutPage,
      onTranslateTheWholePage,
      onToggleEnabled,
      openOptionsPage,
      ontranslateToThePageEndImmediately,
      onSetPageLanguage,
      onToggleTranslate,
      onTranslateLocalPdfFile,
      onTranslatePdf,
      onRestorePage,
      ctx,
      currentUrl,
      currentLang,
      onClose,
      onTranslatePage,
      onSetLocalConfig,
      onMouseTriggerChanged
    } = props, setSettings = onUserConfigChange, [message, setMessage] = P2(""), [errorMessage, _setErrorMessage] = P2(""), { t: t2 } = useI18n(), isAlwaysTranslateDomain = null, isAlwaysTranslateWildDomain = null, isNeverTranslaateDomain = null, isNeverTranslateWildDomain = null, isAlwaysTranslateLang = null, isAlwaysTranslateUrl = null, isNeverTranslateUrl = null, curentTranslationServiceItem = null, currentUrlObj = null, currentWildHostname = null, currentUrlWithoutHash = null, currentTranslationServiceConfig = null, isShowPricing = !0;
    if (config) {
      isSafari() && config && (isShowPricing = config.generalRule.showSponsorOnSafari);
      let { translationService, translationServices, translationUrlPattern } = config;
      if (PureTranslationServices[translationService] && (curentTranslationServiceItem = formatTranslationService(
        translationService,
        ctx
      )), translationServices && translationServices[translationService] ? currentTranslationServiceConfig = translationServices[translationService] || {} : currentTranslationServiceConfig = {}, currentUrl && isValidHtmlUrl(currentUrl)) {
        currentUrlObj = new URL(currentUrl), currentWildHostname = hostnameToWildHostname(currentUrlObj.hostname), currentUrlWithoutHash = getUrlWithoutHash(currentUrl);
        let { matches, excludeMatches } = translationUrlPattern;
        isAlwaysTranslateWildDomain = matches.includes(currentWildHostname), isAlwaysTranslateDomain = matches.includes(currentUrlObj.hostname), isNeverTranslateWildDomain = excludeMatches.includes(
          currentWildHostname
        ), isNeverTranslaateDomain = excludeMatches.includes(
          currentUrlObj.hostname
        ), isAlwaysTranslateUrl = matches.includes(currentUrlWithoutHash), isNeverTranslateUrl = excludeMatches.includes(currentUrlWithoutHash);
      }
    }
    if (config && currentLang && currentLang !== "auto") {
      let { translationLanguagePattern } = config, { matches } = translationLanguagePattern;
      matches.includes(currentLang) ? isAlwaysTranslateLang = !0 : isAlwaysTranslateLang = !1;
    }
    let handleOpenOptions = (e) => {
      e.preventDefault(), openOptionsPage();
    }, handleToggleAlpha = (_e3) => {
      setSettings((settings) => (settings.alpha ? setMessage("Success disable alpha!") : setMessage("Success enable alpha!"), {
        ...settings,
        alpha: !settings.alpha
      }));
    }, handleChangeToTranslateTheWholePage = () => {
      setSettings((state) => ({
        ...state,
        translationArea: "body"
      })), onTranslateTheWholePage();
    }, handleChangeToTranslateTheMainPage = () => {
      setSettings((state) => ({
        ...state,
        translationArea: "main"
      })), onTranslateTheMainPage();
    }, handleTranslationUrlPatternSelected = (value, matchString, reverseRemoveStrings, removeStrings) => {
      if (value === "default") {
        setSettings((state) => {
          let translationUrlPattern = { ...state.translationUrlPattern };
          return {
            ...state,
            translationUrlPattern: {
              ...state.translationUrlPattern,
              matches: removeFromArray(
                [currentUrlObj?.hostname, currentWildHostname, currentUrl],
                translationUrlPattern.matches
              ),
              excludeMatches: removeFromArray(
                [currentUrlObj?.hostname, currentWildHostname, currentUrl],
                translationUrlPattern.excludeMatches
              )
            }
          };
        });
        return;
      }
      let name = value, reverseName = name === "matches" ? "excludeMatches" : "matches";
      currentUrlObj && setSettings((state) => {
        let translationUrlPattern = { ...state.translationUrlPattern };
        return translationUrlPattern[name] = addToUniqueArray(
          matchString,
          translationUrlPattern[name]
        ), removeStrings.length > 0 && (translationUrlPattern[name] = removeFromArray(
          removeStrings,
          translationUrlPattern[name]
        )), translationUrlPattern[reverseName] = removeFromArray(
          reverseRemoveStrings,
          translationUrlPattern[reverseName]
        ), {
          ...state,
          translationUrlPattern: {
            ...state.translationUrlPattern,
            ...translationUrlPattern
          }
        };
      }), name === "matches" && pageStatus === "Original" ? setTimeout(() => {
        onTranslatePage(), onClose();
      }, 100) : name === "excludeMatches" && pageStatus === "Translated" && setTimeout(() => {
        onRestorePage(), onClose();
      }, 100);
    }, handleTranslationLanguagePatternSelected = (value) => {
      if (!value) {
        setSettings((state) => {
          let translationLanguagePattern = {
            ...state.translationLanguagePattern
          };
          return {
            ...state,
            translationLanguagePattern: {
              ...state.translationLanguagePattern,
              matches: removeFromArray(
                currentLang,
                translationLanguagePattern.matches
              ),
              excludeMatches: removeFromArray(
                currentLang,
                translationLanguagePattern.excludeMatches
              )
            }
          };
        });
        return;
      }
      let name = value, reverseName = name === "matches" ? "excludeMatches" : "matches";
      currentLang && setSettings((state) => {
        let translationLanguagePattern = {
          ...state.translationLanguagePattern
        };
        return translationLanguagePattern[name] = addToUniqueArray(
          currentLang,
          translationLanguagePattern[name]
        ), translationLanguagePattern[reverseName] = removeFromArray(
          currentLang,
          translationLanguagePattern[reverseName]
        ), {
          ...state,
          translationLanguagePattern: {
            ...state.translationLanguagePattern,
            ...translationLanguagePattern
          }
        };
      }), name === "matches" && pageStatus === "Original" && setTimeout(() => {
        onTranslatePage(), onClose();
      }, 100);
    }, isPdfUrl = currentUrlObj?.pathname.toLowerCase().endsWith(".pdf"), buttonLabel = t2("translate");
    pageStatus === "Translated" || pageStatus === "Error" ? buttonLabel = t2("show-original") : pageStatus === "Original" ? isPdfUrl ? isFirefox() && currentUrlObj.protocol === "file:" ? buttonLabel = t2("translate-firefox-local-pdf") : isMonkey() ? buttonLabel = t2("noSupportTranslate-pdf") : buttonLabel = t2("translate-pdf") : buttonLabel = t2("translate") : buttonLabel = t2(pageStatus);
    let translateToThePageEndImmediatelyLabel = t2(
      "translateToThePageEndImmediately"
    );
    (pageStatus === "Original" || pageStatus === "Translated") && (config.shortcuts.toggleTranslatePage && (isTouchDevice() && ctx.rule.fingerCountToToggleTranslagePageWhenTouching >= 2 ? buttonLabel += ` (${t2(
      `fingers.${ctx.rule.fingerCountToToggleTranslagePageWhenTouching}`
    )})` : buttonLabel += ` (${config.shortcuts.toggleTranslatePage})`), config.shortcuts.toggleTranslateToThePageEndImmediately && (translateToThePageEndImmediatelyLabel += ` (${config.shortcuts.toggleTranslateToThePageEndImmediately})`));
    let translationServiceItems = [];
    ctx && (translationServiceItems = getTranslationServices(ctx));
    let handleClosePopup = (e) => {
      e.preventDefault(), onClose();
    };
    return /* @__PURE__ */ p3("div", { class: "p-3", children: [
      /* @__PURE__ */ p3("div", { class: "text-sm", children: [
        /* @__PURE__ */ p3("div", { class: "flex justify-between mb-2", children: [
          /* @__PURE__ */ p3("label", { class: "inline-block", children: [
            t2("popupSourceLanguage"),
            "\uFF1A"
          ] }),
          /* @__PURE__ */ p3(
            SelectLink,
            {
              items: languages.map((code2) => ({
                label: getLanguageName(code2, config.interfaceLanguage),
                value: code2,
                selected: code2 === currentLang,
                onSelected: (item) => {
                  onSetPageLanguage(item.value);
                }
              }))
            }
          )
        ] }),
        config && config.targetLanguage && /* @__PURE__ */ p3("div", { class: "flex justify-between mb-2", children: [
          /* @__PURE__ */ p3("label", { class: "inline-block", children: [
            t2("popupTarget"),
            "\uFF1A"
          ] }),
          /* @__PURE__ */ p3(
            SelectLink,
            {
              items: languages.filter((code2) => code2 !== "auto").map((code2) => ({
                label: getLanguageName(code2, config.interfaceLanguage),
                value: code2,
                selected: code2 === config.targetLanguage,
                onSelected: (item) => {
                  setSettings((state) => ({
                    ...state,
                    targetLanguage: item.value
                  }));
                }
              }))
            }
          )
        ] }),
        curentTranslationServiceItem && translationServiceItems.length > 0 && /* @__PURE__ */ p3(L, { children: [
          /* @__PURE__ */ p3("div", { class: "flex justify-between mb-2", children: [
            /* @__PURE__ */ p3("label", { class: "inline-block", children: [
              t2("popupService"),
              "\uFF1A"
            ] }),
            /* @__PURE__ */ p3(
              SelectLink,
              {
                items: translationServiceItems.map((translationServiceItem) => ({
                  label: `${t2(
                    "translationServices." + translationServiceItem.id
                  )}${translationServiceItem.ok ? "" : " " + t2("needAction")}`,
                  value: translationServiceItem.id,
                  selected: translationServiceItem.selected,
                  onSelected: (option) => {
                    let selectedItem = translationServiceItems.find(
                      (item) => item.id === option.value
                    );
                    selectedItem.ok ? (setSettings((state) => ({
                      ...state,
                      translationService: selectedItem.id
                    })), selectedItem.props.length === 0 ? setTimeout(() => {
                      onTranslatePage();
                    }, 1) : setTimeout(() => {
                      onRestorePage();
                    }, 1)) : (setSettings((state) => ({
                      ...state,
                      translationService: selectedItem.id
                    })), setTimeout(() => {
                      openOptionsPage();
                    }, 100));
                  }
                }))
              }
            )
          ] }),
          currentTranslationServiceConfig && curentTranslationServiceItem.props.length > 0 && curentTranslationServiceItem.props.map((prop, index) => /* @__PURE__ */ p3("div", { class: "pl-4 text-sm", children: /* @__PURE__ */ p3(
            PopupField,
            {
              field: prop,
              value: currentTranslationServiceConfig[prop.name],
              onChange: (value) => {
                setSettings((state) => {
                  let currentServices = state.translationServices || {}, currentServiceConfig = currentServices[curentTranslationServiceItem.id] || {};
                  return setTimeout(() => {
                    onRestorePage();
                  }, 1), {
                    ...state,
                    translationServices: {
                      ...currentServices,
                      [curentTranslationServiceItem.id]: {
                        ...currentServiceConfig,
                        [prop.name]: value
                      }
                    }
                  };
                });
              }
            },
            "field-" + index
          ) }, "service" + index))
        ] }),
        currentUrlObj && /* @__PURE__ */ p3("div", { class: "flex justify-between mb-2", children: [
          /* @__PURE__ */ p3("label", { class: "inline-block", children: t2("forThisSite") }),
          /* @__PURE__ */ p3(
            SelectLink,
            {
              items: [
                {
                  label: t2("default"),
                  value: "default",
                  selected: isAlwaysTranslateDomain === !1 && isNeverTranslaateDomain === !1 && !isAlwaysTranslateWildDomain && !isNeverTranslateWildDomain && !isAlwaysTranslateUrl && !isNeverTranslateUrl,
                  onSelected: () => {
                    handleTranslationUrlPatternSelected(
                      "default",
                      currentUrlObj.hostname,
                      [],
                      []
                    );
                    let currentDomain = currentUrlObj.hostname, currentTempTranslationDomains = ctx.localConfig.tempTranslationUrlMatches || [], filteredDomains = currentTempTranslationDomains.filter(
                      (item) => item.match !== currentDomain
                    ), isChanged = !1;
                    filteredDomains.length !== currentTempTranslationDomains.length && (isChanged = !0), isChanged && onSetLocalConfig({
                      ...ctx.localConfig,
                      tempTranslationUrlMatches: [...filteredDomains]
                    });
                  }
                },
                currentUrlWithoutHash && {
                  label: t2("alwaysTranslateSomeSite", {
                    hostname: t2("currentUrl")
                  }),
                  value: "matchesUrl",
                  selected: isAlwaysTranslateUrl,
                  onSelected: () => {
                    handleTranslationUrlPatternSelected(
                      "matches",
                      currentUrlWithoutHash,
                      [currentUrlWithoutHash],
                      []
                    );
                  }
                },
                {
                  label: t2("alwaysTranslateSomeSite", {
                    hostname: currentUrlObj.hostname
                  }),
                  value: "matches",
                  selected: isAlwaysTranslateDomain,
                  onSelected: (item) => {
                    handleTranslationUrlPatternSelected(
                      item.value,
                      currentUrlObj.hostname,
                      [
                        currentUrlObj.hostname,
                        currentWildHostname,
                        currentUrlWithoutHash
                      ],
                      [currentWildHostname]
                    );
                  }
                },
                currentWildHostname && {
                  label: t2("alwaysTranslateSomeSite", {
                    hostname: currentWildHostname
                  }),
                  value: "matchesWild",
                  selected: isAlwaysTranslateWildDomain,
                  onSelected: () => {
                    handleTranslationUrlPatternSelected(
                      "matches",
                      currentWildHostname,
                      [
                        currentUrlWithoutHash,
                        currentUrlObj.hostname,
                        currentWildHostname
                      ],
                      [currentUrlObj.hostname]
                    );
                  }
                },
                currentUrlWithoutHash && {
                  label: t2("neverTranslateSomeSite", {
                    hostname: t2("currentUrl")
                  }),
                  value: "excludeMatchesUrl",
                  selected: isNeverTranslateUrl,
                  onSelected: () => {
                    handleTranslationUrlPatternSelected(
                      "excludeMatches",
                      currentUrlWithoutHash,
                      [currentUrlWithoutHash],
                      []
                    );
                  }
                },
                {
                  label: t2("neverTranslateSomeSite", {
                    hostname: currentUrlObj.hostname
                  }),
                  value: "excludeMatches",
                  selected: isNeverTranslaateDomain,
                  onSelected: (item) => {
                    handleTranslationUrlPatternSelected(
                      item.value,
                      currentUrlObj.hostname,
                      [
                        currentUrlObj.hostname,
                        currentWildHostname,
                        currentUrlWithoutHash
                      ],
                      [currentWildHostname]
                    );
                  }
                },
                currentWildHostname && {
                  label: t2("neverTranslateSomeSite", {
                    hostname: currentWildHostname
                  }),
                  value: "excludeMatchesWild",
                  selected: isNeverTranslateWildDomain,
                  onSelected: () => {
                    handleTranslationUrlPatternSelected(
                      "excludeMatches",
                      currentWildHostname,
                      [
                        currentUrlObj.hostname,
                        currentUrlWithoutHash,
                        currentWildHostname
                      ],
                      [currentUrlObj.hostname]
                    );
                  }
                }
              ].filter(Boolean)
            }
          )
        ] }),
        onMouseTriggerChanged && isMouseSupport() && /* @__PURE__ */ p3("div", { class: "flex justify-between mb-2", children: [
          /* @__PURE__ */ p3("label", { class: "inline-block", children: [
            t2("mouse-translate"),
            "\uFF1A"
          ] }),
          /* @__PURE__ */ p3(
            SelectLink,
            {
              items: MouseTranslateTriggerMechanism.filter((code2) => !(MouseTranslateTriggerMechanism.includes(
                config.generalRule.mouseHoverHoldKey
              ) && code2 === "OtherCustom")).map((code2) => {
                let label = t2("mouseHoldKey", {
                  key: code2
                }), isBuildinKey = MouseTranslateTriggerMechanism.includes(
                  config.generalRule.mouseHoverHoldKey
                ), isSelected = code2 === config.generalRule.mouseHoverHoldKey;
                return code2 === "Auto" ? label = t2("mouseHoldKeyAuto") : code2 === "Off" ? label = t2("mouseHoldKeyOff") : code2 === "OtherCustom" ? label = isBuildinKey ? t2("mouseHoldKeyOther") : t2("mouseHoldKeyCustomKey", {
                  key: config.generalRule.mouseHoverHoldKey
                }) : code2 === "Other" && (label = t2("mouseHoldKeyOther")), isBuildinKey || code2 === "OtherCustom" && (isSelected = !0), {
                  // Note: translate the code label
                  label,
                  value: code2,
                  selected: isSelected,
                  onSelected: (item) => {
                    item.value === "Other" ? openOptionsPage("#interface") : onMouseTriggerChanged(item.value);
                  }
                };
              })
            }
          )
        ] })
      ] }),
      /* @__PURE__ */ p3("div", { class: "", children: /* @__PURE__ */ p3(
        "button",
        {
          class: "py-2 mt-1 mb-2 main-button ",
          onClick: () => {
            isPdfUrl ? onTranslatePdf && onTranslatePdf() : onToggleTranslate();
          },
          "aria-busy": pageStatus === "Translating",
          disabled: pageStatus === "Translating",
          children: buttonLabel
        }
      ) }),
      /* @__PURE__ */ p3("div", { class: "flex justify-between", children: [
        currentLang && currentLang !== "auto" ? /* @__PURE__ */ p3("label", { for: "alwaysTranslateThisLanugage", class: "text-sm", children: [
          /* @__PURE__ */ p3(
            "input",
            {
              type: "checkbox",
              id: "alwaysTranslateThisLanugage",
              name: "alwaysTranslateThisLanugage",
              checked: !!isAlwaysTranslateLang,
              onChange: (e) => {
                let checked = e.target.checked;
                handleTranslationLanguagePatternSelected(
                  checked ? "matches" : void 0
                );
              }
            }
          ),
          t2("alwaysTranslateSomeLanguage", {
            language: getLanguageName(
              currentLang,
              config.interfaceLanguage
            )
          })
        ] }) : /* @__PURE__ */ p3("span", {}),
        /* @__PURE__ */ p3(
          SelectDropDown,
          {
            label: t2("more"),
            showArrow: !0,
            onSelected: (item) => {
              item.value === "translateTheWholePage" ? onTranslateTheWholePage() : item.value === "translateToThePageEndImmediately" ? ontranslateToThePageEndImmediately() : item.value === "translateTheMainPage" ? onTranslateTheMainPage() : item.value === "showTranslationOnly" || (item.value === "translateLocalPdfFile" ? onTranslateLocalPdfFile && onTranslateLocalPdfFile() : item.value === "translateLocalHtmlFile" ? onTranslateLocalHtmlFile && onTranslateLocalHtmlFile() : item.value === "translateLocalSubtitleFile" ? onTranslateLocalSubtitleFile && onTranslateLocalSubtitleFile() : item.value === "donate" ? (globalThis.open(config.donateUrl), onClose()) : item.value === "feedback" ? (globalThis.open(config.feedbackUrl), onClose()) : item.value === "options" ? (openOptionsPage(), onClose()) : item.value === "changeToTranslateTheWholePage" ? handleChangeToTranslateTheWholePage() : item.value === "changeToTranslateTheMainPage" ? handleChangeToTranslateTheMainPage() : item.value === "about" ? openAboutPage() : item.value === "toggleEnabled" ? onToggleEnabled() : item.value === "openEbookViewer" ? openEbookViewerPage() : item.value === "openEbookBuilder" && openEbookBuilderPage());
            },
            menus: [
              config.translationArea === "main" && {
                label: "\u{1F480} " + t2("changeToTranslateTheWholePage"),
                value: "changeToTranslateTheWholePage"
              },
              config.translationArea === "body" && {
                label: "\u{1F4D6} " + t2("changeToTranslateTheMainPage"),
                value: "changeToTranslateTheMainPage"
              },
              {
                label: "\u26A1 " + translateToThePageEndImmediatelyLabel,
                value: "translateToThePageEndImmediately"
              },
              {
                label: "\u{1F4D8} " + t2("browser.openEbookViewer"),
                value: "openEbookViewer"
              },
              {
                label: "\u{1F4DA} " + t2("browser.openEbookBuilder"),
                value: "openEbookBuilder"
              },
              !isMonkey() && {
                label: "\u{1F4C1} " + t2("browser.translateLocalPdfFile"),
                value: "translateLocalPdfFile"
              },
              !isMonkey() && {
                label: "\u{1F310} " + t2("browser.translateLocalHtmlFile"),
                value: "translateLocalHtmlFile"
              },
              {
                label: "\u{1F4FA} " + t2("browser.translateLocalSubtitleFile"),
                value: "translateLocalSubtitleFile"
              },
              {
                label: (config.enabled ? "\u{1F6AB} " : "\u{1F44B} ") + (config.enabled ? t2("clickToDisableExtension") : t2("clickToEnableExtension")),
                value: "toggleEnabled"
              },
              {
                label: "\u2764\uFE0F " + t2(isShowPricing ? "aboutLabel" : "aboutLabelWithoutSponsor"),
                value: "about"
              }
            ].filter(Boolean)
          }
        )
      ] }),
      /* @__PURE__ */ p3("div", { class: "text-sm", children: message }),
      /* @__PURE__ */ p3("div", { class: "text-sm", children: errorMessage }),
      /* @__PURE__ */ p3("footer", { children: [
        /* @__PURE__ */ p3(
          SyncLatest,
          {
            request: request2,
            setStorageBuildinConfig: onSetBuildinConfig
          }
        ),
        /* @__PURE__ */ p3("div", { class: "mt-3 text-sm flex justify-between", children: [
          /* @__PURE__ */ p3("a", { href: "#", class: "secondary", onClick: handleOpenOptions, children: t2("options") }),
          isMonkey() && /* @__PURE__ */ p3("a", { href: "#", class: "secondary", onClick: handleClosePopup, children: t2("close") }),
          /* @__PURE__ */ p3(
            "span",
            {
              class: "immersive-translate-no-select muted",
              onClick: onClickMultipleTimes(7)(handleToggleAlpha),
              children: [
                "V",
                version,
                config.enabled ? null : /* @__PURE__ */ p3("a", { href: "#", onClick: onToggleEnabled, children: [
                  " ",
                  "(",
                  t2("hasBeenDisabled"),
                  ")"
                ] })
              ]
            }
          )
        ] })
      ] })
    ] });
  }