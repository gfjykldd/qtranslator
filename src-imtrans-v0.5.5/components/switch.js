
  // components/switch.tsx
  function Switch({
    value,
    defaultCase = null,
    cases
  }) {
    return value == null ? null : /* @__PURE__ */ p4(L, { children: cases[value] != null ? cases[value] : defaultCase });
  }