
  // components/sync_latest.tsx
  var SyncSuccess = ({ date }) => {
    let { t: t2 } = useI18n(), [isHide, setIsHide] = P2(!1);
    return j2(() => {
      setTimeout(() => {
        setIsHide(!0);
      }, 5e3);
    }, []), isHide ? null : /* @__PURE__ */ p3("p", { class: "text-sm", children: [
      t2("Successfully synchronized with the latest official rules:"),
      " ",
      new Date(date).toLocaleString()
    ] });
  }, LocalVersionIsTooOld = ({ minVersion }) => {
    let { t: t2 } = useI18n();
    return /* @__PURE__ */ p3("p", { class: "text-sm", children: t2("localVersionIsTooOld", {
      minVersion
    }) });
  }, BadUserscriptBrowser = () => {
    let { t: t2 } = useI18n();
    return /* @__PURE__ */ p3(
      "p",
      {
        class: "text-sm",
        dangerouslySetInnerHTML: {
          __html: t2("badUserscriptBrowser", {
            1: "https://immersive-translate.owenyoung.com/installation.html"
          })
        }
      }
    );
  }, SyncFailed = ({
    message,
    handleSyncing,
    date
  }) => {
    let { t: t2 } = useI18n();
    return /* @__PURE__ */ p3("p", { class: "text-sm", children: [
      t2("failToSyncRules"),
      " ",
      /* @__PURE__ */ p3("a", { onClick: handleSyncing, children: t2("retry") }),
      /* @__PURE__ */ p3("br", {}),
      t2("failedReason"),
      "\uFF1A",
      message,
      /* @__PURE__ */ p3("br", {}),
      t2("currentRuleVersion"),
      "\uFF1A",
      date
    ] });
  };
  function SyncLatest(props) {
    let { request: request2 } = props, [localBuildinConfigUpdatedAt, setLocalBuildinConfigUpdatedAt] = P2(null), { t: t2 } = useI18n(), [remoteConfig, setRemoteConfig] = P2(null), [isNeedUpdate, setIsNeedUpdate] = P2(null), [syncErrorMessage, setSyncErrorMessage] = P2(""), [isSyncSuccess, setIsSyncSuccess] = P2(!1), [isInvalidLocalVersion, setIsInvalidLocalVersion] = P2(!1), [isBadUserscriptBrowser, setIsBadUserscriptBrowser] = P2(!1), [config, setConfig] = P2(null), [_isLatestVersion, setIsLatestVersion] = P2(null), version = getVersion(), handleSyncing = async () => {
      setSyncErrorMessage("");
      let finalRemoteConfig = remoteConfig;
      if (remoteConfig === null)
        try {
          let response = await request2({
            url: buildinConfigSyncUrl
          });
          response ? (setRemoteConfig(response), finalRemoteConfig = response, setIsSyncSuccess(!0)) : (setSyncErrorMessage(t2("unknownError")), setIsNeedUpdate(null));
        } catch (e) {
          setIsNeedUpdate(null), setSyncErrorMessage(e.message);
          return;
        }
      finalRemoteConfig !== null ? (props.setStorageBuildinConfig(finalRemoteConfig), setIsNeedUpdate(!1), setLocalBuildinConfigUpdatedAt(finalRemoteConfig.buildinConfigUpdatedAt)) : (setSyncErrorMessage(t2("canNotFetchRemoteRule")), setIsNeedUpdate(null));
    };
    return j2(() => {
      getConfig().then((localConfig) => {
        let localConfigUpdatedAtIsoString = localConfig.buildinConfigUpdatedAt;
        setConfig(localConfig);
        let localConfigUpdatedAt = new Date(localConfigUpdatedAtIsoString);
        if (setLocalBuildinConfigUpdatedAt(localConfigUpdatedAtIsoString), version === "0.0.0") {
          setIsBadUserscriptBrowser(!0);
          return;
        }
        request2({
          url: buildinConfigSyncUrl
        }).then((response) => {
          let data = response, remoteMinVersion = data.minVersion, localVersion = version;
          setRemoteConfig(data);
          let remoteVersion = data.latestVersion;
          if (remoteVersion && (isAVersionGreaterOrEqualWithB(localVersion, remoteVersion) ? setIsLatestVersion(!0) : setIsLatestVersion(!1)), isAVersionGreaterOrEqualWithB(localVersion, remoteMinVersion)) {
            let latestIsoTime = data.buildinConfigUpdatedAt;
            new Date(latestIsoTime) > localConfigUpdatedAt ? (setIsNeedUpdate(!0), handleSyncing()) : setIsNeedUpdate(!1);
          } else
            setIsInvalidLocalVersion(!0), setIsNeedUpdate(null);
        }).catch((e) => {
          setIsNeedUpdate(null), setSyncErrorMessage(e.message);
        });
      });
    }, []), j2(() => {
      getConfig().then((config2) => {
        setConfig(config2);
      });
    }, [localBuildinConfigUpdatedAt]), config ? /* @__PURE__ */ p3("div", { class: "text-sm mt-2", style: { maxWidth: 218 }, children: isBadUserscriptBrowser ? /* @__PURE__ */ p3(BadUserscriptBrowser, {}) : syncErrorMessage ? /* @__PURE__ */ p3(
      SyncFailed,
      {
        handleSyncing,
        message: syncErrorMessage,
        date: localBuildinConfigUpdatedAt || ""
      }
    ) : isInvalidLocalVersion ? /* @__PURE__ */ p3(LocalVersionIsTooOld, { minVersion: remoteConfig.minVersion }) : isNeedUpdate === null || isNeedUpdate === !0 ? null : isSyncSuccess ? /* @__PURE__ */ p3(SyncSuccess, { date: localBuildinConfigUpdatedAt }) : null }) : null;
  }