
  // sync/authorize.ts
  var VALIDATION_BASE_URL = "https://www.googleapis.com/oauth2/v3/tokeninfo", CLIENT_ID = GOOGLE_CLIENT_ID, REDIRECT_URL = getEnv().REDIRECT_URL;
  function validate(accessToken) {
    if (!accessToken)
      throw "Authorization failure";
    let validationURL = `${VALIDATION_BASE_URL}?access_token=${accessToken}`, validationRequest = new Request(validationURL, {
      method: "GET"
    });
    function checkResponse(response) {
      return new Promise((resolve, reject) => {
        response.status != 200 && reject("Token validation error"), response.json().then((json) => {
          json.aud && json.aud === CLIENT_ID ? resolve(accessToken) : reject("Token validation error");
        });
      });
    }
    return fetch(validationRequest).then(checkResponse);
  }
  function getAuthInfo(state, userscriptSyncStartAuthFlow = !1) {
    return new GoogleAuth(state, REDIRECT_URL).auth(userscriptSyncStartAuthFlow);
  }