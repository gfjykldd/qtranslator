
  // sync/google_drive_api.ts
  var GoogleDriveAPI = class {
    constructor(accessToken) {
      this.accessToken = accessToken;
    }
    async listAll() {
      let result = [], pageToken = "";
      do {
        let { nextPageToken, files } = await this.list(pageToken).catch(
          (error) => {
            throw error;
          }
        );
        result.push(...files), pageToken = nextPageToken || "";
      } while (pageToken);
      return result;
    }
    async getConfig(id) {
      try {
        return await (await fetch(
          `https://www.googleapis.com/drive/v3/files/${id}?alt=media`,
          {
            headers: {
              Authorization: `Bearer ${this.accessToken}`
            }
          }
        )).json();
      } catch (_e2) {
        return log_default.error("get config error, use default", _e2), {};
      }
    }
    async delete(id) {
      await fetch(`https://www.googleapis.com/drive/v3/files/${id}`, {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${this.accessToken}`
        }
      });
    }
    findByName(fileName) {
      return this.list(void 0, `name = '${fileName}'`);
    }
    uploadConfig(settings, filename = LATEST_FILE_NAME) {
      let blob = new Blob([JSON.stringify(settings, null, 2)], {
        type: "application/json"
      });
      return this.upload(
        {
          name: filename,
          parents: ["appDataFolder"],
          mimeType: "application/json"
        },
        blob
      );
    }
    updateConfig(id, settings) {
      let blob = new Blob([JSON.stringify(settings, null, 2)], {
        type: "application/json"
      });
      return this.updateContent(id, blob);
    }
    async upload(metadata, blob) {
      let data = new FormData();
      data.append(
        "metadata",
        new Blob([JSON.stringify(metadata)], {
          type: "application/json; charset=UTF-8"
        })
      ), data.append("file", blob);
      let res = await fetch(
        "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart",
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${this.accessToken}`
          },
          body: data
        }
      );
      return res.ok ? await res.json() : Promise.reject(res.text());
    }
    async list(pageToken, query) {
      let url = new URL("https://www.googleapis.com/drive/v3/files");
      pageToken && url.searchParams.append("pageToken", pageToken), query && url.searchParams.append("q", query), url.searchParams.append("spaces", "appDataFolder"), url.searchParams.append(
        "fields",
        "files(id,name,createdTime,modifiedTime,size)"
      ), url.searchParams.append("pageSize", "100"), url.searchParams.append("orderBy", "createdTime desc");
      try {
        return log_default.debug("list api:", url.toString(), this.accessToken), await (await fetch(url.toString(), {
          headers: {
            Authorization: `Bearer ${this.accessToken}`
          }
        })).json();
      } catch (e) {
        throw log_default.error("fetch google ip error", e), e;
      }
    }
    async updateContent(id, blob) {
      return await (await fetch(
        `https://www.googleapis.com/upload/drive/v3/files/${id}?uploadType=media`,
        {
          method: "PATCH",
          headers: {
            Authorization: `Bearer ${this.accessToken}`
          },
          body: blob
        }
      )).text();
    }
  };