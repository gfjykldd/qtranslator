
  // sync/util.ts
  function getAuthUrl(state, redirect_url) {
    let SCOPES = ["https://www.googleapis.com/auth/drive.appdata"];
    return `https://accounts.google.com/o/oauth2/v2/auth?client_id=${GOOGLE_CLIENT_ID}&response_type=token&redirect_uri=${encodeURIComponent(redirect_url)}&scope=${encodeURIComponent(SCOPES.join(" "))}&state=${encodeURIComponent(JSON.stringify(state))}`;
  }
  function extractAccessToken(redirectUri) {
    let m2 = redirectUri.match(/[#?](.*)/);
    return !m2 || m2.length < 1 ? null : {
      access_token: new URLSearchParams(m2[1].split("#")[0]).get("access_token")
    };
  }
  async function autoSyncStrategy(accessToken, settings, handleChangeValue, handleUpdateLocalConfigLastSyncedAt, handleUpdateSettingUpdateAt, handleSuccess, handleFail) {
    log_default.debug(`autoSyncStrategy accessToken: ${accessToken}`);
    let api = new GoogleDriveAPI(accessToken);
    try {
      let files = (await api.findByName(LATEST_FILE_NAME)).files;
      log_default.debug("files", files);
      let latestFileId = files[0]?.id, latestRemoteConfigResult = null;
      if (latestFileId && (latestRemoteConfigResult = await api.getConfig(latestFileId).then((config) => ({
        fileId: latestFileId,
        config
      }))), latestRemoteConfigResult) {
        let { config: latestRemoteConfig, fileId } = latestRemoteConfigResult, remoteUpdatedAt = latestRemoteConfig.updatedAt ? new Date(latestRemoteConfig.updatedAt) : /* @__PURE__ */ new Date(0), localUpdatedAt = settings.updatedAt ? new Date(settings.updatedAt) : /* @__PURE__ */ new Date(0);
        if (log_default.debug(
          "remoteUpdatedAt",
          remoteUpdatedAt,
          "localUpdatedAt",
          localUpdatedAt
        ), remoteUpdatedAt > localUpdatedAt)
          log_default.debug("remote is newer, update local config", latestRemoteConfig), handleChangeValue(latestRemoteConfig), handleSuccess && handleSuccess(!0);
        else if (remoteUpdatedAt.getTime() === localUpdatedAt.getTime())
          log_default.debug("remote and local are the same, do nothing"), handleSuccess && handleSuccess(!1);
        else if (remoteUpdatedAt < localUpdatedAt)
          log_default.debug("local is newer, update remote config", settings), await api.updateConfig(fileId, settings), handleSuccess && handleSuccess(!0);
        else {
          handleFail && handleFail(": unknown error");
          return;
        }
        handleUpdateLocalConfigLastSyncedAt((/* @__PURE__ */ new Date()).toISOString());
      } else if (latestRemoteConfigResult === null)
        if (settings) {
          if (!settings.updatedAt) {
            let newDate = (/* @__PURE__ */ new Date()).toISOString();
            handleUpdateSettingUpdateAt(newDate), settings.updatedAt = newDate;
          }
          await api.uploadConfig(settings), handleUpdateLocalConfigLastSyncedAt((/* @__PURE__ */ new Date()).toISOString()), handleSuccess && handleSuccess(!0);
        } else
          handleFail && handleFail(": Local Config is empty");
      else
        handleFail && handleFail(": latestConfig is " + latestRemoteConfigResult);
    } catch (e) {
      log_default.error("syncLatestWithDrive error", e), handleFail && handleFail(": " + e.message);
    }
  }