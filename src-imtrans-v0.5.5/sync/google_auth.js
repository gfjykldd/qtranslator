
  // sync/google_auth.ts
  var GOOGLE_REVOKE_URL = "https://oauth2.googleapis.com/revoke", GoogleAuth = class {
    constructor(state, redirectUrl) {
      this.CLASSNAME = "GoogleAuth";
      this._state = state, this._redirectUrl = redirectUrl;
    }
    static revoke(token) {
      let url = `${GOOGLE_REVOKE_URL}?token=${token}`;
      return fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      }).then(async (res) => (await GoogleAuth.removeAuthInfo(), isUserscriptRuntime() && globalThis.location.reload(), res));
    }
    static async getAuthInfo() {
      let tokenIndex = await browserAPI.storage.local.get(GOOGLE_ACCESS_TOKEN_KEY);
      if (tokenIndex[GOOGLE_ACCESS_TOKEN_KEY])
        return tokenIndex[GOOGLE_ACCESS_TOKEN_KEY];
    }
    static async removeAuthInfo() {
      return await browserAPI.storage.local.remove(GOOGLE_ACCESS_TOKEN_KEY);
    }
    static setAuthInfo(authInfo) {
      return browserAPI.storage.local.set({
        [GOOGLE_ACCESS_TOKEN_KEY]: authInfo
      });
    }
    async auth(userscriptSyncPageStartAuthFlow = !1) {
      let authInfo = await GoogleAuth.getAuthInfo();
      if (log_default.debug(this.CLASSNAME, "token from cache:", authInfo), authInfo && authInfo.access_token && await validate(authInfo.access_token).then((_token) => !0).catch((_err) => !1))
        return Promise.resolve(authInfo);
      let authUrlWithState = getAuthUrl(
        this._state,
        this._redirectUrl
      );
      return log_default.debug(this.CLASSNAME, "auth url: " + authUrlWithState), isUserscriptRuntime() ? this.userscriptAuthWorkflow(
        authUrlWithState,
        userscriptSyncPageStartAuthFlow
      ) : this.extensionAuthWorkflow(authUrlWithState).then((res) => (GoogleAuth.setAuthInfo(res), res));
    }
    async userscriptAuthWorkflow(authUrl, syncPageStartAuthFlow) {
      return syncPageStartAuthFlow && await browserAPI.storage.local.set({ [AUTH_FLOW_FLAG]: !0 }), globalThis.open(authUrl, "_self"), Promise.resolve({});
    }
    extensionAuthWorkflow(authUrl) {
      let _tabId, _success = !1;
      return new Promise((resolve, reject) => {
        let cleanup = () => {
          browserAPI.tabs.onUpdated.removeListener(tabUpdateListener), browserAPI.tabs.onRemoved.removeListener(tabRemovedListener);
        }, tabUpdateListener = (tabId, _changeInfo, tab) => {
          if (log_default.debug(this.CLASSNAME, "create tab onUpdated: " + tab.url), _tabId === tabId) {
            let url = new URL(tab.url || ""), authInfo = extractAccessToken(tab.url);
            url.pathname.startsWith("/auth-done") && authInfo?.access_token && (log_default.debug(this.CLASSNAME, "auth done: " + tab.url), resolve({ access_token: authInfo.access_token }), _success = !0, browserAPI.tabs.remove(tabId), cleanup());
          }
        }, tabRemovedListener = (tabId, _removeInfo) => {
          log_default.debug(this.CLASSNAME, "create tab onRemoved: " + tabId), (tabId === _tabId || !_success) && (cleanup(), reject(new Error("auth failed")));
        }, width = Math.min(500, screen.availWidth), height = Math.min(650, screen.availHeight), newTab;
        browserAPI.windows === void 0 ? newTab = browserAPI.tabs.create({
          url: authUrl
        }).then((tab) => {
          _tabId = tab.id;
        }) : newTab = browserAPI.windows.create({
          url: authUrl,
          type: "popup",
          width,
          height,
          left: Math.round((screen.width - width) / 2),
          top: Math.round((screen.height - height) / 2)
        }).then((window2) => {
          _tabId = window2.tabs[0].id;
        }), newTab.then(() => {
          browserAPI.tabs.onUpdated.addListener(tabUpdateListener), browserAPI.tabs.onRemoved.addListener(tabRemovedListener);
        }).catch((error) => {
          log_default.debug(this.CLASSNAME, "create tab failed: " + error), reject(error);
        });
      });
    }
  };