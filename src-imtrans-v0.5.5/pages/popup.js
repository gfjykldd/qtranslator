
  // pages/popup.tsx
  var callbacksWhenReady = {}, readyTabs = {};
  function callWhenReady(tabId, callback) {
    if (readyTabs[tabId])
      try {
        callback();
      } catch (e) {
        log_default.error("run callback failed", e);
      }
    else
      callbacksWhenReady[tabId] || (callbacksWhenReady[tabId] = []), callbacksWhenReady[tabId].push(callback);
  }
  function runCallbacksWhenReady(tabId) {
    if (callbacksWhenReady[tabId] && callbacksWhenReady[tabId].length) {
      let callbacks = [...callbacksWhenReady[tabId]];
      callbacksWhenReady[tabId] = [], callbacks.forEach((cb) => cb());
    }
  }
  function Popup2() {
    let [pageStatus, setPageStatus] = P2("Original"), [settings, setSettings, _isPersistent, _error] = useUserConfig(), [config, setConfig] = P2(null), [currentUrl, setCurrentUrl] = P2(null), [currentTabId, setCurrentTabId] = P2(null), [currentLang, setCurrentLang] = P2("auto"), [ctx, setContext] = P2(null), messageHandler = (e) => {
      let { tabId, payload } = e.detail, { method, data } = payload;
      log_default.debug("popup received message", method, data || " "), method === "setPageStatus" && tabId && setPageStatus(data);
    }, handleToggleEnabled = () => {
      setSettings((state) => ({
        ...state,
        enabled: !state.enabled
      })), setTimeout(() => {
        handleClose();
      }, 50);
    };
    j2(() => {
      let c3 = getConnection();
      return browserAPI.tabs.query({
        currentWindow: !0,
        active: !0
      }).then((tabs) => {
        let tabId = tabs[0].id;
        setCurrentTabId(tabId);
        let tabUrl = tabs[0].url;
        browserAPI.tabs.onUpdated.addListener((tabId2, _changeInfo, tab) => {
          tabId2 === tabId2 && tab.url && currentUrl && tab.url !== currentUrl && (setCurrentUrl(tab.url), window.location.reload());
        }), globalThis.document.addEventListener(
          popupReceiveMessageEventName,
          messageHandler
        ), tabUrl && setCurrentUrl(tabUrl), tabUrl && isValidHtmlUrl(tabUrl) && (setCurrentUrl(tabUrl), c3.sendMessage(`content_script:main_sync:${tabId}`, {
          method: "ping"
        }).then((_response) => {
          readyTabs[tabId] = !0, runCallbacksWhenReady(tabId);
        }).catch((_e3) => {
          log_default.debug(
            "ping failed, but it is ok. cause maybe content is not injected",
            _e3
          );
        }), callWhenReady(tabId, async () => {
          let result = await c3.sendMessage(
            `content_script:main_sync:${tabId}`,
            {
              method: "getPageStatus"
            }
          );
          setPageStatus(result);
        }), callWhenReady(tabId, async () => {
          let result = await getConfig();
          setConfig(result);
        }), callWhenReady(tabId, async () => {
          let lang = await c3.sendMessage(
            `content_script:main_sync:${tabId}`,
            {
              method: "getCurrentPageLanguage"
            }
          );
          setCurrentLang(lang);
        }));
      }), () => {
        globalThis.document.removeEventListener(
          popupReceiveMessageEventName,
          messageHandler
        );
      };
    }, []), j2(() => {
      getConfig().then((config2) => {
        setConfig(config2);
      });
    }, [settings]), j2(() => {
      currentUrl && config && getContext({
        url: currentUrl,
        config
      }).then((ctx2) => {
        setContext(ctx2);
      });
    }, [currentUrl, config]);
    let handleSendMessageToContent = (method, isClose = !0) => async () => {
      let c3 = getConnection(), tabId = (await browserAPI.tabs.query({
        currentWindow: !0,
        active: !0
      }))[0].id;
      c3.sendMessage(`content_script:main:${tabId}`, {
        method
      }), isClose && setTimeout(() => {
        globalThis.close();
      }, 10);
    }, handleTranslateLocalPdfFile = () => {
      let pdfViewerRuntimeUrl = browserAPI.runtime.getURL(pdfViewerUrl);
      browserAPI.tabs.create({
        url: pdfViewerRuntimeUrl
      }), globalThis.close();
    }, handleTranslateLocalHtmlFile = () => {
      let pdfViewerRuntimeUrl = browserAPI.runtime.getURL(htmlViewerUrl);
      browserAPI.tabs.create({
        url: pdfViewerRuntimeUrl
      }), globalThis.close();
    }, handleTranslateLocalSubtitleFile = () => {
      let pdfViewerRuntimeUrl = browserAPI.runtime.getURL(subtitleBuilderUrl);
      isSafari() && (pdfViewerRuntimeUrl = getEnv().SUBTITLE_BUILDER_URL), browserAPI.tabs.create({
        url: pdfViewerRuntimeUrl
      }), globalThis.close();
    }, handleTranslatePdf = () => {
      currentUrl && currentTabId && (browserAPI.tabs.update(currentTabId, {
        url: formatToPdfViewerUrl(currentUrl)
      }), globalThis.close());
    }, handleSetPageLanguage = (lang) => {
      let c3 = getConnection();
      if (setCurrentLang(lang), currentTabId && currentUrl) {
        let newSourceLanguageUrlPattern = handleSourceLanguageUrlPattern(
          currentUrl,
          lang,
          config.sourceLanguageUrlPattern
        );
        setSettings((state) => ({
          ...state,
          sourceLanguageUrlPattern: newSourceLanguageUrlPattern
        })), callWhenReady(currentTabId, () => {
          c3.sendMessage(`content_script:main:${currentTabId}`, {
            method: "setCurrentPageLanguageByClient",
            data: lang
          });
        });
      }
    }, handleClose = () => {
      globalThis.close();
    }, handleOpenOptionsPage = (pageRoute = "") => {
      if (isSafari()) {
        let optionsUrl = getEnv().OPTIONS_URL;
        browserAPI.tabs.create({
          url: optionsUrl + pageRoute
        });
      } else {
        let optionsUrl = browserAPI.runtime.getURL("options.html");
        browserAPI.tabs.create({
          url: optionsUrl + pageRoute
        });
      }
      setTimeout(() => {
        globalThis.close();
      }, 50);
    }, handleOpenAboutPage = () => {
      if (isSafari()) {
        let optionsUrl = getEnv().OPTIONS_URL;
        browserAPI.tabs.create({
          url: optionsUrl + "#about"
        });
      } else
        browserAPI.tabs.create({
          url: browserAPI.runtime.getURL("options.html#about")
        });
      setTimeout(() => {
        globalThis.close();
      }, 50);
    }, handleOpenEbookBuilderPage = () => {
      let url = browserAPI.runtime.getURL("ebook/make/index.html");
      isSafari() && (url = getEnv().EBOOK_BUILDER_URL), browserAPI.tabs.create({
        url
      }), setTimeout(() => {
        globalThis.close();
      }, 50);
    }, handleOpenEbookViewerPage = () => {
      browserAPI.tabs.create({
        url: browserAPI.runtime.getURL("ebook/index.html")
      }), setTimeout(() => {
        globalThis.close();
      }, 50);
    }, handleMouseTranslateTriggerConfig = (trigger) => {
      setSettings((state) => ({
        ...state,
        generalRule: {
          ...state.generalRule,
          mouseHoverHoldKey: trigger
        }
      }));
    };
    return !config || !ctx ? null : /* @__PURE__ */ p3(
      Popup,
      {
        onClose: handleClose,
        onToggleTranslate: handleSendMessageToContent("toggleTranslatePage"),
        openEbookBuilderPage: handleOpenEbookBuilderPage,
        openEbookViewerPage: handleOpenEbookViewerPage,
        onTranslateLocalSubtitleFile: handleTranslateLocalSubtitleFile,
        onTranslateLocalHtmlFile: handleTranslateLocalHtmlFile,
        onToggleEnabled: handleToggleEnabled,
        openOptionsPage: handleOpenOptionsPage,
        openAboutPage: handleOpenAboutPage,
        onTranslatePdf: handleTranslatePdf,
        onTranslateLocalPdfFile: handleTranslateLocalPdfFile,
        onTranslateTheMainPage: handleSendMessageToContent(
          "translateTheMainPage"
        ),
        onTranslateTheWholePage: handleSendMessageToContent(
          "translateTheWholePage"
        ),
        ontranslateToThePageEndImmediately: handleSendMessageToContent(
          "translateToThePageEndImmediately"
        ),
        onTranslatePage: handleSendMessageToContent("translatePage"),
        onRestorePage: handleSendMessageToContent("restorePage", !1),
        onSetPageLanguage: handleSetPageLanguage,
        onUserConfigChange: setSettings,
        config,
        pageStatus,
        ctx,
        currentUrl,
        currentLang,
        onSetLocalConfig: setLocalConfig,
        onSetBuildinConfig: setBuildinConfig,
        request,
        onMouseTriggerChanged: handleMouseTranslateTriggerConfig
      }
    );
  }
