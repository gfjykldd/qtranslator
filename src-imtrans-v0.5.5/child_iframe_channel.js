
  // child_iframe_channel.ts
  var theChannel;
  async function setupChildIframeChannel() {
    if (getIsInIframe()) {
      let topFrame = globalThis;
      for (; topFrame.top != topFrame.self; )
        topFrame = topFrame.top;
      let topFrameInstance = ProtoframePubsub.iframe(
        childFrameToRootFrameIdentifier,
        "*",
        {
          targetWindow: topFrame
        }
      );
      theChannel = topFrameInstance, await ProtoframePubsub.connect(topFrameInstance).catch((e) => {
        log_default.error("connect with parent frame error", e);
      });
    }
  }
  function getIframeMessageChannel() {
    return theChannel;
  }