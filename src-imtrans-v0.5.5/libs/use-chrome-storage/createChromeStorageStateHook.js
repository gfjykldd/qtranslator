
  // libs/use-chrome-storage/createChromeStorageStateHook.ts
  function createChromeStorageStateHook(key, initialValue, storageArea) {
    let consumers = [];
    return function() {
      let [value, setValue, isPersistent, error2] = useChromeStorage(
        key,
        initialValue,
        storageArea
      ), setValueAll = L2((newValue) => {
        for (let consumer of consumers)
          consumer(newValue);
      }, []);
      return j2(() => (consumers.push(setValue), () => {
        consumers.splice(consumers.indexOf(setValue), 1);
      }), [setValue]), [value, setValueAll, isPersistent, error2];
    };
  }