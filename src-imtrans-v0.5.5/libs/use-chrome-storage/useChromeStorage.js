
  // libs/use-chrome-storage/useChromeStorage.ts
  function useChromeStorage(key, initialValue, storageArea) {
    let [INITIAL_VALUE3] = P2(() => typeof initialValue == "function" ? initialValue() : initialValue), [STORAGE_AREA] = P2(storageArea), [state, setState] = P2(INITIAL_VALUE3), [isPersistent, setIsPersistent] = P2(!1), [error2, setError] = P2("");
    j2(() => {
      storage.get(key, INITIAL_VALUE3, STORAGE_AREA).then((res) => {
        res[key] && setState(res[key]), setIsPersistent(!0), setError("");
      }).catch((error3) => {
        setIsPersistent(!1), setError(error3);
      });
    }, [key, INITIAL_VALUE3, STORAGE_AREA]);
    let updateValue = L2(
      // @ts-ignore: npm package is not typed
      (newValue) => {
        let toStore = typeof newValue == "function" ? newValue(state) : newValue;
        log_default.debug("new settings", toStore), storage.set(key, toStore, STORAGE_AREA).then(() => {
          setState(toStore), setIsPersistent(!0), setError("");
        }).catch((error3) => {
          setState(toStore), setIsPersistent(!1), setError(error3);
        });
      },
      [STORAGE_AREA, key, state]
    );
    return [state, updateValue, isPersistent, error2];
  }