
  // libs/use-chrome-storage/mod.ts
  function createChromeStorageStateHookLocal(key, initialValue) {
    return createChromeStorageStateHook(key, initialValue, "local");
  }
  function createChromeStorageStateHookSync(key, initialValue) {
    return createChromeStorageStateHook(key, initialValue, "sync");
  }