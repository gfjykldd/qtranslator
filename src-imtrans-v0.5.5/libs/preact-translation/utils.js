
  // libs/preact-translation/utils.ts
  function format(str, params) {
    let result = str;
    return params && Object.keys(params).forEach((key) => {
      let rawValue = params[key], keyIsNumber = isNumber(key);
      if (typeof rawValue == "object" || keyIsNumber) {
        let value = rawValue;
        keyIsNumber && typeof value == "string" && (value = {
          tag: "a",
          href: value,
          target: "_blank"
        });
        let tag = `<${key}>`, tagIndex = result.indexOf(tag);
        if (tagIndex !== -1) {
          let actualTag = value.tag || "a", endIndex = result.indexOf(`</${key}>`);
          if (endIndex !== -1) {
            let html = result.substring(tagIndex + tag.length, endIndex), attrs = Object.keys(value).filter((attr) => attr !== "tag").map((attr) => `${attr}="${value[attr]}"`).join(" ");
            result = result.replace(
              `${tag}${html}</${key}>`,
              `<${actualTag} ${attrs}>${html}</${actualTag}>`
            );
          }
        }
      } else {
        let template = new RegExp("{" + key + "}", "gm");
        result = result.replace(template, rawValue.toString());
      }
    }), result;
  }
  function getValue(languageData, lang, key) {
    let localeData = languageData[lang];
    if (!localeData)
      return key;
    let keys = key.split("."), propKey = "";
    do {
      propKey += keys.shift();
      let value = localeData[propKey];
      value !== void 0 && (typeof value == "object" || !keys.length) ? (localeData = value, propKey = "") : keys.length ? propKey += "." : localeData = key;
    } while (keys.length);
    return localeData;
  }
  function t(data, key, lang, fallbackLang, params) {
    if (!data.hasOwnProperty(lang))
      return key;
    let value = getValue(data, lang, key);
    return value === key && lang !== fallbackLang && (value = getValue(data, fallbackLang, key)), format(value, params);
  }
  function isNumber(value) {
    if (typeof value == "number")
      return !0;
    if (value) {
      let num = parseInt(value);
      return !isNaN(num);
    } else
      return !1;
  }
