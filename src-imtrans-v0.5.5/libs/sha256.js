
  // libs/sha256.js
  var ERROR = "input is invalid type", WINDOW = typeof window == "object", root = WINDOW ? window : {};
  root.JS_SHA256_NO_WINDOW && (WINDOW = !1);
  var WEB_WORKER = !WINDOW && typeof self == "object", NODE_JS = !root.JS_SHA256_NO_NODE_JS && typeof process == "object" && process.versions && process.versions.node;
  NODE_JS ? root = global : WEB_WORKER && (root = self);
  var COMMON_JS = !root.JS_SHA256_NO_COMMON_JS && typeof module == "object" && module.exports, AMD = typeof define == "function" && define.amd, ARRAY_BUFFER = !root.JS_SHA256_NO_ARRAY_BUFFER && typeof ArrayBuffer < "u", HEX_CHARS = "0123456789abcdef".split(""), EXTRA = [-2147483648, 8388608, 32768, 128], SHIFT = [24, 16, 8, 0], K5 = [
    1116352408,
    1899447441,
    3049323471,
    3921009573,
    961987163,
    1508970993,
    2453635748,
    2870763221,
    3624381080,
    310598401,
    607225278,
    1426881987,
    1925078388,
    2162078206,
    2614888103,
    3248222580,
    3835390401,
    4022224774,
    264347078,
    604807628,
    770255983,
    1249150122,
    1555081692,
    1996064986,
    2554220882,
    2821834349,
    2952996808,
    3210313671,
    3336571891,
    3584528711,
    113926993,
    338241895,
    666307205,
    773529912,
    1294757372,
    1396182291,
    1695183700,
    1986661051,
    2177026350,
    2456956037,
    2730485921,
    2820302411,
    3259730800,
    3345764771,
    3516065817,
    3600352804,
    4094571909,
    275423344,
    430227734,
    506948616,
    659060556,
    883997877,
    958139571,
    1322822218,
    1537002063,
    1747873779,
    1955562222,
    2024104815,
    2227730452,
    2361852424,
    2428436474,
    2756734187,
    3204031479,
    3329325298
  ], OUTPUT_TYPES = ["hex", "array", "digest", "arrayBuffer"], blocks = [];
  (root.JS_SHA256_NO_NODE_JS || !Array.isArray) && (Array.isArray = function(obj) {
    return Object.prototype.toString.call(obj) === "[object Array]";
  });
  ARRAY_BUFFER && (root.JS_SHA256_NO_ARRAY_BUFFER_IS_VIEW || !ArrayBuffer.isView) && (ArrayBuffer.isView = function(obj) {
    return typeof obj == "object" && obj.buffer && obj.buffer.constructor === ArrayBuffer;
  });
  var createOutputMethod = function(outputType, is224) {
    return function(message) {
      return new Sha256(is224, !0).update(message)[outputType]();
    };
  }, createMethod = function(is224) {
    var method = createOutputMethod("hex", is224);
    method.create = function() {
      return new Sha256(is224);
    }, method.update = function(message) {
      return method.create().update(message);
    };
    for (var i4 = 0; i4 < OUTPUT_TYPES.length; ++i4) {
      var type = OUTPUT_TYPES[i4];
      method[type] = createOutputMethod(type, is224);
    }
    return method;
  }, createHmacOutputMethod = function(outputType, is224) {
    return function(key, message) {
      return new HmacSha256(key, is224, !0).update(message)[outputType]();
    };
  }, createHmacMethod = function(is224) {
    var method = createHmacOutputMethod("hex", is224);
    method.create = function(key) {
      return new HmacSha256(key, is224);
    }, method.update = function(key, message) {
      return method.create(key).update(message);
    };
    for (var i4 = 0; i4 < OUTPUT_TYPES.length; ++i4) {
      var type = OUTPUT_TYPES[i4];
      method[type] = createHmacOutputMethod(type, is224);
    }
    return method;
  };
  function Sha256(is224, sharedMemory) {
    sharedMemory ? (blocks[0] = blocks[16] = blocks[1] = blocks[2] = blocks[3] = blocks[4] = blocks[5] = blocks[6] = blocks[7] = blocks[8] = blocks[9] = blocks[10] = blocks[11] = blocks[12] = blocks[13] = blocks[14] = blocks[15] = 0, this.blocks = blocks) : this.blocks = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], is224 ? (this.h0 = 3238371032, this.h1 = 914150663, this.h2 = 812702999, this.h3 = 4144912697, this.h4 = 4290775857, this.h5 = 1750603025, this.h6 = 1694076839, this.h7 = 3204075428) : (this.h0 = 1779033703, this.h1 = 3144134277, this.h2 = 1013904242, this.h3 = 2773480762, this.h4 = 1359893119, this.h5 = 2600822924, this.h6 = 528734635, this.h7 = 1541459225), this.block = this.start = this.bytes = this.hBytes = 0, this.finalized = this.hashed = !1, this.first = !0, this.is224 = is224;
  }
  Sha256.prototype.update = function(message) {
    if (!this.finalized) {
      var notString, type = typeof message;
      if (type !== "string") {
        if (type === "object") {
          if (message === null)
            throw new Error(ERROR);
          if (ARRAY_BUFFER && message.constructor === ArrayBuffer)
            message = new Uint8Array(message);
          else if (!Array.isArray(message) && (!ARRAY_BUFFER || !ArrayBuffer.isView(message)))
            throw new Error(ERROR);
        } else
          throw new Error(ERROR);
        notString = !0;
      }
      for (var code2, index = 0, i4, length = message.length, blocks2 = this.blocks; index < length; ) {
        if (this.hashed && (this.hashed = !1, blocks2[0] = this.block, blocks2[16] = blocks2[1] = blocks2[2] = blocks2[3] = blocks2[4] = blocks2[5] = blocks2[6] = blocks2[7] = blocks2[8] = blocks2[9] = blocks2[10] = blocks2[11] = blocks2[12] = blocks2[13] = blocks2[14] = blocks2[15] = 0), notString)
          for (i4 = this.start; index < length && i4 < 64; ++index)
            blocks2[i4 >> 2] |= message[index] << SHIFT[i4++ & 3];
        else
          for (i4 = this.start; index < length && i4 < 64; ++index)
            code2 = message.charCodeAt(index), code2 < 128 ? blocks2[i4 >> 2] |= code2 << SHIFT[i4++ & 3] : code2 < 2048 ? (blocks2[i4 >> 2] |= (192 | code2 >> 6) << SHIFT[i4++ & 3], blocks2[i4 >> 2] |= (128 | code2 & 63) << SHIFT[i4++ & 3]) : code2 < 55296 || code2 >= 57344 ? (blocks2[i4 >> 2] |= (224 | code2 >> 12) << SHIFT[i4++ & 3], blocks2[i4 >> 2] |= (128 | code2 >> 6 & 63) << SHIFT[i4++ & 3], blocks2[i4 >> 2] |= (128 | code2 & 63) << SHIFT[i4++ & 3]) : (code2 = 65536 + ((code2 & 1023) << 10 | message.charCodeAt(++index) & 1023), blocks2[i4 >> 2] |= (240 | code2 >> 18) << SHIFT[i4++ & 3], blocks2[i4 >> 2] |= (128 | code2 >> 12 & 63) << SHIFT[i4++ & 3], blocks2[i4 >> 2] |= (128 | code2 >> 6 & 63) << SHIFT[i4++ & 3], blocks2[i4 >> 2] |= (128 | code2 & 63) << SHIFT[i4++ & 3]);
        this.lastByteIndex = i4, this.bytes += i4 - this.start, i4 >= 64 ? (this.block = blocks2[16], this.start = i4 - 64, this.hash(), this.hashed = !0) : this.start = i4;
      }
      return this.bytes > 4294967295 && (this.hBytes += this.bytes / 4294967296 << 0, this.bytes = this.bytes % 4294967296), this;
    }
  };
  Sha256.prototype.finalize = function() {
    if (!this.finalized) {
      this.finalized = !0;
      var blocks2 = this.blocks, i4 = this.lastByteIndex;
      blocks2[16] = this.block, blocks2[i4 >> 2] |= EXTRA[i4 & 3], this.block = blocks2[16], i4 >= 56 && (this.hashed || this.hash(), blocks2[0] = this.block, blocks2[16] = blocks2[1] = blocks2[2] = blocks2[3] = blocks2[4] = blocks2[5] = blocks2[6] = blocks2[7] = blocks2[8] = blocks2[9] = blocks2[10] = blocks2[11] = blocks2[12] = blocks2[13] = blocks2[14] = blocks2[15] = 0), blocks2[14] = this.hBytes << 3 | this.bytes >>> 29, blocks2[15] = this.bytes << 3, this.hash();
    }
  };
  Sha256.prototype.hash = function() {
    var a6 = this.h0, b8 = this.h1, c5 = this.h2, d7 = this.h3, e3 = this.h4, f8 = this.h5, g8 = this.h6, h5 = this.h7, blocks2 = this.blocks, j6, s0, s1, maj, t1, t22, ch, ab, da, cd, bc;
    for (j6 = 16; j6 < 64; ++j6)
      t1 = blocks2[j6 - 15], s0 = (t1 >>> 7 | t1 << 25) ^ (t1 >>> 18 | t1 << 14) ^ t1 >>> 3, t1 = blocks2[j6 - 2], s1 = (t1 >>> 17 | t1 << 15) ^ (t1 >>> 19 | t1 << 13) ^ t1 >>> 10, blocks2[j6] = blocks2[j6 - 16] + s0 + blocks2[j6 - 7] + s1 << 0;
    for (bc = b8 & c5, j6 = 0; j6 < 64; j6 += 4)
      this.first ? (this.is224 ? (ab = 300032, t1 = blocks2[0] - 1413257819, h5 = t1 - 150054599 << 0, d7 = t1 + 24177077 << 0) : (ab = 704751109, t1 = blocks2[0] - 210244248, h5 = t1 - 1521486534 << 0, d7 = t1 + 143694565 << 0), this.first = !1) : (s0 = (a6 >>> 2 | a6 << 30) ^ (a6 >>> 13 | a6 << 19) ^ (a6 >>> 22 | a6 << 10), s1 = (e3 >>> 6 | e3 << 26) ^ (e3 >>> 11 | e3 << 21) ^ (e3 >>> 25 | e3 << 7), ab = a6 & b8, maj = ab ^ a6 & c5 ^ bc, ch = e3 & f8 ^ ~e3 & g8, t1 = h5 + s1 + ch + K5[j6] + blocks2[j6], t22 = s0 + maj, h5 = d7 + t1 << 0, d7 = t1 + t22 << 0), s0 = (d7 >>> 2 | d7 << 30) ^ (d7 >>> 13 | d7 << 19) ^ (d7 >>> 22 | d7 << 10), s1 = (h5 >>> 6 | h5 << 26) ^ (h5 >>> 11 | h5 << 21) ^ (h5 >>> 25 | h5 << 7), da = d7 & a6, maj = da ^ d7 & b8 ^ ab, ch = h5 & e3 ^ ~h5 & f8, t1 = g8 + s1 + ch + K5[j6 + 1] + blocks2[j6 + 1], t22 = s0 + maj, g8 = c5 + t1 << 0, c5 = t1 + t22 << 0, s0 = (c5 >>> 2 | c5 << 30) ^ (c5 >>> 13 | c5 << 19) ^ (c5 >>> 22 | c5 << 10), s1 = (g8 >>> 6 | g8 << 26) ^ (g8 >>> 11 | g8 << 21) ^ (g8 >>> 25 | g8 << 7), cd = c5 & d7, maj = cd ^ c5 & a6 ^ da, ch = g8 & h5 ^ ~g8 & e3, t1 = f8 + s1 + ch + K5[j6 + 2] + blocks2[j6 + 2], t22 = s0 + maj, f8 = b8 + t1 << 0, b8 = t1 + t22 << 0, s0 = (b8 >>> 2 | b8 << 30) ^ (b8 >>> 13 | b8 << 19) ^ (b8 >>> 22 | b8 << 10), s1 = (f8 >>> 6 | f8 << 26) ^ (f8 >>> 11 | f8 << 21) ^ (f8 >>> 25 | f8 << 7), bc = b8 & c5, maj = bc ^ b8 & d7 ^ cd, ch = f8 & g8 ^ ~f8 & h5, t1 = e3 + s1 + ch + K5[j6 + 3] + blocks2[j6 + 3], t22 = s0 + maj, e3 = a6 + t1 << 0, a6 = t1 + t22 << 0;
    this.h0 = this.h0 + a6 << 0, this.h1 = this.h1 + b8 << 0, this.h2 = this.h2 + c5 << 0, this.h3 = this.h3 + d7 << 0, this.h4 = this.h4 + e3 << 0, this.h5 = this.h5 + f8 << 0, this.h6 = this.h6 + g8 << 0, this.h7 = this.h7 + h5 << 0;
  };
  Sha256.prototype.hex = function() {
    this.finalize();
    var h0 = this.h0, h1 = this.h1, h22 = this.h2, h32 = this.h3, h42 = this.h4, h5 = this.h5, h6 = this.h6, h7 = this.h7, hex2 = HEX_CHARS[h0 >> 28 & 15] + HEX_CHARS[h0 >> 24 & 15] + HEX_CHARS[h0 >> 20 & 15] + HEX_CHARS[h0 >> 16 & 15] + HEX_CHARS[h0 >> 12 & 15] + HEX_CHARS[h0 >> 8 & 15] + HEX_CHARS[h0 >> 4 & 15] + HEX_CHARS[h0 & 15] + HEX_CHARS[h1 >> 28 & 15] + HEX_CHARS[h1 >> 24 & 15] + HEX_CHARS[h1 >> 20 & 15] + HEX_CHARS[h1 >> 16 & 15] + HEX_CHARS[h1 >> 12 & 15] + HEX_CHARS[h1 >> 8 & 15] + HEX_CHARS[h1 >> 4 & 15] + HEX_CHARS[h1 & 15] + HEX_CHARS[h22 >> 28 & 15] + HEX_CHARS[h22 >> 24 & 15] + HEX_CHARS[h22 >> 20 & 15] + HEX_CHARS[h22 >> 16 & 15] + HEX_CHARS[h22 >> 12 & 15] + HEX_CHARS[h22 >> 8 & 15] + HEX_CHARS[h22 >> 4 & 15] + HEX_CHARS[h22 & 15] + HEX_CHARS[h32 >> 28 & 15] + HEX_CHARS[h32 >> 24 & 15] + HEX_CHARS[h32 >> 20 & 15] + HEX_CHARS[h32 >> 16 & 15] + HEX_CHARS[h32 >> 12 & 15] + HEX_CHARS[h32 >> 8 & 15] + HEX_CHARS[h32 >> 4 & 15] + HEX_CHARS[h32 & 15] + HEX_CHARS[h42 >> 28 & 15] + HEX_CHARS[h42 >> 24 & 15] + HEX_CHARS[h42 >> 20 & 15] + HEX_CHARS[h42 >> 16 & 15] + HEX_CHARS[h42 >> 12 & 15] + HEX_CHARS[h42 >> 8 & 15] + HEX_CHARS[h42 >> 4 & 15] + HEX_CHARS[h42 & 15] + HEX_CHARS[h5 >> 28 & 15] + HEX_CHARS[h5 >> 24 & 15] + HEX_CHARS[h5 >> 20 & 15] + HEX_CHARS[h5 >> 16 & 15] + HEX_CHARS[h5 >> 12 & 15] + HEX_CHARS[h5 >> 8 & 15] + HEX_CHARS[h5 >> 4 & 15] + HEX_CHARS[h5 & 15] + HEX_CHARS[h6 >> 28 & 15] + HEX_CHARS[h6 >> 24 & 15] + HEX_CHARS[h6 >> 20 & 15] + HEX_CHARS[h6 >> 16 & 15] + HEX_CHARS[h6 >> 12 & 15] + HEX_CHARS[h6 >> 8 & 15] + HEX_CHARS[h6 >> 4 & 15] + HEX_CHARS[h6 & 15];
    return this.is224 || (hex2 += HEX_CHARS[h7 >> 28 & 15] + HEX_CHARS[h7 >> 24 & 15] + HEX_CHARS[h7 >> 20 & 15] + HEX_CHARS[h7 >> 16 & 15] + HEX_CHARS[h7 >> 12 & 15] + HEX_CHARS[h7 >> 8 & 15] + HEX_CHARS[h7 >> 4 & 15] + HEX_CHARS[h7 & 15]), hex2;
  };
  Sha256.prototype.toString = Sha256.prototype.hex;
  Sha256.prototype.digest = function() {
    this.finalize();
    var h0 = this.h0, h1 = this.h1, h22 = this.h2, h32 = this.h3, h42 = this.h4, h5 = this.h5, h6 = this.h6, h7 = this.h7, arr = [
      h0 >> 24 & 255,
      h0 >> 16 & 255,
      h0 >> 8 & 255,
      h0 & 255,
      h1 >> 24 & 255,
      h1 >> 16 & 255,
      h1 >> 8 & 255,
      h1 & 255,
      h22 >> 24 & 255,
      h22 >> 16 & 255,
      h22 >> 8 & 255,
      h22 & 255,
      h32 >> 24 & 255,
      h32 >> 16 & 255,
      h32 >> 8 & 255,
      h32 & 255,
      h42 >> 24 & 255,
      h42 >> 16 & 255,
      h42 >> 8 & 255,
      h42 & 255,
      h5 >> 24 & 255,
      h5 >> 16 & 255,
      h5 >> 8 & 255,
      h5 & 255,
      h6 >> 24 & 255,
      h6 >> 16 & 255,
      h6 >> 8 & 255,
      h6 & 255
    ];
    return this.is224 || arr.push(h7 >> 24 & 255, h7 >> 16 & 255, h7 >> 8 & 255, h7 & 255), arr;
  };
  Sha256.prototype.array = Sha256.prototype.digest;
  Sha256.prototype.arrayBuffer = function() {
    this.finalize();
    var buffer = new ArrayBuffer(this.is224 ? 28 : 32), dataView = new DataView(buffer);
    return dataView.setUint32(0, this.h0), dataView.setUint32(4, this.h1), dataView.setUint32(8, this.h2), dataView.setUint32(12, this.h3), dataView.setUint32(16, this.h4), dataView.setUint32(20, this.h5), dataView.setUint32(24, this.h6), this.is224 || dataView.setUint32(28, this.h7), buffer;
  };
  function HmacSha256(key, is224, sharedMemory) {
    var i4, type = typeof key;
    if (type === "string") {
      var bytes = [], length = key.length, index = 0, code2;
      for (i4 = 0; i4 < length; ++i4)
        code2 = key.charCodeAt(i4), code2 < 128 ? bytes[index++] = code2 : code2 < 2048 ? (bytes[index++] = 192 | code2 >> 6, bytes[index++] = 128 | code2 & 63) : code2 < 55296 || code2 >= 57344 ? (bytes[index++] = 224 | code2 >> 12, bytes[index++] = 128 | code2 >> 6 & 63, bytes[index++] = 128 | code2 & 63) : (code2 = 65536 + ((code2 & 1023) << 10 | key.charCodeAt(++i4) & 1023), bytes[index++] = 240 | code2 >> 18, bytes[index++] = 128 | code2 >> 12 & 63, bytes[index++] = 128 | code2 >> 6 & 63, bytes[index++] = 128 | code2 & 63);
      key = bytes;
    } else if (type === "object") {
      if (key === null)
        throw new Error(ERROR);
      if (ARRAY_BUFFER && key.constructor === ArrayBuffer)
        key = new Uint8Array(key);
      else if (!Array.isArray(key) && (!ARRAY_BUFFER || !ArrayBuffer.isView(key)))
        throw new Error(ERROR);
    } else
      throw new Error(ERROR);
    key.length > 64 && (key = new Sha256(is224, !0).update(key).array());
    var oKeyPad = [], iKeyPad = [];
    for (i4 = 0; i4 < 64; ++i4) {
      var b8 = key[i4] || 0;
      oKeyPad[i4] = 92 ^ b8, iKeyPad[i4] = 54 ^ b8;
    }
    Sha256.call(this, is224, sharedMemory), this.update(iKeyPad), this.oKeyPad = oKeyPad, this.inner = !0, this.sharedMemory = sharedMemory;
  }
  HmacSha256.prototype = new Sha256();
  HmacSha256.prototype.finalize = function() {
    if (Sha256.prototype.finalize.call(this), this.inner) {
      this.inner = !1;
      var innerHash = this.array();
      Sha256.call(this, this.is224, this.sharedMemory), this.update(this.oKeyPad), this.update(innerHash), Sha256.prototype.finalize.call(this);
    }
  };
  var exports = createMethod();
  exports.sha256 = exports;
  exports.sha224 = createMethod(!0);
  exports.sha256.hmac = createHmacMethod();
  exports.sha224.hmac = createHmacMethod(!0);
  var sha256_default = exports;