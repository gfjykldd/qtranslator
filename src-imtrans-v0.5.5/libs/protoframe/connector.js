
  // libs/protoframe/connector.ts
  function mkPayloadType(protocol, action, type) {
    return `${protocol.type}#${action}#${type}`;
  }
  function mkPayloadBody(protocol, action, type, id, body) {
    return {
      body,
      id,
      type: mkPayloadType(protocol, action, type)
    };
  }
  function mkPayloadResponse(protocol, type, id, response) {
    return {
      id,
      response,
      type: mkPayloadType(protocol, "ask", type)
    };
  }
  function isPayloadBodyOfType(protocol, action, type, payload) {
    if (hasValue(payload)) {
      let payloadType = payload.type;
      if (hasValue(payloadType) && hasValue(payload.body)) {
        let [p6, a6, t5] = payloadType.split("#");
        return p6 === protocol.type && a6 === action && t5 === type;
      } else
        return !1;
    } else
      return !1;
  }
  function isPayloadResponseOfType(protocol, type, payload) {
    if (hasValue(payload)) {
      let payloadType = payload.type;
      if (hasValue(payloadType) && hasValue(payload.response)) {
        let [p6, a6, t5] = payloadType.split("#");
        return p6 === protocol.type && a6 === "ask" && t5 === type;
      } else
        return !1;
    } else
      return !1;
  }
  function destroyAll(listeners3) {
    listeners3.forEach(([w7, l6]) => w7.removeEventListener("message", l6)), listeners3.length = 0;
  }
  function awaitResponse(thisWindow, protocol, type, id) {
    return new Promise((accept) => {
      let handle = (ev) => {
        let payload = ev.data;
        isPayloadResponseOfType(protocol, type, payload) && payload.id === id && (thisWindow.removeEventListener("message", handle), accept(payload.response));
      };
      thisWindow.addEventListener("message", handle);
    });
  }
  function handleTell0(thisWindow, protocol, type, handler) {
    let listener = (ev) => {
      let payload = ev.data;
      isPayloadBodyOfType(protocol, "tell", type, payload) && handler(payload.body);
    };
    return thisWindow.addEventListener("message", listener), [thisWindow, listener];
  }
  function handleAsk0(thisWindow, targetWindow, protocol, type, targetOrigin, handler) {
    let listener = async (ev) => {
      let payload = ev.data;
      if (isPayloadBodyOfType(protocol, "ask", type, payload)) {
        let response = await handler(payload.body);
        targetWindow = ev.source, targetWindow.postMessage(
          mkPayloadResponse(protocol, type, payload.id, response),
          ev.origin
        );
      }
    };
    return thisWindow.addEventListener("message", listener), [thisWindow, listener];
  }
  function tell0(targetWindow, protocol, type, body, targetOrigin) {
    let id = Math.random().toString();
    return targetWindow.postMessage(
      mkPayloadBody(protocol, "tell", type, id, body),
      targetOrigin
    );
  }
  async function ask0(thisWindow, targetWindow, protocol, type, body, targetOrigin, timeout) {
    let id = Math.random().toString(), run2 = new Promise(async (accept, reject) => {
      let timeoutHandler = setTimeout(
        () => reject(new Error(`Failed to get response within ${timeout}ms`)),
        timeout
      ), response = await awaitResponse(thisWindow, protocol, type, id);
      clearTimeout(timeoutHandler), accept(response);
    });
    return targetWindow.postMessage(
      mkPayloadBody(protocol, "ask", type, id, body),
      targetOrigin
    ), run2;
  }
  var ProtoframePubsub = class {
    constructor(protocol, targetWindow, thisWindow = window, targetOrigin = "*") {
      this.protocol = protocol;
      this.targetWindow = targetWindow;
      this.thisWindow = thisWindow;
      this.targetOrigin = targetOrigin;
      this.listeners = [];
      handleAsk0(
        thisWindow,
        targetWindow,
        this.getSystemProtocol("ping"),
        "ping",
        targetOrigin,
        () => Promise.resolve({})
      );
    }
    /**
     * Connect to the target configured in the supplied pubsub connector by
     * sending ping requests over and over until we get a response.
     *
     * @param pubsub The pubsub connector to wait until is "connected" to its
     *  target
     * @param retries How many times to retry and ping the target. By default,
     *  this will retry 50 times (thus waiting 25 seconds total)
     * @param timeout How long to wait for a response from the target before
     *  retrying. By default the timeout is 500ms (thus waiting 25 seconds total)
     */
    static async connect(pubsub, retries = 10, timeout = 500) {
      for (let i4 = 0; i4 <= retries; i4++)
        try {
          return await pubsub.ping({ timeout }), pubsub;
        } catch {
          continue;
        }
      throw new Error(
        `Could not connect on protocol ${pubsub.protocol.type} after ${retries * timeout}ms`
      );
    }
    /**
     * We are a "parent" page that is embedding an iframe, and we wish to connect
     * to that iframe for communication.
     *
     * @param protocol The protocol this connector will communicate with
     * @param iframe The target iframe HTML element we are connecting to
     * @param targetOrigin The target scheme and host we expect the receiver to be
     * @param thisWindow The parent window (our window). This should normally be
     *  the current `window`
     */
    static parent(protocol, iframe, targetOrigin = "*", thisWindow = window) {
      let targetWindow = iframe.contentWindow;
      if (hasValue(targetWindow))
        return new ProtoframePubsub(
          protocol,
          targetWindow,
          thisWindow,
          targetOrigin
        );
      throw new Error("iframe.contentWindow was null");
    }
    /**
     * We are an "iframe" page that will be embedded, and we wish to connect to a
     * parent page for communication.
     *
     * @param protocol The protocol this connector will communicate with
     * @param targetOrigin The target scheme and host we expect the receiver to be
     * @param thisWindow The window of the current iframe. This should normally be
     *  the current `window`
     * @param targetWindow The window of the parent frame. This should normally be
     *  the `window.parent`
     */
    static iframe(protocol, targetOrigin = "*", {
      thisWindow = window,
      targetWindow = window.parent
    } = {}) {
      return new ProtoframePubsub(
        protocol,
        targetWindow,
        thisWindow,
        targetOrigin
      );
    }
    static rootIframe(protocol, targetOrigin = "*", {
      thisWindow = window
    } = {}) {
      return new ProtoframePubsub(
        protocol,
        null,
        thisWindow,
        targetOrigin
      );
    }
    getSystemProtocol(type) {
      return {
        type: `system|${type}`
      };
    }
    /**
     * Send a 'ping' request to check if there is a listener open at the target
     * window. If this times out, then it means no listener was available *at the
     * time the ping request was sent*. Since requests are not buffered, then this
     * should be retried if we're waiting for some target iframe to start up and
     * load its assets. See `ProtoframePubsub.connect` as an implementation of
     * this functionality.
     *
     * @param timeout How long to wait for the reply before resulting in an error
     */
    async ping({ timeout = 1e4 }) {
      await ask0(
        this.thisWindow,
        this.targetWindow,
        this.getSystemProtocol("ping"),
        "ping",
        {
          data: {},
          payload: {}
        },
        this.targetOrigin,
        timeout
      );
    }
    handleTell(type, handler) {
      this.listeners.push(
        handleTell0(this.thisWindow, this.protocol, type, handler)
      );
    }
    tell(type, body) {
      tell0(this.targetWindow, this.protocol, type, body, this.targetOrigin);
    }
    handleAsk(type, handler) {
      this.listeners.push(
        handleAsk0(
          this.thisWindow,
          this.targetWindow,
          this.protocol,
          type,
          this.targetOrigin,
          handler
        )
      );
    }
    ask(type, body, timeout = 1e4) {
      if (this.targetWindow)
        return ask0(
          this.thisWindow,
          this.targetWindow,
          this.protocol,
          type,
          body,
          this.targetOrigin,
          timeout
        );
      throw new Error("target window is requried");
    }
    destroy() {
      destroyAll(this.listeners);
    }
  };