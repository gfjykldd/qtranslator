
  // services/transmart.ts
  var langMap7 = [
    ["auto", "auto"],
    ["zh-CN", "zh"],
    ["zh-TW", "zh-TW"],
    ["de", "de"],
    ["en", "en"],
    ["es", "es"],
    ["fr", "fr"],
    ["id", "id"],
    ["it", "it"],
    ["ja", "ja"],
    ["ko", "kr"],
    ["ms", "ms"],
    ["pt", "pt"],
    ["ru", "ru"],
    ["th", "th"],
    ["tr", "tr"],
    ["vi", "vi"]
  ], API = "https://transmart.qq.com/api/imt", _Transmart = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.maxTextGroupLength = 25;
      this.maxTextLength = 1e3;
      this.isSupportList = !1;
      this.clientKey = btoa(
        "transmart_crx_" + navigator.userAgent
      ).slice(0, 100);
    }
    async translate(payload) {
      let { text, to } = payload, sourceLanguage = await this.detectLanguage(text), remoteSourceLanguage = _Transmart.langMap.get(sourceLanguage) || sourceLanguage, remoteTargetLanguage = _Transmart.langMap.get(to) || to;
      if (sourceLanguage === to)
        return {
          text,
          from: sourceLanguage,
          to
        };
      let requestPayload = JSON.stringify(
        {
          header: {
            fn: "auto_translation_block",
            client_key: this.clientKey
          },
          source: {
            text_block: text,
            lang: remoteSourceLanguage,
            orig_url: payload.url
          },
          target: { lang: remoteTargetLanguage }
        }
      ), data = await request2({
        url: API,
        body: requestPayload,
        method: "POST",
        retry: 2
      });
      if (data.header.ret_code !== "succ")
        throw new Error(data.message || data.header.ret_code);
      return {
        text: data.auto_translation,
        from: sourceLanguage || "auto",
        to
      };
    }
    getDefaultRateLimit() {
      return { limit: 30, interval: 1050 };
    }
    async translateList(payload) {
      let { from, text, to } = payload;
      if (text.length === 1) {
        let result = await this.translate({
          from,
          text: text[0],
          to,
          url: payload.url,
          options: payload.options
        });
        return {
          text: [result.text],
          from: result.from,
          to: result.to
        };
      }
      let sourceLanguage = await this.detectLanguage(text.join(`
`));
      if (sourceLanguage === to)
        return {
          text,
          from: sourceLanguage,
          to
        };
      let remoteSourceLanguage = _Transmart.langMap.get(sourceLanguage) || sourceLanguage, remoteTargetLanguage = _Transmart.langMap.get(to) || to, requestPayload = JSON.stringify(
        {
          header: {
            fn: "auto_translation",
            client_key: this.clientKey
          },
          source: {
            text_list: text,
            lang: remoteSourceLanguage,
            orig_url: payload.url
          },
          target: { lang: remoteTargetLanguage },
          type: "plain"
        }
      ), data = await request2({
        url: API,
        body: requestPayload,
        method: "POST"
      });
      if (data.header.ret_code !== "succ")
        throw new Error(data.message || data.header.ret_code);
      return {
        text: data.auto_translation,
        from: sourceLanguage || "auto",
        to
      };
    }
    detectLanguageLocally(text) {
      return this.detectLanguageRemotely(text);
    }
    async detectLanguageRemotely(text) {
      let payload = {
        header: {
          fn: "text_analysis",
          client_key: this.clientKey
        },
        text: text.slice(0, 280)
      }, response = await request2({
        url: API,
        method: "POST",
        body: JSON.stringify(payload)
      });
      if (response.header.ret_code !== "succ")
        throw new Error(response.message || response.header.ret_code);
      let remoteLanguage = response.language, language = _Transmart.langMapReverse.get(remoteLanguage);
      return language || remoteLanguage;
    }
  }, Transmart = _Transmart;
  /** Translator lang to custom lang */
  Transmart.langMap = new Map(langMap7), /** Custom lang to translator lang */
  Transmart.langMapReverse = new Map(
    langMap7.map(([translatorLang, lang]) => [lang, translatorLang])
  );