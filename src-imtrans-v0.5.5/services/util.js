
  // services/util.ts
  function formatTranslationService(key, ctx) {
    let service = PureTranslationServices[key], translationConfig = ctx.config.translationServices[key] || {}, ok = !0, allProps = service.allProps || [], explicitProps = [], optionalProps = [];
    if (allProps.length > 0) {
      let requiredProps = allProps.filter((prop) => prop.required);
      if (requiredProps.length > 0) {
        for (let prop of requiredProps)
          if (!translationConfig[prop.name] && !prop.default) {
            ok = !1;
            break;
          }
      }
      allProps.forEach((prop) => {
        prop.optional ? optionalProps.push(prop) : explicitProps.push(prop);
      });
    }
    return {
      ...service,
      id: key,
      selected: ctx.translationService === key,
      ok,
      config: translationConfig,
      props: service.props || [],
      allProps,
      optionalProps,
      explicitProps
    };
  }
  var getTranslationServices = (ctx) => {
    let { config } = ctx, alpha = config.alpha, beta = config.beta, canary = config.canary, debug = config.debug;
    return Object.keys(
      PureTranslationServices
    ).filter((key) => {
      let service = PureTranslationServices[key];
      if (key.startsWith("mock"))
        return debug ? !0 : key === ctx.config.translationService;
      if (key === ctx.config.translationService)
        return !0;
      let isCanaryFeature = !!service.canary, isAlphaFeature = !!service.alpha, isBetaFeature = !!service.beta;
      return key === ctx.translationService || isCanaryFeature && canary || isAlphaFeature && (alpha || canary) || isBetaFeature && (beta || alpha || canary) ? !0 : !isAlphaFeature && !isBetaFeature && !isCanaryFeature;
    }).map((key) => formatTranslationService(key, ctx));
  };
