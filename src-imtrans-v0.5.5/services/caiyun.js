
  // services/caiyun.ts
  var rawLangMap14 = [
    ["auto", "auto"],
    ["zh-CN", "zh"],
    ["en", "en"],
    ["ja", "ja"]
  ], langMap21 = new Map(rawLangMap14), Caiyun = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.token = "";
      if (!serviceConfig || !serviceConfig.token)
        throw new Error("token are required");
      this.token = serviceConfig.token?.trim();
    }
    async translateList(payload) {
      let { text, from, to } = payload;
      if (!langMap21.get(to))
        throw new Error(`Unsupported language: ${to}`);
      from === "auto" && (from = await detectLanguage({ text: text.join(" "), minLength: 10 }));
      let source = text;
      return {
        text: (await request2(
          {
            retry: 2,
            url: "https://api.interpreter.caiyunai.com/v1/translator",
            headers: {
              "content-type": "application/json",
              "x-authorization": "token " + this.token
            },
            method: "POST",
            body: JSON.stringify({
              source,
              trans_type: `${langMap21.get(from) || "auto"}2${langMap21.get(to)}`
            })
          }
        )).target,
        from,
        to
      };
    }
  }, caiyun_default = Caiyun;

  // services/cai.ts
  var rawLangMap15 = [
    ["auto", "auto"],
    ["zh-CN", "zh"],
    ["en", "en"],
    ["ja", "ja"]
  ], langMap22 = new Map(rawLangMap15), Cai = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.token = "ssdj273ksdiwi923bsd9";
    }
    getDefaultRateLimit() {
      return { limit: 5, interval: 1050 };
    }
    async translateList(payload) {
      let { text, from, to } = payload;
      if (!langMap22.get(to))
        throw new Error(`Unsupported language: ${to}`);
      from === "auto" && (from = await detectLanguage({ text: text.join(" "), minLength: 10 }));
      let source = text;
      return {
        text: (await request2(
          {
            retry: 2,
            url: "https://api.interpreter.caiyunai.com/v1/translator",
            headers: {
              "content-type": "application/json",
              "x-authorization": "token " + this.token
            },
            method: "POST",
            body: JSON.stringify({
              source,
              trans_type: `${langMap22.get(from) || "auto"}2${langMap22.get(to)}`
            })
          }
        )).target,
        from,
        to
      };
    }
  }, cai_default = Cai;