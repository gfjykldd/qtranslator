
  // services/mod.ts
  var TranslationServicesClass = {
    mock: Mock,
    mock2: Mock,
    google: Google,
    transmart: Transmart,
    deepl: deepl_default,
    volc: mod_default,
    volcAlpha: VolcAlpha,
    bing: Bing,
    tencent: Tencent,
    tenAlpha: TenAlpha,
    baidu: baidu_default,
    caiyun: caiyun_default,
    cai: cai_default,
    openl: openl_default,
    youdao: youdao_default,
    you: you_default,
    d: D5,
    dpro: D5,
    deeplx: Deeplx,
    niu: niu_default,
    azure: azure_default,
    openai: openai_default,
    chatgpt: ChatGPT,
    papago: papago_default
  }, TranslationServices = {};
  Object.keys(PureTranslationServices).forEach((key) => {
    TranslationServices[key] = {
      ...PureTranslationServices[key],
      // @ts-ignore: it's ok
      class: TranslationServicesClass[key]
    };
  });
  async function translateSingleSentence(sentence, ctx) {
    if (!sentence.text)
      return sentence;
    let result = await translateMultipleSentences(
      {
        sentences: [sentence]
      },
      ctx
    );
    if (result.sentences.length > 0)
      return {
        ...sentence,
        ...result.sentences[0]
      };
    throw new CommonError("translateFailed", "translate failed");
  }
  async function initTranslationEngine(ctx) {
    let { config, translationService } = ctx, generalConfig = config.translationGeneralConfig, services = config.translationServices, defaultTranslationEngine = translationService, serviceConfig = services[defaultTranslationEngine] || {}, translator = new TranslationServices[defaultTranslationEngine].class(
      serviceConfig,
      generalConfig,
      {
        translationService: defaultTranslationEngine
      }
    ), defaultRateLimit = translator.getDefaultRateLimit();
    defaultRateLimit && (serviceConfig && !serviceConfig.limit && (serviceConfig.limit = defaultRateLimit.limit), serviceConfig && !serviceConfig.interval && (serviceConfig.interval = defaultRateLimit.interval)), await setRateLimiter(
      defaultTranslationEngine,
      serviceConfig
    ), await translator.init();
  }
  async function translateMultipleSentences(payload, ctx, everySentenceCallback) {
    if (!payload.sentences.length)
      return {
        ...payload
      };
    let { config, translationService, state } = ctx, generalConfig = config.translationGeneralConfig, services = config.translationServices, defaultTranslationEngine = translationService, serviceConfig = services[defaultTranslationEngine] || {};
    defaultTranslationEngine === "openai" && (payload.sentences = payload.sentences.map((sentence) => ({
      ...sentence,
      from: "auto"
    })));
    let noCacheSentences = [], finalResult = {
      sentences: Array(payload.sentences.length)
    }, sourceLength = payload.sentences.length, sentenceIndex = -1;
    if (state.cache)
      for (let sentence of payload.sentences) {
        sentenceIndex++;
        let cacheServiceKey = defaultTranslationEngine;
        defaultTranslationEngine === "openl" && (cacheServiceKey = defaultTranslationEngine + "-" + serviceConfig.codename || openl_default.DEFAULT_CODENAME);
        let res = null;
        try {
          res = await deadline(
            queryDb({
              originalText: sentence.text,
              from: sentence.from,
              to: sentence.to,
              service: cacheServiceKey
            }),
            1e3
          );
        } catch (e) {
          log_default.warn("query cache DB error, but it's ok", e);
        }
        if (res) {
          let result = {
            ...sentence,
            text: res.translatedText
          };
          finalResult.sentences[sentenceIndex] = result, everySentenceCallback && everySentenceCallback(null, result, sentence);
        } else
          noCacheSentences.push(sentence);
      }
    else
      noCacheSentences.push(...payload.sentences);
    let resultLength = noCacheSentences.length;
    if (sourceLength - resultLength > 0 && log_default.debug(`use ${sourceLength - resultLength} sentences from cache`), !noCacheSentences.length)
      return finalResult;
    let translator;
    try {
      translator = new TranslationServices[defaultTranslationEngine].class(
        serviceConfig,
        generalConfig,
        {
          translationService: defaultTranslationEngine
        }
      ), await translator.init();
    } catch (e) {
      if (everySentenceCallback)
        for (let sentence of noCacheSentences)
          everySentenceCallback(e, null, sentence);
      throw e;
    }
    let noCacheResult = await translator.multipleTranslate(
      {
        sentences: noCacheSentences
      },
      serviceConfig,
      (err, a6, b5) => {
        if (everySentenceCallback && everySentenceCallback(err, a6, b5), !err && a6 && !defaultTranslationEngine.startsWith("mock") && state.cache) {
          let cacheServiceKey = defaultTranslationEngine;
          defaultTranslationEngine === "openl" && (cacheServiceKey = defaultTranslationEngine + "-" + serviceConfig.codename || openl_default.DEFAULT_CODENAME), state.cache && deadline(
            setDbStore({
              translatedText: a6.text,
              from: b5.from,
              to: b5.to,
              detectedFrom: a6.from,
              key: md5(b5.text),
              service: cacheServiceKey
            }),
            3e3
          ).catch((e) => {
            log_default.warn("set cache DB error", e);
          });
        }
      }
    );
    for (let sentence of noCacheResult.sentences) {
      let index = finalResult.sentences.findIndex((s7) => !s7);
      if (index === -1)
        throw new CommonError("translateFailed", "can not match the result");
      finalResult.sentences[index] = sentence;
    }
    return finalResult;
  }