
  // services/chatgpt.ts
  var rawLangMap7 = [
    ["auto", "auto"],
    ["zh-CN", "zh-Hans"],
    ["zh-TW", "zh-Hant"],
    ["en", "en"],
    ["yue", "\u7CA4\u8BED"],
    ["wyw", "\u53E4\u6587"],
    ["en", "en"],
    ["ja", "ja"],
    ["ko", "ko"],
    ["fr", "fr"],
    ["de", "de"],
    ["es", "es"],
    ["it", "it"],
    ["ru", "ru"],
    ["pt", "pt"],
    ["nl", "nl"],
    ["pl", "pl"],
    ["ar", "ar"],
    ["af", "af"],
    ["am", "am"],
    ["az", "az"],
    ["be", "be"],
    ["bg", "bg"],
    ["bn", "bn"],
    ["bs", "bs"],
    ["ca", "ca"],
    ["ceb", "ceb"],
    ["co", "co"],
    ["cs", "cs"],
    ["cy", "cy"],
    ["da", "da"],
    ["el", "el"],
    ["eo", "eo"],
    ["et", "et"],
    ["eu", "eu"],
    ["fa", "fa"],
    ["fi", "fi"],
    ["fj", "fj"],
    ["fy", "fy"],
    ["ga", "ga"],
    ["gd", "gd"],
    ["gl", "gl"],
    ["gu", "gu"],
    ["ha", "ha"],
    ["haw", "haw"],
    ["he", "he"],
    ["hi", "hi"],
    ["hmn", "hmn"],
    ["hr", "hr"],
    ["ht", "ht"],
    ["hu", "hu"],
    ["hy", "hy"],
    ["id", "id"],
    ["ig", "ig"],
    ["is", "is"],
    ["jw", "jw"],
    ["ka", "ka"],
    ["kk", "kk"],
    ["km", "km"],
    ["kn", "kn"],
    ["ku", "ku"],
    ["ky", "ky"],
    ["la", "lo"],
    ["lb", "lb"],
    ["lo", "lo"],
    ["lt", "lt"],
    ["lv", "lv"],
    ["mg", "mg"],
    ["mi", "mi"],
    ["mk", "mk"],
    ["ml", "ml"],
    ["mn", "mn"],
    ["mr", "mr"],
    ["ms", "ms"],
    ["mt", "mt"],
    ["my", "my"],
    ["ne", "ne"],
    ["no", "no"],
    ["ny", "ny"],
    ["pa", "pa"],
    ["ps", "ps"],
    ["ro", "ro"],
    ["si", "si"],
    ["sk", "sk"],
    ["sl", "sl"],
    ["sm", "sm"],
    ["sn", "sn"],
    ["so", "so"],
    ["sq", "sq"],
    ["sr", "sr"],
    ["sr-Cyrl", "sr"],
    ["sr-Latn", "sr"],
    ["st", "st"],
    ["su", "su"],
    ["sv", "sv"],
    ["sw", "sw"],
    ["ta", "ta"],
    ["te", "te"],
    ["tg", "tg"],
    ["th", "th"],
    ["tr", "tr"],
    ["ug", "ug"],
    ["uk", "uk"],
    ["ur", "ur"],
    ["uz", "uz"],
    ["vi", "vi"],
    ["xh", "xh"],
    ["yi", "yi"],
    ["yo", "yo"],
    ["zu", "zu"]
  ], langMap14 = new Map(rawLangMap7), KEY_ACCESS_TOKEN = "accessToken";
  async function getChatGptAccessToken() {
    let cachedValue = await getExpired(KEY_ACCESS_TOKEN);
    if (cachedValue)
      return cachedValue;
    let data = await request2({
      url: "https://chat.openai.com/api/auth/session",
      method: "get",
      responseType: "json"
    });
    if (!data.accessToken)
      throw new Error("UNAUTHORIZED");
    return await setExpired(KEY_ACCESS_TOKEN, data.accessToken, 10 * 60 * 1e3), data.accessToken;
  }
  var _taskQueue, _TranslationManager = class {
    constructor() {
      __privateAdd(this, _taskQueue, Promise.resolve());
      return _TranslationManager.instance || (_TranslationManager.instance = this), this;
    }
    enqueue(task) {
      return __privateSet(this, _taskQueue, __privateGet(this, _taskQueue).then(() => task())), __privateGet(this, _taskQueue);
    }
  }, TranslationManager = _TranslationManager;
  _taskQueue = new WeakMap();
  var translationManager = new TranslationManager();
  Object.freeze(translationManager);
  var ChatGPT = class extends Translation {
    // model = "gpt-3.5-turbo";
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.accessToken = "";
      this.customChatGptWebApiUrl = "https://chat.openai.com";
      this.customChatGptWebApiPath = "/backend-api/conversation";
      this.maxTextGroupLength = 1;
      this.maxTextLength = 1200;
      this.isStream = !1;
      this.isSupportList = !1;
      this.prompt = `You are a translation engine, you can only translate text and cannot interpret it, and do not explain.Translate the text below to {{to}}:

{{text}}`;
    }
    // deno-lint-ignore ban-types
    throttleDebounce(func, throttleDelay, debounceDelay) {
      let lastCall = 0, debounceTimeout;
      return (...args) => {
        let now = Date.now(), context = this, executeFunc = () => {
          lastCall = now, func.apply(context, args);
        };
        now - lastCall >= throttleDelay ? (clearTimeout(debounceTimeout), executeFunc()) : (clearTimeout(debounceTimeout), debounceTimeout = setTimeout(() => {
          executeFunc();
        }, debounceDelay));
      };
    }
    async deleteConversation(conversationId) {
      if (conversationId) {
        let resp = await request2({
          url: `${this.customChatGptWebApiUrl}${this.customChatGptWebApiPath}/${conversationId}`,
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${this.accessToken}`
          },
          body: JSON.stringify({ is_visible: !1 })
        });
        return log_default.debug("delete conversation res:", resp), resp;
      }
    }
    async translate(payload) {
      return await translationManager.enqueue(async () => {
        let { text, from, to } = payload;
        if (text.length === 0)
          return Promise.resolve({
            from,
            to,
            text: ""
          });
        let remoteFrom = langMap14.get(from) || from, remoteTo = langMap14.get(to) || to;
        if (this.accessToken = await getChatGptAccessToken(), !this.accessToken || this.accessToken === "")
          throw new Error("token error");
        let selectedModel = "text-davinci-002-render-sha";
        if (!selectedModel)
          throw new Error("No available model");
        let prompt = this.prompt.replace(/{{to}}/g, remoteTo).replace(
          /{{text}}/g,
          text
        ).replace(/{{from}}/g, remoteFrom), data = await request2({
          url: `${this.customChatGptWebApiUrl}${this.customChatGptWebApiPath}`,
          method: "POST",
          responseType: "stream",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${this.accessToken}`
          },
          body: JSON.stringify({
            action: "next",
            // conversation_id: session!.conversationId,
            messages: [
              {
                id: uuidv4(),
                role: "user",
                content: {
                  content_type: "text",
                  parts: [prompt]
                }
              }
            ],
            model: selectedModel,
            parent_message_id: uuidv4()
          })
        });
        return log_default.debug("get chatgpt res:", data), await this.deleteConversation(data?.conversation_id), Promise.resolve({
          from,
          to,
          text: data?.message?.content?.parts?.[0]
        });
      });
    }
    async translateStream(payload, callback) {
      let { text, from, to } = payload;
      if (text.length === 0) {
        callback(null, {
          from,
          to,
          text: ""
        });
        return;
      }
      let throttleDebounceCallback = this.throttleDebounce(
        // deno-lint-ignore no-explicit-any
        function(x4) {
          let { from: from2, to: to2, text: text2 } = x4;
          callback(null, {
            from: from2,
            to: to2,
            text: text2
          });
        },
        300,
        // 节流延迟
        200
        // 防抖延迟
      );
      await translationManager.enqueue(async () => {
        let portName = "chatgpt" + uuidv4(), port = browserAPI.runtime.connect({ name: portName }), conversionId, data;
        port.onMessage.addListener((message) => {
          if (!(!message || message === "")) {
            if (message === "[DONE]") {
              port.disconnect(), this.deleteConversation(conversionId);
              return;
            }
            try {
              data = JSON.parse(message);
            } catch (error) {
              log_default.debug("chatgpt json error", error, message);
              return;
            }
            conversionId = data?.conversation_id, data?.message?.author?.role === "assistant" && throttleDebounceCallback({
              from,
              to,
              text: data?.message?.content?.parts?.[0]
            });
          }
        });
        let remoteFrom = langMap14.get(from) || from, remoteTo = langMap14.get(to) || to;
        if (this.accessToken = await getChatGptAccessToken(), !this.accessToken || this.accessToken === "")
          throw new Error("token error");
        let selectedModel = "text-davinci-002-render-sha";
        if (!selectedModel)
          throw new Error("No available model");
        let prompt = this.prompt.replace(/{{to}}/g, remoteTo).replace(
          /{{text}}/g,
          text
        ).replace(/{{from}}/g, remoteFrom);
        request2({
          url: `${this.customChatGptWebApiUrl}${this.customChatGptWebApiPath}`,
          method: "POST",
          responseType: "realStream",
          extra: { portName },
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${this.accessToken}`
          },
          body: JSON.stringify({
            action: "next",
            // conversation_id: session!.conversationId,
            messages: [
              {
                id: uuidv4(),
                role: "user",
                content: {
                  content_type: "text",
                  parts: [prompt]
                }
              }
            ],
            model: selectedModel,
            parent_message_id: uuidv4()
          })
        }).catch((e) => {
          log_default.debug("chatgpt error", e), port.disconnect(), this.deleteConversation(conversionId), callback(e, {
            from,
            to,
            text: ""
          });
        });
      });
    }
  };