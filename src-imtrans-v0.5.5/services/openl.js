
  // services/openl.ts
  var rawLangMap = [
    ["auto", "auto"],
    ["zh-CN", "zh"],
    ["zh-TW", "zh"],
    ["en", "en"],
    ["ja", "ja"],
    ["de", "de"],
    ["fr", "fr"],
    ["it", "it"],
    ["es", "es"],
    ["nl", "nl"],
    ["pl", "pl"],
    ["pt", "pt"],
    ["ru", "ru"]
  ], langMap8 = new Map(rawLangMap), langMapReverse = new Map(
    rawLangMap.map(([translatorLang, lang]) => [lang, translatorLang])
  ), _Openl = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.apikey = "";
      this.codename = _Openl.DEFAULT_CODENAME;
      this.isSupportList = !1;
      this.maxTextGroupLength = 1;
      if (!serviceConfig || !serviceConfig.apikey)
        throw new Error("apikey are required");
      this.apikey = serviceConfig.apikey?.trim(), serviceConfig.codename && (this.codename = serviceConfig.codename);
    }
    async translate(payload) {
      let { text, from, to } = payload, response = await request2(
        {
          retry: 2,
          url: `https://api.openl.club/services/${this.codename}/translate`,
          headers: {
            "content-type": "application/json"
          },
          method: "POST",
          body: JSON.stringify({
            apikey: this.apikey,
            text,
            source_lang: langMap8.get(from) || "auto",
            target_lang: langMap8.get(to) || to
          })
        }
      );
      if (response.status) {
        let result = response;
        return result.result && to == "zh-TW", {
          text: result.result,
          from: langMapReverse.get(result.source_lang),
          to: langMapReverse.get(result.target_lang)
        };
      } else
        throw new Error(response.msg);
    }
  }, Openl = _Openl;
  Openl.DEFAULT_CODENAME = "deepl";
  var openl_default = Openl;