
  // services/volc/sign.ts
  var unsignableHeaders = [
    "authorization",
    "content-type",
    "content-length",
    "user-agent",
    "presigned-expires",
    "expect"
  ], constant = {
    algorithm: "HMAC-SHA256",
    v4Identifier: "request",
    dateHeader: "X-Date",
    tokenHeader: "X-Security-Token",
    contentSha256Header: "X-Content-Sha256",
    notSignBody: "X-NotSignBody",
    kDatePrefix: "",
    credential: "X-Credential",
    algorithmKey: "X-Algorithm",
    signHeadersKey: "X-SignedHeaders",
    signQueriesKey: "X-SignedQueries",
    signatureKey: "X-Signature"
  }, uriEscape = (str) => {
    try {
      return encodeURIComponent(str).replace(/[^A-Za-z0-9_.~\-%]+/g, escape).replace(
        /[*]/g,
        (ch) => `%${ch.charCodeAt(0).toString(16).toUpperCase()}`
      );
    } catch {
      return "";
    }
  }, queryParamsToString = (params) => Object.keys(params).map((key) => {
    let val = params[key];
    if (typeof val > "u" || val === null)
      return;
    let escapedKey = uriEscape(key);
    if (escapedKey)
      return Array.isArray(val) ? `${escapedKey}=${val.map(uriEscape).sort().join(`&${escapedKey}=`)}` : `${escapedKey}=${uriEscape(val)}`;
  }).filter((v6) => v6).join("&"), Signer = class {
    constructor(request3, serviceName, options2) {
      this.request = request3, this.request.headers = request3.headers || {}, this.serviceName = serviceName, options2 = options2 || {}, this.bodySha256 = options2.bodySha256, this.request.params = this.sortParams(this.request.params);
    }
    sortParams(params) {
      let newParams = {};
      return params && Object.keys(params).filter((key) => {
        let value = params[key];
        return typeof value < "u" && value !== null;
      }).sort().map((key) => {
        newParams[key] = params[key];
      }), newParams;
    }
    async addAuthorization(credentials, date) {
      let datetime = this.getDateTime(date);
      await this.addHeaders(credentials, datetime), this.request.headers.Authorization = await this.authorization(
        credentials,
        datetime
      );
    }
    async authorization(credentials, datetime) {
      let parts = [], credString = this.credentialString(datetime);
      return parts.push(
        `${constant.algorithm} Credential=${credentials.accessKeyId}/${credString}`
      ), parts.push(`SignedHeaders=${this.signedHeaders()}`), parts.push(`Signature=${await this.signature(credentials, datetime)}`), parts.join(", ");
    }
    async getSignUrl(credentials, date) {
      let datetime = this.getDateTime(date), query = { ...this.request.params }, params = this.request.params, headers3 = this.request.headers;
      credentials.sessionToken && (query[constant.tokenHeader] = credentials.sessionToken), query[constant.dateHeader] = datetime, query[constant.notSignBody] = "", query[constant.credential] = `${credentials.accessKeyId}/${this.credentialString(datetime)}`, query[constant.algorithmKey] = constant.algorithm, query[constant.signHeadersKey] = "", query[constant.signQueriesKey] = void 0, query[constant.signatureKey] = void 0, query = this.sortParams(query), this.request.params = query, this.request.headers = {};
      let sig = await this.signature(credentials, datetime);
      return this.request.params = params, this.request.headers = headers3, query[constant.signQueriesKey] = Object.keys(query).sort().join(";"), query[constant.signatureKey] = sig, queryParamsToString(query);
    }
    getDateTime(date) {
      return this.iso8601(date).replace(/[:\-]|\.\d{3}/g, "");
    }
    async addHeaders(credentials, datetime) {
      if (this.request.headers[constant.dateHeader] = datetime, credentials.sessionToken && (this.request.headers[constant.tokenHeader] = credentials.sessionToken), this.request.body) {
        let body = this.request.body;
        this.request.headers[constant.contentSha256Header] = await sha256(body);
      }
    }
    async signature(credentials, datetime) {
      let signingKey = await this.getSigningKey(
        credentials,
        datetime.substr(0, 8),
        this.request.region,
        this.serviceName
      );
      return hex(await hmacSha256(await this.stringToSign(datetime), signingKey));
    }
    async stringToSign(datetime) {
      let parts = [];
      parts.push(constant.algorithm), parts.push(datetime), parts.push(this.credentialString(datetime));
      let x4 = await this.canonicalString();
      return parts.push(
        await this.hexEncodedHash(x4)
      ), parts.join(`
`);
    }
    async canonicalString() {
      let parts = [], pathname = this.request.pathname || "/";
      parts.push(this.request.method.toUpperCase()), parts.push(pathname);
      let queryString = queryParamsToString(this.request.params) || "";
      return parts.push(queryString), parts.push(`${this.canonicalHeaders()}
`), parts.push(this.signedHeaders()), parts.push(await this.hexEncodedBodyHash()), parts.join(`
`);
    }
    canonicalHeaders() {
      let headers3 = [];
      Object.keys(this.request.headers).forEach((key) => {
        headers3.push([key, this.request.headers[key]]);
      }), headers3.sort((a6, b5) => a6[0].toLowerCase() < b5[0].toLowerCase() ? -1 : 1);
      let parts = [];
      return headers3.forEach((item) => {
        let key = item[0].toLowerCase();
        if (this.isSignableHeader(key)) {
          let value = item[1];
          if (typeof value > "u" || value === null || typeof value.toString != "function")
            throw new Error(`Header ${key} contains invalid value`);
          parts.push(`${key}:${this.canonicalHeaderValues(value.toString())}`);
        }
      }), parts.join(`
`);
    }
    canonicalHeaderValues(values) {
      return values.replace(/\s+/g, " ").replace(/^\s+|\s+$/g, "");
    }
    signedHeaders() {
      let keys = [];
      return Object.keys(this.request.headers).forEach((key) => {
        key = key.toLowerCase(), this.isSignableHeader(key) && keys.push(key);
      }), keys.sort().join(";");
    }
    signedQueries() {
      return Object.keys(this.request.params).join(";");
    }
    credentialString(datetime) {
      return this.createScope(
        datetime.substr(0, 8),
        this.request.region,
        this.serviceName
      );
    }
    async hexEncodedHash(str) {
      return await sha256(str);
    }
    async hexEncodedBodyHash() {
      return this.request.headers[constant.contentSha256Header] ? this.request.headers[constant.contentSha256Header] : this.request.body ? await this.hexEncodedHash(queryParamsToString(this.request.body)) : await this.hexEncodedHash("");
    }
    isSignableHeader(key) {
      return unsignableHeaders.indexOf(key) < 0;
    }
    iso8601(date) {
      return date === void 0 && (date = /* @__PURE__ */ new Date()), date.toISOString().replace(/\.\d{3}Z$/, "Z");
    }
    async getSigningKey(credentials, date, region, service) {
      let kDate = await hmacSha256(
        date,
        `${constant.kDatePrefix}${credentials.secretKey}`
      ), kRegion = await hmacSha256(region, kDate), kService = await hmacSha256(service, kRegion);
      return hmacSha256(constant.v4Identifier, kService);
    }
    createScope(date, region, serviceName) {
      return [date.substr(0, 8), region, serviceName, constant.v4Identifier].join(
        "/"
      );
    }
  };

  // services/volc/mod.ts
  var rawLangMap8 = [
    ["af", "af"],
    ["am", "am"],
    ["ar", "ar"],
    ["az", "az"],
    ["be", "be"],
    ["bg", "bg"],
    ["bn", "bn"],
    ["bs", "bs"],
    ["ca", "ca"],
    ["co", "co"],
    ["cs", "cs"],
    ["cy", "cy"],
    ["da", "da"],
    ["de", "de"],
    ["el", "el"],
    ["en", "en"],
    ["eo", "eo"],
    ["es", "es"],
    ["et", "et"],
    ["eu", "eu"],
    ["fa", "fa"],
    ["fi", "fi"],
    ["fj", "fj"],
    ["fr", "fr"],
    ["fy", "fy"],
    ["ga", "ga"],
    ["gd", "gd"],
    ["gl", "gl"],
    ["gu", "gu"],
    ["ha", "ha"],
    ["he", "he"],
    ["hi", "hi"],
    ["hr", "hr"],
    ["ht", "ht"],
    ["hu", "hu"],
    ["hy", "hy"],
    ["id", "id"],
    ["ig", "ig"],
    ["is", "is"],
    ["it", "it"],
    ["ja", "ja"],
    ["ka", "ka"],
    ["kk", "kk"],
    ["km", "km"],
    ["kn", "kn"],
    ["ko", "ko"],
    ["ku", "ku"],
    ["ky", "ky"],
    ["la", "la"],
    ["lb", "lb"],
    ["lo", "lo"],
    ["lt", "lt"],
    ["lv", "lv"],
    ["mg", "mg"],
    ["mi", "mi"],
    ["mk", "mk"],
    ["ml", "ml"],
    ["mn", "mn"],
    ["mr", "mr"],
    ["ms", "ms"],
    ["mt", "mt"],
    ["my", "my"],
    ["ne", "ne"],
    ["nl", "nl"],
    ["no", "no"],
    ["ny", "ny"],
    ["pa", "pa"],
    ["pl", "pl"],
    ["ps", "ps"],
    ["pt", "pt"],
    ["ro", "ro"],
    ["ru", "ru"],
    ["sd", "sd"],
    ["si", "si"],
    ["sk", "sk"],
    ["sl", "sl"],
    ["sm", "sm"],
    ["sn", "sn"],
    ["so", "so"],
    ["sq", "sq"],
    ["sr", "sr"],
    ["st", "st"],
    ["su", "su"],
    ["sv", "sv"],
    ["sw", "sw"],
    ["ta", "ta"],
    ["te", "te"],
    ["tg", "tg"],
    ["th", "th"],
    ["tn", "tn"],
    ["to", "to"],
    ["tr", "tr"],
    ["ty", "ty"],
    ["ug", "ug"],
    ["uk", "uk"],
    ["ur", "ur"],
    ["uz", "uz"],
    ["vi", "vi"],
    ["xh", "xh"],
    ["yi", "yi"],
    ["yo", "yo"],
    ["zh-CN", "zh"],
    ["zh-TW", "zh-Hans"],
    ["zu", "zu"]
  ], langMap15 = new Map(rawLangMap8), langMapReverse3 = new Map(
    rawLangMap8.map(([translatorLang, lang]) => [lang, translatorLang])
  ), Volc = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.accessKeyId = "";
      this.secretAccessKey = "";
      this.maxTextGroupLength = 8;
      if (!serviceConfig || !serviceConfig.accessKeyId || !serviceConfig.secretAccessKey)
        throw new Error("accessKeyId and secretAccessKey are required");
      this.accessKeyId = serviceConfig.accessKeyId?.trim(), this.secretAccessKey = serviceConfig.secretAccessKey?.trim();
    }
    async remoteDetectLanguage(text) {
      let requestObj = {
        region: "cn-north-1",
        method: "POST",
        params: {
          Action: "LangDetect",
          Version: "2020-06-01"
        },
        pathname: "/",
        headers: {
          "Content-Type": "application/json",
          host: "open.volcengineapi.com"
        },
        body: JSON.stringify({
          TextList: [text]
        })
      }, signer = new Signer(requestObj, "translate");
      await signer.addAuthorization({
        accessKeyId: this.accessKeyId,
        secretKey: this.secretAccessKey
      });
      let urlSearchParams = new URLSearchParams(requestObj.params), response = await request2(
        {
          retry: 2,
          url: "https://open.volcengineapi.com" + requestObj.pathname + "?" + urlSearchParams.toString(),
          headers: signer.request.headers,
          method: requestObj.method,
          body: requestObj.body
        }
      );
      if (response.DetectedLanguageList && response.DetectedLanguageList.length > 0)
        return response.DetectedLanguageList[0].Language;
      if (response.ResponseMetadata && response.ResponseMetadata.Error) {
        let error = response.ResponseMetadata.Error;
        throw new CommonError(error.Code, error.Message);
      } else if (response.ResponseMetaData && response.ResponseMetaData.Error) {
        let error = response.ResponseMetaData.Error;
        throw new CommonError(error.Code, error.Message);
      } else
        throw new Error("response: " + JSON.stringify(response));
    }
    async translateList(payload) {
      let { text, from, to } = payload, remoteFrom = langMap15.get(from), bodyParams = {
        TargetLanguage: langMap15.get(to) || to,
        TextList: text
      };
      remoteFrom ? bodyParams.SourceLanguage = remoteFrom : bodyParams.SourceLanguage = await this.remoteDetectLanguage(
        text.join(`
`).slice(0, 1e3)
      );
      let requestObj = {
        region: "cn-north-1",
        method: "POST",
        params: {
          Action: "TranslateText",
          Version: "2020-06-01"
        },
        pathname: "/",
        headers: {
          "Content-Type": "application/json",
          host: "open.volcengineapi.com"
        },
        body: JSON.stringify(bodyParams)
      }, signer = new Signer(requestObj, "translate");
      await signer.addAuthorization({
        accessKeyId: this.accessKeyId,
        secretKey: this.secretAccessKey
      });
      let urlSearchParams = new URLSearchParams(requestObj.params), response = await request2(
        {
          retry: 2,
          url: "https://open.volcengineapi.com" + requestObj.pathname + "?" + urlSearchParams.toString(),
          headers: signer.request.headers,
          method: requestObj.method,
          body: requestObj.body
        }
      );
      if (response.TranslationList) {
        let resultText = response.TranslationList.map((item) => item.Translation), remoteFrom2 = from;
        return response.TranslationList.length > 0 && response.TranslationList[0].DetectedSourceLanguage && (remoteFrom2 = langMapReverse3.get(
          response.TranslationList[0].DetectedSourceLanguage
        ) || from), {
          text: resultText,
          from: remoteFrom2,
          to
        };
      } else if (response.ResponseMetadata && response.ResponseMetadata.Error) {
        let error = response.ResponseMetadata.Error;
        throw new CommonError(error.Code, error.Message);
      } else if (response.ResponseMetaData && response.ResponseMetaData.Error) {
        let error = response.ResponseMetaData.Error;
        throw new CommonError(error.Code, error.Message);
      } else
        throw new Error("response: " + JSON.stringify(response));
    }
  }, mod_default = Volc;

  // services/volc_alpha.ts
  var rawLangMap9 = [
    ["auto", "detect"],
    ["af", "af"],
    ["am", "am"],
    ["ar", "ar"],
    ["az", "az"],
    ["be", "be"],
    ["bg", "bg"],
    ["bn", "bn"],
    ["bs", "bs"],
    ["ca", "ca"],
    ["co", "co"],
    ["cs", "cs"],
    ["cy", "cy"],
    ["da", "da"],
    ["de", "de"],
    ["el", "el"],
    ["en", "en"],
    ["eo", "eo"],
    ["es", "es"],
    ["et", "et"],
    ["eu", "eu"],
    ["fa", "fa"],
    ["fi", "fi"],
    ["fj", "fj"],
    ["fr", "fr"],
    ["fy", "fy"],
    ["ga", "ga"],
    ["gd", "gd"],
    ["gl", "gl"],
    ["gu", "gu"],
    ["ha", "ha"],
    ["he", "he"],
    ["hi", "hi"],
    ["hr", "hr"],
    ["ht", "ht"],
    ["hu", "hu"],
    ["hy", "hy"],
    ["id", "id"],
    ["ig", "ig"],
    ["is", "is"],
    ["it", "it"],
    ["ja", "ja"],
    ["ka", "ka"],
    ["kk", "kk"],
    ["km", "km"],
    ["kn", "kn"],
    ["ko", "ko"],
    ["ku", "ku"],
    ["ky", "ky"],
    ["la", "la"],
    ["lb", "lb"],
    ["lo", "lo"],
    ["lt", "lt"],
    ["lv", "lv"],
    ["mg", "mg"],
    ["mi", "mi"],
    ["mk", "mk"],
    ["ml", "ml"],
    ["mn", "mn"],
    ["mr", "mr"],
    ["ms", "ms"],
    ["mt", "mt"],
    ["my", "my"],
    ["ne", "ne"],
    ["nl", "nl"],
    ["no", "no"],
    ["ny", "ny"],
    ["pa", "pa"],
    ["pl", "pl"],
    ["ps", "ps"],
    ["pt", "pt"],
    ["ro", "ro"],
    ["ru", "ru"],
    ["sd", "sd"],
    ["si", "si"],
    ["sk", "sk"],
    ["sl", "sl"],
    ["sm", "sm"],
    ["sn", "sn"],
    ["so", "so"],
    ["sq", "sq"],
    ["sr", "sr"],
    ["st", "st"],
    ["su", "su"],
    ["sv", "sv"],
    ["sw", "sw"],
    ["ta", "ta"],
    ["te", "te"],
    ["tg", "tg"],
    ["th", "th"],
    ["tn", "tn"],
    ["to", "to"],
    ["tr", "tr"],
    ["ty", "ty"],
    ["ug", "ug"],
    ["uk", "uk"],
    ["ur", "ur"],
    ["uz", "uz"],
    ["vi", "vi"],
    ["xh", "xh"],
    ["yi", "yi"],
    ["yo", "yo"],
    ["zh-CN", "zh"],
    ["zh-TW", "zh-Hans"],
    ["zu", "zu"]
  ], langMap16 = new Map(rawLangMap9), langMapReverse4 = new Map(
    rawLangMap9.map(([translatorLang, lang]) => [lang, translatorLang])
  ), VolcAlpha = class extends Translation {
    constructor() {
      super(...arguments);
      this.maxTextGroupLength = 50;
      this.isSupportList = !1;
    }
    async translate(payload) {
      let { text, from, to } = payload, remoteFrom = langMap16.get(from) || "detect", remoteTo = langMap16.get(to) || to, response = await request2(
        {
          url: "https://translate.volcengine.com/crx/translate/v1/",
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            source_language: remoteFrom,
            target_language: remoteTo,
            text
          })
        }
      );
      if (response.base_resp && response.base_resp.status_code === 0) {
        let resultText = response.translation, remoteFrom2 = from;
        return response.detected_language && (remoteFrom2 = langMapReverse4.get(response.detected_language) || from), {
          text: resultText,
          from: remoteFrom2,
          to
        };
      } else {
        let error = response.base_resp;
        throw new CommonError(error.status_code.toString(), error.status_message);
      }
    }
  };