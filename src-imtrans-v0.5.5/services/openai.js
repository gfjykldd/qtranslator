
  // services/openai.ts
  var rawLangMap6 = [
    ["auto", "auto"],
    ["zh-CN", "zh-Hans"],
    ["zh-TW", "zh-Hant"],
    ["en", "en"],
    ["yue", "\u7CA4\u8BED"],
    ["wyw", "\u53E4\u6587"],
    ["en", "en"],
    ["ja", "ja"],
    ["ko", "ko"],
    ["fr", "fr"],
    ["de", "de"],
    ["es", "es"],
    ["it", "it"],
    ["ru", "ru"],
    ["pt", "pt"],
    ["nl", "nl"],
    ["pl", "pl"],
    ["ar", "ar"],
    ["af", "af"],
    ["am", "am"],
    ["az", "az"],
    ["be", "be"],
    ["bg", "bg"],
    ["bn", "bn"],
    ["bs", "bs"],
    ["ca", "ca"],
    ["ceb", "ceb"],
    ["co", "co"],
    ["cs", "cs"],
    ["cy", "cy"],
    ["da", "da"],
    ["el", "el"],
    ["eo", "eo"],
    ["et", "et"],
    ["eu", "eu"],
    ["fa", "fa"],
    ["fi", "fi"],
    ["fj", "fj"],
    ["fy", "fy"],
    ["ga", "ga"],
    ["gd", "gd"],
    ["gl", "gl"],
    ["gu", "gu"],
    ["ha", "ha"],
    ["haw", "haw"],
    ["he", "he"],
    ["hi", "hi"],
    ["hmn", "hmn"],
    ["hr", "hr"],
    ["ht", "ht"],
    ["hu", "hu"],
    ["hy", "hy"],
    ["id", "id"],
    ["ig", "ig"],
    ["is", "is"],
    ["jw", "jw"],
    ["ka", "ka"],
    ["kk", "kk"],
    ["km", "km"],
    ["kn", "kn"],
    ["ku", "ku"],
    ["ky", "ky"],
    ["la", "lo"],
    ["lb", "lb"],
    ["lo", "lo"],
    ["lt", "lt"],
    ["lv", "lv"],
    ["mg", "mg"],
    ["mi", "mi"],
    ["mk", "mk"],
    ["ml", "ml"],
    ["mn", "mn"],
    ["mr", "mr"],
    ["ms", "ms"],
    ["mt", "mt"],
    ["my", "my"],
    ["pa", "pa"],
    ["ps", "ps"],
    ["ro", "ro"],
    ["si", "si"],
    ["sk", "sk"],
    ["sl", "sl"],
    ["sm", "sm"],
    ["sn", "sn"],
    ["so", "so"],
    ["sq", "sq"],
    ["sr", "sr"],
    ["sr-Cyrl", "sr"],
    ["sr-Latn", "sr"],
    ["st", "st"],
    ["su", "su"],
    ["sv", "sv"],
    ["sw", "sw"],
    ["ta", "ta"],
    ["te", "te"],
    ["tg", "tg"],
    ["th", "th"],
    ["tr", "tr"],
    ["ug", "ug"],
    ["uk", "uk"],
    ["ur", "ur"],
    ["uz", "uz"],
    ["vi", "vi"],
    ["xh", "xh"],
    ["yi", "yi"],
    ["yo", "yo"],
    ["zu", "zu"]
  ], langMap13 = new Map(rawLangMap6), OpenAI = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.APIKEY = "";
      // api key, spit by ',' if you have multiple keys
      this.apiKeys = [];
      // api key list
      this.isSupportList = !1;
      this.maxTextLength = 1200;
      this.maxTextGroupLength = 1;
      this.systemPrompt = "You are a translation engine, you can only translate text and cannot interpret it, and do not explain.";
      this.prompt = "Translate the text below to {{to}}:\n\n```\n{{text}}\n```";
      this.model = "gpt-3.5-turbo";
      this.apiUrl = "https://api.openai.com/v1/chat/completions";
      this.immersiveApiUrl = "https://openai-api.immersivetranslate.com/v1/chat/completions";
      serviceConfig || (serviceConfig = {}), serviceConfig.APIKEY && (this.APIKEY = serviceConfig.APIKEY?.trim(), this.apiKeys = this.APIKEY.split(",").map((key) => key.trim())), serviceConfig.prompt && (this.prompt = serviceConfig.prompt), serviceConfig.model && (this.model = serviceConfig.model), serviceConfig && serviceConfig.apiUrl && (this.apiUrl = mergeUrl(this.apiUrl, serviceConfig.apiUrl)), serviceConfig && serviceConfig.immersiveApiUrl && (this.immersiveApiUrl = mergeUrl(
        this.immersiveApiUrl,
        serviceConfig.immersiveApiUrl
      )), serviceConfig && serviceConfig.systemPrompt && (this.systemPrompt = serviceConfig.systemPrompt);
    }
    getDefaultRateLimit() {
      return { limit: 5, interval: 1300 };
    }
    translate(payload) {
      return this.model.includes("003") ? (this.maxTextGroupLength = 1, this.translate3(payload)) : this.translate3_5(payload);
    }
    /**
     * get random api key
     * @returns random api key
     */
    getRandomKey() {
      let index = Math.floor(Math.random() * this.apiKeys.length);
      return this.apiKeys[index];
    }
    async translate3_5(payload) {
      let { text, from, to } = payload;
      if (text.length === 0)
        return {
          from,
          to,
          text: ""
        };
      let remoteFrom = langMap13.get(from) || from, remoteTo = langMap13.get(to) || to, prompt = this.prompt.replace(/{{to}}/g, remoteTo).replace(
        /{{text}}/g,
        text
      ).replace(/{{from}}/g, remoteFrom), messages = [];
      if (this.systemPrompt) {
        let systemPrompt = this.systemPrompt.replace(/{{to}}/g, remoteTo).replace(
          /{{text}}/g,
          text
        ).replace(/{{from}}/g, remoteFrom);
        messages.push({
          role: "system",
          content: systemPrompt
        });
      }
      this.prompt && messages.push({
        role: "user",
        content: prompt
      }), this.prompt.indexOf("{{text}}") === -1 && messages.push({
        role: "user",
        content: text
      });
      let options2 = {
        url: this.apiUrl,
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          model: this.model,
          temperature: 0,
          max_tokens: 2048,
          messages
        })
      }, randomKey = this.getRandomKey();
      randomKey && (options2.headers.Authorization = "Bearer " + randomKey, options2.headers["api-key"] = randomKey, randomKey.startsWith("immersiveopenai_") && (options2.url = this.immersiveApiUrl));
      let response = await request2(options2);
      if (response && response.choices && response.choices.length > 0 && response.choices[0].message && response.choices[0].message.content) {
        let text2 = response.choices[0].message.content.trim();
        return {
          from,
          to,
          text: text2
        };
      } else
        throw new Error("server response invalid: " + JSON.stringify(response));
    }
    async translate3(payload) {
      let { text, from, to } = payload;
      if (text.length === 0)
        return {
          from,
          to,
          text: ""
        };
      let prompt = this.prompt.replace(/{{to}}/g, langMap13.get(to) || to).replace(
        /{{text}}/g,
        text
      ).replace(/{{from}}/g, langMap13.get(from) || from), options2 = {
        url: "https://api.openai.com/v1/completions",
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + this.APIKEY
        },
        body: JSON.stringify({
          model: this.model,
          prompt,
          temperature: 0,
          max_tokens: 2048
        })
      }, response = await request2(options2);
      if (response && response.choices && response.choices.length > 0 && response.choices[0].text) {
        let text2 = response.choices[0].text.trim();
        return {
          from,
          to,
          text: text2
        };
      } else
        throw new Error("server response invalid: " + JSON.stringify(response));
    }
  }, openai_default = OpenAI;

  // cache.ts
  var CACHE_KEY_PREFIX = brandIdForJs + "CacheKey_";
  function getExpired(rawKey, defaultValue) {
    let key = CACHE_KEY_PREFIX + rawKey;
    return browserAPI.storage.local.get(key).then((result) => {
      if (result[key] === void 0)
        return defaultValue;
      let { value, expired } = result[key];
      return expired && expired < Date.now() ? defaultValue : value;
    });
  }
  function setExpired(rawKey, value, expiredIn) {
    let key = CACHE_KEY_PREFIX + rawKey, expired = Date.now() + expiredIn;
    return browserAPI.storage.local.set({ [key]: { value, expired } });
  }
  function remove(rawKey) {
    let key = CACHE_KEY_PREFIX + rawKey;
    return browserAPI.storage.local.remove(key);
  }