
  // services/deepl.ts
  var globalState = null, rawLangMap2 = [
    ["auto", ""],
    ["zh-CN", "ZH"],
    ["zh-TW", "ZH"],
    ["en", "EN"],
    ["de", "DE"],
    ["fr", "FR"],
    ["it", "IT"],
    ["ja", "JA"],
    ["es", "ES"],
    ["nl", "NL"],
    ["pl", "PL"],
    ["pt", "PT"],
    ["ru", "RU"]
  ], langMap9 = new Map(rawLangMap2), langMapReverse2 = new Map(
    rawLangMap2.map(([translatorLang, lang]) => [lang, translatorLang])
  ), Deepl = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.authKey = "";
      this.maxTextLength = 1800;
      this.freeApiUrl = "https://api-free.deepl.com/v2/translate";
      this.proApiUrl = "https://api.deepl.com/v2/translate";
      this.immersiveTranslateApiUrl = "https://deepl.immersivetranslate.com/v2/translate";
      this.immersiveTranslateDeeplTokenUrl = "https://api.immersivetranslate.com";
      if (!serviceConfig || !serviceConfig.authKey)
        throw new Error("authKey are required");
      serviceConfig && serviceConfig.freeApiUrl && (this.freeApiUrl = mergeUrl(this.freeApiUrl, serviceConfig.freeApiUrl)), serviceConfig && serviceConfig.proApiUrl && (this.proApiUrl = mergeUrl(this.proApiUrl, serviceConfig.proApiUrl)), serviceConfig && serviceConfig.immersiveTranslateApiUrl && (this.immersiveTranslateApiUrl = mergeUrl(
        this.immersiveTranslateApiUrl,
        serviceConfig.immersiveTranslateApiUrl
      )), serviceConfig && serviceConfig.immersiveTranslateDeeplTokenUrl && (this.immersiveTranslateDeeplTokenUrl = mergeUrl(
        this.immersiveTranslateDeeplTokenUrl,
        serviceConfig.immersiveTranslateDeeplTokenUrl
      )), this.authKey = serviceConfig.authKey?.trim();
    }
    static async clearState() {
      globalState = null, await browserAPI.storage.local.remove(
        immersiveTranslateGlobalConfigStorageKey
      );
    }
    async init() {
      if (!globalState) {
        globalState = {};
        let globalStateValue = await browserAPI.storage.local.get(
          immersiveTranslateGlobalConfigStorageKey
        );
        globalStateValue && globalStateValue[immersiveTranslateGlobalConfigStorageKey] && (globalState = globalStateValue[immersiveTranslateGlobalConfigStorageKey]);
      }
      if (this.authKey.startsWith("immersive_")) {
        let deeplInstance = new Q2(
          this.authKey,
          {
            state: globalState,
            onFetch: (url, options2) => {
              let pathname = new URL(url).pathname;
              return pathname === "/refresh_token" || pathname === "/oidc/token" ? throttleRequest({
                url,
                ...options2
              }) : request2({
                url,
                ...options2
              });
            },
            refreshTokenEndpoint: this.immersiveTranslateDeeplTokenUrl,
            onStateChange: (state) => {
              globalState = state, browserAPI.storage.local.set({
                [immersiveTranslateGlobalConfigStorageKey]: globalState
              });
            }
          }
        );
        await deeplInstance.updateToken(), globalState = deeplInstance.getState(), deeplInstance.getIsStateChanged() && await browserAPI.storage.local.set({
          [immersiveTranslateGlobalConfigStorageKey]: globalState
        });
      }
    }
    getDefaultRateLimit() {
      return { limit: 3, interval: 1050 };
    }
    async translateList(payload) {
      let { from, to, text } = payload, bodyParams = {
        source_lang: langMap9.get(from) || "",
        target_lang: langMap9.get(to) || to
      }, bodySearchParams = new URLSearchParams(bodyParams);
      text.forEach((item) => {
        bodySearchParams.append("text", item);
      });
      let body = bodySearchParams.toString(), deeplEndpoint = this.freeApiUrl;
      this.authKey.endsWith(":im") ? deeplEndpoint = this.immersiveTranslateApiUrl : this.authKey.endsWith(":fx") || (deeplEndpoint = this.proApiUrl);
      let response;
      if (this.authKey.startsWith("immersive_")) {
        let deeplInstance = new Q2(
          this.authKey,
          {
            state: globalState,
            onFetch: (url, options2) => {
              let pathname = new URL(url).pathname;
              return pathname === "/refresh_token" || pathname === "/oidc/token" ? throttleRequest({
                url,
                ...options2
              }) : request2({
                url,
                ...options2
              });
            },
            refreshTokenEndpoint: this.immersiveTranslateDeeplTokenUrl,
            onStateChange: (state) => {
              globalState = state, browserAPI.storage.local.set({
                [immersiveTranslateGlobalConfigStorageKey]: globalState
              });
            }
          }
        );
        response = await deeplInstance.translateApi(body), deeplInstance.getIsStateChanged() && (globalState = deeplInstance.getState(), await browserAPI.storage.local.set({
          [immersiveTranslateGlobalConfigStorageKey]: globalState
        }));
      } else
        response = await request2(
          {
            retry: 2,
            url: deeplEndpoint,
            method: "POST",
            body,
            headers: {
              Authorization: "DeepL-Auth-Key " + this.authKey,
              "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            extra: {
              overrideMimeType: "application/json; charset=utf-8"
            }
          }
        );
      let { translations: translations2 } = response, resultText = translations2.map((item) => item.text);
      if (to === "zh-TW") {
        let serviceConfig = this.serviceConfig || {};
        serviceConfig && serviceConfig.googleApiUrl && (serviceConfig.apiUrl = serviceConfig.googleApiUrl);
        let googleResult = await new Google(
          this.serviceConfig,
          this.generalConfig,
          this.translationOptions
        ).translate({
          from: "zh-CN",
          to: "zh-TW",
          text: translations2.map((item) => item.text).join(`
`),
          url: "",
          options: {}
        });
        googleResult && googleResult.text && (resultText = googleResult.text.split(`
`));
      }
      return {
        text: resultText,
        from: translations2[0] && langMapReverse2.get(translations2[0].detected_source_language) || from,
        to
      };
    }
  }, deepl_default = Deepl;