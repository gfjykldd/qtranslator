
  // services/google.ts
  var langMap5 = [
    ["auto", "auto"],
    ["zh-CN", "zh-CN"],
    ["zh-TW", "zh-TW"],
    ["en", "en"],
    ["af", "af"],
    ["am", "am"],
    ["ar", "ar"],
    ["az", "az"],
    ["be", "be"],
    ["bg", "bg"],
    ["bn", "bn"],
    ["bs", "bs"],
    ["ca", "ca"],
    ["ceb", "ceb"],
    ["co", "co"],
    ["cs", "cs"],
    ["cy", "cy"],
    ["da", "da"],
    ["de", "de"],
    ["el", "el"],
    ["eo", "eo"],
    ["es", "es"],
    ["et", "et"],
    ["eu", "eu"],
    ["fa", "fa"],
    ["fi", "fi"],
    ["fr", "fr"],
    ["fy", "fy"],
    ["ga", "ga"],
    ["gd", "gd"],
    ["gl", "gl"],
    ["gu", "gu"],
    ["ha", "ha"],
    ["haw", "haw"],
    ["he", "he"],
    ["hi", "hi"],
    ["hmn", "hmn"],
    ["hr", "hr"],
    ["ht", "ht"],
    ["hu", "hu"],
    ["hy", "hy"],
    ["id", "id"],
    ["ig", "ig"],
    ["is", "is"],
    ["it", "it"],
    ["ja", "ja"],
    ["jw", "jw"],
    ["ka", "ka"],
    ["kk", "kk"],
    ["km", "km"],
    ["kn", "kn"],
    ["ko", "ko"],
    ["ku", "ku"],
    ["ky", "ky"],
    ["la", "la"],
    ["lb", "lb"],
    ["lo", "lo"],
    ["lt", "lt"],
    ["lv", "lv"],
    ["mg", "mg"],
    ["mi", "mi"],
    ["mk", "mk"],
    ["ml", "ml"],
    ["mn", "mn"],
    ["mr", "mr"],
    ["ms", "ms"],
    ["mt", "mt"],
    ["my", "my"],
    ["ne", "ne"],
    ["nl", "nl"],
    ["no", "no"],
    ["ny", "ny"],
    ["pa", "pa"],
    ["pl", "pl"],
    ["ps", "ps"],
    ["pt", "pt"],
    ["ro", "ro"],
    ["ru", "ru"],
    ["sd", "sd"],
    ["si", "si"],
    ["sk", "sk"],
    ["sl", "sl"],
    ["sm", "sm"],
    ["sn", "sn"],
    ["so", "so"],
    ["sq", "sq"],
    ["sr", "sr"],
    ["st", "st"],
    ["su", "su"],
    ["sv", "sv"],
    ["sw", "sw"],
    ["ta", "ta"],
    ["te", "te"],
    ["tg", "tg"],
    ["th", "th"],
    ["fil", "tl"],
    ["tr", "tr"],
    ["ug", "ug"],
    ["uk", "uk"],
    ["ur", "ur"],
    ["uz", "uz"],
    ["vi", "vi"],
    ["xh", "xh"],
    ["yi", "yi"],
    ["yo", "yo"],
    ["zu", "zu"]
  ], _Google = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.isSupportList = !1;
      this.apiUrl = "https://translate.googleapis.com/translate_a/single";
      serviceConfig && serviceConfig.apiUrl && (this.apiUrl = mergeUrl(this.apiUrl, serviceConfig.apiUrl));
    }
    getDefaultRateLimit() {
      return { limit: 10, interval: 1050 };
    }
    async translate(payload) {
      let { text, from, to } = payload;
      if (!text)
        return { ...payload };
      let adaptedFrom = _Google.langMap.get(from) || "auto", adaptedTo = _Google.langMap.get(to) || to, result = await this.fetchWithoutToken(text, adaptedFrom, adaptedTo);
      if (!result)
        throw new Error("google translate NETWORK_ERROR");
      if (!result.data[0] || result.data[0].length <= 0)
        throw new Error("google translate API_SERVER_ERROR");
      return {
        text: result.data[0].map((item) => item[0]).filter(Boolean).join(""),
        from: _Google.langMapReverse.get(result.data[2]) || "auto",
        to
      };
    }
    async translateXml(payload) {
      let { text, from, to } = payload;
      if (!text)
        return { ...payload };
      let adaptedFrom = _Google.langMap.get(from) || "auto", adaptedTo = _Google.langMap.get(to) || to, result = await this.fetchXmlWithoutToken(
        text,
        adaptedFrom,
        adaptedTo
      );
      if (!result)
        throw new Error("google translate NETWORK_ERROR");
      if (!result.data[0] || result.data[0].length <= 0)
        throw new Error("google translate API_SERVER_ERROR");
      return {
        text: result.data[0].map((item) => item[0]).filter(Boolean).join(""),
        from: _Google.langMapReverse.get(result.data[2]) || "auto",
        to
      };
    }
    async fetchXmlWithoutToken(text, from, to) {
      let url = "https://translate.googleapis.com/translate_a/t?" + new URLSearchParams({
        client: "gtx",
        dt: "t",
        sl: from,
        tl: to,
        q: text
      }).toString();
      return { data: await request2({
        retry: 2,
        url
      }) };
    }
    async fetchWithoutToken(text, from, to) {
      let params = new URLSearchParams({
        client: "gtx",
        dt: "t",
        sl: from,
        tl: to,
        q: text
      }), url = this.apiUrl + "?" + params.toString();
      return { data: await request2({
        retry: 2,
        url
      }) };
    }
  }, Google = _Google;
  Google.langMap = new Map(langMap5), Google.langMapReverse = new Map(
    langMap5.map(([translatorLang, lang]) => [lang, translatorLang])
  );