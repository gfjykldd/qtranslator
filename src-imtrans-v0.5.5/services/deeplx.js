
  // services/deeplx.ts
  var rawLangMap10 = [
    ["auto", "auto"],
    ["zh-CN", "ZH"],
    ["zh-TW", "ZH"],
    ["de", "DE"],
    ["en", "EN"],
    ["es", "ES"],
    ["fr", "FR"],
    ["it", "IT"],
    ["ja", "JA"],
    ["pt", "PT"],
    ["ru", "RU"],
    ["tr", "tr"]
  ], langMap17 = new Map(rawLangMap10), Deeplx = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.url = "";
      this.isSupportList = !1;
      this.maxTextGroupLength = 1;
      if (!serviceConfig || !serviceConfig.url)
        throw new Error(
          "deeplx custom url are required, please check your settings."
        );
      this.url = serviceConfig.url;
    }
    async translate(payload) {
      let { text, from, to } = payload, result = await request2(
        {
          retry: 2,
          url: this.url,
          headers: {
            "content-type": "application/json"
          },
          method: "POST",
          body: JSON.stringify({
            source_lang: langMap17.get(from) || from,
            target_lang: langMap17.get(to) || to,
            text
          })
        }
      );
      if (result.code === 200)
        return {
          text: result.data,
          from,
          to
        };
      throw new Error(result.message || result.message || "API Error");
    }
  };