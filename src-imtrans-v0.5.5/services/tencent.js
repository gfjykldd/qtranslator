
  // services/tencent.ts
  var langMap3 = [
    ["auto", "auto"],
    ["zh-CN", "zh"],
    ["zh-TW", "zh-TW"],
    ["de", "de"],
    ["en", "en"],
    ["es", "es"],
    ["fr", "fr"],
    ["id", "id"],
    ["it", "it"],
    ["ja", "jp"],
    ["ko", "kr"],
    ["ms", "ms"],
    ["pt", "pt"],
    ["ru", "ru"],
    ["th", "th"],
    ["tr", "tr"],
    ["vi", "vi"]
  ], _Tencent = class extends Translation {
    // throttleLimit = 1;
    // maxTextGroupLength = 1;
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.secretId = "";
      this.secretKey = "";
      this.isSupportList = !0;
      if (!serviceConfig || !serviceConfig.secretId || !serviceConfig.secretKey)
        throw new Error("secretId and secretKey are required");
      this.secretId = serviceConfig.secretId?.trim(), this.secretKey = serviceConfig.secretKey?.trim();
    }
    static getUTCDate(dateObj) {
      let year = dateObj.getUTCFullYear(), month = `${dateObj.getUTCMonth() + 1}`.padStart(2, "0"), date = `${dateObj.getUTCDate()}`.padStart(2, "0");
      return `${year}-${month}-${date}`;
    }
    async translate(payload) {
      let { text, from, to } = payload, RequestPayload = JSON.stringify({
        ProjectId: 0,
        Source: _Tencent.langMap.get(from) || "auto",
        SourceText: text,
        Target: _Tencent.langMap.get(to) || to
      }), data = await this.signedRequest({
        secretId: this.secretId,
        secretKey: this.secretKey,
        action: "TextTranslate",
        payload: RequestPayload,
        service: "tmt",
        version: "2018-03-21"
      });
      return {
        text: data.Response.TargetText,
        from: _Tencent.langMapReverse.get(data.Response.Source) || from,
        to: _Tencent.langMapReverse.get(data.Response.Target) || to
      };
    }
    async translateList(payload) {
      let { text, from, to } = payload, RequestPayload = JSON.stringify({
        ProjectId: 0,
        Source: _Tencent.langMap.get(from) || "auto",
        SourceTextList: text,
        Target: _Tencent.langMap.get(to) || to
      }), data = await this.signedRequest({
        secretId: this.secretId,
        secretKey: this.secretKey,
        action: "TextTranslateBatch",
        payload: RequestPayload,
        service: "tmt",
        version: "2018-03-21"
      });
      return {
        text: data.Response.TargetTextList,
        from: _Tencent.langMapReverse.get(data.Response.Source) || from,
        to: _Tencent.langMapReverse.get(data.Response.Target) || to
      };
    }
    async signedRequest({
      secretId,
      secretKey,
      action,
      payload,
      service,
      version
    }) {
      let host = `${service}.tencentcloudapi.com`, now = /* @__PURE__ */ new Date(), timestamp = `${(/* @__PURE__ */ new Date()).valueOf()}`.slice(0, 10), CanonicalRequest = [
        "POST",
        "/",
        "",
        "content-type:application/json; charset=utf-8",
        `host:${host}`,
        "",
        "content-type;host",
        await sha256(payload)
      ].join(`
`), datestamp = _Tencent.getUTCDate(now), StringToSign = [
        "TC3-HMAC-SHA256",
        timestamp,
        `${datestamp}/${service}/tc3_request`,
        await sha256(CanonicalRequest)
      ].join(`
`), SecretDate = await hmacSha256ByString(datestamp, `TC3${secretKey}`), SecretService = await hmacSha256ByArrayBuffer(
        service,
        SecretDate
      ), SecretSigning = await hmacSha256ByArrayBuffer(
        "tc3_request",
        SecretService
      ), Signature = await hmacSha256ByArrayBuffer(
        StringToSign,
        SecretSigning
      ), response = await request2({
        retry: 1,
        url: `https://${service}.tencentcloudapi.com`,
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          Host: host,
          "X-TC-Action": action,
          "X-TC-Timestamp": timestamp,
          "X-TC-Region": "ap-beijing",
          "X-TC-Version": version,
          Authorization: `TC3-HMAC-SHA256 Credential=${secretId}/${datestamp}/${service}/tc3_request, SignedHeaders=content-type;host, Signature=${Signature}`
        },
        body: payload
      });
      if (response instanceof Error)
        throw response;
      if (response.Response && response.Response.Error && response.Response.Error.Message)
        throw new Error(
          response.Response.Error.Message
        );
      return response;
    }
  }, Tencent = _Tencent;
  /** Translator lang to custom lang */
  Tencent.langMap = new Map(langMap3), /** Custom lang to translator lang */
  Tencent.langMapReverse = new Map(
    langMap3.map(([translatorLang, lang]) => [lang, translatorLang])
  );