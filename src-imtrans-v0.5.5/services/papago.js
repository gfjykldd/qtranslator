
  // services/papago.ts
  var rawLangMap5 = [
    ["auto", "auto"],
    ["zh-CN", "zh-CN"],
    ["zh-TW", "zh-TW"],
    ["en", "en"],
    ["ja", "ja"],
    ["ru", "ru"],
    ["es", "es"],
    ["de", "de"],
    ["ko", "ko"],
    ["fr", "fr"],
    ["th", "th"],
    ["vi", "vi"],
    ["id", "id"]
  ], langMap12 = new Map(rawLangMap5), Papago = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.isSupportList = !0;
      this.maxTextGroupLength = 25;
      this.maxTextLength = 1200;
    }
    getDefaultRateLimit() {
      return { limit: 3, interval: 1050 };
    }
    async translateList(payload) {
      let { text, from, to } = payload, langCode;
      if (text.length === 0)
        return {
          from,
          to,
          text: []
        };
      let newLineSplitter = `
<br>
`, bodyStr = text.join(newLineSplitter), options2 = {
        url: "https://api.papago-chrome.com/v2/translate/openapi",
        retry: 2,
        method: "POST",
        headers: {
          authority: "api.papago-chrome.com",
          "content-type": "application/json"
        },
        body: ""
      };
      from === "auto" || !langMap12.get(from) ? (langCode = (await request2({
        url: "https://api.papago-chrome.com/v2/translate/detect",
        method: "POST",
        headers: {
          authority: "api.papago-chrome.com",
          "content-type": "application/json"
        },
        body: bodyStr
      })).langCode, options2.body = JSON.stringify({
        text: bodyStr,
        source: langCode || langMap12.get(from) || from,
        target: langMap12.get(to) || to
      })) : options2.body = JSON.stringify({
        text: bodyStr,
        source: langMap12.get(from),
        target: langMap12.get(to) || to
      });
      let response = await request2(options2);
      if (response.translatedText === "")
        throw new Error("server response invalid");
      let texts = response.translatedText.split("<br>").map(
        (item) => item.trim()
      );
      return {
        from,
        to,
        text: texts
      };
    }
  }, papago_default = Papago;