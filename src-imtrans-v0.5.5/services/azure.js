
  // services/azure.ts
  var rawLangMap4 = [
    ["auto", ""],
    ["ar", "ar"],
    ["ga", "ga"],
    ["et", "et"],
    ["bg", "bg"],
    ["is", "is"],
    ["pl", "pl"],
    ["bs", "bs-Latn"],
    ["fa", "fa"],
    ["da", "da"],
    ["de", "de"],
    ["ru", "ru"],
    ["fr", "fr"],
    ["zh-TW", "zh-Hant"],
    ["fil", "fil"],
    ["fj", "fj"],
    ["fi", "fi"],
    ["gu", "gu"],
    ["kk", "kk"],
    ["ht", "ht"],
    ["ko", "ko"],
    ["nl", "nl"],
    ["ca", "ca"],
    ["zh-CN", "zh-Hans"],
    ["cs", "cs"],
    ["kn", "kn"],
    ["otq", "otq"],
    ["tlh", "tlh"],
    ["hr", "hr"],
    ["lv", "lv"],
    ["lt", "lt"],
    ["ro", "ro"],
    ["mg", "mg"],
    ["mt", "mt"],
    ["mr", "mr"],
    ["ml", "ml"],
    ["ms", "ms"],
    ["mi", "mi"],
    ["bn", "bn-BD"],
    ["hmn", "mww"],
    ["af", "af"],
    ["pa", "pa"],
    ["pt", "pt"],
    ["ps", "ps"],
    ["ja", "ja"],
    ["sv", "sv"],
    ["sm", "sm"],
    ["sr-Latn", "sr-Latn"],
    ["sr-Cyrl", "sr-Cyrl"],
    ["no", "nb"],
    ["sk", "sk"],
    ["sl", "sl"],
    ["sw", "sw"],
    ["ty", "ty"],
    ["te", "te"],
    ["ta", "ta"],
    ["th", "th"],
    ["to", "to"],
    ["tr", "tr"],
    ["cy", "cy"],
    ["ur", "ur"],
    ["uk", "uk"],
    ["es", "es"],
    ["he", "iw"],
    ["el", "el"],
    ["hu", "hu"],
    ["it", "it"],
    ["hi", "hi"],
    ["id", "id"],
    ["en", "en"],
    ["yua", "yua"]
  ], 
  langMap11 = new Map(rawLangMap4), 
  Azure = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.APIKEY = "";
      this.region = "eastasia";
      this.isSupportList = !0;
      if (!serviceConfig || !serviceConfig.APIKEY)
        throw new Error("APIKEY are required");
      this.APIKEY = serviceConfig.APIKEY?.trim(), serviceConfig.region && (this.region = serviceConfig.region);
    }
    async translateList(payload) {
      let { text, from, to } = payload;
      if (text.length === 0)
        return {
          from,
          to,
          text: []
        };
      let paramsObj = {
        "api-version": "3.0",
        to: langMap11.get(to) || to
      };
      from !== "auto" && (paramsObj.from = langMap11.get(from) || from);
      let params = new URLSearchParams(paramsObj), bodyStr = JSON.stringify(text.map((t4) => ({ text: t4 }))), options2 = {
        url: "https://api.cognitive.microsofttranslator.com/translate?" + params.toString(),
        retry: 2,
        method: "POST",
        headers: {
          "Ocp-Apim-Subscription-Key": this.APIKEY,
          "Ocp-Apim-Subscription-Region": this.region,
          "content-type": "application/json"
        },
        body: bodyStr
      }, response = await request2(options2);
      if (response.length === 0)
        throw new Error("server response invalid");
      return {
        from,
        to,
        text: response.map((item) => item.translations.map((i3) => i3.text).join(" "))
      };
    }
  },   
  // azure_default = Azure;
  azure_default = Scidown;