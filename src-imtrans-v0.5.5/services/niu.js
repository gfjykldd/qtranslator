
  // services/niu.ts
  var rawLangMap3 = [
    ["auto", "auto"],
    ["zh-CN", "zh"],
    ["zh-TW", "cht"],
    ["en", "en"],
    ["ja", "ja"],
    ["ru", "ru"],
    ["es", "es"],
    ["de", "de"],
    ["ko", "ko"],
    ["fr", "fr"]
  ], langMap10 = new Map(rawLangMap3), Niu = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.APIKEY = "";
      this.isSupportList = !1;
      if (!serviceConfig || !serviceConfig.APIKEY)
        throw new Error("APIKEY are required");
      this.APIKEY = serviceConfig.APIKEY?.trim();
    }
    async translate(payload) {
      let { text, from, to } = payload, options2 = {
        url: "https://api.niutrans.com/NiuTransServer/translation",
        retry: 2,
        headers: {
          "content-type": "application/json"
        },
        method: "POST",
        body: JSON.stringify({
          src_text: text,
          from: langMap10.get(from) || from,
          to: langMap10.get(to) || to,
          apikey: this.APIKEY
        })
      }, response = await request2(
        options2
      );
      if (response.tgt_text) {
        let result = response.tgt_text;
        return result.endsWith(`
`) && (result = result.slice(0, -1)), {
          text: result,
          from,
          to
        };
      } else
        throw new Error(JSON.stringify(response));
    }
  }, niu_default = Niu;