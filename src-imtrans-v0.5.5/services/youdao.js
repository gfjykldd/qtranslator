
  // services/youdao.ts
  var rawLangMap16 = [
    ["auto", "auto"],
    ["en", "en"],
    ["ru", "ru"],
    ["pt", "pt"],
    ["es", "es"],
    ["zh-CN", "zh-CHS"],
    ["ja", "ja"],
    ["ko", "ko"],
    ["fr", "fr"],
    ["ar", "ar"],
    ["id", "id"],
    ["vi", "vi"],
    ["it", "it"]
  ], langMap23 = new Map(rawLangMap16), langMapReverse7 = new Map(
    rawLangMap16.map(([translatorLang, lang]) => [lang, translatorLang])
  );
  function truncate(q6) {
    let len = q6.length;
    return len <= 20 ? q6 : q6.substring(0, 10) + len + q6.substring(len - 10, len);
  }
  var Youdao = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.isSupportList = !1;
      this.appId = "";
      this.appSecret = "";
      this.throttleLimit = 5;
      if (!serviceConfig || !serviceConfig.appId || !serviceConfig.appSecret)
        throw new Error("appId and appSecret are required");
      this.appId = serviceConfig.appId?.trim(), this.appSecret = serviceConfig.appSecret?.trim();
    }
    getDefaultRateLimit() {
      return { limit: 5, interval: 1050 };
    }
    async translate(payload) {
      let { text, from, to } = payload, salt = makeid2(32), curTime = Math.round((/* @__PURE__ */ new Date()).getTime() / 1e3), str1 = this.appId + truncate(text) + salt + curTime + this.appSecret, sign = await sha256(str1), params = {
        q: text,
        appKey: this.appId,
        salt: salt.toString(),
        from: langMap23.get(from) || "auto",
        to: langMap23.get(to) || to,
        sign,
        signType: "v3",
        curtime: curTime.toString()
      }, urlSearchParams = new URLSearchParams(params), result = await request2(
        {
          url: "https://openapi.youdao.com/api",
          method: "POST",
          body: urlSearchParams.toString(),
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      );
      if (!result.translation)
        throw new Error(JSON.stringify(result));
      let l6 = result.l, [remoteFrom, _3] = l6.split("2");
      return {
        text: result.translation.join(`
`),
        from: langMapReverse7.get(remoteFrom),
        to
      };
    }
  };
  function makeid2(length) {
    let result = "", characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", charactersLength = characters.length, counter = 0;
    for (; counter < length; )
      result += characters.charAt(Math.floor(Math.random() * charactersLength)), counter += 1;
    return result;
  }
  var youdao_default = Youdao;

  // services/you.ts
  var rawLangMap17 = [
    ["auto", "auto"],
    ["en", "en"],
    ["ru", "ru"],
    ["pt", "pt"],
    ["es", "es"],
    ["zh-CN", "zh-CHS"],
    ["ja", "ja"],
    ["ko", "ko"],
    ["fr", "fr"],
    ["ar", "ar"],
    ["id", "id"],
    ["vi", "vi"],
    ["it", "it"]
  ], langMap24 = new Map(rawLangMap17), langMapReverse8 = new Map(
    rawLangMap17.map(([translatorLang, lang]) => [lang, translatorLang])
  );
  var You = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.isSupportList = !1;
      this.appId = "";
      this.appSecret = "";
      this.maxTextLength = 800;
    }
    getDefaultRateLimit() {
      return { limit: 5, interval: 1050 };
    }
    async translate(payload) {
      let { text, from, to } = payload, params = {
        q: text,
        from: langMap24.get(from) || "auto",
        to: langMap24.get(to) || to
      }, urlSearchParams = new URLSearchParams(params), res = await request2(
        {
          url: "https://aidemo.youdao.com/trans",
          method: "POST",
          body: urlSearchParams.toString(),
          headers: {
            "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36"
          }
        }
      );
      if (res && res.translation)
        return {
          text: res.translation.join(`
`),
          from,
          to
        };
      throw new Error("Youdao translation failed: " + JSON.stringify(res));
    }
  }, you_default = You;