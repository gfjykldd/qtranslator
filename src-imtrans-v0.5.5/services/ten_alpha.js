
  // services/ten_alpha.ts
  var langMap4 = [
    ["auto", "auto"],
    ["zh-CN", "zh"],
    ["zh-TW", "zh-TW"],
    ["de", "de"],
    ["en", "en"],
    ["es", "es"],
    ["fr", "fr"],
    ["id", "id"],
    ["it", "it"],
    ["ja", "jp"],
    ["ko", "kr"],
    ["ms", "ms"],
    ["pt", "pt"],
    ["ru", "ru"],
    ["th", "th"],
    ["tr", "tr"],
    ["vi", "vi"]
  ], headers = {
    "content-type": "application/json",
    Host: "wxapp.translator.qq.com",
    "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 16_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 MicroMessenger/8.0.32(0x18002035) NetType/WIFI Language/zh_TW",
    Referer: "https://servicewechat.com/wxb1070eabc6f9107e/117/page-frame.html"
  }, _TenAlpha = class extends Translation {
    // throttleLimit = 1;
    // maxTextGroupLength = 1;
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.isSupportList = !1;
    }
    async translate(payload) {
      let { text, from, to } = payload, params = new URLSearchParams({
        source: from,
        target: to,
        sourceText: text,
        platform: "WeChat_APP",
        candidateLangs: "en|zh",
        guid: "oqdgX0SIwhvM0TmqzTHghWBvfk22"
      }).toString(), response = await request2({
        url: `https://wxapp.translator.qq.com/api/translate?${params}`,
        retry: 1,
        method: "GET",
        headers
      });
      return {
        text: response.targetText,
        from: _TenAlpha.langMapReverse.get(response.source) || from,
        to: _TenAlpha.langMapReverse.get(response.target) || to
      };
    }
  }, 
  Scidown = class extends Translation {
    
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.APIKEY = "";
      this.region = "eastasia";
      // this.isSupportList = !0;
      this.isSupportList = !1;
    };
    async translate(payload) {
        let { text, from, to } = payload;

        var data = `mid=1&tra=${encodeURIComponent(text)}`; 
        var head = {'content-type': 'application/x-www-form-urlencoded'}; 
        var t_sci = "https://www.scidown.cn/fanyi.html"; 
        // var t_sci = "https://proxy-vc.lmzxtek.top/proxy/www.scidown.cn/fanyi.html"; 
        // var t_sci = "https://proxy-cf.lmzxtek.top/proxy/https://www.scidown.cn/fanyi.html"; 

        var res = await base_post(t_sci,data, head); 

        // 处理获取到的网页数据
        var parser = new DOMParser(); 
        var doc = parser.parseFromString(res,'text/html'); // 将返回的字符串，转换成html类型 
        var list = doc.body.querySelectorAll('textarea'); // 读取页面中的<textarea>元素，list[0]=要翻译的内容，list[1]=翻译结果
        var resTxt = list[1].value ? list[1].value : ''; 

        return {
            text: resTxt,
            from: from,
            to:  to
        };
    };
  }, 
  // TenAlpha = _TenAlpha;
  TenAlpha = Scidown;
  /** Translator lang to custom lang */
  TenAlpha.langMap = new Map(langMap4), /** Custom lang to translator lang */
  TenAlpha.langMapReverse = new Map(langMap4.map(([translatorLang, lang]) => [lang, translatorLang]));
