
  // services/cache.ts
  var dbNames = [];
  async function openIndexedDB(dbName, _storageName) {
    return await new Promise((resolve, reject) => {
      let name = dbName, version = 1, request3 = indexedDB.open(name, version);
      request3.onsuccess = (_event) => {
        resolve(request3.result);
      }, request3.onerror = (event) => {
        reject();
      }, request3.onupgradeneeded = (_event) => {
        let db = request3.result, storageName = _storageName || "cache";
        db.createObjectStore(storageName, {
          keyPath: "key"
        });
      };
    });
  }
  async function setParagraphCache(payload) {
    let storageName = `${brandId}-${payload.service}@${payload.from}->${payload.to}`;
    return await addInDB(storageName, payload);
  }
  async function queryParagraphCache(payload) {
    let payload_hash = md5(payload.originalText), storageName = `${brandId}-${payload.service}@${payload.from}->${payload.to}`;
    return await queryInDB(storageName, payload_hash);
  }
  async function queryInDB(dbName, origTextHash) {
    let db = await openIndexedDB(dbName);
    return await new Promise((resolve, reject) => {
      if (!db)
        return reject();
      let storageName = "cache", request3 = db.transaction([storageName], "readonly").objectStore(storageName).get(origTextHash);
      request3.onsuccess = (_event) => {
        db.close();
        let result = request3.result;
        resolve(result);
      }, request3.onerror = (event) => {
        db.close(), reject();
      };
    });
  }
  async function addInDB(dbName, cacheEntry) {
    let db = await openIndexedDB(dbName);
    return (await getAllDBNames()).includes(dbName) || await addCacheList(dbName), await new Promise((resolve) => {
      if (!db)
        return resolve(!1);
      let storageName = "cache", request3 = db.transaction([storageName], "readwrite").objectStore(storageName).put(cacheEntry);
      request3.onsuccess = (_event) => {
        db.close(), resolve(!0);
      }, request3.onerror = (event) => {
        db.close(), resolve(!1);
      };
    });
  }
  async function addCacheList(dbName) {
    let storageName = "cache_list", db = await openIndexedDB(brandId + "-cacheList", storageName), request3 = db.transaction([storageName], "readwrite").objectStore(storageName).put({ key: dbName });
    request3.onsuccess = (_event) => {
      db.close(), dbNames.push(dbName);
    }, request3.onerror = (event) => {
      db.close();
    };
  }
  async function getAllDBNames() {
    if (dbNames && dbNames.length > 0)
      return dbNames;
    let db = await openIndexedDB(brandId + "-cacheList", "cache_list");
    return dbNames = await new Promise((resolve) => {
      let storageName = "cache_list", request3 = db.transaction([storageName], "readonly").objectStore(storageName).getAllKeys();
      request3.onsuccess = (_event) => {
        db.close(), resolve(request3.result);
      }, request3.onerror = (event) => {
        db.close(), resolve([]);
      };
    }), dbNames;
  }