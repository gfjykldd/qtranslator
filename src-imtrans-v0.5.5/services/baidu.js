
  // services/baidu.ts
  var rawLangMap13 = [
    ["auto", "auto"],
    ["zh-CN", "zh"],
    ["en", "en"],
    ["yue", "yue"],
    ["wyw", "wyw"],
    ["ja", "jp"],
    ["ko", "kor"],
    ["fr", "fra"],
    ["es", "spa"],
    ["th", "th"],
    ["ar", "ara"],
    ["ru", "ru"],
    ["pt", "pt"],
    ["de", "de"],
    ["it", "it"],
    ["el", "el"],
    ["nl", "nl"],
    ["pl", "pl"],
    ["bg", "bul"],
    ["et", "est"],
    ["da", "dan"],
    ["fi", "fin"],
    ["cs", "cs"],
    ["ro", "rom"],
    ["sl", "slo"],
    ["sv", "swe"],
    ["hu", "hu"],
    ["zh-TW", "cht"],
    ["vi", "vie"]
  ], langMap20 = new Map(rawLangMap13), langMapReverse6 = new Map(
    rawLangMap13.map(([translatorLang, lang]) => [lang, translatorLang])
  ), Baidu = class extends Translation {
    constructor(serviceConfig, generalConfig, options2) {
      super(serviceConfig, generalConfig, options2);
      this.endpoint = "https://api.fanyi.baidu.com/api/trans/vip/translate";
      this.appid = "";
      this.key = "";
      this.isSupportList = !1;
      this.maxTextGroupLength = 20;
      if (!serviceConfig || !serviceConfig.appid || !serviceConfig.key)
        throw new Error("appid and key are required");
      this.appid = serviceConfig.appid?.trim(), this.key = serviceConfig.key?.trim();
    }
    getDefaultRateLimit() {
      return { limit: 1, interval: 1550 };
    }
    async translate(payload) {
      let salt = Date.now().toString(), { endpoint } = this, { appid, key } = this, { text, from, to } = payload, params = new URLSearchParams({
        from: langMap20.get(from) || "auto",
        to: langMap20.get(to) || to,
        q: text,
        salt,
        appid,
        sign: md5(appid + text + salt + key)
      }), urlObj = new URL(endpoint);
      urlObj.search = params.toString();
      let data = await request2(
        {
          url: urlObj.toString()
        }
      );
      if (data.error_code)
        throw new CommonError(
          "API_SERVER_ERROR",
          data.error_msg
        );
      let {
        trans_result: transResult,
        from: langDetected
      } = data, transParagraphs = transResult.map(({ dst }) => dst);
      return {
        from: langMapReverse6.get(langDetected) || langDetected,
        to,
        text: transParagraphs.join(`
`)
      };
    }
  }, baidu_default = Baidu;