
  // services/bing/api.js
  var TRANSLATE_API_ROOT = "https://{s}bing.com", TRANSLATE_WEBSITE = TRANSLATE_API_ROOT + "/translator", TRANSLATE_API = TRANSLATE_API_ROOT + "/ttranslatev3", TRANSLATE_SPELL_CHECK_API = TRANSLATE_API_ROOT + "/tspellcheckv3", globalConfigStorageKey = "bingGlobalConfig", rawLangMap11 = [
    ["auto", "auto-detect"],
    ["ar", "ar"],
    ["ga", "ga"],
    ["et", "et"],
    ["bg", "bg"],
    ["is", "is"],
    ["pl", "pl"],
    ["bs", "bs-Latn"],
    ["fa", "fa"],
    ["da", "da"],
    ["de", "de"],
    ["ru", "ru"],
    ["fr", "fr"],
    ["zh-TW", "zh-Hant"],
    ["fil", "fil"],
    ["fj", "fj"],
    ["fi", "fi"],
    ["gu", "gu"],
    ["kk", "kk"],
    ["ht", "ht"],
    ["ko", "ko"],
    ["nl", "nl"],
    ["ca", "ca"],
    ["zh-CN", "zh-Hans"],
    ["cs", "cs"],
    ["kn", "kn"],
    ["otq", "otq"],
    ["tlh", "tlh"],
    ["hr", "hr"],
    ["lv", "lv"],
    ["lt", "lt"],
    ["ro", "ro"],
    ["mg", "mg"],
    ["mt", "mt"],
    ["mr", "mr"],
    ["ml", "ml"],
    ["ms", "ms"],
    ["mi", "mi"],
    ["bn", "bn-BD"],
    ["hmn", "mww"],
    ["af", "af"],
    ["pa", "pa"],
    ["pt", "pt"],
    ["ps", "ps"],
    ["ja", "ja"],
    ["sv", "sv"],
    ["sm", "sm"],
    ["sr-Latn", "sr-Latn"],
    ["sr-Cyrl", "sr-Cyrl"],
    ["no", "nb"],
    ["sk", "sk"],
    ["sl", "sl"],
    ["sw", "sw"],
    ["ty", "ty"],
    ["te", "te"],
    ["ta", "ta"],
    ["th", "th"],
    ["to", "to"],
    ["tr", "tr"],
    ["cy", "cy"],
    ["ur", "ur"],
    ["uk", "uk"],
    ["es", "es"],
    ["he", "iw"],
    ["el", "el"],
    ["hu", "hu"],
    ["it", "it"],
    ["hi", "hi"],
    ["id", "id"],
    ["en", "en"],
    ["yua", "yua"],
    ["yue", "yua"],
    ["vi", "vi"],
    ["ku", "ku"],
    ["km", "kmr"]
  ], langMap18 = new Map(rawLangMap11), langMapReverse5 = new Map(
    rawLangMap11.map(([translatorLang, lang]) => [lang, translatorLang])
  ), MAX_TEXT_LEN = 1e3, globalConfig, globalConfigPromise;
  function replaceSubdomain(url, subdomain) {
    return url.replace("{s}", subdomain ? subdomain + "." : "");
  }
  async function isTokenExpired() {
    if (!globalConfig) {
      let storageValue = await browserAPI.storage.local.get(
        globalConfigStorageKey
      );
      return storageValue && (globalConfig = storageValue[globalConfigStorageKey]), !0;
    }
    let { tokenTs, tokenExpiryInterval } = globalConfig;
    return Date.now() - tokenTs > tokenExpiryInterval;
  }
  async function fetchGlobalConfig() {
    let subdomain, IG, IID, token, key, tokenExpiryInterval, isVertical, frontDoorBotClassification, isSignedInOrCorporateUser, cookie;
    try {
      let finalUrl = replaceSubdomain(TRANSLATE_WEBSITE, subdomain), response = await request2({
        retry: 2,
        url: finalUrl,
        responseType: "raw"
      }), { body, headers: headers3, url } = response;
      subdomain = url.match(/^https?:\/\/(\w+)\.bing\.com/)[1], cookie = headers3["set-cookie"], IG = body.match(/IG:"([^"]+)"/)[1], IID = body.match(/data-iid="([^"]+)"/)[1], [
        key,
        token,
        tokenExpiryInterval,
        isVertical,
        frontDoorBotClassification,
        isSignedInOrCorporateUser
      ] = JSON.parse(
        body.match(/params_AbusePreventionHelper\s?=\s?([^\]]+\])/)[1]
      );
    } catch (e) {
      throw e;
    }
    return globalConfig = {
      subdomain,
      IG,
      IID,
      key,
      token,
      tokenTs: key,
      tokenExpiryInterval,
      isVertical,
      frontDoorBotClassification,
      isSignedInOrCorporateUser,
      cookie,
      // PENDING: reset count if count value is large?
      count: 0
    }, await browserAPI.storage.local.set({
      [globalConfigStorageKey]: globalConfig
    }), globalConfig;
  }
  function makeRequestURL(isSpellCheck) {
    let { IG, IID, subdomain, isVertical } = globalConfig;
    return replaceSubdomain(
      isSpellCheck ? TRANSLATE_SPELL_CHECK_API : TRANSLATE_API,
      subdomain
    ) + "?isVertical=1" + (IG && IG.length ? "&IG=" + IG : "") + (IID && IID.length ? "&IID=" + IID + "." + globalConfig.count++ : "");
  }
  function makeRequestBody(isSpellCheck, text, fromLang, toLang) {
    let { token, key } = globalConfig, body = {
      fromLang,
      text,
      token,
      key
    };
    return !isSpellCheck && toLang && (body.to = toLang), body;
  }
  async function translate2(text, from, to) {
    if (!text || !(text = text.trim()))
      return;
    if (text.length > MAX_TEXT_LEN)
      throw new Error(
        `The supported maximum length of text is ${MAX_TEXT_LEN}. Please shorten the text.`
      );
    globalConfigPromise || (globalConfigPromise = fetchGlobalConfig()), await globalConfigPromise, await isTokenExpired() && (globalConfigPromise = fetchGlobalConfig(), await globalConfigPromise), from = from || "auto", to = to || "zh-CN", from = langMap18.get(from) || from, to = langMap18.get(to) || to;
    let requestURL = makeRequestURL(!1), requestBody = makeRequestBody(
      !1,
      text,
      from,
      to === "auto-detect" ? "zh-Hans" : to
    ), requestHeaders = {
      referer: replaceSubdomain(TRANSLATE_WEBSITE, globalConfig.subdomain),
      // cookie: globalConfig.cookie,
      "content-type": "application/x-www-form-urlencoded"
    }, searchParams = new URLSearchParams(requestBody), finalUrl = requestURL, requestBodyString = searchParams.toString(), body = await request2({
      retry: 2,
      url: finalUrl,
      headers: requestHeaders,
      method: "POST",
      body: requestBodyString
    });
    if (body.ShowCaptcha || body.StatusCode === 401 || body.statusCode) {
      if (globalConfig = null, globalConfigPromise = null, await browserAPI.storage.local.remove(globalConfigStorageKey), body.ShowCaptcha)
        throw new Error(`
      Sorry that bing translator seems to be asking for the captcha,
      Please take care not to request too frequently.
      The response code is ${body.StatusCode}.
    `);
      if (body.StatusCode === 401)
        throw new Error(`
      Max count of translation exceeded. Please try it again later.
      The response code is 401.
    `);
      if (body.statusCode)
        throw new Error(
          `Something went wrong! The response is ${JSON.stringify(body)}.`
        );
    }
    let translation = body[0].translations[0], detectedLang = body[0].detectedLanguage;
    return {
      text: translation.text,
      from: langMapReverse5.get(detectedLang.language),
      to: langMapReverse5.get(translation.to)
    };
  }

  
  // utils/parse_jwt.ts
  function parseJWT(token) {
    let parts = token.split(".");
    if (parts.length <= 1)
      throw new Error("invlaid token");
    let base64Url = parts[1];
    if (!base64Url)
      throw new Error("invalid base64 url token");
    let base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/"), jsonPayload = decodeURIComponent(
      globalThis.atob(base64).split("").map(function(c5) {
        return "%" + ("00" + c5.charCodeAt(0).toString(16)).slice(-2);
      }).join("")
    ), parsed = JSON.parse(jsonPayload), expiredDate = new Date(parsed.exp * 1e3);
    return {
      accessToken: token,
      accessTokenExpiresAt: expiredDate.toISOString()
    };
  }

  // services/bing/mod.ts
  var globalState2 = null, rawLangMap12 = [
    ["auto", ""],
    ["ar", "ar"],
    ["ga", "ga"],
    ["et", "et"],
    ["bg", "bg"],
    ["is", "is"],
    ["pl", "pl"],
    ["bs", "bs-Latn"],
    ["fa", "fa"],
    ["da", "da"],
    ["de", "de"],
    ["ru", "ru"],
    ["fr", "fr"],
    ["zh-TW", "zh-Hant"],
    ["fil", "fil"],
    ["fj", "fj"],
    ["fi", "fi"],
    ["gu", "gu"],
    ["kk", "kk"],
    ["ht", "ht"],
    ["ko", "ko"],
    ["nl", "nl"],
    ["ca", "ca"],
    ["zh-CN", "zh-Hans"],
    ["cs", "cs"],
    ["kn", "kn"],
    ["otq", "otq"],
    ["tlh", "tlh"],
    ["hr", "hr"],
    ["lv", "lv"],
    ["lt", "lt"],
    ["ro", "ro"],
    ["mg", "mg"],
    ["mt", "mt"],
    ["mr", "mr"],
    ["ml", "ml"],
    ["ms", "ms"],
    ["mi", "mi"],
    ["bn", "bn-BD"],
    ["hmn", "mww"],
    ["af", "af"],
    ["pa", "pa"],
    ["pt", "pt"],
    ["ps", "ps"],
    ["ja", "ja"],
    ["sv", "sv"],
    ["sm", "sm"],
    ["sr-Latn", "sr-Latn"],
    ["sr-Cyrl", "sr-Cyrl"],
    ["no", "nb"],
    ["sk", "sk"],
    ["sl", "sl"],
    ["sw", "sw"],
    ["ty", "ty"],
    ["te", "te"],
    ["ta", "ta"],
    ["th", "th"],
    ["to", "to"],
    ["tr", "tr"],
    ["cy", "cy"],
    ["ur", "ur"],
    ["uk", "uk"],
    ["es", "es"],
    ["he", "iw"],
    ["el", "el"],
    ["hu", "hu"],
    ["it", "it"],
    ["hi", "hi"],
    ["id", "id"],
    ["en", "en"],
    ["yua", "yua"]
  ], langMap19 = new Map(rawLangMap12), Bing = class extends Translation {
    constructor() {
      super(...arguments);
      this.isSupportList = !0;
      this.maxTextLength = 1800;
    }
    static async clearState() {
      globalState2 = null, await remove(immersiveTranslateBingGlobalConfigStorageKey);
    }
    async translate(payload) {
      let { text, from, to } = payload;
      return text ? await translate2(text, from, to) : { ...payload };
    }
    async init() {
      if (!globalState2) {
        globalState2 = {};
        let globalStateValue = await getExpired(
          immersiveTranslateBingGlobalConfigStorageKey
        );
        globalStateValue && (globalState2 = globalStateValue);
      }
      await tryUpdateAccessToken(globalState2);
    }
    async translateList(payload) {
      let { from, to, text } = payload, remoteFrom = langMap19.get(from) || "auto", remoteTo = langMap19.get(to) || to;
      remoteFrom === "auto" && (remoteFrom = "");
      let auth = await tryUpdateAccessToken(globalState2), body = [];
      for (let item of text)
        body.push({ Text: item });
      let bodyString = JSON.stringify(body), url = `https://api-edge.cognitive.microsofttranslator.com/translate?from=${remoteFrom}&to=${remoteTo}&api-version=3.0&includeSentenceLength=true`, result = await request2(
        {
          url,
          headers: {
            accept: "*/*",
            "accept-language": "zh-TW,zh;q=0.9,ja;q=0.8,zh-CN;q=0.7,en-US;q=0.6,en;q=0.5",
            authorization: "Bearer " + auth.accessToken,
            "cache-control": "no-cache",
            "content-type": "application/json",
            pragma: "no-cache",
            "sec-ch-ua": '"Microsoft Edge";v="113", "Chromium";v="113", "Not-A.Brand";v="24"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"Windows"',
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site",
            "Referrer-Policy": "strict-origin-when-cross-origin"
          },
          body: bodyString,
          method: "POST"
        }
      );
      if (result && result.length > 0 && result[0].translations && result[0].translations.length > 0)
        return {
          text: result.map((item) => item.translations[0]?.text || ""),
          from,
          to
        };
      throw new Error("Microsoft translate error " + JSON.stringify(result));
    }
  };
  async function tryUpdateAccessToken(state) {
    if (state && state.accessToken && state.accessTokenExpiresAt) {
      let now = Date.now(), expiresAt = new Date(state.accessTokenExpiresAt).getTime();
      if (expiresAt - now > 30 * 1e3)
        return state;
      if (expiresAt - now > 3e3)
        return forceUpdateAccessToken().catch((e) => {
          log_default.error(e);
        }), state;
    }
    return forceUpdateAccessToken();
  }
  async function forceUpdateAccessToken() {
    let result = await throttleRequest({
      responseType: "text",
      url: "https://edge.microsoft.com/translate/auth",
      headers: {
        accept: "*/*",
        "accept-language": "zh-TW,zh;q=0.9,ja;q=0.8,zh-CN;q=0.7,en-US;q=0.6,en;q=0.5",
        "cache-control": "no-cache",
        pragma: "no-cache",
        "sec-ch-ua": '"Microsoft Edge";v="113", "Chromium";v="113", "Not-A.Brand";v="24"',
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": '"Windows"',
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "cross-site",
        "sec-mesh-client-arch": "x86_64",
        "sec-mesh-client-edge-channel": "beta",
        "sec-mesh-client-edge-version": "113.0.1774.23",
        "sec-mesh-client-os": "Windows",
        "sec-mesh-client-os-version": "10.0.19044",
        "sec-mesh-client-webview": "0",
        Referer: "https://appsumo.com/",
        "Referrer-Policy": "strict-origin-when-cross-origin"
      },
      body: null,
      method: "GET"
    }), parsedAceeessToken = parseJWT(result), expiresAt = parsedAceeessToken.accessTokenExpiresAt, expiresIn = new Date(expiresAt).getTime() - Date.now();
    return globalState2 = parsedAceeessToken, await setExpired(
      immersiveTranslateBingGlobalConfigStorageKey,
      parsedAceeessToken,
      expiresIn - 1e3
    ), {
      accessToken: result,
      accessTokenExpiresAt: parsedAceeessToken.accessTokenExpiresAt
    };
  }