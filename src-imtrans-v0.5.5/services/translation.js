
  // services/translation.ts
  var Translation = class {
    constructor(serviceConfig, generalConfig, options2) {
      this.maxTextLength = 1800;
      this.isSupportList = !0;
      this.maxTextGroupLength = 200;
      this.serviceConfig = serviceConfig, this.generalConfig = generalConfig, this.translationOptions = options2;
    }
    async init() {
    }
    getMaxTextGroupLength() {
      return this.maxTextGroupLength;
    }
    getDefaultRateLimit() {
      return null;
    }
    translate(_payload) {
      throw new Error("Not implemented");
    }
    translateList(_payload) {
      throw new Error("Not implemented");
    }
    async multipleTranslate(payload, options2, everySentenceCallback) {
      if (payload.sentences.length === 0)
        return {
          sentences: []
        };
      let { sentences } = payload, tempSentenceGroups = [], globalError = null, languages2 = /* @__PURE__ */ new Set();
      for (let sentence of sentences)
        sentence.from && sentence.from !== "auto" && languages2.add(sentence.from);
      let isMultipleLanguage = !1;
      languages2.size > 1 && (isMultipleLanguage = !0);
      let maxTextLength = this.maxTextLength;
      this.serviceConfig && this.serviceConfig.maxTextLengthPerRequest && (maxTextLength = this.serviceConfig.maxTextLengthPerRequest);
      let maxTextGroupLength = this.maxTextGroupLength;
      this.serviceConfig && this.serviceConfig.maxTextGroupLengthPerRequest && (maxTextGroupLength = this.serviceConfig.maxTextGroupLengthPerRequest);
      try {
        tempSentenceGroups = splitSentences(
          sentences,
          maxTextLength,
          maxTextGroupLength
        );
      } catch (e) {
        if (everySentenceCallback)
          for (let i3 = 0; i3 < sentences.length; i3++) {
            let sentence = sentences[i3];
            everySentenceCallback(e, null, sentence);
          }
        throw e;
      }
      log_default.debug(
        "tempSentenceGroups",
        tempSentenceGroups.map((item) => item)
      );
      let promises = [], sentenceCallbacks = [], addToSentenceCallback = (index, sentenceCallback, error) => {
        let currentText = sentenceCallback.translatedTexts[0];
        sentenceCallbacks[index] || (sentenceCallbacks[index] = sentenceCallback, sentenceCallbacks[index].translatedTexts = Array(4).fill(null)), sentenceCallbacks[index].translatedTexts[sentenceCallback.partIndex] = currentText;
        let currentSentenceCallback = sentenceCallbacks[index], realLength = 0;
        for (let i3 = 0; i3 < currentSentenceCallback.sentenceTotalParts; i3++)
          currentSentenceCallback.translatedTexts[i3] !== null && (realLength += 1);
        if (realLength === currentSentenceCallback.sentenceTotalParts) {
          let translatedText = currentSentenceCallback.translatedTexts.join(""), translatedSentence = {
            ...currentSentenceCallback.sentence,
            text: translatedText
          };
          sentenceCallback.callback && sentenceCallback.callback(
            null,
            translatedSentence,
            sentenceCallback.sentence
          );
        }
      }, defaultRateLimiter = { ...this.getDefaultRateLimit() }, serviceConfig = this.serviceConfig;
      serviceConfig && serviceConfig.limit && (defaultRateLimiter.limit = Number(serviceConfig.limit)), serviceConfig.interval && (defaultRateLimiter.interval = Number(serviceConfig.interval));
      for (let i3 = 0; i3 < tempSentenceGroups.length; i3++) {
        let tempSentenceGroup = tempSentenceGroups[i3], url = tempSentenceGroup.url, throttled = async () => {
          let nextDelay = await getRateLimiterDelay(
            this.translationOptions.translationService
          );
          await delay(nextDelay || 0);
          let finalFrom = tempSentenceGroup.from;
          if (isMultipleLanguage && (finalFrom = "auto"), tempSentenceGroup.fromByClient && tempSentenceGroup.fromByClient !== "auto" && (finalFrom = tempSentenceGroup.fromByClient), this.isSupportList)
            return {
              ...await this.translateList({
                text: tempSentenceGroup.tempSentences.map((item) => item.text),
                from: finalFrom,
                to: tempSentenceGroup.to,
                url,
                options: options2
              }),
              sourceTempSentences: tempSentenceGroup.tempSentences
            };
          {
            let finalTranslationTextSeparator = translationTextSeparator;
            this.serviceConfig && this.serviceConfig.translationTextSeparator && (finalTranslationTextSeparator = this.serviceConfig.translationTextSeparator);
            let newlinePlaceholderDelimiters = null;
            this.serviceConfig && this.serviceConfig.newlinePlaceholderDelimiters && (newlinePlaceholderDelimiters = this.serviceConfig.newlinePlaceholderDelimiters);
            let mergedText = "";
            newlinePlaceholderDelimiters && newlinePlaceholderDelimiters.length >= 2 ? mergedText = tempSentenceGroup.tempSentences.map((item, index) => index === tempSentenceGroup.tempSentences.length - 1 ? item.text : item.text + newlinePlaceholderDelimiters[0] + (index + 1) + newlinePlaceholderDelimiters[1]).join("") : mergedText = tempSentenceGroup.tempSentences.map(
              (item) => item.text
            ).join(finalTranslationTextSeparator);
            let result = await this.translate({
              text: mergedText,
              from: finalFrom,
              to: tempSentenceGroup.to,
              url,
              options: options2
            }), { text } = result, rawTranslatedTexts = [];
            if (newlinePlaceholderDelimiters && newlinePlaceholderDelimiters.length >= 2) {
              let defaultRegex = `${newlinePlaceholderDelimiters[0]}\\d+${newlinePlaceholderDelimiters[1]}`;
              newlinePlaceholderDelimiters && newlinePlaceholderDelimiters.length >= 3 && (defaultRegex = newlinePlaceholderDelimiters[2]);
              let regex = new RegExp(
                defaultRegex,
                "g"
              );
              rawTranslatedTexts = text.split(regex);
            } else
              rawTranslatedTexts = text.split(
                finalTranslationTextSeparator
              );
            let translatedTexts = [];
            if (rawTranslatedTexts.length > tempSentenceGroup.tempSentences.length) {
              for (let j6 = 0; j6 < tempSentenceGroup.tempSentences.length - 1; j6++)
                translatedTexts[j6] = rawTranslatedTexts[j6];
              translatedTexts[tempSentenceGroup.tempSentences.length - 1] = rawTranslatedTexts.slice(
                tempSentenceGroup.tempSentences.length - 1
              ).join(finalTranslationTextSeparator);
            } else if (rawTranslatedTexts.length < tempSentenceGroup.tempSentences.length)
              for (let j6 = rawTranslatedTexts.length; j6 < tempSentenceGroup.tempSentences.length; j6++)
                rawTranslatedTexts[j6] = "";
            else
              translatedTexts = rawTranslatedTexts;
            return {
              sourceTempSentences: tempSentenceGroup.tempSentences,
              text: translatedTexts,
              from: result.from,
              to: result.to
            };
          }
        };
        promises.push(
          throttled().then((result) => {
            let { text: translatedTexts, sourceTempSentences } = result;
            for (let j6 = 0; j6 < translatedTexts.length; j6++) {
              let translatedText = translatedTexts[j6], tempSentence = sourceTempSentences[j6];
              if (tempSentence) {
                let { index, prefix, suffix } = tempSentence;
                addToSentenceCallback(index, {
                  sentence: {
                    ...sentences[index],
                    from: tempSentenceGroup.from,
                    to: tempSentenceGroup.to
                  },
                  sentenceTotalParts: tempSentence.sentenceTotalParts,
                  partIndex: tempSentence.partIndex,
                  translatedTexts: [prefix + translatedText + suffix],
                  callback: everySentenceCallback
                }, null);
              }
            }
          }).catch((e) => {
            if (everySentenceCallback) {
              for (let j6 = 0; j6 < tempSentenceGroup.tempSentences.length; j6++) {
                let tempSentence = tempSentenceGroup.tempSentences[j6];
                tempSentence.sentenceTotalParts === 1 ? everySentenceCallback(e, null, {
                  ...sentences[tempSentence.index],
                  from: tempSentenceGroup.from,
                  to: tempSentenceGroup.to
                }) : (everySentenceCallback(e, null, {
                  ...sentences[tempSentence.index],
                  from: tempSentenceGroup.from,
                  to: tempSentenceGroup.to
                }), j6 += tempSentence.sentenceTotalParts - 1);
              }
              globalError = e;
            } else
              globalError = e;
          })
        );
      }
      if (await Promise.allSettled(promises), globalError)
        throw globalError;
      return {
        sentences: sentenceCallbacks.map((item) => ({
          ...item.sentence,
          text: item.translatedTexts.join("")
        }))
      };
    }
    detectLanguageLocally(text) {
      return detectLanguage({
        text,
        minLength: 18
      });
    }
    detectLanguageRemotely(_text) {
      return Promise.resolve("auto");
    }
    detectLanguage(text) {
      return text.length >= 50 ? this.detectLanguageLocally(text) : this.detectLanguageRemotely(text);
    }
  };