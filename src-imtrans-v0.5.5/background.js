
  // background.ts
  steupMessageListeners();
  setupOnInstalledListener();
  setupCommandListeners();
  browserAPI.contextMenus && setupContextMenuListeners();
  async function mainAsync() {
    browserAPI.contextMenus && tryToCreateContextMenu(), (await getConfig()).debug && log_default.setLevel("debug");
  }
  mainAsync().catch((e) => {
  });